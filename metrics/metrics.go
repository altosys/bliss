package metrics

import (
	"bliss"
	"context"
	"fmt"
	"net/http"
	"strings"
	"sync"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
)

// Config holds configuration parameters for metrics
type Config struct {
	Port int
}

// Metrics handles metrics serving and recording
type Metrics struct {
	logger           *zap.SugaredLogger
	registry         *prometheus.Registry
	lowestPriceGauge *prometheus.GaugeVec
	port             int
}

// NewMetrics creates a new Metrics
func NewMetrics(port int, lg *zap.SugaredLogger) (Metrics, error) {
	if port < 1 || port > 65535 {
		return Metrics{}, bliss.ErrInvalidPort
	}

	if lg == nil {
		return Metrics{}, fmt.Errorf("%w: %s", bliss.ErrNilPointer, "lg")
	}

	gauge := prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "bliss_lowest_book_price",
		Help: "The lowest price scraped by book",
	}, []string{"title"})

	reg := prometheus.NewRegistry()
	reg.MustRegister(gauge)

	metrics := Metrics{
		port:             port,
		logger:           lg,
		registry:         reg,
		lowestPriceGauge: gauge,
	}

	return metrics, nil
}

// Serve starts a new metrics server
func (m *Metrics) Serve(ctx context.Context, errCh chan<- error, wg *sync.WaitGroup, lg *zap.SugaredLogger) {
	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.Handler())
	server := &http.Server{
		Addr:    fmt.Sprintf(":%v", m.port),
		Handler: mux,
	}

	wg.Add(1)
	go func() {
		defer wg.Done()
		lg.Info("starting metrics server")
		errCh <- server.ListenAndServe()
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		<-ctx.Done()

		lg.Info("stopping metrics server")
		errCh <- server.Shutdown(ctx)
	}()
}

// Record starts metrics recording
// Listings send into the returned channel will be exposed as metrics
func (m *Metrics) Record(ctx context.Context, wg *sync.WaitGroup, lg *zap.SugaredLogger) chan bliss.Listing {
	metricsCh := make(chan bliss.Listing)

	wg.Add(1)
	go func() {
		defer wg.Done()

		lg.Debug("starting metrics recording")

		for {
			select {
			case <-ctx.Done():
				close(metricsCh)
				lg.Debug("stopping metrics recording")
				return
			case listing := <-metricsCh:
				if listing == (bliss.Listing{}) {
					continue
				}

				title := strings.ToLower(listing.Book.Title)

				if listing.Price == bliss.NotFound {
					if metric, _ := m.lowestPriceGauge.GetMetricWithLabelValues(title); metric != nil {
						lg.Debugf("deleted metric: '%s'", title)
						m.lowestPriceGauge.DeleteLabelValues(title)
						continue
					}
				}

				lg.Debugf("set metric: '%s' = %v", title, listing.Price)
				m.lowestPriceGauge.WithLabelValues(title).Set(float64(listing.Price))
			}
		}
	}()

	return metricsCh
}
