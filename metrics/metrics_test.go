package metrics

import (
	"bliss"
	"bliss/logger"
	"bliss/utils"
	"context"
	"fmt"
	"math/rand"
	"sync"
	"testing"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/testutil"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

func TestNewMetrics(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc   string
		port   int
		logger *zap.SugaredLogger
		err    error
	}{
		{
			desc:   "return error on port below valid range",
			port:   0,
			logger: nil,
			err:    bliss.ErrInvalidPort,
		},
		{
			desc:   "return error on port above valid range",
			port:   65536,
			logger: nil,
			err:    bliss.ErrInvalidPort,
		},
		{
			desc:   "return error on nil logger",
			port:   8080,
			logger: nil,
			err:    fmt.Errorf("%w: %s", bliss.ErrNilPointer, "lg"),
		},
		{
			desc:   "return nil on valid input",
			port:   8080,
			logger: lg,
			err:    nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			m, err := NewMetrics(test.port, test.logger)

			if test.err == nil {
				assert.Equal(t, test.logger, m.logger)
				assert.Equal(t, test.port, m.port)
				return
			}

			assert.Equal(t, test.err, err)
		})
	}
}

func TestServe(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	wg := sync.WaitGroup{}
	ctx, cancel := context.WithCancel(context.Background())
	errCh := make(chan error, 100)
	min := 1024
	max := 65535
	rand.Seed(time.Now().UnixNano())
	port := rand.Intn(max-min) + min
	portStr := fmt.Sprint(port)

	m, err := NewMetrics(port, lg)
	assert.NoError(t, err)
	m.Serve(ctx, errCh, &wg, lg)

	for !utils.IsPortListening("metrics server", "localhost", portStr, lg) {
		time.Sleep(250 * time.Millisecond)
	}
	cancel()

	// wait for port release
	for utils.IsPortListening("metrics server", "localhost", portStr, lg) {
		time.Sleep(250 * time.Millisecond)
	}
}

func TestRecord(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc     string
		old      bliss.Listing
		new      bliss.Listing
		expected float32
	}{
		{
			desc: "do nothing on invalid listing with no previous listings",
			old:  bliss.Listing{},
			new: bliss.Listing{
				Book: bliss.Book{
					Title: "test",
				},
				Price: float32(bliss.NotFound),
				Site:  "test.com",
			},
			expected: 0,
		},
		{
			desc: "update listing with decreasing price",
			old: bliss.Listing{
				Book: bliss.Book{
					Title: "test",
				},
				Price: float32(2),
				Site:  "site1",
			},
			new: bliss.Listing{
				Book: bliss.Book{
					Title: "test",
				},
				Price: float32(1),
				Site:  "test.com",
			},
			expected: 1,
		},
		{
			desc: "update listing with increasing price",
			old: bliss.Listing{
				Book: bliss.Book{
					Title: "test",
				},
				Price: float32(1),
				Site:  "test.com",
			},
			new: bliss.Listing{
				Book: bliss.Book{
					Title: "test",
				},
				Price: float32(2),
				Site:  "test.com",
			},
			expected: 2,
		},
		{
			desc: "delete metric when listing with missing price received",
			old: bliss.Listing{
				Book: bliss.Book{
					Title: "test",
				},
				Price: float32(1),
				Site:  "test.com",
			},
			new: bliss.Listing{
				Book: bliss.Book{
					Title: "test",
				},
				Price: float32(bliss.NotFound),
				Site:  "test.com",
			},
			expected: 0,
		},
	}

	wg := sync.WaitGroup{}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())

			m, err := NewMetrics(8080, lg)
			assert.NoError(t, err)

			metricsCh := m.Record(ctx, &wg, lg)
			assert.NotNil(t, metricsCh)

			metricsCh <- test.old
			time.Sleep(100 * time.Millisecond)
			metricsCh <- test.new
			time.Sleep(100 * time.Millisecond)

			gauge := m.lowestPriceGauge.With(prometheus.Labels{"title": "test"})
			gaugeVal := testutil.ToFloat64(gauge)
			assert.Equal(t, test.expected, float32(gaugeVal))

			cancel()
			wg.Wait()
			m = Metrics{}
		})
	}
}
