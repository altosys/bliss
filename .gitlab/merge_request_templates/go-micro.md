# Merge Request - Go Microservice

## Merge Commit Review

### Merge Commit Type

- [ ] Fix
- [ ] Feature
- [ ] BREAKING CHANGE
- [ ] Other

### Merge Commit Title

Example: **fix(grpc): patch build error in proto files**

- [ ] Merge commit title follows conventional commit style:

  - [ ] Merge commit title is prefixed with one of:
    - **build**: Changes that affect build systems or external dependencies
    - **ci**: Changes to CI configuration
    - **chore**: Tool changes, configuration changes and changes that do not actually go into production
    - **docs**: Documentation only changes
    - **feat**: A new feature
    - **fix**: A bug fix
    - **perf**: A code change that improves performance
    - **refactor**: A code change that neither fixes a bug nor adds a feature
    - **style**: Changes that do not affect the meaning of the code (white-space, formatting, punctuation, etc)
    - **test**: Adding or updating tests
    Example: **feat**(api): add new api endpoint '/metrics'
  - [ ] Merge commit title prefix has a scope (optional)\
         Example: feat(**api**): add new api endpoint '/metrics'
  - [ ] Merge commit title has a description.\
         Example: feat(api): **add new api endpoint '/metrics'**

### Merge Commit Message

Example: **Add new gRPC service endpoints to allow fetching of historic data**\
Example: **BREAKING CHANGE: Remove deprecated rpc endpoints, backwards incompatible.**

- [ ] Merge commit message has a summary description of changes to be merged
- [ ] Major version update merge commit message is prefixed with **BREAKING CHANGE:**

See [conventional commits](https://www.conventionalcommits.org) for reference.

### Merge Commit Content

- [ ] Changes are confined to the commit title and message scope
- [ ] No production credentials have been committed into the project
- [ ] All relevant credentials have been checked into secure storage

## Documentation Review

- [ ] README.md has been updated if needed
- [ ] Other relevant docs have been added or updated (Wiki, Gitlab Pages etc.)
- [ ] Project home page has a summary description on Gitlab

## Testing Review

- [ ] Gitlab CI pipeline is passing successfully
- [ ] All functions have dedicated tests
- [ ] All integration tests are skipped when running tests with the `-short` flag
- [ ] All integration tests are named "Test\_\_\_\_Integration"
- [ ] Integration test dependencies in docker-test.yml have been updated
- [ ] Code coverage is >90% and presented by the Gitlab CI Merge Request page

## Code Review

- [ ] docker-compose.yml has been updated and successfully runs the service locally
- [ ] Any difficult to read code has explanatory comments
- [ ] Running "go run main.go" works
- [ ] No vendor catalog is committed into the project
- [ ] No binaries are committed into the project
