package bliss

import (
	"github.com/PuerkitoBio/goquery"
	"go.uber.org/zap"
)

const (
	NotFound = -1
	// Not Available
	NA Format = iota - 1
	// Softcover
	SC
	// Hardcover
	HC
)

var (
	// Builder is the identity of the builder of the binary
	Builder = "n/a"
	// Commit is the git commit sha of the current commit
	Commit = "n/a"
	// Date is the date when the binary was built
	Date = "n/a"
	// Version is the version of the binary
	Version = "n/a"

	// Formats is a map for string to Format constants
	Formats = map[string]Format{
		"NA": NA,
		"PB": SC,
		"SC": SC,
		"HC": HC,
	}
)

// Book holds book metadata
type Book struct {
	ISBN   string
	Title  string
	Format Format
}

// Format is an integer representation of a book format
type Format int

// Listing holds book metadata and price information
type Listing struct {
	Book     Book
	Currency string
	Site     string
	Price    float32
}

// Scraper defines an interface for parsing listing data
type Scraper interface {
	Scrape(b Book, s Scraper, parseMeta bool, lg *zap.SugaredLogger) (Listing, error)
	Site() string
	SearchURL(isbn string) string
	ParseFormat(dom *goquery.Selection) Format
	ParsePrice(
		dom *goquery.Selection,
		b Book,
		lg *zap.SugaredLogger,
	) (currency string, price float32)
	ParseTitle(dom *goquery.Selection) string
}

func (f Format) String() string {
	switch f {
	case SC:
		return "SC"
	case HC:
		return "HC"
	case NA:
		return "NA"
	default:
		return "NA"
	}
}
