ARG BASE_IMAGE
FROM $BASE_IMAGE
RUN apk add -Uuq --no-cache tzdata ca-certificates
COPY bliss /opt/bliss
ENTRYPOINT ["/opt/bliss"]
CMD ["run"]
