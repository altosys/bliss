package exchange

import (
	"bliss"
	"bliss/logger"
	"context"
	"errors"
	"sync"
	"testing"
	"time"

	"github.com/alicebob/miniredis"
	"github.com/go-redis/redis/v8"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

func TestNewExchange(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc     string
		currency string
		err      error
	}{
		{
			desc:     "return error on invalid currency code",
			currency: "INVALID",
			err:      bliss.ErrInvalidCurrencyCode,
		},
		{
			desc:     "return nil on valid currency code",
			currency: "EUR",
			err:      nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			e, err := NewExchange(test.currency, lg)
			assert.Equal(t, test.err, err)

			if test.err == nil {
				assert.Equal(t, lg, e.lg)
				assert.NotNil(t, sync.RWMutex{}, &e.mutex)
			}
		})
	}
}

func TestConvertPrice(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc             string
		amount           float32
		expectedPrice    float32
		expectedCurrency string
		from             string
		to               string
	}{
		{
			desc:             "return toCurrency and bliss.NotFound on negative amount",
			amount:           bliss.NotFound,
			from:             "EUR",
			to:               "USD",
			expectedPrice:    float32(bliss.NotFound),
			expectedCurrency: "USD",
		},
		{
			desc:             "return price not found on invalid base currency",
			amount:           1,
			from:             "INVALID",
			to:               "USD",
			expectedPrice:    float32(bliss.NotFound),
			expectedCurrency: "USD",
		},
		{
			desc:             "return price not found on invalid target currency",
			amount:           1,
			from:             "EUR",
			to:               "INVALID",
			expectedPrice:    float32(bliss.NotFound),
			expectedCurrency: "INVALID",
		},
		{
			desc:             "return valid currency conversion",
			amount:           10,
			from:             "USD",
			to:               "EUR",
			expectedPrice:    float32(2),
			expectedCurrency: "EUR",
		},
	}

	e := Exchange{
		Base: "EUR",
		Rates: map[string]float64{
			"EUR": 1,
			"USD": 5,
		},
		lg: lg,
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			price, currency := e.ConvertPrice(test.amount, test.from, test.to, lg)
			assert.Equal(t, test.expectedPrice, price)
			assert.Equal(t, test.expectedCurrency, currency)
		})
	}
}

func TestUpdateExchangeRates(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc     string
		currency string
		updated  bool
		err      error
	}{
		{
			desc:     "skip fetch on up to date rates stored",
			currency: "EUR",
			updated:  false,
			err:      nil,
		},
		{
			desc:     "return error from rates api request",
			currency: "EUR",
			updated:  false,
			err:      errors.New("parse \"https://api.exchangerate-api.com/v4/latest/%\": invalid URL escape \"%\""),
		},
		{
			desc:     "return nil on successful rates update",
			currency: "EUR",
			updated:  true,
			err:      nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			e, err := NewExchange(test.currency, lg)
			assert.NoError(t, err)

			ctx := context.Background()
			rc := newTestRedisClient(t)

			switch test.desc {
			case "return error from rates api request":
				e.Base = "%"
			case "skip fetch on up to date rates stored":
				e.Base = "%"
				_, err = rc.Set(ctx, "exchange:date", time.Now().Format("2006-01-02"), 0*time.Second).Result()
				assert.NoError(t, err)
				_, err = rc.Set(ctx, "exchange:base", test.currency, 0*time.Second).Result()
				assert.NoError(t, err)
			}

			updated, err := e.UpdateRates(ctx, rc)
			assert.Equal(t, test.updated, updated)

			switch test.desc {
			case "return error from rates api request":
				assert.NotEmpty(t, e)
				assert.Empty(t, e.Rates)
				assert.Equal(t, test.err.Error(), err.Error())

			case "skip fetch on up to date rates stored":
				assert.NotEmpty(t, e)
				assert.NoError(t, err)

			default:
				assert.NotEmpty(t, e)
				assert.NotEmpty(t, e.Rates)
				assert.Equal(t, test.currency, e.Base)
				assert.Equal(t, test.err, err)
			}
		})
	}
}

func newTestRedisClient(t *testing.T) *redis.Client {
	mr, err := miniredis.Run()
	assert.NoError(t, err)

	client := redis.NewClient(&redis.Options{
		Addr: mr.Addr(),
	})

	return client
}
