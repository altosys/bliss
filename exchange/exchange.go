package exchange

import (
	"bliss"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/go-redis/redis/v8"
	"go.uber.org/zap"
	c "golang.org/x/text/currency"
)

// Exchange holds exchange rates and metadata
type Exchange struct {
	Base        string             `json:"base"`
	Date        string             `json:"date"`
	LastUpdated int32              `json:"time_last_updated"`
	Rates       map[string]float64 `json:"rates"`
	lg          *zap.SugaredLogger
	mutex       sync.RWMutex
}

// NewExchange creates a new Exchange
func NewExchange(baseCurrency string, lg *zap.SugaredLogger) (*Exchange, error) {
	if _, err := c.ParseISO(baseCurrency); err != nil {
		return &Exchange{}, bliss.ErrInvalidCurrencyCode
	}

	e := Exchange{
		Base:  baseCurrency,
		lg:    lg,
		mutex: sync.RWMutex{},
	}

	return &e, nil
}

// UpdateRates fetches exchange rates from an external rest api.
// Returns true if rates were updated and false if update failed or was skipped
func (e *Exchange) UpdateRates(ctx context.Context, r *redis.Client) (bool, error) {
	date := r.Get(ctx, "exchange:date").Val()
	today := time.Now().Format("2006-01-02")
	if date == today {
		e.lg.Info("exchange rates are up to date, skipping fetch")
		return false, nil
	}

	e.mutex.Lock()
	defer e.mutex.Unlock()

	client := &http.Client{
		Timeout: 30 * time.Second,
	}

	// set up new request
	base := strings.ToLower(e.Base)
	url := fmt.Sprintf("https://api.exchangerate-api.com/v4/latest/%s", base)
	req, err := http.NewRequestWithContext(ctx, "GET", url, http.NoBody)
	if err != nil {
		return false, err
	}
	req.Header.Set("User-Agent", "Firefox")
	req.Header.Set("Content-Type", "application/json")

	// send request
	e.lg.Info("fetching exchange rates...")
	rsp, err := client.Do(req)
	if err != nil {
		return false, err
	}
	defer rsp.Body.Close()

	bodyBytes, err := io.ReadAll(rsp.Body)
	if err != nil {
		return false, err
	}

	if err := json.Unmarshal(bodyBytes, e); err != nil {
		return false, err
	}
	e.lg.Info("exchange rates fetched")
	return true, nil
}

// ConvertPrice converts a given amount from one currency to another
func (e *Exchange) ConvertPrice(amount float32, fromCurrency, toCurrency string, lg *zap.SugaredLogger) (price float32, currency string) {
	e.mutex.RLock()
	defer e.mutex.RUnlock()

	if amount < 0 {
		return amount, toCurrency
	}

	if _, exists := e.Rates[fromCurrency]; !exists {
		lg.Errorf("invalid currency code: %s", fromCurrency)
		return bliss.NotFound, toCurrency
	}

	if _, exists := e.Rates[toCurrency]; !exists {
		lg.Errorf("invalid currency code: %s", toCurrency)
		return bliss.NotFound, "INVALID"
	}

	converted := float64(amount) * (e.Rates[toCurrency] / e.Rates[fromCurrency])
	return float32(converted), toCurrency
}
