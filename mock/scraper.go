package mock

import (
	"bliss"
	"bliss/utils"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"go.uber.org/zap"
)

type Scraper struct {
	ScrapeFn      func(b bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error)
	SiteFn        func() string
	SearchURLFn   func(isbn string) string
	ParseFormatFn func(dom *goquery.Selection) bliss.Format
	ParsePriceFn  func(dom *goquery.Selection, b bliss.Book, lg *zap.SugaredLogger) (currency string, price float32)
	ParseTitleFn  func(dom *goquery.Selection) string
}

func (mockBookDepository Scraper) Scrape(
	book bliss.Book,
	scraper bliss.Scraper,
	parseBookMeta bool,
	lg *zap.SugaredLogger,
) (bliss.Listing, error) {
	return mockBookDepository.ScrapeFn(book, scraper, parseBookMeta, lg)
}

func (mockBookDepository Scraper) Site() string {
	return mockBookDepository.SiteFn()
}

func (mockBookDepository Scraper) SearchURL(isbn string) string {
	return mockBookDepository.SearchURLFn(isbn)
}

func (mockBookDepository Scraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	return mockBookDepository.ParseFormatFn(dom)
}

func (mockBookDepository Scraper) ParsePrice(
	dom *goquery.Selection,
	book bliss.Book,
	lg *zap.SugaredLogger,
) (currency string,
	price float32) {
	return mockBookDepository.ParsePriceFn(dom, book, lg)
}

func (mockBookDepository Scraper) ParseTitle(dom *goquery.Selection) string {
	return mockBookDepository.ParseTitleFn(dom)
}

// GetScraper returns a mock bookdepository scraper
func GetScraper() Scraper {
	scrapeFn := func(book bliss.Book, scraper bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
		return bliss.Listing{}, nil
	}

	siteFn := func() string {
		return "bookdepository.com"
	}

	searchURLFn := func(isbn string) string {
		return "https://www.bookdepository.com/search?searchTerm="
	}

	parseFormatFn := func(dom *goquery.Selection) bliss.Format {
		switch strings.ToLower(strings.TrimSpace(dom.Find("ul.meta-info").Children().First().Text())) {
		case "hardback":
			return bliss.HC
		case "paperback":
			return bliss.SC
		default:
			return bliss.NA
		}
	}

	parsePriceFn := func(dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
		priceStr := dom.Find(".sale-price").First().Text()
		priceStr = strings.TrimSpace(priceStr)

		dom.Find("#copyright").Each(func(i int, s *goquery.Selection) {
			if title, _ := s.Attr("title"); title != "" {
				titleFields := strings.Split(title, ",")
				for _, s := range titleFields {
					if strings.Contains(s, "userSessionCurrencyCode") {
						currency = strings.Split(s, "'")[1]
					}
				}
			}
		})
		return currency, utils.PriceToFloat(currency, priceStr, lg)
	}

	parseTitleFn := func(dom *goquery.Selection) string {
		return dom.Find(".item-info").Find("h1").First().Text()
	}

	return Scraper{
		ScrapeFn:      scrapeFn,
		SiteFn:        siteFn,
		SearchURLFn:   searchURLFn,
		ParseFormatFn: parseFormatFn,
		ParsePriceFn:  parsePriceFn,
		ParseTitleFn:  parseTitleFn,
	}
}
