package mock

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func HTTPServer(t *testing.T, path string, response []byte) *httptest.Server {
	mux := http.NewServeMux()
	mux.HandleFunc(path, func(res http.ResponseWriter, req *http.Request) {
		_, err := res.Write(response)
		assert.Empty(t, err)
	})
	return httptest.NewServer(mux)
}
