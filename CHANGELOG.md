
<a name="v1.31.4"></a>
## [v1.31.4](https://gitlab.com/altosys/bliss/compare/v1.31.3...v1.31.4) (2022-05-14)

### Ci

* fix workflow rule to not filter out triggered jobs on branches
* format .gitlab-ci.yml for less syntax
* update git-chglog and svu
* replace commitsar with bash regex check for conventional commits

### Fix

* **scraper:** update bookdepository price scraping

### Test

* add test to cover printing config using env for book list


<a name="v1.31.3"></a>
## [v1.31.3](https://gitlab.com/altosys/bliss/compare/v1.31.2...v1.31.3) (2022-01-16)

### Chore

* move books list source logging to PrintConfig()
* **release:** v1.31.3 [skip ci]

### Fix

* add missing newline to splash command line output


<a name="v1.31.2"></a>
## [v1.31.2](https://gitlab.com/altosys/bliss/compare/v1.31.1...v1.31.2) (2022-01-16)

### Chore

* print splash graphic to stderr instead of stdout
* **release:** v1.31.2 [skip ci]

### Ci

* remove docker-compose up job

### Fix

* **config:** correct usage of /etc/bliss/books.yaml as default book file


<a name="v1.31.1"></a>
## [v1.31.1](https://gitlab.com/altosys/bliss/compare/v1.31.0...v1.31.1) (2022-01-16)

### Chore

* **release:** v1.31.1 [skip ci]

### Fix

* **config:** remove check for BLISS_BOOKS variable set when building config


<a name="v1.31.0"></a>
## [v1.31.0](https://gitlab.com/altosys/bliss/compare/v1.30.0...v1.31.0) (2022-01-16)

### Chore

* update dependencies to follow golangci-lint recommendations
* move sites testdata to internal
* **release:** v1.31.0 [skip ci]

### Ci

* add .pre stage to pull all docker images if .gitlab-ci.yml changed
* update python image to version 3.9
* update docker base image to alpine 3.15
* update golangci-lint to 1.43.0

### Docs

* **README:** add screenshots

### Feat

* **config:** read config from file


<a name="v1.30.0"></a>
## [v1.30.0](https://gitlab.com/altosys/bliss/compare/v1.29.1...v1.30.0) (2021-08-22)

### Chore

* update toolkit to v1.0.23
* go mod tidy
* remove deprecated package-lock.json and package.json
* **golangci-lint:** fix linter errors
* **golangci-lint:** disable golint (deprecated) and enable revive
* **goreleaser:** replace gocenter.io as GOPROXY with goproxy.io
* **release:** v1.30.0 [skip ci]

### Ci

* fix release commit job
* update job trigger flows
* **golangci-lint:** bump version to 1.42.0
* **goreleaser:** bump version to 0.175.0

### Feat

* **scraper:** add walt's comic shop scraper

### Refactor

* move metadata from 'meta' package to 'bliss'


<a name="v1.29.1"></a>
## [v1.29.1](https://gitlab.com/altosys/bliss/compare/v1.29.0...v1.29.1) (2021-04-18)

### Chore

* update toolkit to v1.0.20
* update toolkit to v1.0.18
* update toolkit to v1.0.22
* log redis address on startup
* **docker:** store only REGISTRY variable in .env for easy switch to local cache
* **go:** bump version to 1.16.2
* **goreleaser:** remove redundant quoting from build ldflags
* **release:** v1.29.1 [skip ci]

### Ci

* update release jobs and remove git-chglog tag message
* deprecate use of semantic-release and use goreleaser exclusively
* remove static:dox job and set golangci-lint job to use .golangci.yml

### Fix

* missing currency code in low stock notifications

### Refactor

* use lib to handle currency parsing
* project package structure overhaul

### Test

* update golangci-lint to stricter linting and use .golangci.yml for config
* switch to testcontainers-go for integration tests


<a name="v1.29.0"></a>
## [v1.29.0](https://gitlab.com/altosys/bliss/compare/v1.28.6...v1.29.0) (2021-02-21)

### Chore

* new logo design
* **release:** v1.29.0 [skip ci]

### Feat

* add proxy support

### Fix

* handle indian price format from bookdepository.com
* add 'PB' as alias for 'SC' format in BLISS_BOOKS


<a name="v1.28.6"></a>
## [v1.28.6](https://gitlab.com/altosys/bliss/compare/v1.28.5...v1.28.6) (2021-01-27)

### Chore

* change 'restock' notification to 'available'
* **grafana:** update queries to include retention policy variable
* **release:** v1.28.6 [skip ci]

### Fix

* update 'next' isbn if invalid or missing


<a name="v1.28.5"></a>
## [v1.28.5](https://gitlab.com/altosys/bliss/compare/v1.28.4...v1.28.5) (2021-01-14)

### Chore

* **release:** v1.28.5 [skip ci]

### Fix

* **scraper:** handle blackwells 'not available for sale' listings


<a name="v1.28.4"></a>
## [v1.28.4](https://gitlab.com/altosys/bliss/compare/v1.28.3...v1.28.4) (2021-01-12)

### Chore

* remove redis-commander from docker-compose.yml
* update toolkit to v1.0.17
* **release:** v1.28.4 [skip ci]

### Docs

* **README:** shorten description of redis

### Fix

* **config:** improve error messages on invalid configuration

### Refactor

* fix go report card issues including minor refactorings

### Test

* **scraper:** improve TestNotifyLowStock


<a name="v1.28.3"></a>
## [v1.28.3](https://gitlab.com/altosys/bliss/compare/v1.28.2...v1.28.3) (2021-01-08)

### Chore

* **release:** v1.28.3 [skip ci]

### Fix

* **exchange:** skip exchange rates update if updated same day


<a name="v1.28.2"></a>
## [v1.28.2](https://gitlab.com/altosys/bliss/compare/v1.28.1...v1.28.2) (2021-01-07)

### Chore

* **release:** v1.28.2 [skip ci]

### Fix

* **scraper:** writing price drop notification hashes not filtered by drop level


<a name="v1.28.1"></a>
## [v1.28.1](https://gitlab.com/altosys/bliss/compare/v1.28.0...v1.28.1) (2021-01-07)

### Chore

* **release:** v1.28.1 [skip ci]

### Fix

* **redis:** notification hash not written to redis


<a name="v1.28.0"></a>
## [v1.28.0](https://gitlab.com/altosys/bliss/compare/v1.27.3...v1.28.0) (2021-01-07)

### Chore

* **release:** v1.28.0 [skip ci]

### Feat

* migrate influxdb data when changing retention policy


<a name="v1.27.3"></a>
## [v1.27.3](https://gitlab.com/altosys/bliss/compare/v1.27.2...v1.27.3) (2021-01-06)

### Chore

* **release:** v1.27.3 [skip ci]

### Fix

* prevent duplicate notifications within a 7 day period


<a name="v1.27.2"></a>
## [v1.27.2](https://gitlab.com/altosys/bliss/compare/v1.27.1...v1.27.2) (2021-01-05)

### Chore

* **release:** v1.27.2 [skip ci]

### Fix

* **scraper:** truncate price drop notifications if message length is too long


<a name="v1.27.1"></a>
## [v1.27.1](https://gitlab.com/altosys/bliss/compare/v1.27.0...v1.27.1) (2021-01-05)

### Chore

* set default values for redis configuration
* use explicit docker registry when pulling images
* shorter wording in pushover messages
* update go to 1.15.6
* update merge request template for testing review
* **release:** v1.27.1 [skip ci]

### Docs

* **README:** update configuration parameter table with descriptions

### Fix

* restore missing currency code in low stock notifications

### Refactor

* move all config structs to their respective packages
* rename 'db' package to 'influxdb' to distinguish from redis

### Test

* set up all tests with descriptions and subtest execution


<a name="v1.27.0"></a>
## [v1.27.0](https://gitlab.com/altosys/bliss/compare/v1.26.4...v1.27.0) (2021-01-03)

### Chore

* add todo file to .gitignore
* **release:** v1.27.0 [skip ci]

### Feat

* implement persistent state with redis


<a name="v1.26.4"></a>
## [v1.26.4](https://gitlab.com/altosys/bliss/compare/v1.26.3...v1.26.4) (2020-11-30)

### Chore

* **release:** v1.26.4 [skip ci]

### Fix

* handle promoted items in search results on amazon


<a name="v1.26.3"></a>
## [v1.26.3](https://gitlab.com/altosys/bliss/compare/v1.26.2...v1.26.3) (2020-11-28)

### Chore

* update toolkit to v1.0.14
* update influxdb to v1.8.3
* **grafana:** dynamically update price displays based on selected books
* **release:** v1.26.3 [skip ci]

### Ci

* define all images at the top of .gitlab-ci.yml

### Fix

* set request jitter min time to 75% of interval instead of 25%
* currency not being written to influxdb listing entries


<a name="v1.26.2"></a>
## [v1.26.2](https://gitlab.com/altosys/bliss/compare/v1.26.1...v1.26.2) (2020-11-15)

### Chore

* **release:** v1.26.2 [skip ci]

### Fix

* restore prioritization of missing books for scraping


<a name="v1.26.1"></a>
## [v1.26.1](https://gitlab.com/altosys/bliss/compare/v1.26.0...v1.26.1) (2020-11-14)

### Chore

* remove redundant viper function calls
* **release:** v1.26.1 [skip ci]

### Ci

* add gpg signing and upx compression for release artifacts

### Fix

* patch panic on closed dbCh on shutdown

### Refactor

* move books from global to own package
* move exchange from utils to own package

### Style

* update wording in restock and low stock notifications to match drops
* decrease width of whitespace in scraped price log output


<a name="v1.26.0"></a>
## [v1.26.0](https://gitlab.com/altosys/bliss/compare/v1.25.0...v1.26.0) (2020-11-10)

### Chore

* **release:** v1.26.0 [skip ci]

### Feat

* add option for developer or production console log output

### Refactor

* change 'DB_ADDR' to 'DB_ADDRESS' and 'DB_PSWD' to 'DB_PASSWORD'
* minimize use of package level variables and init functions

### Test

* patch randomTicker test race condition


<a name="v1.25.0"></a>
## [v1.25.0](https://gitlab.com/altosys/bliss/compare/v1.24.2...v1.25.0) (2020-11-02)

### Chore

* **release:** v1.25.0 [skip ci]

### Feat

* add options to enable/disable low stock and restock notifications


<a name="v1.24.2"></a>
## [v1.24.2](https://gitlab.com/altosys/bliss/compare/v1.24.1...v1.24.2) (2020-11-02)

### Chore

* **grafana:** set book title and site dropdown menus to allow multi-value selections
* **release:** v1.24.2 [skip ci]
* **utils:** update log levels and wording

### Ci

* add removal of temporary 'v0.0.0' tag in go:releaser build job

### Fix

* replace automatic links in notification messages and add price summary


<a name="v1.24.1"></a>
## [v1.24.1](https://gitlab.com/altosys/bliss/compare/v1.24.0...v1.24.1) (2020-10-29)

### Chore

* remove unused grafana plugins and rename shared variables in docker-compose.yml
* **release:** v1.24.1 [skip ci]

### Docs

* update mermaid diagram and add copyright to README.md

### Fix

* handle bookswagon.com redirect to freeform.com


<a name="v1.24.0"></a>
## [v1.24.0](https://gitlab.com/altosys/bliss/compare/v1.23.0...v1.24.0) (2020-10-28)

### Chore

* update toolkit to v1.0.13
* add lowest and highest price views to grafana dashboard
* **grafana:** change y axis max to dynamic value for price graphs
* **release:** v1.24.0 [skip ci]

### Ci

* fix use of CI_PROJECT_PATH with sed in .releaserc
* set up docker-compose.yml and semantic-release to use docker hub
* cleanup and restructuring of .gitlab-ci.yml

### Feat

* add amazon.se scraper


<a name="v1.23.0"></a>
## [v1.23.0](https://gitlab.com/altosys/bliss/compare/v1.22.0...v1.23.0) (2020-10-17)

### Chore

* improve 'books unavailable' query in grafana
* **release:** v1.23.0 [skip ci]

### Feat

* add flipkart.com scraper


<a name="v1.22.0"></a>
## [v1.22.0](https://gitlab.com/altosys/bliss/compare/v1.21.1...v1.22.0) (2020-10-17)

### Chore

* **release:** v1.22.0 [skip ci]

### Feat

* add bookswagon.com scraper


<a name="v1.21.1"></a>
## [v1.21.1](https://gitlab.com/altosys/bliss/compare/v1.21.0...v1.21.1) (2020-10-16)

### Chore

* **release:** v1.21.1 [skip ci]

### Fix

* handle amazon.in comma delimiter in price string


<a name="v1.21.0"></a>
## [v1.21.0](https://gitlab.com/altosys/bliss/compare/v1.20.1...v1.21.0) (2020-10-16)

### Chore

* **release:** v1.21.0 [skip ci]

### Docs

* clean up redundancies in go-micro merge request template

### Feat

* add amazon.in scraper

### Refactor

* rename 'user token' to 'user key' to follow official pushover nomenclature


<a name="v1.20.1"></a>
## [v1.20.1](https://gitlab.com/altosys/bliss/compare/v1.20.0...v1.20.1) (2020-10-03)

### Chore

* **release:** v1.20.1 [skip ci]

### Ci

* change .releaserc to yml format and improve sed commands in release

### Docs

* clean up go-micro mr template and update formatting

### Fix

* ignore adlibris preorder listings


<a name="v1.20.0"></a>
## [v1.20.0](https://gitlab.com/altosys/bliss/compare/v1.19.0...v1.20.0) (2020-09-27)

### Chore

* **release:** v1.20.0 [skip ci]

### Docs

* add license
* add new logo to README.md
* restructure and clarify README.md

### Feat

* add notification level

### Refactor

* **config:** split InitEnv into smaller functions

### Style

* update startup ascii art with new project name


<a name="v1.19.0"></a>
## [v1.19.0](https://gitlab.com/altosys/bliss/compare/v1.18.3...v1.19.0) (2020-09-20)

### Chore

* update go-task to v3.0.0
* update toolkit to v1.0.11
* change project name to 'bliss'
* **release:** v1.19.0 [skip ci]

### Feat

* add sfbok.se scraper


<a name="v1.18.3"></a>
## [v1.18.3](https://gitlab.com/altosys/bliss/compare/v1.18.2...v1.18.3) (2020-09-19)

### Chore

* adjust indentation of logging in db package
* **release:** v1.18.3 [skip ci]

### Docs

* add clarifications to README.md

### Fix

* **metrics:** check if metric exists before executing delete operation
* **scraper:** handle bokus false positives when no meta fields received


<a name="v1.18.2"></a>
## [v1.18.2](https://gitlab.com/altosys/bliss/compare/v1.18.1...v1.18.2) (2020-09-18)

### Chore

* **release:** v1.18.2 [skip ci]

### Ci

* improve sed commands in pipeline

### Fix

* **scraper:** ignore bokus preorder prices


<a name="v1.18.1"></a>
## [v1.18.1](https://gitlab.com/altosys/bliss/compare/v1.18.0...v1.18.1) (2020-09-17)

### Chore

* **release:** v1.18.1 [skip ci]

### Ci

* update 'docker-compose up' job (sed version + cleanup)

### Docs

* remove font-awesome icons from README.md, not supported by gitlab

### Fix

* **scraper:** patch incorrect update of lowest listings to -1


<a name="v1.18.0"></a>
## [v1.18.0](https://gitlab.com/altosys/bliss/compare/v1.17.0...v1.18.0) (2020-09-15)

### Chore

* move influxdb init script to own folder
* add paypal donation button to readme
* update toolkit to v1.0.10
* **release:** v1.18.0 [skip ci]

### Ci

* remove docker env vars + switch to using "extends" instead of yaml anchors
* remove slack notification
* update image version in docker-compose on release

### Docs

* update README.md with detailed usage instructions

### Feat

* add book format to data structure for scraping both softcover and hardcover books

### Refactor

* **scraper:** separate Scrape and Start functions


<a name="v1.17.0"></a>
## [v1.17.0](https://gitlab.com/altosys/bliss/compare/v1.16.1...v1.17.0) (2020-08-25)

### Chore

* **release:** v1.17.0 [skip ci]

### Feat

* **scraper:** add amazon.com.au scraper


<a name="v1.16.1"></a>
## [v1.16.1](https://gitlab.com/altosys/bliss/compare/v1.16.0...v1.16.1) (2020-08-25)

### Chore

* **release:** v1.16.1 [skip ci]

### Fix

* **utils:** handle invalid currency codes in ConvertPrice


<a name="v1.16.0"></a>
## [v1.16.0](https://gitlab.com/altosys/bliss/compare/v1.15.0...v1.16.0) (2020-08-25)

### Chore

* **release:** v1.16.0 [skip ci]

### Feat

* **scraper:** add amazon.ca scraper


<a name="v1.15.0"></a>
## [v1.15.0](https://gitlab.com/altosys/bliss/compare/v1.14.0...v1.15.0) (2020-08-24)

### Chore

* **release:** v1.15.0 [skip ci]

### Feat

* **scraper:** add amazon.ae scraper


<a name="v1.14.0"></a>
## [v1.14.0](https://gitlab.com/altosys/bliss/compare/v1.13.2...v1.14.0) (2020-08-20)

### Chore

* update grafana aggregated panel titles
* **release:** v1.14.0 [skip ci]

### Feat

* **scraper:** add amazon.nl scraper


<a name="v1.13.2"></a>
## [v1.13.2](https://gitlab.com/altosys/bliss/compare/v1.13.1...v1.13.2) (2020-08-16)

### Chore

* **release:** v1.13.2 [skip ci]

### Fix

* correct and simplify time calculations for baseline ETA and scrape cycle


<a name="v1.13.1"></a>
## [v1.13.1](https://gitlab.com/altosys/bliss/compare/v1.13.0...v1.13.1) (2020-08-15)

### Chore

* **release:** v1.13.1 [skip ci]

### Ci

* update python version to 3.8.5 to use alpine 3.12

### Fix

* **scraper:** startup notification not sent

### Refactor

* **scraper:** correct debug logging for Prioritize()


<a name="v1.13.0"></a>
## [v1.13.0](https://gitlab.com/altosys/bliss/compare/v1.12.6...v1.13.0) (2020-08-15)

### Chore

* update alpine to 3.12 and golang to 1.14.7
* update toolkit to v1.0.9
* **grafana:** update grafana to 7.1.3 and add aggregated data panels to dashboard
* **release:** v1.13.0 [skip ci]

### Feat

* **scraper:** prioritize books with missing listings for scraping


<a name="v1.12.6"></a>
## [v1.12.6](https://gitlab.com/altosys/bliss/compare/v1.12.5...v1.12.6) (2020-08-14)

### Chore

* **release:** v1.12.6 [skip ci]

### Fix

* **db:** prune listings from disabled scrapers at start


<a name="v1.12.5"></a>
## [v1.12.5](https://gitlab.com/altosys/bliss/compare/v1.12.4...v1.12.5) (2020-08-14)

### Chore

* **release:** v1.12.5 [skip ci]

### Fix

* **scraper:** send lowest listings loaded from db to metrics server on startup


<a name="v1.12.4"></a>
## [v1.12.4](https://gitlab.com/altosys/bliss/compare/v1.12.3...v1.12.4) (2020-08-14)

### Chore

* **release:** v1.12.4 [skip ci]

### Fix

* **db:** increase client timeout


<a name="v1.12.3"></a>
## [v1.12.3](https://gitlab.com/altosys/bliss/compare/v1.12.2...v1.12.3) (2020-08-14)

### Chore

* **release:** v1.12.3 [skip ci]

### Fix

* **db:** remove redundant scrapeCycle variable


<a name="v1.12.2"></a>
## [v1.12.2](https://gitlab.com/altosys/bliss/compare/v1.12.1...v1.12.2) (2020-08-13)

### Chore

* **release:** v1.12.2 [skip ci]

### Fix

* log level setting not working

### Refactor

* code cleanup and test improvements for config, cmd, metrics and notify packages
* **db:** code cleanup and tests overhaul
* **db:** return errors instead of using error channel
* **scraper:** code cleanup and tests overhaul


<a name="v1.12.1"></a>
## [v1.12.1](https://gitlab.com/altosys/bliss/compare/v1.12.0...v1.12.1) (2020-08-09)

### Chore

* set "task test" to run static, unit and cover tests in Taskfile.yml
* add thoth image to docker-compose.yml for easy switching
* **release:** v1.12.1 [skip ci]

### Fix

* patch false low stock notifications due to empty listings

### Refactor

* improve structure and tests for db and scraper packages


<a name="v1.12.0"></a>
## [v1.12.0](https://gitlab.com/altosys/bliss/compare/v1.11.4...v1.12.0) (2020-08-05)

### Chore

* **release:** v1.12.0 [skip ci]

### Docs

* add description of notifications to README.md

### Feat

* add low stock notifications

### Refactor

* improve channel structure


<a name="v1.11.4"></a>
## [v1.11.4](https://gitlab.com/altosys/bliss/compare/v1.11.3...v1.11.4) (2020-08-05)

### Chore

* update toolkit to v1.0.8
* update toolkit to v1.0.7
* **release:** v1.11.4 [skip ci]

### Ci

* update goreleaser to grab BUILDER from environment
* remove BUILDER from gitlab-ci.yml, set as Gitlab CI/CD variable instead

### Docs

* update description of scraping implementation in README.md

### Fix

* **notify:** disable pushover notifications by omitting env var tokens

### Refactor

* set all packages to init own loggers
* print title splash and version at startup
* move build metadata from 'cmd' package to new 'meta' package


<a name="v1.11.3"></a>
## [v1.11.3](https://gitlab.com/altosys/bliss/compare/v1.11.2...v1.11.3) (2020-08-04)

### Chore

* **release:** v1.11.3 [skip ci]

### Fix

* **scraper:** increase minimum scrape interval from 1s to 25% of set interval


<a name="v1.11.2"></a>
## [v1.11.2](https://gitlab.com/altosys/bliss/compare/v1.11.1...v1.11.2) (2020-08-03)

### Chore

* **release:** v1.11.2 [skip ci]

### Fix

* **scraper:** patch blackwells period price notation


<a name="v1.11.1"></a>
## [v1.11.1](https://gitlab.com/altosys/bliss/compare/v1.11.0...v1.11.1) (2020-08-03)

### Chore

* **release:** v1.11.1 [skip ci]

### Fix

* **scraper:** patch false restock notifications

### Refactor

* rename common package to global


<a name="v1.11.0"></a>
## [v1.11.0](https://gitlab.com/altosys/bliss/compare/v1.10.0...v1.11.0) (2020-08-02)

### Chore

* **release:** v1.11.0 [skip ci]

### Feat

* set scrape interval instead of cycle


<a name="v1.10.0"></a>
## [v1.10.0](https://gitlab.com/altosys/bliss/compare/v1.9.0...v1.10.0) (2020-08-02)

### Chore

* **release:** v1.10.0 [skip ci]

### Feat

* **scraper:** add forbiddenplanet.com scraper


<a name="v1.9.0"></a>
## [v1.9.0](https://gitlab.com/altosys/bliss/compare/v1.8.0...v1.9.0) (2020-08-01)

### Chore

* **release:** v1.9.0 [skip ci]

### Feat

* **scraper:** add waterstones.com scraper


<a name="v1.8.0"></a>
## [v1.8.0](https://gitlab.com/altosys/bliss/compare/v1.7.0...v1.8.0) (2020-08-01)

### Chore

* **release:** v1.8.0 [skip ci]

### Feat

* **scraper:** add blackwells.co.uk scraper


<a name="v1.7.0"></a>
## [v1.7.0](https://gitlab.com/altosys/bliss/compare/v1.6.0...v1.7.0) (2020-08-01)

### Chore

* **release:** v1.7.0 [skip ci]

### Feat

* **scraper:** notify on book restock


<a name="v1.6.0"></a>
## [v1.6.0](https://gitlab.com/altosys/bliss/compare/v1.5.2...v1.6.0) (2020-08-01)

### Chore

* **release:** v1.6.0 [skip ci]

### Feat

* **db:** prune books not in scrape list at startup


<a name="v1.5.2"></a>
## [v1.5.2](https://gitlab.com/altosys/bliss/compare/v1.5.1...v1.5.2) (2020-08-01)

### Chore

* **release:** v1.5.2 [skip ci]

### Fix

* **scraper:** prevent false price drop notifications after lowest price increase

### Refactor

* move scraper definitions to separate file


<a name="v1.5.1"></a>
## [v1.5.1](https://gitlab.com/altosys/bliss/compare/v1.5.0...v1.5.1) (2020-08-01)

### Chore

* **release:** v1.5.1 [skip ci]

### Fix

* **scraper:** filter out of stock books from adlibris, akademibokhandeln and bokus


<a name="v1.5.0"></a>
## [v1.5.0](https://gitlab.com/altosys/bliss/compare/v1.4.0...v1.5.0) (2020-08-01)

### Chore

* update toolkit to v1.0.6
* **release:** v1.5.0 [skip ci]

### Feat

* add pushover price notifications and expose metrics


<a name="v1.4.0"></a>
## [v1.4.0](https://gitlab.com/altosys/bliss/compare/v1.3.4...v1.4.0) (2020-07-05)

### Chore

* **release:** v1.4.0 [skip ci]

### Ci

* add quotes to COVERAGE variable to avoid split words

### Feat

* **logger:** add log level setting as environment variable

### Test

* **cmd:** increase time for TestRun to initialize fully


<a name="v1.3.4"></a>
## [v1.3.4](https://gitlab.com/altosys/bliss/compare/v1.3.3...v1.3.4) (2020-07-04)

### Chore

* **release:** v1.3.4 [skip ci]

### Ci

* format docker-compose test for loop

### Fix

* **db:** patch race condition in db package


<a name="v1.3.3"></a>
## [v1.3.3](https://gitlab.com/altosys/bliss/compare/v1.3.2...v1.3.3) (2020-07-04)

### Chore

* **release:** v1.3.3 [skip ci]

### Fix

* **db:** handle empty book titles response from db


<a name="v1.3.2"></a>
## [v1.3.2](https://gitlab.com/altosys/bliss/compare/v1.3.1...v1.3.2) (2020-07-04)

### Chore

* update toolkit to v1.0.4
* set default docker-compose.yml to scrape real book
* **release:** v1.3.2 [skip ci]

### Ci

* add docker-compose up test run to pipeline

### Fix

* patch mutex lock crash on start


<a name="v1.3.1"></a>
## [v1.3.1](https://gitlab.com/altosys/bliss/compare/v1.3.0...v1.3.1) (2020-07-03)

### Chore

* remove grafana single view graph
* **release:** v1.3.1 [skip ci]

### Fix

* patch race conditions between scraper and cmd


<a name="v1.3.0"></a>
## [v1.3.0](https://gitlab.com/altosys/bliss/compare/v1.2.1...v1.3.0) (2020-06-28)

### Chore

* configure grafana to allow embedding in x-frames
* add missing 'chore' type commit to go-micro merge template examples
* update toolkit to v1.0.3
* **changelog:** remove leftover line from changelog
* **release:** v1.3.0 [skip ci]

### Ci

* store code coverage in gitlab ci variable for badge generation
* use directed acyclic graph to improve pipeline execution time

### Docs

* update commit types list to table in go-micro.md
* **readme:** update variable table with type column in README.md

### Feat

* add parsing for including book title with isbn in env

### Style

* **grafana:** add lowest price table and graph


<a name="v1.2.1"></a>
## [v1.2.1](https://gitlab.com/altosys/bliss/compare/v1.2.0...v1.2.1) (2020-04-13)

### Chore

* **release:** v1.2.1 [skip ci]

### Fix

* thoth access to db for fetching book names


<a name="v1.2.0"></a>
## [v1.2.0](https://gitlab.com/altosys/bliss/compare/v1.1.3...v1.2.0) (2020-04-05)

### Chore

* **release:** v1.2.0 [skip ci]

### Feat

* **grafana:** add site selection dropdown menu


<a name="v1.1.3"></a>
## [v1.1.3](https://gitlab.com/altosys/bliss/compare/v1.1.2...v1.1.3) (2020-03-26)

### Chore

* update toolkit to v1.0.2
* **release:** v1.1.3 [skip ci]

### Docs

* README.md formatting

### Fix

* add fallback book name sources

### Refactor

* **docker:** move influxdb env to docker-compose .env file
* **scraper:** use cfg.Scraper settings in InitSites func


<a name="v1.1.2"></a>
## [v1.1.2](https://gitlab.com/altosys/bliss/compare/v1.1.1...v1.1.2) (2020-03-18)

### Chore

* **release:** v1.1.2 [skip ci]

### Docs

* update README.md variable descriptions

### Fix

* **scraper:** patch cdon scraping without search result


<a name="v1.1.1"></a>
## [v1.1.1](https://gitlab.com/altosys/bliss/compare/v1.1.0...v1.1.1) (2020-03-09)

### Chore

* **release:** v1.1.1 [skip ci]

### Ci

* remove redundant git commands from badge job

### Fix

* add loop to main error/exit select statement


<a name="v1.1.0"></a>
## [v1.1.0](https://gitlab.com/altosys/bliss/compare/v1.0.2...v1.1.0) (2020-03-07)

### Chore

* **release:** v1.1.0 [skip ci]

### Feat

* **db:** add retention policy setting


<a name="v1.0.2"></a>
## [v1.0.2](https://gitlab.com/altosys/bliss/compare/v1.0.1...v1.0.2) (2020-03-04)

### Chore

* **release:** v1.0.2 [skip ci]

### Fix

* **scraper:** add currency check for DOM response


<a name="v1.0.1"></a>
## [v1.0.1](https://gitlab.com/altosys/bliss/compare/v1.0.0...v1.0.1) (2020-03-03)

### Chore

* **release:** v1.0.1 [skip ci]

### Ci

* set pipeline trigger to run only on master/prerelease

### Docs

* add explanations for commit types to mr template

### Fix

* **scraper:** patch scraper loop panic

### Refactor

* **scraper:** add more user agents to avoid scraper detection


<a name="v1.0.0"></a>
## v1.0.0 (2020-02-29)

### Chore

* **git:** new repository
* **release:** v1.0.0 [skip ci]

### Feat

* book price scraper with db and dashboard

