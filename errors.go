package bliss

import "errors"

var (
	// ErrInvalidCurrencyCode is a common error for an invalid currency code
	ErrInvalidCurrencyCode = errors.New("invalid currency code")
	// ErrEmptyMap is a common error for an unexpected empty map
	ErrEmptyMap = errors.New("unexpected empty map")
	// ErrEmptyString is a common error for an unexpected empty string
	ErrEmptyString = errors.New("unexpected empty string")
	// ErrEmptyStruct is a common error for an unexpected empty struct
	ErrEmptyStruct = errors.New("unexpected empty struct")
	// ErrEmptySlice is a common error for an unexpected empty slice
	ErrEmptySlice = errors.New("unexpected empty slice")
	// ErrNilPointer is a common error for an unexpected nil pointer
	ErrNilPointer = errors.New("unexpected nil pointer")
	// ErrInvalidAddr is a common error for an invalid address
	ErrInvalidAddr = errors.New("address is invalid")
	// ErrInvalidPort is a common error for an invalid port
	ErrInvalidPort = errors.New("port is invalid")
	// ErrPortOutOfRange is a common error for a port out of range
	ErrPortOutOfRange = errors.New("port is out of range [1-65535]")
)
