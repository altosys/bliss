package influxdb

import (
	"bliss"
	"context"
	"fmt"
	"sync"
	"time"

	_ "github.com/influxdata/influxdb1-client" // this is important because of the bug in go mod
	influxdb "github.com/influxdata/influxdb1-client/v2"
	"go.uber.org/zap"
)

// Config holds configuration parameters for InfluxDB
type Config struct {
	Database, Address, User, Password, Precision, Retention string
}

// InfluxDB runs queries against influxdb
type InfluxDB struct {
	lg        *zap.SugaredLogger
	client    influxdb.Client
	database  string
	precision string
	retention string
}

// NewInfluxDB creates a new InfluxDB
func NewInfluxDB(cfg *Config, lg *zap.SugaredLogger) (InfluxDB, error) {
	if lg == nil {
		return InfluxDB{}, fmt.Errorf("%w: %s", bliss.ErrNilPointer, "lg")
	}
	if cfg.Address == "" {
		return InfluxDB{}, bliss.ErrEmptyString
	}

	client, err := influxdb.NewHTTPClient(influxdb.HTTPConfig{
		Addr:     "http://" + cfg.Address,
		Username: cfg.User,
		Password: cfg.Password,
		Timeout:  3 * time.Second,
	})
	if err != nil {
		return InfluxDB{}, err
	}

	idb := InfluxDB{
		lg:        lg,
		database:  cfg.Database,
		precision: cfg.Precision,
		retention: cfg.Retention,
		client:    client,
	}
	return idb, nil
}

// Ping sends a request to InfluxDBs ping endpoint
func (idb *InfluxDB) Ping(timeout time.Duration) (time.Duration, string, error) {
	return idb.client.Ping(time.Second)
}

// MigrateRetentionPolicy migrates series from one retention policy to another by copying
func (idb *InfluxDB) MigrateRetentionPolicy(oldRetentionPolicy, newRetentionPolicy string) error {
	idb.lg.Infof("migrating influxdb data to retention policy '%s'...", newRetentionPolicy)
	q := influxdb.Query{
		Command: fmt.Sprintf("select * into %s.%s.listings from %s.%s.listings group by *",
			idb.database, newRetentionPolicy, idb.database, oldRetentionPolicy),
		Database:        idb.database,
		Precision:       idb.precision,
		RetentionPolicy: oldRetentionPolicy,
	}

	_, err := idb.client.Query(q)
	if err == nil {
		idb.lg.Infof("finished migrating influxdb data to retention policy  '%s'", newRetentionPolicy)
	}
	return err
}

// Prune removes listings from InfluxDB which are not among the books or scrapers passed to the function.
func (idb *InfluxDB) Prune(books []bliss.Book, scrapers []bliss.Scraper, lg *zap.SugaredLogger) error {
	scrapersMap := map[string]bliss.Scraper{}
	pruned := map[string]string{}

	for _, s := range scrapers {
		scrapersMap[s.Site()] = s
	}

	q := influxdb.Query{
		Command:         "select last(*) from listings group by format,isbn,title,site",
		Database:        idb.database,
		Precision:       idb.precision,
		RetentionPolicy: idb.retention,
	}

	bm := map[string]bliss.Book{}
	for _, b := range books {
		bm[b.ISBN] = b
	}

	rsp, err := idb.client.Query(q)
	if err != nil {
		return err
	}

	if rsp != nil && len(rsp.Results) > 0 {
		for _, series := range rsp.Results[0].Series {
			isbn := series.Tags["isbn"]
			format := bliss.Formats[series.Tags["format"]]
			title := series.Tags["title"]
			site := series.Tags["site"]

			if book, exists := bm[isbn]; exists {
				if book.Format != format && book.Format != bliss.NA {
					q.Command = fmt.Sprintf("drop series where isbn='%s' and format='%s'", isbn, format)
					if _, err := idb.client.Query(q); err != nil {
						return err
					}
					pruned[isbn] = fmt.Sprintf("pruned listings with format '%s' for %s (format does not match BLISS_BOOKS)", format, title)
				}
			} else {
				q.Command = fmt.Sprintf("drop series where isbn='%s'", isbn)
				if _, err := idb.client.Query(q); err != nil {
					return err
				}
				pruned[isbn] = fmt.Sprintf("pruned %s %s '%s' (not in BLISS_BOOKS)", isbn, format.String(), title)
			}

			if _, exists := scrapersMap[site]; !exists {
				q.Command = fmt.Sprintf("drop series where site='%s'", series.Tags["site"])
				if _, err := idb.client.Query(q); err != nil {
					return err
				}
				pruned[site] = fmt.Sprintf("pruned all listings from %s (scraper disabled)", site)
			}
		}

		q.Command = fmt.Sprintf("select last(*) from listings where format='%s'", bliss.NA.String())
		rsp, _ := idb.client.Query(q)
		if rsp != nil && len(rsp.Results) > 0 && len(rsp.Results[0].Series) > 0 {
			q.Command = fmt.Sprintf("drop series where format='%s'", bliss.NA.String())
			if _, err := idb.client.Query(q); err != nil {
				return err
			}
			pruned["unknown"] = "pruned all listings with unknown format"
		}
	}

	if len(pruned) > 0 {
		lg.Info("database pruned: ")
		for _, v := range pruned {
			lg.Infof("%s", v)
		}
	}

	return nil
}

func (idb *InfluxDB) Write(ctx context.Context, errCh chan<- error, wg *sync.WaitGroup) chan bliss.Listing {
	dbChBuffer := 100
	dbCh := make(chan bliss.Listing, dbChBuffer)

	wg.Add(1)
	go func() {
		defer wg.Done()

		var tags map[string]string
		var fields map[string]interface{}

		for {
			select {
			case <-ctx.Done():
				idb.client.Close()
				close(dbCh)
				return
			case l := <-dbCh:
				tags = map[string]string{
					"format": l.Book.Format.String(),
					"isbn":   l.Book.ISBN,
					"site":   l.Site,
					"title":  l.Book.Title,
				}

				fields = map[string]interface{}{
					"currency": l.Currency,
					"price":    l.Price,
				}

				pt, err := influxdb.NewPoint("listings", tags, fields, time.Now())
				if err != nil {
					errCh <- err
					continue
				}

				bp, err := influxdb.NewBatchPoints(influxdb.BatchPointsConfig{
					Database:        idb.database,
					Precision:       idb.precision,
					RetentionPolicy: idb.retention,
				})

				if err != nil {
					errCh <- err
					continue
				}

				bp.AddPoint(pt)
				if err = idb.client.Write(bp); err != nil {
					errCh <- err
					continue
				}

				errCh <- nil
			}
		}
	}()
	return dbCh
}
