package influxdb

import (
	"bliss"
	it "bliss/internal/testing/influxdb"
	"bliss/logger"
	"bliss/mock"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"net/url"
	"strconv"
	"sync"
	"testing"
	"time"

	influxdb "github.com/influxdata/influxdb1-client/v2"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

func TestNewInfluxDB(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc      string
		address   string
		database  string
		user      string
		password  string
		precision string
		retention string
		logger    *zap.SugaredLogger
		err       error
	}{
		{
			desc:    "return error on nil logger",
			address: "influxdb:8086",
			logger:  nil,
			err:     fmt.Errorf("%w: %s", bliss.ErrNilPointer, "lg"),
		},
		{
			desc:    "return error on empty address",
			address: "",
			logger:  lg,
			err:     bliss.ErrEmptyString,
		},
		{
			desc:    "return error on invalid port",
			address: "influxdb:xyz",
			logger:  lg,
			err:     &url.Error{},
		},
		{
			desc:      "return new client and nil error on success",
			address:   "influxdb:8086",
			logger:    lg,
			database:  "bliss",
			precision: "s",
			retention: "one_month",
			err:       nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			dbConfig := Config{
				Address:   test.address,
				Database:  test.database,
				User:      test.user,
				Password:  test.password,
				Precision: test.precision,
				Retention: test.retention,
			}

			i, err := NewInfluxDB(&dbConfig, test.logger)
			switch test.err.(type) {
			case *url.Error:
				assert.Equal(t, "parse \"http://influxdb:xyz\": invalid port \":xyz\" after host", err.Error())
				return
			case error:
				assert.Equal(t, test.err, err)
				return
			default:
				assert.Equal(t, test.logger, i.lg)
				assert.Equal(t, test.database, i.database)
				assert.Equal(t, test.precision, i.precision)
				assert.Equal(t, test.retention, i.retention)
				assert.NotEmpty(t, i.client)
			}
		})
	}
}

func TestMigrateRetentionPolicyIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	testdata := []bliss.Listing{
		{
			Book: bliss.Book{
				Format: bliss.HC,
				ISBN:   "1111111111111",
				Title:  "book1",
			},
			Price: float32(1),
			Site:  "test1.com",
		},
		{
			Book: bliss.Book{
				Format: bliss.SC,
				ISBN:   "2222222222222",
				Title:  "book2",
			},
			Price: float32(1),
			Site:  "test2.com",
		},
		{
			Book: bliss.Book{
				Format: bliss.HC,
				ISBN:   "2222222222222",
				Title:  "book2",
			},
			Price: float32(1),
			Site:  "test2.com",
		},
	}

	tt := []struct {
		desc      string
		oldPolicy string
		newPolicy string
		err       error
	}{
		{
			desc:      "migrate to new retention policy",
			oldPolicy: "one_day",
			newPolicy: "one_week",
		},
	}

	idbC, addr := it.GetInfluxDBContainer(lg, t)
	cfg := getDefaultConfig(addr)
	idb, err := NewInfluxDB(&cfg, lg)
	assert.NoError(t, err)
	defer func() {
		defer idb.client.Close()
		assert.NoError(t, idbC.Terminate(context.Background()))
	}()

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			errCh := make(chan error)
			wg := sync.WaitGroup{}
			defer wg.Wait()
			defer cancel()
			defer close(errCh)

			listings, err := getListings(idb.client, test.oldPolicy, lg)
			assert.NoError(t, err)
			assert.Empty(t, listings)
			idb.retention = test.oldPolicy

			// populate db
			dbCh := idb.Write(ctx, errCh, &wg)
			for _, td := range testdata {
				assert.Empty(t, dbCh)
				dbCh <- bliss.Listing{
					Book:  td.Book,
					Price: td.Price,
					Site:  td.Site,
				}
				assert.NoError(t, <-errCh)
				assert.Empty(t, errCh)
			}

			listings, err = getListings(idb.client, test.oldPolicy, lg)
			assert.NoError(t, err)
			assert.Len(t, listings, len(testdata))
			assert.NoError(t, idb.MigrateRetentionPolicy(test.oldPolicy, test.newPolicy))
			listings, err = getListings(idb.client, test.newPolicy, lg)
			assert.NoError(t, err)
			assert.Len(t, listings, len(testdata))
		})
	}
}

func TestPruneIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	mockScraper1 := mock.GetScraper()
	mockScraper1.SiteFn = func() string { return "test1.com" }
	mockScraper2 := mock.GetScraper()
	mockScraper2.SiteFn = func() string { return "test2.com" }
	mockScraper3 := mock.GetScraper()
	mockScraper3.SiteFn = func() string { return "test3.com" }

	testdata := []bliss.Listing{
		{
			Book: bliss.Book{
				Format: bliss.HC,
				ISBN:   "1111111111111",
				Title:  "book1",
			},
			Price: float32(1),
			Site:  "test1.com",
		},
		{
			Book: bliss.Book{
				Format: bliss.SC,
				ISBN:   "2222222222222",
				Title:  "book2",
			},
			Price: float32(1),
			Site:  "test2.com",
		},
		{
			Book: bliss.Book{
				Format: bliss.HC,
				ISBN:   "2222222222222",
				Title:  "book2",
			},
			Price: float32(1),
			Site:  "test2.com",
		},
		{
			Book: bliss.Book{
				Format: bliss.NA,
				ISBN:   "3333333333333",
				Title:  "book3",
			},
			Price: float32(1),
			Site:  "test3.com",
		},
	}

	tt := []struct {
		desc     string
		books    []bliss.Book
		scrapers []bliss.Scraper
		expected []bliss.Listing
	}{
		{
			desc:     "prune all on empty books slice input",
			books:    []bliss.Book{},
			scrapers: []bliss.Scraper{},
			expected: []bliss.Listing{},
		},
		{
			desc: "prune all but books and scrapers intersection",
			books: []bliss.Book{
				{
					Format: bliss.HC,
					ISBN:   "1111111111111",
				},
				{
					Format: bliss.HC,
					ISBN:   "2222222222222",
				},
				{
					Format: bliss.NA,
					ISBN:   "3333333333333",
				},
			},
			scrapers: []bliss.Scraper{mockScraper2, mockScraper3},
			expected: []bliss.Listing{
				{
					Book: bliss.Book{
						ISBN:   "2222222222222",
						Title:  "book2",
						Format: bliss.HC,
					},
					Site:  "test2.com",
					Price: 1,
				},
			},
		},
	}

	scrapersMap := map[string]struct{}{}
	idbC, addr := it.GetInfluxDBContainer(lg, t)
	cfg := getDefaultConfig(addr)
	idb, err := NewInfluxDB(&cfg, lg)
	assert.NoError(t, err)
	defer func() {
		defer idb.client.Close()
		assert.NoError(t, idbC.Terminate(context.Background()))
	}()

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			errCh := make(chan error)
			wg := sync.WaitGroup{}
			defer wg.Wait()
			defer cancel()
			defer close(errCh)
			listings, err := getListings(idb.client, "one_week", lg)
			assert.Empty(t, listings)
			assert.NoError(t, err)

			for _, s := range test.scrapers {
				scrapersMap[s.Site()] = struct{}{}
			}

			// populate db
			dbCh := idb.Write(ctx, errCh, &wg)
			for _, td := range testdata {
				assert.Empty(t, dbCh)
				dbCh <- bliss.Listing{
					Book:  td.Book,
					Price: td.Price,
					Site:  td.Site,
				}
				assert.NoError(t, <-errCh)
				assert.Empty(t, errCh)
			}

			assert.NoError(t, idb.Prune(test.books, test.scrapers, lg))
			listings, err = getListings(idb.client, "one_week", lg)
			assert.NoError(t, err)
			assert.Equal(t, test.expected, listings)
		})
	}

	t.Run("return error from influxdb client", func(t *testing.T) {
		udpClient, err := influxdb.NewUDPClient(influxdb.UDPConfig{})
		idb.client = udpClient
		assert.NoError(t, err)
		assert.Error(t, idb.Prune([]bliss.Book{}, []bliss.Scraper{mockScraper1}, lg))
	})
}

func TestPingIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc string
		err  error
	}{
		{
			desc: "return nil on successful ping",
			err:  nil,
		},
		{
			desc: "return error on failed ping",
		},
	}

	idbC, addr := it.GetInfluxDBContainer(lg, t)
	cfg := getDefaultConfig(addr)
	idb, err := NewInfluxDB(&cfg, lg)
	assert.NoError(t, err)
	defer func() {
		defer idb.client.Close()
		assert.NoError(t, idbC.Terminate(context.Background()))
	}()

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			var idbPinger InfluxDB
			switch test.desc {
			case "return nil on successful ping":
				idbPinger = idb
			case "return error on failed ping":
				cfg := Config{Address: "http://localhost:1000"}
				failer, err := NewInfluxDB(&cfg, zap.NewNop().Sugar())
				assert.NoError(t, err)
				idbPinger = failer
			}
			duration, rsp, err := idbPinger.Ping(1 * time.Second)

			switch test.desc {
			case "return nil on successful ping":
				assert.NoError(t, err)
				assert.NotZero(t, duration)
				assert.NotEmpty(t, rsp)
			case "return error on failed ping":
				assert.Error(t, err)
			}
		})
	}
}

func TestWriteIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	lm := logger.NewManager(false, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc      string
		port      string
		listing   bliss.Listing
		precision string
		writeErr  error
	}{
		{
			desc: "return nil on valid listing",
			port: "http",
			listing: bliss.Listing{
				Book: bliss.Book{
					Format: bliss.HC,
					ISBN:   "0000000000000",
					Title:  "book1",
				},
				Currency: "EUR",
				Price:    float32(1),
				Site:     "test.com",
			},
			writeErr: nil,
		},
		{
			desc: "return nil on price not found",
			port: "http",
			listing: bliss.Listing{
				Book: bliss.Book{
					Format: bliss.HC,
					ISBN:   "0000000000000",
					Title:  "book2",
				},
				Currency: "",
				Price:    bliss.NotFound,
				Site:     "test.com",
			},
			writeErr: nil,
		},
		{
			desc: "return error on infinite value for price",
			port: "http",
			listing: bliss.Listing{
				Book: bliss.Book{
					Format: bliss.HC,
					ISBN:   "0000000000000",
					Title:  "book3",
				},
				Price: float32(math.Inf(1)),
				Site:  "test.com",
			},
			writeErr: fmt.Errorf("+/-Inf is an unsupported value for field price"),
		},
		{
			desc: "return error on new batch points",
			port: "http",
			listing: bliss.Listing{
				Book: bliss.Book{
					Format: bliss.HC,
					ISBN:   "0000000000000",
					Title:  "book5",
				},
				Price: float32(1),
				Site:  "test.com",
			},
			precision: "invalid",
			writeErr:  errors.New("time: unknown unit"),
		},
		{
			desc: "return error on write",
			port: "1000",
			listing: bliss.Listing{
				Book: bliss.Book{
					Format: bliss.HC,
					ISBN:   "0000000000000",
					Title:  "book4",
				},
				Price: float32(1),
				Site:  "test.com",
			},
			writeErr: &url.Error{},
		},
	}

	idbC, addr := it.GetInfluxDBContainer(lg, t)
	cfg := getDefaultConfig(addr)
	idb, err := NewInfluxDB(&cfg, lg)
	assert.NoError(t, err)
	defer func() {
		defer idb.client.Close()
		assert.NoError(t, idbC.Terminate(context.Background()))
	}()

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			errCh := make(chan error)
			wg := sync.WaitGroup{}
			defer wg.Wait()
			defer cancel()
			defer close(errCh)

			if test.port != "http" {
				c, err := influxdb.NewHTTPClient(influxdb.HTTPConfig{
					Addr:     "http://localhost:" + test.port,
					Username: "irrelevant",
					Password: "irrelevant",
					Timeout:  3 * time.Second,
				})
				assert.NoError(t, err)
				idb.client = c
			}

			if test.precision != "" {
				defaultPrecision := idb.precision
				idb.precision = test.precision
				defer func() {
					idb.precision = defaultPrecision
				}()
			}

			dbCh := idb.Write(ctx, errCh, &wg)
			if _, isURLError := test.writeErr.(*url.Error); !isURLError {
				resetDB(idb.client, test.precision, t)
			}

			assert.Empty(t, dbCh)
			dbCh <- test.listing
			writeErr := <-errCh

			q := influxdb.Query{
				Command:         "select * from listings",
				Database:        idb.database,
				Precision:       idb.precision,
				RetentionPolicy: idb.retention,
			}
			idbResp, queryErr := idb.client.Query(q)

			switch test.writeErr.(type) {
			case nil:
				assert.NoError(t, writeErr)
				assert.NoError(t, queryErr)

				idx := map[string]int{}
				for i, c := range idbResp.Results[0].Series[0].Columns {
					idx[c] = i
				}
				val := func(v string) interface{} {
					return idbResp.Results[0].Series[0].Values[0][idx[v]]
				}

				assert.NotEmpty(t, idbResp.Results[0].Series)
				assert.Equal(t, test.listing.Currency, val("currency"))
				assert.Equal(t, test.listing.Book.ISBN, val("isbn"))
				assert.Equal(t, json.Number(strconv.FormatFloat(float64(test.listing.Price), 'f', bliss.NotFound, 32)), val("price"))
				assert.Equal(t, test.listing.Site, val("site"))
				assert.Equal(t, test.listing.Book.Title, val("title"))
			case *url.Error:
				assert.IsType(t, &url.Error{}, queryErr)
				expURL := fmt.Sprintf(
					"http://localhost:%s/write?consistency=&db=bliss&precision=%s&rp=one_week",
					test.port,
					idb.precision,
				)
				assert.Equal(t, expURL, writeErr.(*url.Error).URL)
			default:
				assert.Contains(t, writeErr.Error(), test.writeErr.Error())
				assert.Empty(t, idbResp.Results[0])
			}

			if _, isURLError := test.writeErr.(*url.Error); !isURLError {
				resetDB(idb.client, test.precision, t)
			}
		})
	}
}

func getListings(client influxdb.Client, retentionPolicy string, lg *zap.SugaredLogger) ([]bliss.Listing, error) {
	current := []bliss.Listing{}
	cm := make(map[string][]bliss.Listing)
	q := influxdb.Query{
		Command:         "select * from listings group by format,isbn,site,title",
		Database:        "bliss",
		RetentionPolicy: retentionPolicy,
	}

	rsp, err := client.Query(q)
	if err != nil {
		return nil, err
	}

	if rsp != nil && len(rsp.Results) > 0 {
		for _, series := range rsp.Results[0].Series {
			price, _ := series.Values[0][2].(json.Number).Float64()
			isbn := series.Tags["isbn"]
			format := bliss.Formats[series.Tags["format"]]
			title := series.Tags["title"]
			site := series.Tags["site"]

			if format != bliss.NA {
				cm[site] = append(cm[site],
					bliss.Listing{
						Book: bliss.Book{
							Format: format,
							ISBN:   isbn,
							Title:  title,
						},
						Price: float32(price),
						Site:  site,
					})
			}
		}
	}

	if len(cm) > 0 {
		var numListings int
		for site := range cm {
			numListings += len(cm[site])
		}
		lg.Debug("current listings loaded from db: ", numListings)
		for _, v := range cm {
			current = append(current, v...)
		}
	}
	return current, nil
}

func resetDB(c influxdb.Client, precision string, t *testing.T) {
	q := influxdb.Query{
		Command:         "drop series from /.*/",
		Database:        "bliss",
		Precision:       precision,
		RetentionPolicy: "one_week",
	}
	_, err := c.Query(q)
	assert.NoError(t, err)

	q.Command = "select * from listings"
	rsp, err := c.Query(q)
	assert.NoError(t, err)
	assert.Empty(t, rsp.Results[0].Series)
}

func getDefaultConfig(address string) Config {
	return Config{
		Address:   address,
		Database:  "bliss",
		User:      "admin",
		Password:  "bliss",
		Precision: "ms",
		Retention: "one_week",
	}
}
