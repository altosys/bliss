#!/bin/sh

# base command for executing influxdb commands
INFLUX_CMD="influx -host 127.0.0.1 -port 8086 -username $INFLUXDB_ADMIN_USER -password $INFLUXDB_ADMIN_PASSWORD -execute"
DB=bliss

# create retention policies
CREATE_RETENTION_POLICY_ONE_YEAR="CREATE RETENTION POLICY one_year ON $DB DURATION 365d REPLICATION 1 SHARD DURATION 4w"
CREATE_RETENTION_POLICY_SIX_MONTHS="CREATE RETENTION POLICY six_months ON $DB DURATION 26w REPLICATION 1 SHARD DURATION 4w"
CREATE_RETENTION_POLICY_THREE_MONTHS="CREATE RETENTION POLICY three_months ON $DB DURATION 12w REPLICATION 1 SHARD DURATION 1w"
CREATE_RETENTION_POLICY_ONE_MONTH="CREATE RETENTION POLICY one_month ON $DB DURATION 4w REPLICATION 1 SHARD DURATION 1w"
CREATE_RETENTION_POLICY_ONE_WEEK="CREATE RETENTION POLICY one_week ON $DB DURATION 1w REPLICATION 1 SHARD DURATION 24h"
CREATE_RETENTION_POLICY_ONE_DAY="CREATE RETENTION POLICY one_day ON $DB DURATION 24h REPLICATION 1 SHARD DURATION 24h"

# execute commands
$INFLUX_CMD "$CREATE_RETENTION_POLICY_ONE_YEAR"
$INFLUX_CMD "$CREATE_RETENTION_POLICY_SIX_MONTHS"
$INFLUX_CMD "$CREATE_RETENTION_POLICY_THREE_MONTHS"
$INFLUX_CMD "$CREATE_RETENTION_POLICY_ONE_MONTH"
$INFLUX_CMD "$CREATE_RETENTION_POLICY_ONE_WEEK DEFAULT"
$INFLUX_CMD "$CREATE_RETENTION_POLICY_ONE_DAY"
