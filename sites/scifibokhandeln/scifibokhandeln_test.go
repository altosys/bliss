package scifibokhandeln

import (
	"bliss"
	it "bliss/internal/testing/sites"
	"bliss/logger"
	"testing"

	"go.uber.org/zap"
)

func TestSciFiBokhandeln(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []it.SiteTestData{
		{
			Desc: "sfbok.se in stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "SEK",
			ExpFormat:   bliss.NA,
			ExpPrice:    129,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/scifibokhandeln_search_in_stock"},
			},
		},
		{
			Desc: "sfbok.se coming item",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "SEK",
			ExpFormat:   bliss.NA,
			ExpPrice:    299,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {
					Doc:  "../../internal/testing/sites/testdata/scifibokhandeln_search_unknown",
					Link: "/scifibokhandeln_prod_coming",
				},
				"/scifibokhandeln_prod_coming": {
					Doc: "../../internal/testing/sites/testdata/scifibokhandeln_prod_coming",
				},
			},
		},
		{
			Desc: "sfbok.se out of stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "SEK",
			ExpFormat:   bliss.NA,
			ExpPrice:    bliss.NotFound,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {
					Doc:  "../../internal/testing/sites/testdata/scifibokhandeln_search_unknown",
					Link: "/scifibokhandeln_prod_no_stock",
				},
				"/scifibokhandeln_prod_no_stock": {
					Doc: "../../internal/testing/sites/testdata/scifibokhandeln_prod_no_stock",
				},
			},
		},
	}

	it.TestSite(t, tt, lg)
}
