package scifibokhandeln

import (
	"bliss"
	b "bliss/browser"
	"bliss/sites"
	"bliss/utils"

	"strings"

	"github.com/PuerkitoBio/goquery"
	"go.uber.org/zap"
)

type (
	scifibokhandelnScraper struct{}
)

var (
	scifibokhandeln bliss.Scraper = &scifibokhandelnScraper{}
)

func Scraper() bliss.Scraper {
	return scifibokhandeln
}

func (scifibokhandeln *scifibokhandelnScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, scifibokhandeln, parseBookMeta, lg)
}

func (scifibokhandeln *scifibokhandelnScraper) Site() string {
	return "sfbok.se"
}

func (scifibokhandeln *scifibokhandelnScraper) SearchURL(isbn string) string {
	return "https://www.sfbok.se/search?keys=" + isbn
}

func (scifibokhandeln *scifibokhandelnScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	// do not use scifibokhandeln as format source
	return bliss.NA
}

func (scifibokhandeln *scifibokhandelnScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	btnLabel := strings.TrimSpace(dom.Find(".product-links-1").Children().Text())
	price = bliss.NotFound
	currency = "SEK"

	if strings.EqualFold(btnLabel, "köp") {
		priceStr := strings.TrimSpace(dom.Find(".price").First().Text())
		return currency, utils.PriceToFloat(currency, priceStr, lg)
	}

	if link, exists := dom.Find(".btn.btn-default.btn-sm.read-more").First().Attr("href"); exists {
		productPageDOM, err := b.GetDOM(link)
		if err != nil {
			return currency, bliss.NotFound
		}

		var isAvailable bool
		productPageDOM.Find("meta").Each(func(i int, s *goquery.Selection) {
			itemprop, _ := s.Attr("itemprop")
			switch itemprop {
			case "price":
				if content, exists := s.Attr("content"); exists {
					price = utils.PriceToFloat(currency, content, lg)
				}
			case "availability":
				if content, exists := s.Attr("content"); exists {
					if strings.EqualFold(content, "preorder") {
						isAvailable = true
					}
				}
			}
		})

		if !isAvailable {
			return currency, bliss.NotFound
		}
	}
	return currency, price
}

func (scifibokhandeln *scifibokhandelnScraper) ParseTitle(dom *goquery.Selection) string {
	title := dom.Find(".views-row.views-row-1.views-row-odd.views-row-first.col-xs-6.col-sm-3.col-md-2").Find("h2").First().Text()
	return strings.TrimSpace(title)
}
