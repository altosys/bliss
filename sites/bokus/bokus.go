package bokus

import (
	"bliss"
	"bliss/sites"
	"bliss/utils"
	"regexp"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"go.uber.org/zap"
)

type (
	bokusScraper struct{}
)

var (
	bokus bliss.Scraper = &bokusScraper{}
)

func Scraper() bliss.Scraper {
	return bokus
}

func (bokus *bokusScraper) Scrape(book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, bokus, parseBookMeta, lg)
}

func (bokus *bokusScraper) Site() string {
	return "bokus.com"
}

func (bokus *bokusScraper) SearchURL(isbn string) string {
	return "https://www.bokus.com/cgi-bin/product_search.cgi?ac_used=no&search_word=" + isbn
}

func (bokus *bokusScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	switch strings.ToLower(strings.TrimSpace(dom.Find("span.product__format").First().Text())) {
	case "inbunden":
		return bliss.HC
	case "häftad", "pocket", "storpocket":
		return bliss.SC
	default:
		return bliss.NA
	}
}

func (bokus *bokusScraper) ParsePrice(dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	currency = "SEK"
	price = bliss.NotFound

	status := dom.Find(".product-page__replacement-message").First().Text()
	if strings.Contains(strings.ToLower(status), "hittar inte") {
		return
	}

	var isAvailable bool
	dom.Find("meta").Each(func(i int, s *goquery.Selection) {
		if itemprop, _ := s.Attr("name"); itemprop == "description" {
			if content, exists := s.Attr("content"); exists {
				c := strings.ToLower(content)
				isAvailable = !regexp.MustCompile(`slut|ej utkommen|bevaka`).MatchString(c)
			}
		}
	})

	if isAvailable {
		price = bliss.NotFound
		dom.Find("meta").Each(func(i int, s *goquery.Selection) {
			if itemprop, _ := s.Attr("itemprop"); itemprop == "price" {
				priceStr, _ := s.Attr("content")
				price = utils.PriceToFloat(currency, priceStr, lg)
			}
		})
	}
	return
}

func (bokus *bokusScraper) ParseTitle(dom *goquery.Selection) string {
	return strings.TrimSpace(dom.Find(".product-page__title").First().Text())
}
