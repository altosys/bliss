package bokus

import (
	"bliss"
	it "bliss/internal/testing/sites"
	"bliss/logger"
	"testing"

	"go.uber.org/zap"
)

const (
	sek = "SEK"
)

func TestBokus(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []it.SiteTestData{
		{
			Desc: "bokus.com coming item",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.HC,
			ExpPrice:    bliss.NotFound,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/bokus_coming"},
			},
		},
		{
			Desc: "bokus.com in stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.HC,
			ExpPrice:    169.00,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/bokus_in_stock_hc"},
			},
		},
		{
			Desc: "bokus.com in stock softcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.SC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.SC,
			ExpPrice:    169.00,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/bokus_in_stock_sc"},
			},
		},
		{
			Desc: "bokus.com in stock unknown format",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.NA,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.NA,
			ExpPrice:    169.00,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/bokus_in_stock_na"},
			},
		},
		{
			Desc: "bokus.com out of stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.HC,
			ExpPrice:    bliss.NotFound,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/bokus_no_stock"},
			},
		},
		{
			Desc: "bokus.com no metadata returned",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.HC,
			ExpPrice:    bliss.NotFound,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/bokus_no_meta"},
			},
		},
		{
			Desc: "bokus.com requested edition not found",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.SC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.SC,
			ExpPrice:    bliss.NotFound,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/bokus_edition_not_found"},
			},
		},
	}

	it.TestSite(t, tt, lg)
}
