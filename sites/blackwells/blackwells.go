package blackwells

import (
	"bliss"
	"bliss/sites"
	"bliss/utils"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"go.uber.org/zap"
)

type (
	blackwellsScraper struct{}
)

var (
	blackwells bliss.Scraper = &blackwellsScraper{}
)

func Scraper() bliss.Scraper {
	return blackwells
}

func (blackwells *blackwellsScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, blackwells, parseBookMeta, lg)
}

func (blackwells *blackwellsScraper) Site() string {
	return "blackwells.co.uk"
}

func (blackwells *blackwellsScraper) SearchURL(isbn string) string {
	return "https://blackwells.co.uk/bookshop/search/?keyword=" + isbn
}

func (blackwells *blackwellsScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	format := bliss.NA
	dom.Find("span").Each(func(i int, s *goquery.Selection) {
		itemprop, _ := s.Attr("itemprop")
		if itemprop == "bookFormat" {
			content := strings.TrimSpace(strings.ToLower(s.Text()))
			switch content {
			case "hardback":
				format = bliss.HC
			case "paperback":
				format = bliss.SC
			}
		}
	})
	return format
}

func (blackwells *blackwellsScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	currency = dom.Find(".currency__display-code").First().Text()
	status := dom.Find("span.is-information").First().Text()
	status = strings.TrimSpace(strings.ToLower(status))
	if strings.Contains(status, "out of stock") {
		return currency, float32(-1)
	}

	priceContent := dom.Find("div.product__price").First().Text()
	if strings.TrimSpace(priceContent) == "" {
		return currency, bliss.NotFound
	}

	priceStr := dom.Find("li.product-price--current").First().Text()
	priceStr = strings.Replace(priceStr, ".", "", 1)
	priceStr = strings.TrimSpace(priceStr)
	return currency, utils.PriceToFloat(currency, priceStr, lg)
}

func (blackwells *blackwellsScraper) ParseTitle(dom *goquery.Selection) string {
	titleStr := dom.Find(".product__name").First().Text()
	return strings.Join(strings.Fields(strings.TrimSpace(titleStr)), " ")
}
