package blackwells

import (
	"bliss"
	it "bliss/internal/testing/sites"
	"bliss/logger"
	"testing"

	"go.uber.org/zap"
)

const (
	sek = "SEK"
)

func TestBlackwells(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []it.SiteTestData{
		{
			Desc: "blackwells.co.uk coming item",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.HC,
			ExpPrice:    232.35,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/blackwells_coming"},
			},
		},
		{
			Desc: "blackwells.co.uk not available",
			Book: bliss.Book{
				Title:  "Order of the Stick",
				Format: bliss.SC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.NA,
			ExpPrice:    bliss.NotFound,
			ExpTitle:    "Order of the Stick",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/blackwells_not_available"},
			},
		},
		{
			Desc: "blackwells.co.uk in stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein - Penguin Clothbound Classics",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.HC,
			ExpPrice:    1060.13,
			ExpTitle:    "Frankenstein - Penguin Clothbound Classics",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/blackwells_in_stock_hc"},
			},
		},
		{
			Desc: "blackwells.co.uk in stock softcover",
			Book: bliss.Book{
				Title:  "Frankenstein - Penguin Clothbound Classics",
				Format: bliss.SC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.SC,
			ExpPrice:    1060.13,
			ExpTitle:    "Frankenstein - Penguin Clothbound Classics",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/blackwells_in_stock_sc"},
			},
		},
		{
			Desc: "blackwells.co.uk in stock unknown format",
			Book: bliss.Book{
				Title:  "Frankenstein - Penguin Clothbound Classics",
				Format: bliss.NA,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.NA,
			ExpPrice:    1060.13,
			ExpTitle:    "Frankenstein - Penguin Clothbound Classics",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/blackwells_in_stock_na"},
			},
		},
		{
			Desc: "blackwells.co.uk out of stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.HC,
			ExpPrice:    bliss.NotFound,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/blackwells_no_stock"},
			},
		},
	}

	it.TestSite(t, tt, lg)
}
