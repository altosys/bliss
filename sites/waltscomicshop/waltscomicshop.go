package waltscomicshop

import (
	"bliss"
	"bliss/sites"
	"bliss/utils"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"go.uber.org/zap"
)

type (
	waltscomicshopScraper struct{}
)

var (
	waltscomicshop bliss.Scraper = &waltscomicshopScraper{}
)

func Scraper() bliss.Scraper {
	return waltscomicshop
}

func (waltscomicshop *waltscomicshopScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, waltscomicshop, parseBookMeta, lg)
}

func (waltscomicshop *waltscomicshopScraper) Site() string {
	return "waltscomicshop.com"
}

func (waltscomicshop *waltscomicshopScraper) SearchURL(isbn string) string {
	return "https://waltscomicshop.com/search?q=" + isbn
}

func (waltscomicshop *waltscomicshopScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	return bliss.NA
}

func (waltscomicshop *waltscomicshopScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	price = bliss.NotFound
	currency = "EUR"

	isSale := strings.Contains(strings.ToLower(dom.Find(".list-view-item__price-column").First().Text()), "sale")

	var priceElement string
	if isSale {
		priceElement = "span.price-item.price-item--sale"
	} else {
		priceElement = "span.price-item.price-item--regular"
	}

	priceStr := strings.ToLower(strings.TrimSpace(dom.Find(priceElement).First().Text()))
	price = utils.PriceToFloat(currency, priceStr, lg)
	return
}

func (waltscomicshop *waltscomicshopScraper) ParseTitle(dom *goquery.Selection) string {
	return dom.Find("span.product-card__title").First().Text()
}
