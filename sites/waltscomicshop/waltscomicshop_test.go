package waltscomicshop

import (
	"bliss"
	it "bliss/internal/testing/sites"
	"bliss/logger"
	"testing"

	"go.uber.org/zap"
)

func TestWaltsComicShop(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []it.SiteTestData{
		{
			Desc: "waltscomicshop.com in stock hardcover with regular price",
			Book: bliss.Book{
				Title:  "Did You Hear What Eddie Gein Done GN HC",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "EUR",
			ExpFormat:   bliss.NA,
			ExpPrice:    27.50,
			ExpTitle:    "Did You Hear What Eddie Gein Done GN HC",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/waltscomicshop_hc_regular"},
			},
		},
		{
			Desc: "waltscomicshop.com in stock hardcover with sale price",
			Book: bliss.Book{
				Title:  "Deep Sleeper HC",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "EUR",
			ExpFormat:   bliss.NA,
			ExpPrice:    14.90,
			ExpTitle:    "Deep Sleeper HC",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/waltscomicshop_hc_sale"},
			},
		},
		{
			Desc: "waltscomicshop.com out of stock hardcover",
			Book: bliss.Book{
				Title:  "Deep Sleeper HC",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "EUR",
			ExpFormat:   bliss.NA,
			ExpPrice:    bliss.NotFound,
			ExpTitle:    "",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/waltscomicshop_na"},
			},
		},
	}

	it.TestSite(t, tt, lg)
}
