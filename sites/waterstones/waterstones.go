package waterstones

import (
	"bliss"
	"bliss/sites"
	"bliss/utils"

	"regexp"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"go.uber.org/zap"
)

type (
	waterstonesScraper struct{}
)

var (
	waterstones bliss.Scraper = &waterstonesScraper{}
)

func Scraper() bliss.Scraper {
	return waterstones
}

func (waterstones *waterstonesScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, waterstones, parseBookMeta, lg)
}

func (waterstones *waterstonesScraper) Site() string {
	return "waterstones.com"
}

func (waterstones *waterstonesScraper) SearchURL(isbn string) string {
	return "https://www.waterstones.com/books/search/term/" + isbn
}

func (waterstones *waterstonesScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	return bliss.NA
}

func (waterstones *waterstonesScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	var isAvailable bool
	price = bliss.NotFound
	currency = "GBP"

	status := strings.ToLower(dom.Find("span#scope_offer_availability").First().Text())
	if regexp.MustCompile("coming soon|in stock").MatchString(status) {
		isAvailable = true
	}

	if isAvailable {
		dom.Find("b").Each(func(i int, s *goquery.Selection) {
			itemprop, _ := s.Attr("itemprop")
			if itemprop == "price" {
				price = utils.PriceToFloat(currency, s.Text(), lg)
			}
		})
	}
	return
}

func (waterstones *waterstonesScraper) ParseTitle(dom *goquery.Selection) string {
	return dom.Find("h1.title").First().Text()
}
