package waterstones

import (
	"bliss"
	it "bliss/internal/testing/sites"
	"bliss/logger"
	"testing"

	"go.uber.org/zap"
)

func TestWaterstones(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []it.SiteTestData{
		{
			Desc: "waterstones.com coming item",
			Book: bliss.Book{
				Title:  "Frankenstein (Hardback)",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "GBP",
			ExpFormat:   bliss.NA,
			ExpPrice:    20.00,
			ExpTitle:    "Frankenstein (Hardback)",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/waterstones_coming"},
			},
		},
		{
			Desc: "waterstones.com in stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein - Penguin Clothbound Classics (Hardback)",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "GBP",
			ExpFormat:   bliss.NA,
			ExpPrice:    14.99,
			ExpTitle:    "Frankenstein - Penguin Clothbound Classics (Hardback)",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/waterstones_in_stock"},
			},
		},
		{
			Desc: "waterstones.com out of stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein Vol. 1 (Hardback)",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "GBP",
			ExpFormat:   bliss.NA,
			ExpPrice:    bliss.NotFound,
			ExpTitle:    "Frankenstein Vol. 1 (Hardback)",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/waterstones_no_stock"},
			},
		},
	}

	it.TestSite(t, tt, lg)
}
