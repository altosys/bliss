package flipkart

import (
	"bliss"
	it "bliss/internal/testing/sites"
	"bliss/logger"
	"testing"

	"go.uber.org/zap"
)

func TestFlipkart(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []it.SiteTestData{
		{
			Desc: "flipkart.com in stock softcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.SC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "INR",
			ExpFormat:   bliss.NA,
			ExpPrice:    90,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/flipkart_sc"},
			},
		},
		{
			Desc: "flipkart.com out of stock softcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.SC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "INR",
			ExpFormat:   bliss.NA,
			ExpPrice:    bliss.NotFound,
			ExpTitle:    "Saga Of The Swamp Thing Book One",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/flipkart_no_stock"},
			},
		},
		{
			Desc: "flipkart.com item not found",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.SC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "INR",
			ExpFormat:   bliss.NA,
			ExpPrice:    bliss.NotFound,
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/flipkart_not_found"},
			},
		},
	}

	it.TestSite(t, tt, lg)
}
