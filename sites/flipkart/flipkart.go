package flipkart

import (
	"bliss"
	"bliss/sites"
	"bliss/utils"

	"strings"

	"github.com/PuerkitoBio/goquery"
	"go.uber.org/zap"
)

type (
	flipkartScraper struct{}
)

var (
	flipkart bliss.Scraper = &flipkartScraper{}
)

func Scraper() bliss.Scraper {
	return flipkart
}

func (flipkart *flipkartScraper) Site() string {
	return "flipkart.com"
}

func (flipkart *flipkartScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, flipkart, parseBookMeta, lg)
}

func (flipkart *flipkartScraper) SearchURL(isbn string) string {
	return "https://www.flipkart.com/search?augment=false&q=" + isbn
}

func (flipkart *flipkartScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	return bliss.NA
}

func (flipkart *flipkartScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	currency = "INR"
	price = bliss.NotFound
	status := strings.ToLower(dom.Find("span._1GJ2ZM").First().Text())

	if !strings.Contains(status, "unavailable") {
		priceStr := dom.Find("div._1vC4OE").First().Text()
		price = utils.PriceToFloat(currency, priceStr, lg)
	}
	return
}

func (flipkart *flipkartScraper) ParseTitle(dom *goquery.Selection) string {
	return strings.TrimSpace(dom.Find("a._2cLu-l").First().Text())
}
