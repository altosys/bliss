package bookdepository

import (
	"bliss"
	"bliss/sites"
	"bliss/utils"

	"strings"

	"github.com/PuerkitoBio/goquery"
	"go.uber.org/zap"
)

type (
	bookdepositoryScraper struct{}
)

var (
	bookdepository bliss.Scraper = &bookdepositoryScraper{}
)

func Scraper() bliss.Scraper {
	return bookdepository
}

func (bookdepository *bookdepositoryScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, bookdepository, parseBookMeta, lg)
}

func (bookdepository *bookdepositoryScraper) Site() string {
	return "bookdepository.com"
}

func (bookdepository *bookdepositoryScraper) SearchURL(isbn string) string {
	return "https://www.bookdepository.com/search?searchTerm=" + isbn
}

func (bookdepository *bookdepositoryScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	switch strings.ToLower(strings.TrimSpace(dom.Find("ul.meta-info").Children().First().Text())) {
	case "hardback":
		return bliss.HC
	case "paperback":
		return bliss.SC
	default:
		return bliss.NA
	}
}

func (bookdepository *bookdepositoryScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	priceStr := dom.Find(".item-price-wrap .sale-price").First().Text()
	priceStr = strings.TrimSpace(priceStr)

	dom.Find("#copyright").Each(func(i int, s *goquery.Selection) {
		if title, _ := s.Attr("title"); title != "" {
			titleFields := strings.Split(title, ",")
			for _, s := range titleFields {
				if strings.Contains(s, "userSessionCurrencyCode") {
					currency = strings.Split(s, "'")[1]
				}
			}
		}
	})
	return currency, utils.PriceToFloat(currency, priceStr, lg)
}

func (bookdepository *bookdepositoryScraper) ParseTitle(dom *goquery.Selection) string {
	return dom.Find(".item-info").Find("h1").First().Text()
}
