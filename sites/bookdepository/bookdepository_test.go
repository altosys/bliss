package bookdepository

import (
	"bliss"
	it "bliss/internal/testing/sites"
	"bliss/logger"
	"testing"

	"go.uber.org/zap"
)

const (
	sek = "SEK"
)

func TestBookDepository(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []it.SiteTestData{
		{
			Desc: "bookdepository.com in stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.HC,
			ExpPrice:    215.10,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/bookdepository_hc"},
			},
		},
		{
			Desc: "bookdepository.com in stock softcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.SC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.SC,
			ExpPrice:    142.94,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/bookdepository_sc"},
			},
		},
		{
			Desc: "bookdepository.com in stock unknown format",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.NA,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.NA,
			ExpPrice:    164.95,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/bookdepository_na"},
			},
		},
		{
			Desc: "bookdepository.com not in stock",
			Book: bliss.Book{
				Title:  "Three Shadows",
				Format: bliss.SC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.SC,
			ExpPrice:    -1,
			ExpTitle:    "Three Shadows",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/bookdepository_no_stock"},
			},
		},
	}

	it.TestSite(t, tt, lg)
}
