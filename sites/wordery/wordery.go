package wordery

import (
	"bliss"
	"bliss/sites"
	"bliss/utils"

	"regexp"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"go.uber.org/zap"
)

type (
	worderyScraper struct{}
)

var (
	wordery bliss.Scraper = &worderyScraper{}
)

func Scraper() bliss.Scraper {
	return wordery
}

func (wordery *worderyScraper) Scrape(book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, wordery, parseBookMeta, lg)
}

func (wordery *worderyScraper) Site() string {
	return "wordery.com"
}

func (wordery *worderyScraper) SearchURL(isbn string) string {
	return "https://wordery.com/search?term=" + isbn
}

func (wordery *worderyScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	format := bliss.NA
	dom.Find("dl.o-dl-inline.o-dl-inline--colon").Children().Each(func(i int, s *goquery.Selection) {
		if strings.Contains(strings.ToLower(s.Text()), "format") {
			formatStr := regexp.MustCompile("[^a-zA-Z]+").ReplaceAllString(s.Next().Text(), "")
			switch strings.ToLower(formatStr) {
			case "hardback":
				format = bliss.HC
			case "paperback":
				format = bliss.SC
			}
		}
	})
	return format
}

func (wordery *worderyScraper) ParsePrice(dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	price = bliss.NotFound
	var priceStr string
	dom.Find("meta").Each(func(i int, s *goquery.Selection) {
		itemprop, _ := s.Attr("itemprop")
		switch itemprop {
		case "price":
			priceStr, _ = s.Attr("content")
		case "priceCurrency":
			currency, _ = s.Attr("content")
		}
	})

	if priceStr != "" {
		// replace incorrect '.' separator with ','
		priceStr = strings.ReplaceAll(priceStr, ".", ",")
		price = utils.PriceToFloat(currency, priceStr, lg)
	}
	return
}

func (wordery *worderyScraper) ParseTitle(dom *goquery.Selection) string {
	var title string
	dom.Find("strong.u-t--norm").Each(func(i int, s *goquery.Selection) {
		if itemprop, _ := s.Attr("itemprop"); itemprop == "name" {
			title = strings.TrimSpace(s.Text())
			return
		}
	})
	return title
}
