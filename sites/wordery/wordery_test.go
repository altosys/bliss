package wordery

import (
	"bliss"
	it "bliss/internal/testing/sites"
	"bliss/logger"
	"testing"

	"go.uber.org/zap"
)

func TestWordery(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []it.SiteTestData{
		{
			Desc: "wordery.com in stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "EUR",
			ExpFormat:   bliss.HC,
			ExpPrice:    17.59,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/wordery_hc"},
			},
		},
		{
			Desc: "wordery.com in stock softcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.SC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "EUR",
			ExpFormat:   bliss.SC,
			ExpPrice:    17.59,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/wordery_sc"},
			},
		},
		{
			Desc: "wordery.com in stock unknown item",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.NA,
			},
			Scraper:     Scraper(),
			ExpCurrency: "EUR",
			ExpFormat:   bliss.NA,
			ExpPrice:    17.59,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/wordery_na"},
			},
		},
	}

	it.TestSite(t, tt, lg)
}
