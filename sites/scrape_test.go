package sites

import (
	"bliss"
	"bliss/logger"
	"bliss/mock"
	"fmt"
	"net/url"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

func TestScrape(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	book := bliss.Book{
		Format: bliss.HC,
		Title:  "Frankenstein",
		ISBN:   "9780141393391",
	}

	tt := []struct {
		desc          string
		book          bliss.Book
		parseBookMeta bool
		doc           string
		err           error
		expListing    bliss.Listing
		scraper       mock.Scraper
	}{
		{
			desc:          "return listing on successful scrape",
			book:          bliss.Book{ISBN: book.ISBN},
			parseBookMeta: true,
			scraper:       mock.GetScraper(),
			expListing: bliss.Listing{
				Book:     book,
				Currency: "SEK",
				Price:    215.1,
				Site:     "bookdepository.com",
			},
			doc: "../internal/testing/sites/testdata/bookdepository_hc",
		},
		{
			desc:          "return incomplete metadata if not found",
			book:          bliss.Book{ISBN: book.ISBN},
			parseBookMeta: true,
			scraper:       mock.GetScraper(),
			expListing: bliss.Listing{
				Book:     bliss.Book{ISBN: book.ISBN},
				Currency: "SEK",
				Price:    bliss.NotFound,
				Site:     "bookdepository.com",
			},
			doc: "../internal/testing/sites/testdata/bookdepository_not_found",
		},
		{
			desc:          "return error from getDOM",
			book:          book,
			parseBookMeta: true,
			scraper:       mock.GetScraper(),
			err:           &url.Error{},
			expListing: bliss.Listing{
				Book:     book,
				Currency: "",
				Price:    bliss.NotFound,
				Site:     "bookdepository.com",
			},
			doc: "../internal/testing/sites/testdata/empty",
		},
		{
			desc:          "return error when result not found",
			book:          book,
			parseBookMeta: false,
			scraper:       mock.GetScraper(),
			err:           fmt.Errorf("failed to fetch DOM for 'Frankenstein' at bookdepository.com"),
			expListing: bliss.Listing{
				Book:     book,
				Currency: "",
				Price:    bliss.NotFound,
				Site:     "bookdepository.com",
			},
			doc: "../internal/testing/sites/testdata/empty",
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			f, err := os.ReadFile(test.doc)
			assert.NoError(t, err)
			ts := mock.HTTPServer(t, "/", f)
			defer ts.Close()

			searchURL := ts.URL
			_, expURLError := test.err.(*url.Error)
			if expURLError {
				searchURL += "$"
			}

			test.scraper.ScrapeFn = Scrape
			test.scraper.SearchURLFn = func(isbn string) string { return searchURL }
			assert.Contains(t, test.scraper.SearchURL(book.ISBN), "http://127.0.0.1")

			listing, err := test.scraper.Scrape(test.book, test.scraper, test.parseBookMeta, lg)
			assert.Equal(t, test.expListing, listing)

			if expURLError {
				assert.IsType(t, &url.Error{}, err)
			} else {
				assert.Equal(t, test.err, err)
			}
		})
	}
}
