package adlibris

import (
	"bliss"
	it "bliss/internal/testing/sites"
	"bliss/logger"
	"testing"

	"go.uber.org/zap"
)

const (
	sek = "SEK"
)

func TestAdlibris(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []it.SiteTestData{
		{
			Desc: "adlibris.com coming item",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.HC,
			ExpPrice:    bliss.NotFound,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/adlibris_coming"},
			},
		},
		{
			Desc: "adlibris.com in stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.HC,
			ExpPrice:    169,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/adlibris_in_stock_hc"},
			},
		},
		{
			Desc: "adlibris.com in stock softcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.SC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.SC,
			ExpPrice:    169,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/adlibris_in_stock_sc"},
			},
		},
		{
			Desc: "adlibris.com in stock unknown format",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.NA,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.NA,
			ExpPrice:    169,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/adlibris_in_stock_na"},
			},
		},
		{
			Desc: "adlibris.com out of stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.HC,
			ExpPrice:    bliss.NotFound,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/adlibris_no_stock"},
			},
		},
	}

	it.TestSite(t, tt, lg)
}
