package adlibris

import (
	"bliss"
	"bliss/sites"
	"bliss/utils"
	"regexp"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"go.uber.org/zap"
)

type (
	adlibrisScraper struct{}
)

var (
	adlibris bliss.Scraper = &adlibrisScraper{}
)

func Scraper() bliss.Scraper {
	return adlibris
}

func (adlibris *adlibrisScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, adlibris, parseBookMeta, lg)
}

func (adlibris *adlibrisScraper) Site() string {
	return "adlibris.com"
}

func (adlibris *adlibrisScraper) SearchURL(isbn string) string {
	return "https://www.adlibris.com/se/sok?q=" + isbn
}

func (adlibris *adlibrisScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	switch strings.ToLower(strings.TrimSpace(dom.Find("span.format").First().Text())) {
	case "inbunden":
		return bliss.HC
	case "häftad", "pocket", "storpocket":
		return bliss.SC
	default:
		return bliss.NA
	}
}

func (adlibris *adlibrisScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	s := strings.ToLower(dom.Find(".processing-time").First().Text())
	s = strings.TrimSpace(s)
	currency = "SEK"

	if !regexp.MustCompile(`slut|ej utkommen|bevaka`).MatchString(s) {
		element := dom.Find(".price.sek").Children().First()
		span := strings.TrimSpace(element.Text())
		priceStr := strings.Split(span, " ")[0]
		price = utils.PriceToFloat(currency, priceStr, lg)
		return currency, price
	}
	return currency, bliss.NotFound
}

func (adlibris *adlibrisScraper) ParseTitle(dom *goquery.Selection) string {
	return strings.TrimSpace(dom.Find(".search-result__product__name").First().Text())
}
