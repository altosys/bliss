package amazon

import (
	"bliss"
	"bliss/utils"
	"regexp"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/lithammer/fuzzysearch/fuzzy"
	"go.uber.org/zap"
)

func parseFormat(dom *goquery.Selection) bliss.Format {
	// do not use amazon as format source
	return bliss.NA
}

func parsePrice(book bliss.Book, dom *goquery.Selection, site string, lg *zap.SugaredLogger) (currency string, price float32) {
	type match struct {
		rank   int
		format string
		price  string
	}

	isFormatMatch := func(format, site string, book bliss.Book) bool {
		formats := []string{"unknown", "paperback", "hardcover"}

		switch site {
		case "amazon.de":
			formats = []string{"unknown", "taschenbuch", "gebundenes"}
		case "amazon.es":
			formats = []string{"unknown", "tapa blanda", "tapa dura"}
		case "amazon.fr":
			formats = []string{"unknown", "broché", "relié"}
		case "amazon.it":
			formats = []string{"unknown", "flessibile", "rigida"}
		case "amazon.se":
			formats = []string{"unknown", "häftad|pocketbok", "hårt omslag|inbunden"}
		}

		return regexp.MustCompile("(?i)" + formats[book.Format]).MatchString(format)
	}

	parseItemOption := func(s *goquery.Selection) (format, price string) {
		format = strings.TrimSpace(s.Find(".a-size-base.a-link-normal.a-text-bold").First().Text())
		price = strings.TrimSpace(s.Find(".a-offscreen").First().Text())
		return format, price
	}

	m := match{rank: -1}
	results := dom.Find("div[data-component-type=s-search-result]")
	results.EachWithBreak(func(i int, r *goquery.Selection) bool {
		var format, price, title string
		var rank int
		var found bool

		title = strings.TrimSpace(r.Find(".a-color-base.a-text-normal").First().Text())
		rank = fuzzy.RankMatchNormalizedFold(book.Title, title)

		// handle search results with multiple format options
		selections := []*goquery.Selection{}
		if r.Find(".a-row.a-spacing-mini").Length() != 0 {
			selections = append(selections, r.Find(".a-row.a-spacing-mini"))
		}
		if r.Find(".a-section.a-spacing-none.a-spacing-top-small").Length() != 0 {
			selections = append(selections, r.Find(".a-section.a-spacing-none.a-spacing-top-small"))
		}
		for _, sel := range selections {
			if sel.Length() > 1 {
				sel.EachWithBreak(func(j int, s *goquery.Selection) bool {
					format, price = parseItemOption(s)
					found = isFormatMatch(format, site, book)
					return !found
				})
				if found {
					break
				}
			}
			format, price = parseItemOption(sel)
			found = isFormatMatch(format, site, book)
			if found {
				break
			}
		}

		if !isFormatMatch(format, site, book) {
			return false
		}

		if isFormatMatch(format, site, book) && (results.Length() == 1 || rank != -1) {
			m.format = format
			m.price = price
			m.rank = rank
		}
		return true
	})

	switch site {
	case "amazon.ae":
		currency = "AED"
	case "amazon.ca":
		currency = "CAD"
	case "amazon.co.uk":
		currency = "GBP"
	case "amazon.com":
		currency = "USD"
	case "amazon.com.au":
		currency = "AUD"
	case "amazon.in":
		currency = "INR"
	case "amazon.se":
		currency = "SEK"
	default:
		currency = "EUR"
	}

	return currency, utils.PriceToFloat(currency, m.price, lg)
}

func parseTitle(dom *goquery.Selection) string {
	return dom.Find("div[data-component-type=s-search-result]").First().Find("span.a-size-medium.a-color-base.a-text-normal").First().Text()
}

func parseAltTitle(dom *goquery.Selection) string {
	results := dom.Find("div[data-component-type=s-search-result]")
	if results.Length() > 1 {
		return ""
	}
	return dom.Find(".s-result-list.s-search-results.sg-row").Find("span.a-size-base-plus.a-color-base.a-text-normal").First().Text()
}
