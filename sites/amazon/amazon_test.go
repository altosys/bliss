package amazon

import (
	"bliss"
	it "bliss/internal/testing/sites"
	"bliss/logger"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

const (
	aed = "AED"
	aud = "AUD"
	cad = "CAD"
	eur = "EUR"
	gbp = "GBP"
	inr = "INR"
	sek = "SEK"
	usd = "USD"
)

func TestAmazon(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []it.SiteTestData{
		{
			Desc: "amazon.ae item not found",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(".ae"),
			ExpCurrency: aed,
			ExpFormat:   bliss.NA,
			ExpPrice:    bliss.NotFound,
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_not_found"},
			},
		},
		{
			Desc: "amazon.ae in stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(".ae"),
			ExpCurrency: aed,
			ExpFormat:   bliss.NA,
			ExpPrice:    53,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_ae_hc"},
			},
		},
		{
			Desc: "amazon.ca in stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(".ca"),
			ExpCurrency: cad,
			ExpFormat:   bliss.NA,
			ExpPrice:    29.32,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_ca_hc"},
			},
		},
		{
			Desc: "amazon.com in stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(".com"),
			ExpCurrency: usd,
			ExpFormat:   bliss.NA,
			ExpPrice:    21.49,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_com_hc"},
			},
		},
		{
			Desc: "amazon.com.au in stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(".com.au"),
			ExpCurrency: aud,
			ExpFormat:   bliss.NA,
			ExpPrice:    30.69,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_com_au_hc"},
			},
		},
		{
			Desc: "amazon.co.uk in stock multiple formats hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein: Or, the Modern Prometheus (Penguin Clothbound Classics)",
				Format: bliss.HC,
			},
			Scraper:     Scraper(".co.uk"),
			ExpCurrency: gbp,
			ExpFormat:   bliss.NA,
			ExpPrice:    14.99,
			ExpTitle:    "Frankenstein: Or, the Modern Prometheus (Penguin Clothbound Classics)",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_co_uk_multi"},
			},
		},
		{
			Desc: "amazon.co.uk in stock multiple formats softcover",
			Book: bliss.Book{
				Title:  "Frankenstein: Or, the Modern Prometheus (Penguin Clothbound Classics)",
				Format: bliss.SC,
			},
			Scraper:     Scraper(".co.uk"),
			ExpCurrency: gbp,
			ExpFormat:   bliss.NA,
			ExpPrice:    2.25,
			ExpTitle:    "Frankenstein: Or, the Modern Prometheus (Penguin Clothbound Classics)",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_co_uk_multi"},
			},
		},
		{
			Desc: "amazon.de in stock multiple formats hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein: Or, the Modern Prometheus (Penguin Clothbound Classics)",
				Format: bliss.HC,
			},
			Scraper:     Scraper(".de"),
			ExpCurrency: eur,
			ExpFormat:   bliss.NA,
			ExpPrice:    15.89,
			ExpTitle:    "Frankenstein: Or, the Modern Prometheus (Penguin Clothbound Classics)",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_de_multi"},
			},
		},
		{
			Desc: "amazon.de in stock multiple formats softcover",
			Book: bliss.Book{
				Title:  "Frankenstein: Or, the Modern Prometheus (Penguin Clothbound Classics)",
				Format: bliss.SC,
			},
			Scraper:     Scraper(".de"),
			ExpCurrency: eur,
			ExpFormat:   bliss.NA,
			ExpPrice:    3.33,
			ExpTitle:    "Frankenstein: Or, the Modern Prometheus (Penguin Clothbound Classics)",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_de_multi"},
			},
		},
		{
			Desc: "amazon.es in stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein: Or, the Modern Prometheus (Penguin Clothbound Classics)",
				Format: bliss.HC,
			},
			Scraper:     Scraper(".es"),
			ExpCurrency: eur,
			ExpFormat:   bliss.NA,
			ExpPrice:    17.75,
			ExpTitle:    "Frankenstein: Or, the Modern Prometheus (Penguin Clothbound Classics)",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_es_hc"},
			},
		},
		{
			Desc: "amazon.es in stock softcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.SC,
			},
			Scraper:     Scraper(".es"),
			ExpCurrency: eur,
			ExpFormat:   bliss.NA,
			ExpPrice:    3.19,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_es_sc"},
			},
		},
		{
			Desc: "amazon.fr in stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(".fr"),
			ExpCurrency: eur,
			ExpFormat:   bliss.NA,
			ExpPrice:    18.00,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_fr_hc"},
			},
		},
		{
			Desc: "amazon.fr in stock softcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.SC,
			},
			Scraper:     Scraper(".fr"),
			ExpCurrency: eur,
			ExpFormat:   bliss.NA,
			ExpPrice:    34.95,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_fr_sc"},
			},
		},
		{
			Desc: "amazon.in in stock hardcover",
			Book: bliss.Book{
				Title:  "Dracula (Collectors Classics)",
				Format: bliss.HC,
			},
			Scraper:     Scraper(".in"),
			ExpCurrency: inr,
			ExpFormat:   bliss.NA,
			ExpPrice:    3450,
			ExpTitle:    "Dracula (Collectors Classics)",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_in_hc"},
			},
		},
		{
			Desc: "amazon.in in stock hardcover no price",
			Book: bliss.Book{
				Title:  "Frankenstein (Penguin Clothbound Classics)",
				Format: bliss.HC,
			},
			Scraper:     Scraper(".in"),
			ExpCurrency: inr,
			ExpFormat:   bliss.NA,
			ExpPrice:    bliss.NotFound,
			ExpTitle:    "Frankenstein (Penguin Clothbound Classics)",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_in_hc_no_price"},
			},
		},
		{
			Desc: "amazon.in in stock softcover",
			Book: bliss.Book{
				Title:  "Frankenstein (Dover Thrift Editions)",
				Format: bliss.SC,
			},
			Scraper:     Scraper(".in"),
			ExpCurrency: inr,
			ExpFormat:   bliss.NA,
			ExpPrice:    153.88,
			ExpTitle:    "Frankenstein (Dover Thrift Editions)",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_in_sc"},
			},
		},
		{
			Desc: "amazon.it in stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein: Or, the Modern Prometheus",
				Format: bliss.HC,
			},
			Scraper:     Scraper(".it"),
			ExpCurrency: eur,
			ExpFormat:   bliss.NA,
			ExpPrice:    16.39,
			ExpTitle:    "Frankenstein: Or, the Modern Prometheus",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_it_hc"},
			},
		},
		{
			Desc: "amazon.it in stock softcover",
			Book: bliss.Book{
				Title:  "Frankenstein ou le Prométhée moderne",
				Format: bliss.SC,
			},
			Scraper:     Scraper(".it"),
			ExpCurrency: eur,
			ExpFormat:   bliss.NA,
			ExpPrice:    34.95,
			ExpTitle:    "Frankenstein ou le Prométhée moderne",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_it_sc"},
			},
		},
		{
			Desc: "amazon.nl in stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein: Or, the Modern Prometheus HC",
				Format: bliss.HC,
			},
			Scraper:     Scraper(".nl"),
			ExpCurrency: eur,
			ExpFormat:   bliss.NA,
			ExpPrice:    12.80,
			ExpTitle:    "Frankenstein: Or, the Modern Prometheus HC",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_nl_hc"},
			},
		},
		{
			Desc: "amazon.se in stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(".se"),
			ExpCurrency: sek,
			ExpFormat:   bliss.NA,
			ExpPrice:    150.41,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_se_hc"},
			},
		},
		{
			Desc: "amazon.se in stock softcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.SC,
			},
			Scraper:     Scraper(".se"),
			ExpCurrency: sek,
			ExpFormat:   bliss.NA,
			ExpPrice:    50.65,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_se_sc"},
			},
		},
		{
			Desc: "amazon.se item not found",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(".se"),
			ExpCurrency: sek,
			ExpFormat:   bliss.NA,
			ExpPrice:    bliss.NotFound,
			ExpTitle:    "",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_se_not_found"},
			},
		},
		{
			Desc: "amazon.se ignore promoted item",
			Book: bliss.Book{
				Title:  "Fearsome Doctor Fang",
				Format: bliss.SC,
			},
			Scraper:     Scraper(".se"),
			ExpCurrency: sek,
			ExpFormat:   bliss.NA,
			ExpPrice:    180.82,
			ExpTitle:    "",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/amazon_se_promoted_item"},
			},
		},
	}

	assert.Nil(t, Scraper(".none"))
	it.TestSite(t, tt, lg)
}
