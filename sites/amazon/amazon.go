package amazon

import (
	"bliss"
	"bliss/sites"

	"github.com/PuerkitoBio/goquery"
	"go.uber.org/zap"
)

const (
	amazonSeDomain = "amazon.se"
	amazonInDomain = "amazon.in"
)

type (
	amazonAeScraper    struct{}
	amazonCaScraper    struct{}
	amazonComScraper   struct{}
	amazonComAuScraper struct{}
	amazonCoUkScraper  struct{}
	amazonDeScraper    struct{}
	amazonEsScraper    struct{}
	amazonFrScraper    struct{}
	amazonInScraper    struct{}
	amazonItScraper    struct{}
	amazonNlScraper    struct{}
	amazonSeScraper    struct{}
)

var (
	amazonAe    bliss.Scraper = &amazonAeScraper{}
	amazonCa    bliss.Scraper = &amazonCaScraper{}
	amazonCom   bliss.Scraper = &amazonComScraper{}
	amazonComAu bliss.Scraper = &amazonComAuScraper{}
	amazonCoUk  bliss.Scraper = &amazonCoUkScraper{}
	amazonDe    bliss.Scraper = &amazonDeScraper{}
	amazonEs    bliss.Scraper = &amazonEsScraper{}
	amazonFr    bliss.Scraper = &amazonFrScraper{}
	amazonIn    bliss.Scraper = &amazonInScraper{}
	amazonIt    bliss.Scraper = &amazonItScraper{}
	amazonNl    bliss.Scraper = &amazonNlScraper{}
	amazonSe    bliss.Scraper = &amazonSeScraper{}
)

func Scraper(domain string) bliss.Scraper {
	switch domain {
	case ".ae":
		return amazonAe
	case ".ca":
		return amazonCa
	case ".com":
		return amazonCom
	case ".com.au":
		return amazonComAu
	case ".co.uk":
		return amazonCoUk
	case ".de":
		return amazonDe
	case ".es":
		return amazonEs
	case ".fr":
		return amazonFr
	case ".in":
		return amazonIn
	case ".it":
		return amazonIt
	case ".nl":
		return amazonNl
	case ".se":
		return amazonSe
	default:
		return nil
	}
}

func (amazonAe *amazonAeScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, amazonAe, parseBookMeta, lg)
}

func (amazonAe *amazonAeScraper) Site() string {
	return "amazon.ae"
}

func (amazonAe *amazonAeScraper) SearchURL(isbn string) string {
	return "https://www.amazon.ae/s?k=" + isbn
}

func (amazonAe *amazonAeScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	return parseFormat(dom)
}

func (amazonAe *amazonAeScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	return parsePrice(book, dom, "amazon.ae", lg)
}

func (amazonAe *amazonAeScraper) ParseTitle(dom *goquery.Selection) string {
	return parseTitle(dom)
}

func (amazonCa *amazonCaScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, amazonCa, parseBookMeta, lg)
}

func (amazonCa *amazonCaScraper) Site() string {
	return "amazon.ca"
}

func (amazonCa *amazonCaScraper) SearchURL(isbn string) string {
	return "https://www.amazon.ca/s?k=" + isbn
}

func (amazonCa *amazonCaScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	return parseFormat(dom)
}

func (amazonCa *amazonCaScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	return parsePrice(book, dom, "amazon.ca", lg)
}

func (amazonCa *amazonCaScraper) ParseTitle(dom *goquery.Selection) string {
	return parseAltTitle(dom)
}

func (amazonCom *amazonComScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, amazonCom, parseBookMeta, lg)
}

func (amazonCom *amazonComScraper) Site() string {
	return "amazon.com"
}

func (amazonCom *amazonComScraper) SearchURL(isbn string) string {
	return "https://www.amazon.com/s?k=" + isbn
}

func (amazonCom *amazonComScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	return parseFormat(dom)
}

func (amazonCom *amazonComScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	return parsePrice(book, dom, "amazon.com", lg)
}

func (amazonCom *amazonComScraper) ParseTitle(dom *goquery.Selection) string {
	return parseTitle(dom)
}

func (amazonComAu *amazonComAuScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, amazonComAu, parseBookMeta, lg)
}

func (amazonComAu *amazonComAuScraper) Site() string {
	return "amazon.com.au"
}

func (amazonComAu *amazonComAuScraper) SearchURL(isbn string) string {
	return "https://www.amazon.com.au/s?k=" + isbn
}

func (amazonComAu *amazonComAuScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	return parseFormat(dom)
}

func (amazonComAu *amazonComAuScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	return parsePrice(book, dom, "amazon.com.au", lg)
}

func (amazonComAu *amazonComAuScraper) ParseTitle(dom *goquery.Selection) string {
	return parseAltTitle(dom)
}

func (amazonCoUk *amazonCoUkScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, amazonCoUk, parseBookMeta, lg)
}

func (amazonCoUk *amazonCoUkScraper) Site() string {
	return "amazon.co.uk"
}

func (amazonCoUk *amazonCoUkScraper) SearchURL(isbn string) string {
	return "https://www.amazon.co.uk/s?k=" + isbn
}

func (amazonCoUk *amazonCoUkScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	return parseFormat(dom)
}

func (amazonCoUk *amazonCoUkScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	return parsePrice(book, dom, "amazon.co.uk", lg)
}

func (amazonCoUk *amazonCoUkScraper) ParseTitle(dom *goquery.Selection) string {
	return parseTitle(dom)
}

func (amazonDe *amazonDeScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, amazonDe, parseBookMeta, lg)
}

func (amazonDe *amazonDeScraper) Site() string {
	return "amazon.de"
}

func (amazonDe *amazonDeScraper) SearchURL(isbn string) string {
	return "https://www.amazon.de/s?k=" + isbn
}

func (amazonDe *amazonDeScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	return parseFormat(dom)
}

func (amazonDe *amazonDeScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	return parsePrice(book, dom, "amazon.de", lg)
}

func (amazonDe *amazonDeScraper) ParseTitle(dom *goquery.Selection) string {
	return parseTitle(dom)
}

func (amazonEs *amazonEsScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, amazonEs, parseBookMeta, lg)
}

func (amazonEs *amazonEsScraper) Site() string {
	return "amazon.es"
}

func (amazonEs *amazonEsScraper) SearchURL(isbn string) string {
	return "https://www.amazon.es/s?k=" + isbn
}

func (amazonEs *amazonEsScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	return parseFormat(dom)
}

func (amazonEs *amazonEsScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	return parsePrice(book, dom, "amazon.es", lg)
}

func (amazonEs *amazonEsScraper) ParseTitle(dom *goquery.Selection) string {
	return parseAltTitle(dom)
}

func (amazonFr *amazonFrScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, amazonFr, parseBookMeta, lg)
}

func (amazonFr *amazonFrScraper) Site() string {
	return "amazon.fr"
}

func (amazonFr *amazonFrScraper) SearchURL(isbn string) string {
	return "https://www.amazon.fr/s?k=" + isbn
}

func (amazonFr *amazonFrScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	return parseFormat(dom)
}

func (amazonFr *amazonFrScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	return parsePrice(book, dom, "amazon.fr", lg)
}

func (amazonFr *amazonFrScraper) ParseTitle(dom *goquery.Selection) string {
	return parseAltTitle(dom)
}

func (amazonIn *amazonInScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, amazonIn, parseBookMeta, lg)
}

func (amazonIn *amazonInScraper) Site() string {
	return amazonInDomain
}

func (amazonIn *amazonInScraper) SearchURL(isbn string) string {
	return "https://www.amazon.in/s?k=" + isbn
}

func (amazonIn *amazonInScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	return parseFormat(dom)
}

func (amazonIn *amazonInScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	return parsePrice(book, dom, amazonInDomain, lg)
}

func (amazonIn *amazonInScraper) ParseTitle(dom *goquery.Selection) string {
	return parseTitle(dom)
}

func (amazonIt *amazonItScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, amazonIt, parseBookMeta, lg)
}

func (amazonIt *amazonItScraper) Site() string {
	return "amazon.it"
}

func (amazonIt *amazonItScraper) SearchURL(isbn string) string {
	return "https://www.amazon.it/s?k=" + isbn
}

func (amazonIt *amazonItScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	return parseFormat(dom)
}

func (amazonIt *amazonItScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	return parsePrice(book, dom, "amazon.it", lg)
}

func (amazonIt *amazonItScraper) ParseTitle(dom *goquery.Selection) string {
	return parseAltTitle(dom)
}

func (amazonNl *amazonNlScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, amazonNl, parseBookMeta, lg)
}

func (amazonNl *amazonNlScraper) Site() string {
	return "amazon.nl"
}

func (amazonNl *amazonNlScraper) SearchURL(isbn string) string {
	return "https://www.amazon.nl/s?k=" + isbn
}

func (amazonNl *amazonNlScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	return parseFormat(dom)
}

func (amazonNl *amazonNlScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	return parsePrice(book, dom, "amazon.nl", lg)
}

func (amazonNl *amazonNlScraper) ParseTitle(dom *goquery.Selection) string {
	return parseTitle(dom)
}

func (amazonSe *amazonSeScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, amazonSe, parseBookMeta, lg)
}

func (amazonSe *amazonSeScraper) Site() string {
	return amazonSeDomain
}

func (amazonSe *amazonSeScraper) SearchURL(isbn string) string {
	return "https://www.amazon.se/s?k=" + isbn
}

func (amazonSe *amazonSeScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	return parseFormat(dom)
}

func (amazonSe *amazonSeScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	return parsePrice(book, dom, amazonSeDomain, lg)
}

func (amazonSe *amazonSeScraper) ParseTitle(dom *goquery.Selection) string {
	return parseAltTitle(dom)
}
