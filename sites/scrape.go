package sites

import (
	"bliss"
	b "bliss/browser"
	"fmt"
	"regexp"
	"strings"

	"go.uber.org/zap"
)

func Scrape(book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	listing := bliss.Listing{
		Book:  book,
		Site:  s.Site(),
		Price: bliss.NotFound,
	}

	dom, err := b.GetDOM(s.SearchURL(book.ISBN))
	if err != nil {
		return listing, err
	}

	if dom.Find("html").First().Text() == "" {
		identifier := book.ISBN
		if book.Title != "" {
			identifier = book.Title
		}
		return listing, fmt.Errorf("failed to fetch DOM for '%s' at %s", identifier, s.Site())
	}

	currency, price := s.ParsePrice(dom, book, lg)
	listing.Currency = currency
	listing.Price = price

	if parseBookMeta {
		if listing.Book.Title == "" {
			if title := strings.TrimSpace(s.ParseTitle(dom)); title == "" {
				lg.Debugf("no title found for %s from %s", book.ISBN, s.Site())
			} else {
				title = regexp.MustCompile("[\"']+").ReplaceAllString(title, "")
				lg.Debugf("found title '%s' for %s from %s", title, book.ISBN, s.Site())
				listing.Book.Title = title
			}
		}

		if listing.Book.Format == bliss.NA {
			if format := s.ParseFormat(dom); format == bliss.NA {
				lg.Debugf("no format found for %s from %s", book.ISBN, s.Site())
			} else {
				lg.Debugf("found format '%s' for %s from %s", format, book.ISBN, s.Site())
				listing.Book.Format = format
			}
		}
	}
	return listing, nil
}
