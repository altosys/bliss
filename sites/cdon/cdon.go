package cdon

import (
	"bliss"
	"bliss/sites"
	"bliss/utils"

	"strings"

	"github.com/PuerkitoBio/goquery"
	"go.uber.org/zap"
)

type (
	cdonScraper struct{}
)

var (
	cdon bliss.Scraper = &cdonScraper{}
)

func Scraper() bliss.Scraper {
	return cdon
}

func (cdon *cdonScraper) Scrape(book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, cdon, parseBookMeta, lg)
}

func (cdon *cdonScraper) Site() string {
	return "cdon.se"
}

func (cdon *cdonScraper) SearchURL(isbn string) string {
	return "https://cdon.se/catalog/search?q=" + isbn
}

func (cdon *cdonScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	// do not use cdon as format source
	return bliss.NA
}

func (cdon *cdonScraper) ParsePrice(dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	s := dom.Find(".search-results").Find(".product-list.medium.column-count-4.hoverable").Find(".price").First()
	priceStr := strings.TrimSpace(s.Contents().Text())
	return "SEK", utils.PriceToFloat("SEK", priceStr, lg)
}

func (cdon *cdonScraper) ParseTitle(dom *goquery.Selection) string {
	return dom.Find(".product.product-buy").Find("h3.title").First().Text()
}
