package bookswagon

import (
	"bliss"
	it "bliss/internal/testing/sites"
	"bliss/logger"
	"testing"

	"go.uber.org/zap"
)

func TestBooksWagon(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []it.SiteTestData{
		{
			Desc: "bookswagon.com in stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.NA,
			},
			Scraper:     Scraper(),
			ExpCurrency: "INR",
			ExpFormat:   bliss.NA,
			ExpPrice:    1240,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/bookswagon_hc"},
			},
		},
		{
			Desc: "bookswagon.com coming item",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.NA,
			},
			Scraper:     Scraper(),
			ExpCurrency: "INR",
			ExpFormat:   bliss.NA,
			ExpPrice:    6188,
			ExpTitle:    "Locke & Key: Keyhouse Compendium",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/bookswagon_coming"},
			},
		},
		{
			Desc: "bookswagon.com item not found",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.NA,
			},
			Scraper:     Scraper(),
			ExpCurrency: "INR",
			ExpFormat:   bliss.NA,
			ExpPrice:    bliss.NotFound,
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/bookswagon_not_found"},
			},
		},
		{
			Desc: "bookswagon.com out of stock softcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.SC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "INR",
			ExpFormat:   bliss.NA,
			ExpPrice:    bliss.NotFound,
			ExpTitle:    "Alan Moore's Neonomicon",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/bookswagon_no_stock"},
			},
		},
		{
			Desc: "bookswagon.com redirect to freeform.com",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.SC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "INR",
			ExpFormat:   bliss.NA,
			ExpPrice:    bliss.NotFound,
			ExpTitle:    "",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/bookswagon_freeform"},
			},
		},
	}

	it.TestSite(t, tt, lg)
}
