package bookswagon

import (
	"bliss"
	"bliss/sites"
	"bliss/utils"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"go.uber.org/zap"
)

type (
	bookswagonScraper struct{}
)

var (
	bookswagon bliss.Scraper = &bookswagonScraper{}
)

func Scraper() bliss.Scraper {
	return bookswagon
}

func (bookswagon *bookswagonScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, bookswagon, parseBookMeta, lg)
}

func (bookswagon *bookswagonScraper) Site() string {
	return "bookswagon.com"
}

func (bookswagon *bookswagonScraper) SearchURL(isbn string) string {
	return "https://www.bookswagon.com/search-books/" + isbn
}

func (bookswagon *bookswagonScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	// do not use bookswagon as format source
	return bliss.NA
}

func (bookswagon *bookswagonScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	currency = "INR"
	price = bliss.NotFound

	// handle bookswagon.com redirect to freeform.com
	var isBooksWagon bool
	dom.Find("meta").Each(func(i int, s *goquery.Selection) {
		if property, _ := s.Attr("property"); property == "og:url" {
			url, _ := s.Attr("content")
			if strings.Contains(url, "bookswagon.com") {
				isBooksWagon = true
			}
		}
	})

	if !isBooksWagon {
		return
	}

	if dom.Find("div[id=ctl00_phBody_ProductDetail_lblAvailable]").HasClass("out-of-stock") {
		return
	}

	priceStr := dom.Find("label[id=ctl00_phBody_ProductDetail_lblourPrice]").First().Text()
	priceStr = strings.ReplaceAll(priceStr, "Rs.", "")
	price = utils.PriceToFloat(currency, priceStr, lg)
	return currency, price
}

func (bookswagon *bookswagonScraper) ParseTitle(dom *goquery.Selection) string {
	return strings.TrimSpace(dom.Find("#ctl00_phBody_ProductDetail_lblTitle").First().Text())
}
