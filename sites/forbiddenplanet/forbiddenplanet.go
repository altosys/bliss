package forbiddenplanet

import (
	"bliss"
	"bliss/sites"
	"bliss/utils"

	"strings"

	"github.com/PuerkitoBio/goquery"
	"go.uber.org/zap"
)

type (
	forbiddenplanetScraper struct{}
)

var (
	forbiddenplanet bliss.Scraper = &forbiddenplanetScraper{}
)

func Scraper() bliss.Scraper {
	return forbiddenplanet
}

func (forbiddenplanet *forbiddenplanetScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, forbiddenplanet, parseBookMeta, lg)
}

func (forbiddenplanet *forbiddenplanetScraper) Site() string {
	return "forbiddenplanet.com"
}

func (forbiddenplanet *forbiddenplanetScraper) SearchURL(isbn string) string {
	return "https://forbiddenplanet.com/catalog/?q=" + isbn
}

func (forbiddenplanet *forbiddenplanetScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	// do not use forbiddenplanet as format source
	return bliss.NA
}

func (forbiddenplanet *forbiddenplanetScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	currency = "GBP"
	price = bliss.NotFound
	status := strings.ToLower(dom.Find("button.brad.button--brand.button--lg.one-whole").First().Text())
	if !strings.Contains(status, "add to basket") {
		return
	}

	priceFull := dom.Find(".clr-price").First().Text()
	priceDecimals := dom.Find(".t-small.clr-price").First().Text()
	return currency, utils.PriceToFloat(currency, priceFull+priceDecimals, lg)
}

func (forbiddenplanet *forbiddenplanetScraper) ParseTitle(dom *goquery.Selection) string {
	var title string
	dom.Find("h3.h4.clr-black.mqt.ord--02.one-whole.txt-left.dtb--fg.owl-off").Each(func(i int, s *goquery.Selection) {
		title = s.Nodes[0].FirstChild.Data
	})
	return title
}
