package forbiddenplanet

import (
	"bliss"
	it "bliss/internal/testing/sites"
	"bliss/logger"
	"testing"

	"go.uber.org/zap"
)

func TestForbiddenPlanet(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []it.SiteTestData{
		{
			Desc: "forbiddenplanet.com coming item",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "GBP",
			ExpFormat:   bliss.NA,
			ExpPrice:    18.62,
			ExpTitle:    "Bernie Wrightson's Frankenstein (Hardcover)",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/forbiddenplanet_coming"},
			},
		},
		{
			Desc: "forbiddenplanet.com in stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "GBP",
			ExpFormat:   bliss.NA,
			ExpPrice:    28.97,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/forbiddenplanet_in_stock"},
			},
		},
		{
			Desc: "forbiddenplanet.com out of stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: "GBP",
			ExpFormat:   bliss.NA,
			ExpPrice:    bliss.NotFound,
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/forbiddenplanet_no_stock"},
			},
		},
	}

	it.TestSite(t, tt, lg)
}
