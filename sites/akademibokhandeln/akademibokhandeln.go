package akademibokhandeln

import (
	"bliss"
	b "bliss/browser"
	"bliss/sites"
	"bliss/utils"

	"strings"

	"github.com/PuerkitoBio/goquery"
	"go.uber.org/zap"
)

type (
	akademibokhandelnScraper struct{}
)

var (
	akademibokhandeln bliss.Scraper = &akademibokhandelnScraper{}
)

func Scraper() bliss.Scraper {
	return akademibokhandeln
}

func (akademibokhandeln *akademibokhandelnScraper) Scrape(
	book bliss.Book, s bliss.Scraper, parseBookMeta bool, lg *zap.SugaredLogger) (bliss.Listing, error) {
	return sites.Scrape(book, akademibokhandeln, parseBookMeta, lg)
}

func (akademibokhandeln *akademibokhandelnScraper) Site() string {
	return "akademibokhandeln.se"
}

func (akademibokhandeln *akademibokhandelnScraper) SearchURL(isbn string) string {
	return "https://www.akademibokhandeln.se/sok/?sokfraga=" + isbn
}

func (akademibokhandeln *akademibokhandelnScraper) ParseFormat(dom *goquery.Selection) bliss.Format {
	// do not use akademibokhandeln as format source
	return bliss.NA
}

func (akademibokhandeln *akademibokhandelnScraper) ParsePrice(
	dom *goquery.Selection, book bliss.Book, lg *zap.SugaredLogger) (currency string, price float32) {
	var isPreorder bool
	currency = "SEK"
	price = bliss.NotFound

	//nolint:misspell
	if dom.Find(".product-grid-item__comming-badge").Length() != 0 {
		isPreorder = true // return price for books to be released
	}

	var priceStr string
	switch isPreorder {
	case true:
		priceStr = dom.Find(".product-grid-item__price__large").First().Text()
		price = utils.PriceToFloat(currency, priceStr, lg)
	case false:
		if link, exists := dom.Find(".product-grid-item__link").First().Attr("href"); exists {
			productPageDOM, err := b.GetDOM(link)
			if err != nil {
				return
			}

			status := productPageDOM.Find(".btn.btn--outline.btn--centered.btn--block").Children().First().Text()
			if !strings.Contains(strings.ToLower(status), "slut") {
				priceStr = productPageDOM.Find(".product-price__large").First().Text()
				price = utils.PriceToFloat(currency, priceStr, lg)
			}
		}
	}

	if priceStr != "" {
		priceStr = strings.ReplaceAll(priceStr, ":-", "")
		price = utils.PriceToFloat(currency, priceStr, lg)
	}
	return currency, price
}

func (akademibokhandeln *akademibokhandelnScraper) ParseTitle(dom *goquery.Selection) string {
	return dom.Find("h2.product-grid-item__title").First().Text()
}
