package akademibokhandeln

import (
	"bliss"
	it "bliss/internal/testing/sites"
	"bliss/logger"
	"testing"

	"go.uber.org/zap"
)

const (
	sek = "SEK"
)

func TestAkademibokhandeln(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []it.SiteTestData{
		{
			Desc: "akademibokhandeln.se coming item",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.NA,
			ExpPrice:    289,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {Doc: "../../internal/testing/sites/testdata/akademibokhandeln_search_coming"},
			},
		},
		{
			Desc: "akademibokhandeln.se in stock",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.NA,
			ExpPrice:    219,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {
					Doc:  "../../internal/testing/sites/testdata/akademibokhandeln_search",
					Link: "/akademibokhandeln_prod_in_stock",
				},
				"/akademibokhandeln_prod_in_stock": {
					Doc: "../../internal/testing/sites/testdata/akademibokhandeln_prod_in_stock",
				},
			},
		},
		{
			Desc: "akademibokhandeln.se out of stock hardcover",
			Book: bliss.Book{
				Title:  "Frankenstein",
				Format: bliss.HC,
			},
			Scraper:     Scraper(),
			ExpCurrency: sek,
			ExpFormat:   bliss.NA,
			ExpPrice:    bliss.NotFound,
			ExpTitle:    "Frankenstein",
			HTTPMocks: map[string]it.HTTPMock{
				"/": {
					Doc:  "../../internal/testing/sites/testdata/akademibokhandeln_search",
					Link: "/akademibokhandeln_prod_no_stock",
				},
				"/akademibokhandeln_prod_no_stock": {
					Doc: "../../internal/testing/sites/testdata/akademibokhandeln_prod_no_stock",
				},
			},
		},
	}

	it.TestSite(t, tt, lg)
}
