---
env:
  - GO111MODULE=on
  - GOPROXY=https://goproxy.io,direct

before:
  hooks:

archives:
  - name_template: '{{ .ProjectName }}_{{ .Version }}_{{ .Os }}_{{ .Arch }}{{ if .Arm }}v{{ .Arm }}{{ end }}'
    replacements:
      darwin: darwin
      linux: linux
      windows: windows
      386: i386
      amd64: x86_64
    format_overrides:
      - goos: windows
        format: zip

builds:
  - env:
      - CGO_ENABLED=0
    ldflags:
      - "-w -s \
        -X '{{ .ProjectName }}.Builder={{ .Env.BUILDER }}' \
        -X '{{ .ProjectName }}.Commit={{ .Commit }}' \
        -X '{{ .ProjectName }}.Date={{ .Date }}' \
        -X '{{ .ProjectName }}.Version={{ .Version }}'"
    goos:
      - linux
      - darwin
      - windows
    goarch:
      - 386
      - amd64
      - arm64
    hooks:
      pre:
      post: upx {{ .Path }} --no-env --lzma
    ignore:
      - goos: darwin
        goarch: 386
      - goos: windows
        goarch: arm64
    main: ./cmd/{{ .ProjectName }}/main.go

changelog:
  skip: false
  use: git
  groups:
    - title: Features
      regexp: "^.*feat[(\\w)]*:+.*$"
      order: 0
    - title: 'Bug fixes'
      regexp: "^.*fix[(\\w)]*:+.*$"
      order: 1
    - title: Other
      order: 999
  filters:
    exclude:
      - '^build'
      - '^chore'
      - '^ci'
      - '^docs'
      - '^refactor'
      - '^style'
      - '^test'

checksum:
  name_template: '{{ .ProjectName }}_{{ .Version }}_SHA256SUMS'
  algorithm: sha256

signs:
  - artifacts: checksum
    cmd: gpg
    args:
      [
        '--pinentry-mode=loopback',
        '--passphrase={{ .Env.GPG_PASSPHRASE }}',
        '--output=${signature}',
        '--detach-sign',
        '${artifact}',
      ]

source:
  enabled: true
  name_template: '{{ .ProjectName }}-{{ .Version }}'
  format: 'tar.gz'

release:
  extra_files:
    - glob: ./CHANGELOG.md
