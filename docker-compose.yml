version: "3.4"

x-env: &env
  TZ: Europe/Stockholm

services:
  bliss:
    image: $REGISTRY/altosys/bliss
    restart: always
    container_name: bliss
    environment:
      <<: *env
      HTTP_PROXY: ""
      BLISS_PROXY_ADDRESS: ""
      BLISS_DEVELOPER_LOGS: "false"
      BLISS_LOG_LEVEL: INFO
      BLISS_CURRENCY: SEK
      BLISS_INTERVAL: 5
      BLISS_DB_ADDRESS: influxdb:8086
      BLISS_DB_USER: admin
      BLISS_DB_PASSWORD: bliss
      BLISS_DB_NAME: bliss
      BLISS_DB_RETENTION: one_week
      BLISS_REDIS_ADDRESS: redis:6379
      BLISS_REDIS_PASSWORD: ""
      BLISS_REDIS_DATABASE: 0
      BLISS_HTTP_PORT: 8085
      BLISS_ENABLE_ADLIBRIS: "true"
      BLISS_ENABLE_AKADEMIBOKHANDELN: "true"
      BLISS_ENABLE_AMAZON_AE: "true"
      BLISS_ENABLE_AMAZON_CA: "true"
      BLISS_ENABLE_AMAZON_COM: "true"
      BLISS_ENABLE_AMAZON_COM_AU: "true"
      BLISS_ENABLE_AMAZON_CO_UK: "true"
      BLISS_ENABLE_AMAZON_DE: "true"
      BLISS_ENABLE_AMAZON_ES: "true"
      BLISS_ENABLE_AMAZON_FR: "true"
      BLISS_ENABLE_AMAZON_IN: "true"
      BLISS_ENABLE_AMAZON_IT: "true"
      BLISS_ENABLE_AMAZON_NL: "true"
      BLISS_ENABLE_AMAZON_SE: "true"
      BLISS_ENABLE_BLACKWELLS: "true"
      BLISS_ENABLE_BOKUS: "true"
      BLISS_ENABLE_BOOKDEPOSITORY: "true"
      BLISS_ENABLE_BOOKSWAGON: "true"
      BLISS_ENABLE_CDON: "true"
      BLISS_ENABLE_FLIPKART: "true"
      BLISS_ENABLE_FORBIDDEN_PLANET: "true"
      BLISS_ENABLE_SCIFIBOKHANDELN: "true"
      BLISS_ENABLE_WALTS_COMIC_SHOP: "true"
      BLISS_ENABLE_WATERSTONES: "true"
      BLISS_ENABLE_WORDERY: "true"
      BLISS_NOTIFY_DROP_THRESHOLD: 10
      BLISS_NOTIFY_LOW_STOCK: "true"
      BLISS_NOTIFY_AVAILABLE: "true"
      BLISS_PUSHOVER_USER_KEY: ""
      BLISS_PUSHOVER_API_TOKEN: ""
      BLISS_BOOKS_FILE: /etc/bliss/books.yml
      BLISS_BOOKS: |
        9781590171585 War of the Worlds
        9780141393391 HC Frankenstein
        9780141198958 SC Moby-Dick
    ports:
      - 8085:8085
    volumes:
      - ./books.example.yml:/etc/bliss/books.yml
    depends_on:
      - influxdb
      - redis

  grafana:
    image: $REGISTRY/grafana/grafana:7.1.3
    restart: always
    container_name: grafana
    ports:
      - 3000:3000
    environment:
      <<: *env
      GF_AUTH_DISABLE_LOGIN_FORM: "true"
      GF_AUTH_ANONYMOUS_ENABLED: "true"
      GF_AUTH_ANONYMOUS_ORG_NAME: "Main Org."
      GF_AUTH_ANONYMOUS_ORG_ROLE: "Editor"
      GF_USERS_VIEWERS_CAN_EDIT: "true"
      GF_PATHS_PROVISIONING: "/usr/share/grafana/provisioning/"
      GF_SECURITY_ALLOW_EMBEDDING: "true"
    volumes:
      - ./grafana:/usr/share/grafana/provisioning
      - grafana:/var/lib/grafana
    depends_on:
      - influxdb

  influxdb:
    image: $REGISTRY/library/influxdb:1.8.3
    restart: always
    container_name: influxdb
    ports:
      - 8086:8086
      - 8090:8090
    environment:
      <<: *env
      INFLUXDB_DB: "bliss"
      INFLUXDB_HTTP_AUTH_ENABLED: "true"
      INFLUXDB_ADMIN_ENABLED: "true"
      INFLUXDB_ADMIN_USER: "admin"
      INFLUXDB_ADMIN_PASSWORD: "bliss"
      INFLUXDB_WRITE_USER: "writer"
      INFLUXDB_WRITE_USER_PASSWORD: "bliss"
      INFLUXDB_READ_USER: "reader"
      INFLUXDB_READ_USER_PASSWORD: "bliss"
      INFLUXDB_REPORTING_DISABLED: "true"
    ulimits:
      nproc: 65535
      nofile:
        soft: 63356
        hard: 63356
    volumes:
      - influxdb:/var/lib/influxdb
      - ./influxdb/init-influxdb.sh:/docker-entrypoint-initdb.d/init-influxdb.sh

  redis:
    image: $REGISTRY/library/redis:6.0.9-alpine3.12
    restart: always
    container_name: redis
    command: redis-server --appendonly yes
    ports:
      - 6379:6379
    environment: *env
    volumes:
      - redis:/data

volumes:
  grafana:
  influxdb:
  redis:
