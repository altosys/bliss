package redis

import (
	"bliss"
	e "bliss/exchange"
	"bliss/logger"
	"context"
	"errors"
	"fmt"
	"strconv"
	"testing"
	"time"

	"github.com/alicebob/miniredis/v2"
	"github.com/elliotchance/redismock/v8"
	"github.com/go-redis/redis/v8"
	"github.com/gregdel/pushover"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

const (
	returnErrorFromDel           = "return error from redis Del"
	returnErrorFromSet           = "return error from redis Set"
	returnErrorFromExists        = "return error from redis Exists"
	returnErrorFromHSet          = "return error from redis HSet"
	returnErrorFromScan          = "return error from redis Scan"
	returnErrorOnGetSiteListings = "return error from GetSiteListings"
	returnNilOnSuccess           = "return nil on success"
)

func TestNewRedis(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc     string
		address  string
		database int
		password string
		logger   *zap.SugaredLogger
		err      error
	}{
		{
			desc:    "return error on nil logger",
			address: "redis:6379",
			logger:  nil,
			err:     fmt.Errorf("%w: %s", bliss.ErrNilPointer, "lg"),
		},
		{
			desc:     "return nil on success",
			address:  "redis:6379",
			logger:   lg,
			database: 0,
			err:      nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			cfg := Config{
				Address:  test.address,
				Database: test.database,
				Password: test.password,
			}

			r, err := NewRedis(cfg, test.logger)
			assert.Equal(t, test.err, err)

			if test.err == nil {
				assert.NotNil(t, r.Client)
			}
		})
	}
}

func TestAssertKeyType(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc      string
		key       string
		keyType   string
		keyResult string
		err       error
	}{
		{
			desc:      "return error on type request",
			key:       "TestAssertKeyType:key",
			keyType:   "",
			keyResult: "",
			err:       errors.New("failed Type"),
		},
		{
			desc:      "return error on key not found",
			key:       "TestAssertKeyType:key",
			keyType:   "string",
			keyResult: "",
			err:       errors.New("expected redis key type 'string', found 'none'"),
		},
		{
			desc:      "return error on key type mismatch",
			key:       "TestAssertKeyType:key",
			keyType:   "string",
			keyResult: "list",
			err:       errors.New("expected redis key type 'string', found 'list'"),
		},
		{
			desc:      "return nil on key type match",
			key:       "TestAssertKeyType:key",
			keyType:   "string",
			keyResult: "string",
			err:       nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			switch test.keyResult {
			case "":
			case "list":
				_, err := mr.RPush(ctx, test.key, "value").Result()
				assert.NoError(t, err)
			case "string":
				_, err := mr.Set(ctx, test.key, "value", 0*time.Second).Result()
				assert.NoError(t, err)
			}

			switch test.desc {
			case "return error on type request":
				mr.On("Type", ctx, test.key).Return(redis.NewStatusResult(test.keyResult, test.err))
			default:
			}

			err := r.assertKeyType(ctx, mr, test.key, test.keyType)
			assert.Equal(t, test.err, err)
		})
	}
}

func TestCheckBaseline(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc                 string
		baselineIn           bool
		baselineOut          bool
		startupIn            bool
		startupOut           bool
		baselineNotification bool
		startupNotification  bool
		expectMessage        bool
		err                  error
		notifyCh             chan *pushover.Message
	}{
		{
			desc:     "return error if notifyCh is nil",
			notifyCh: nil,
			err:      fmt.Errorf("%w: %s", bliss.ErrNilPointer, "notifyCh"),
		},
		{
			desc:        "return nil if baseline is established",
			baselineIn:  true,
			baselineOut: true,
			notifyCh:    make(chan *pushover.Message, 1),
			err:         nil,
		},
		{
			desc:        "return error from GetBookMeta",
			baselineIn:  false,
			baselineOut: false,
			notifyCh:    make(chan *pushover.Message, 1),
			err:         errors.New("failed Scan"),
		},
		{
			desc:        "return error from GetSites",
			baselineIn:  false,
			baselineOut: false,
			notifyCh:    make(chan *pushover.Message, 1),
			err:         errors.New("failed Scan"),
		},
		{
			desc:        returnErrorOnGetSiteListings,
			baselineIn:  false,
			baselineOut: false,
			notifyCh:    make(chan *pushover.Message, 1),
			err:         errors.New("failed Scan"),
		},
		{
			desc:          "return nil with baseline not established on startup",
			baselineIn:    false,
			baselineOut:   false,
			startupIn:     true,
			startupOut:    false,
			expectMessage: true,
			notifyCh:      make(chan *pushover.Message, 1),
			err:           nil,
		},
		{
			desc:          "return nil with baseline established on startup",
			baselineIn:    false,
			baselineOut:   true,
			startupIn:     true,
			startupOut:    false,
			expectMessage: true,
			notifyCh:      make(chan *pushover.Message, 1),
			err:           nil,
		},
		{
			desc:          "return nil with baseline established, not on startup",
			baselineIn:    false,
			baselineOut:   true,
			startupIn:     false,
			startupOut:    false,
			expectMessage: true,
			notifyCh:      make(chan *pushover.Message, 1),
			err:           nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			interval := 1

			_, err := mr.Set(ctx, "scrapers:site:next", "1", 0*time.Second).Result()
			assert.NoError(t, err)
			_, err = mr.HSet(ctx, "books:meta:1", "isbn", "1", "price", "1").Result()
			assert.NoError(t, err)
			_, err = mr.HSet(ctx, "listings:site:site:1", "isbn", "1", "price", "1").Result()
			assert.NoError(t, err)

			switch test.desc {
			case "return error if notifyCh is nil":

			case "return error from GetBookMeta":
				mr.On("Scan", ctx, uint64(0), "books:meta:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case "return error from GetSites":
				mr.On("Scan", ctx, uint64(0), "books:meta:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), nil))
				mr.On("Scan", ctx, uint64(0), "scrapers:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case returnErrorOnGetSiteListings:
				mr.On("Scan", ctx, uint64(0), "books:meta:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), nil))
				mr.On("Scan", ctx, uint64(0), "scrapers:*", int64(0)).Return(redis.NewScanCmdResult([]string{"scrapers:site:next"}, uint64(0), nil))
				mr.On("Scan", ctx, uint64(0), "listings:site:site:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case "return nil with baseline not established on startup":
				_, err = mr.Set(ctx, "scrapers:site2:next", "1", 0*time.Second).Result()
				assert.NoError(t, err)
			}

			baseline, startup, err := r.CheckBaseline(ctx, mr, test.baselineIn, test.startupIn, interval, test.notifyCh, lg)
			assert.Equal(t, test.err, err)
			assert.Equal(t, test.baselineOut, baseline)
			assert.Equal(t, test.startupOut, startup)

			if test.expectMessage {
				assert.Len(t, test.notifyCh, 1)
			}

			if test.notifyCh != nil {
				close(test.notifyCh)
			}
		})
	}
}

func TestDeleteLowestListing(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc string
		err  error
	}{
		{
			desc: "handle error on Del",
			err:  errors.New("failed Del"),
		},
		{
			desc: returnNilOnSuccess,
			err:  nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			_, err := mr.HSet(ctx, "listings:lowest:1", "isbn", "1").Result()
			assert.NoError(t, err)

			switch test.desc {
			case "handle error on Del":
				mr.On("Del", ctx, []string{"listings:lowest:1"}).Return(redis.NewIntResult(0, test.err))
			case returnNilOnSuccess:
			}

			assert.Equal(t, test.err, r.DeleteLowestListing(ctx, mr, "1"))
		})
	}
}

func TestGetBookMeta(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())
	tt := []struct {
		desc  string
		isbns []string
		meta  map[string]bliss.Book
		err   error
	}{
		{
			desc: returnErrorFromScan,
			meta: nil,
			err:  errors.New("failed Scan"),
		},
		{
			desc: "return error from redis HGetAll",
			meta: nil,
			err:  errors.New("failed HGetAll"),
		},
		{
			desc: "return all metadata by omitting isbn",
			meta: map[string]bliss.Book{
				"1": {ISBN: "1", Title: "book1", Format: bliss.SC},
				"2": {ISBN: "2", Title: "book2", Format: bliss.HC},
			},
			err: nil,
		},
		{
			desc:  "return metadata for specific isbn",
			isbns: []string{"1"},
			meta: map[string]bliss.Book{
				"1": {ISBN: "1", Title: "book1", Format: bliss.SC},
			},
			err: nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			_, err := mr.HSet(ctx, "books:meta:1", "isbn", "1", "title", "book1", "format", "SC").Result()
			assert.NoError(t, err)
			_, err = mr.HSet(ctx, "books:meta:2", "isbn", "2", "title", "book2", "format", "HC").Result()
			assert.NoError(t, err)

			switch test.desc {
			case returnErrorFromScan:
				mr.On("Scan", ctx, uint64(0), "books:meta:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case "return error from redis HGetAll":
				mr.On("HGetAll", ctx, "books:meta:1").Return(redis.NewStringStringMapResult(nil, test.err))

			case returnNilOnSuccess:
			}

			meta, err := r.GetBookMeta(ctx, mr, test.isbns...)
			assert.Equal(t, test.err, err)
			assert.Equal(t, test.meta, meta)
		})
	}
}

func TestGetExchange(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc     string
		exchange *e.Exchange
		err      error
	}{
		{
			desc:     "handle error on get base",
			exchange: nil,
			err:      errors.New("get base failed"),
		},
		{
			desc:     "handle error on get date",
			exchange: nil,
			err:      errors.New("get date failed"),
		},
		{
			desc:     "handle error on get last_updated",
			exchange: nil,
			err:      errors.New("get last_updated failed"),
		},
		{
			desc:     "handle error on get rates",
			exchange: nil,
			err:      errors.New("failed HGetAll rates"),
		},
		{
			desc:     "handle error on last_updated atoi",
			exchange: nil,
			err:      &strconv.NumError{Func: "Atoi", Num: "$", Err: errors.New("invalid syntax")},
		},
		{
			desc:     "handle error on rates parsefloat",
			exchange: nil,
			err:      &strconv.NumError{Func: "ParseFloat", Num: "#", Err: errors.New("invalid syntax")},
		},
		{
			desc:     returnNilOnSuccess,
			exchange: &e.Exchange{Base: "EUR", Date: "2020-12-20", LastUpdated: 1, Rates: map[string]float64{"EUR": 1, "USD": 2}},
			err:      nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			mr.Set(ctx, "exchange:base", "EUR", 0*time.Second)
			mr.Set(ctx, "exchange:date", "2020-12-20", 0*time.Second)
			mr.Set(ctx, "exchange:last_updated", "1", 0*time.Second)
			mr.HSet(ctx, "exchange:rates", "EUR", float64(1), "USD", float64(2))

			switch test.desc {
			case "handle error on get base":
				mr.On("Get", ctx, "exchange:base").Return(redis.NewStringResult("", test.err))

			case "handle error on get date":
				mr.On("Get", ctx, "exchange:base").Return(redis.NewStringResult("EUR", nil))
				mr.On("Get", ctx, "exchange:date").Return(redis.NewStringResult("", test.err))

			case "handle error on get last_updated":
				mr.On("Get", ctx, "exchange:base").Return(redis.NewStringResult("EUR", nil))
				mr.On("Get", ctx, "exchange:date").Return(redis.NewStringResult("2020-12-19", nil))
				mr.On("Get", ctx, "exchange:last_updated").Return(redis.NewStringResult("", test.err))

			case "handle error on get rates":
				mr.On("Get", ctx, "exchange:base").Return(redis.NewStringResult("EUR", nil))
				mr.On("Get", ctx, "exchange:date").Return(redis.NewStringResult("2020-12-19", nil))
				mr.On("Get", ctx, "exchange:last_updated").Return(redis.NewStringResult("1", nil))
				mr.On("HGetAll", ctx, "exchange:rates").Return(redis.NewStringStringMapResult(map[string]string{}, test.err))

			case "handle error on last_updated atoi":
				mr.On("Get", ctx, "exchange:base").Return(redis.NewStringResult("EUR", nil))
				mr.On("Get", ctx, "exchange:date").Return(redis.NewStringResult("2020-12-19", nil))
				mr.On("Get", ctx, "exchange:last_updated").Return(redis.NewStringResult("$", nil))

			case "handle error on rates parsefloat":
				mr.On("HGetAll", ctx, "exchange:rates").Return(redis.NewStringStringMapResult(map[string]string{"EUR": "#"}, nil))

			case returnNilOnSuccess:
			}

			exchange, err := r.GetExchange(ctx, mr)
			assert.Equal(t, test.err, err)
			assert.Equal(t, test.exchange, exchange)
		})
	}
}

func TestGetListings(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc     string
		listings map[string]map[string]bliss.Listing
		err      error
	}{
		{
			desc:     "return error from GetSites",
			listings: nil,
			err:      errors.New("failed GetSites"),
		},
		{
			desc:     returnErrorOnGetSiteListings,
			listings: nil,
			err:      errors.New("failed GetSiteListings"),
		},
		{
			desc:     "return empty map on no listings found",
			listings: map[string]map[string]bliss.Listing{},
			err:      nil,
		},
		{
			desc: "return map on listings found",
			listings: map[string]map[string]bliss.Listing{
				"site1": {
					"1": {Book: bliss.Book{
						ISBN:   "1",
						Title:  "book1",
						Format: 1,
					},
						Currency: "",
						Site:     "site1",
						Price:    10,
					},
				},
				"site2": {
					"2": {Book: bliss.Book{
						ISBN:   "2",
						Title:  "book2",
						Format: 2},
						Currency: "",
						Site:     "site2",
						Price:    20,
					},
				},
			},
			err: nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			_, err := mr.Set(ctx, "scrapers:site1:next", "isbn", 0*time.Second).Result()
			assert.NoError(t, err)
			_, err = mr.Set(ctx, "scrapers:site2:next", "isbn", 0*time.Second).Result()
			assert.NoError(t, err)

			_, err = mr.HSet(ctx,
				"listings:site:site1:1",
				"isbn", "1",
				"format", "SC",
				"title", "book1",
				"price", "10",
				"site", "site1").Result()
			assert.NoError(t, err)

			_, err = mr.HSet(ctx,
				"listings:site:site2:2",
				"isbn", "2",
				"format", "HC",
				"title", "book2",
				"price", "20",
				"site", "site2").Result()
			assert.NoError(t, err)

			switch test.desc {
			case "return error from GetSites":
				mr.On("Scan", ctx, uint64(0), "scrapers:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case returnErrorOnGetSiteListings:
				mr.On("HGetAll", ctx, "listings:site:site1:1").Return(redis.NewStringStringMapResult(nil, test.err))

			case "return empty map on no listings found":
				keys := []string{
					"scrapers:site1:next",
					"scrapers:site2:next",
					"listings:site:site1:1",
					"listings:site:site2:2",
				}
				_, err = mr.Del(ctx, keys...).Result()
				assert.NoError(t, err)
			}

			listings, err := r.GetListings(ctx, mr)
			assert.Equal(t, test.err, err)
			assert.Equal(t, test.listings, listings)
		})
	}
}

func TestGetLowestListings(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc     string
		listings map[string]bliss.Listing
		err      error
	}{
		{
			desc: returnErrorFromScan,
			err:  errors.New("failed Scan"),
		},
		{
			desc: "return error from HGetAll",
			listings: map[string]bliss.Listing{
				"1": {Book: bliss.Book{ISBN: "1", Title: "book1", Format: 1}, Currency: "EUR", Site: "site1", Price: 10}},
			err: errors.New("failed HGetAll"),
		},
		{
			desc: "return error from ParseFloat",
			listings: map[string]bliss.Listing{
				"1": {Book: bliss.Book{ISBN: "1", Title: "book1", Format: 1}, Currency: "EUR", Site: "site1", Price: 10}},
			err: &strconv.NumError{Func: "ParseFloat", Num: "#", Err: errors.New("invalid syntax")},
		},
		{
			desc: returnNilOnSuccess,
			listings: map[string]bliss.Listing{
				"1": {Book: bliss.Book{ISBN: "1", Title: "book1", Format: 1}, Currency: "EUR", Site: "site1", Price: 10},
				"2": {Book: bliss.Book{ISBN: "2", Title: "book2", Format: 2}, Currency: "USD", Site: "site2", Price: 20}},
			err: nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			if test.listings != nil {
				for i, l := range test.listings {
					_, err := mr.HSet(ctx,
						"listings:lowest:"+i,
						"isbn", l.Book.ISBN,
						"format", l.Book.Format.String(),
						"title", l.Book.Title,
						"currency", l.Currency,
						"price", l.Price,
						"site", l.Site).Result()
					assert.NoError(t, err)
				}
			}

			switch test.desc {
			case returnErrorFromScan:
				mr.On("Scan", ctx, uint64(0), "listings:lowest:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case "return error from HGetAll":
				mr.On("HGetAll", ctx, "listings:lowest:1").Return(redis.NewStringStringMapResult(nil, test.err))

			case "return error from ParseFloat":
				mr.On("HGetAll", ctx, "listings:lowest:1").Return(redis.NewStringStringMapResult(map[string]string{"price": "#"}, nil))

			case returnNilOnSuccess:
			}

			listings, err := r.GetLowestListings(ctx, mr)
			assert.Equal(t, test.err, err)
			if test.err == nil {
				assert.Equal(t, test.listings, listings)
			} else {
				assert.Nil(t, listings)
			}
		})
	}
}

func TestGetNextISBN(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc        string
		next        string
		prioritized bool
		err         error
	}{
		{
			desc:        "return error from Get previous",
			next:        "",
			prioritized: false,
			err:         errors.New("failed Get"),
		},
		{
			desc:        "return error from LPop",
			next:        "",
			prioritized: false,
			err:         errors.New("failed LPop"),
		},
		{
			desc:        returnErrorFromExists,
			next:        "",
			prioritized: false,
			err:         errors.New("failed Exists"),
		},
		{
			desc:        "return error from updateNext",
			next:        "",
			prioritized: false,
			err:         errors.New("failed Type"),
		},
		{
			desc:        "return next from scraper priority list",
			next:        "1",
			prioritized: true,
			err:         nil,
		},
		{
			desc:        "return next from scraper next",
			next:        "2",
			prioritized: false,
			err:         nil,
		},
		{
			desc:        "invalid isbn - update next from books meta",
			next:        "1",
			prioritized: false,
			err:         nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()

			ctx := context.Background()

			switch test.desc {
			case "return error from Get previous":
				mr.On("Get", ctx, "scrapers:site:next").Return(redis.NewStringResult(test.next, test.err))

			case "return error from LPop":
				mr.On("LPop", ctx, "scrapers:site:priority").Return(redis.NewStringResult("", test.err))

			case returnErrorFromExists:
				mr.On("Get", ctx, "scrapers:site:next").Return(redis.NewStringResult("1", nil))
				mr.On("Exists", ctx, []string{"books:meta:1"}).Return(redis.NewIntResult(0, test.err))

			case "return error from updateNext":
				mr.On("Type", ctx, "books:loop").Return(redis.NewStatusResult("0", test.err))

			case "return next from scraper priority list":
				mr.RPush(ctx, "scrapers:site:priority", "1", "2", "3")

			case "return next from scraper next":
				mr.HSet(ctx, "books:meta:2", "isbn", "2")
				mr.RPush(ctx, "books:loop", "1", "2")
				mr.Set(ctx, "scrapers:site:next", "2", 0*time.Second)

			case "invalid isbn - update next from books meta":
				mr.HSet(ctx, "books:meta:1", "isbn", "1")
				mr.HSet(ctx, "books:meta:2", "isbn", "2")
				mr.RPush(ctx, "books:loop", "1", "2")
				mr.Set(ctx, "scrapers:site:next", "3", 0*time.Second)
			}

			next, prioritized, err := r.GetNextISBN(ctx, mr, "site")
			assert.Equal(t, test.err, err)
			assert.Equal(t, test.next, next)
			assert.Equal(t, test.prioritized, prioritized)
		})
	}
}

func TestGetSites(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc     string
		scrapers []string
		err      error
	}{
		{
			desc:     returnErrorFromScan,
			scrapers: nil,
			err:      errors.New("failed Scan"),
		},
		{
			desc:     returnNilOnSuccess,
			scrapers: []string{"site1", "site2"},
			err:      nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			_, err := mr.Set(ctx, "scrapers:site1:next", "1", 0*time.Second).Result()
			assert.NoError(t, err)
			_, err = mr.Set(ctx, "scrapers:site2:history", "2", 0*time.Second).Result()
			assert.NoError(t, err)

			switch test.desc {
			case returnErrorFromScan:
				mr.On("Scan", ctx, uint64(0), "scrapers:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case returnNilOnSuccess:
			}

			sites, err := r.GetSites(ctx, mr)
			assert.Equal(t, test.err, err)
			assert.Equal(t, test.scrapers, sites)
		})
	}
}

func TestGetSiteListings(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc     string
		listings map[string]bliss.Listing
		err      error
	}{
		{
			desc:     returnErrorFromScan,
			listings: nil,
			err:      errors.New("failed Scan"),
		},
		{
			desc:     "handle error on HGetAll",
			listings: nil,
			err:      errors.New("failed HGetAll"),
		},
		{
			desc:     "handle error on ParseFloat",
			listings: nil,
			err:      &strconv.NumError{Func: "ParseFloat", Num: "#", Err: errors.New("invalid syntax")},
		},
		{
			desc: returnNilOnSuccess,
			listings: map[string]bliss.Listing{
				"1": {Book: bliss.Book{ISBN: "1", Title: "book1", Format: 1}, Currency: "", Site: "site1", Price: 10},
			},
			err: nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			_, err := mr.HSet(ctx,
				"listings:site:site1:1",
				"isbn", "1",
				"format", "SC",
				"title", "book1",
				"price", "10",
				"site", "site1").Result()
			assert.NoError(t, err)

			_, err = mr.HSet(ctx,
				"listings:site:site2:2",
				"isbn", "2",
				"format", "HC",
				"title", "book2",
				"price", "20",
				"site", "site2").Result()
			assert.NoError(t, err)

			switch test.desc {
			case returnErrorFromScan:
				mr.On("Scan", ctx, uint64(0), "listings:site:site1:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case "handle error on HGetAll":
				mr.On("HGetAll", ctx, "listings:site:site1:1").Return(redis.NewStringStringMapResult(nil, test.err))

			case "handle error on ParseFloat":
				mr.On("HGetAll", ctx, "listings:site:site1:1").Return(redis.NewStringStringMapResult(map[string]string{"price": "#"}, nil))

			case returnNilOnSuccess:
			}

			listings, err := r.GetSiteListings(ctx, mr, "site1")
			assert.Equal(t, test.err, err)
			assert.Equal(t, test.listings, listings)
		})
	}
}

func TestPruneBookMeta(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc string
		book bliss.Book
		err  error
	}{
		{
			desc: "return error from redis Scan",
			book: bliss.Book{ISBN: "1", Format: bliss.HC, Title: "title1"},
			err:  errors.New("failed Scan"),
		},
		{
			desc: returnErrorFromDel,
			book: bliss.Book{ISBN: "1", Format: bliss.HC, Title: "title1"},
			err:  errors.New("failed Del"),
		},
		{
			desc: returnNilOnSuccess,
			book: bliss.Book{ISBN: "0", Format: bliss.HC, Title: "title0"},
			err:  nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			mr.HSet(ctx, "books:meta:0", "isbn", "0")

			switch test.desc {
			case "return error from redis Scan":
				mr.On("Scan", ctx, uint64(0), "books:meta:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case returnErrorFromDel:
				mr.On("Del", ctx, []string{"books:meta:0"}).Return(redis.NewIntResult(0, test.err))

			case returnNilOnSuccess:
			}

			err := r.pruneBookMeta(ctx, mr, test.book)
			assert.Equal(t, test.err, err)
		})
	}
}

func TestPruneListingsByBook(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc  string
		books []bliss.Book
		err   error
	}{
		{
			desc:  "return error on empty books slice",
			books: []bliss.Book{},
			err:   fmt.Errorf("%w: %s", bliss.ErrEmptySlice, "books"),
		},
		{
			desc:  "return error from redis Scan site listings",
			books: []bliss.Book{{ISBN: "1", Format: bliss.HC, Title: "title1"}},
			err:   errors.New("failed Scan"),
		},
		{
			desc:  "return error from redis Scan lowest listings",
			books: []bliss.Book{{ISBN: "1", Format: bliss.HC, Title: "title1"}},
			err:   errors.New("failed Scan"),
		},
		{
			desc:  returnErrorFromDel,
			books: []bliss.Book{{ISBN: "1", Format: bliss.HC, Title: "title1"}},
			err:   errors.New("failed Del"),
		},
		{
			desc:  "prune listings on success",
			books: []bliss.Book{{ISBN: "1", Format: bliss.HC, Title: "title1"}},
			err:   nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			mr.HSet(ctx, "books:meta:0", "isbn", "0")
			mr.HSet(ctx, "listings:lowest:0", "isbn", "0")
			mr.HSet(ctx, "listings:site:site1:0", "isbn", "0")
			assert.Equal(t, int64(1), mr.Exists(ctx, "listings:lowest:0").Val())
			assert.Equal(t, int64(1), mr.Exists(ctx, "listings:site:site1:0").Val())

			switch test.desc {
			case "return error from redis Scan site listings":
				mr.On("Scan", ctx, uint64(0), "listings:site:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case "return error from redis Scan lowest listings":
				mr.On("Scan", ctx, uint64(0), "listings:site:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), nil))
				mr.On("Scan", ctx, uint64(0), "listings:lowest:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case returnErrorFromDel:
				mr.On("Del", ctx, []string{"listings:site:site1:0", "listings:lowest:0"}).Return(redis.NewIntResult(0, test.err))
			}

			err := r.pruneListingsByBook(ctx, mr, test.books...)
			assert.Equal(t, test.err, err)

			if test.err == nil {
				assert.Equal(t, int64(0), mr.Exists(ctx, "listings:lowest:0").Val())
				assert.Equal(t, int64(0), mr.Exists(ctx, "listings:site:site1:0").Val())
			}
		})
	}
}

func TestPruneListingsBySite(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc  string
		sites []string
		err   error
	}{
		{
			desc:  "return error on empty sites map",
			sites: []string{},
			err:   fmt.Errorf("%w: %s", bliss.ErrEmptySlice, "sites"),
		},
		{
			desc:  "return error from redis Scan site listings",
			sites: []string{"site1"},
			err:   errors.New("failed Scan"),
		},
		{
			desc:  "return error from redis Scan lowest listings",
			sites: []string{"site1"},
			err:   errors.New("failed Scan"),
		},
		{
			desc:  "return error from redis HGet",
			sites: []string{"site1"},
			err:   errors.New("failed HGet"),
		},
		{
			desc:  returnErrorFromDel,
			sites: []string{"site1"},
			err:   errors.New("failed Del"),
		},
		{
			desc:  "prune listings on success",
			sites: []string{"site1"},
			err:   nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			mr.HSet(ctx, "listings:lowest:0", "isbn", "0", "site", "site2")
			mr.HSet(ctx, "listings:site:site1:0", "isbn", "0")
			mr.HSet(ctx, "listings:site:site2:0", "isbn", "0")
			assert.Equal(t, int64(1), mr.Exists(ctx, "listings:lowest:0").Val())
			assert.Equal(t, int64(1), mr.Exists(ctx, "listings:site:site1:0").Val())
			assert.Equal(t, int64(1), mr.Exists(ctx, "listings:site:site2:0").Val())

			switch test.desc {
			case "return error from redis Scan site listings":
				mr.On("Scan", ctx, uint64(0), "listings:site:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case "return error from redis Scan lowest listings":
				mr.On("Scan", ctx, uint64(0), "listings:site:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), nil))
				mr.On("Scan", ctx, uint64(0), "listings:lowest:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case "return error from redis HGet":
				mr.On("HGet", ctx, "listings:lowest:0", "site").Return(redis.NewStringResult("", test.err))

			case returnErrorFromDel:
				mr.On("Del", ctx, []string{"listings:site:site2:0", "listings:lowest:0"}).Return(redis.NewIntResult(0, test.err))
			}

			err := r.pruneListingsBySite(ctx, mr, test.sites)
			assert.Equal(t, test.err, err)

			if test.err == nil {
				assert.Equal(t, int64(0), mr.Exists(ctx, "listings:lowest:0").Val())
				assert.Equal(t, int64(0), mr.Exists(ctx, "listings:site:site2:0").Val())
			}
		})
	}
}

func TestPruneSites(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc  string
		sites []string
		err   error
	}{
		{
			desc:  "return error on empty scrapers map",
			sites: []string{},
			err:   fmt.Errorf("%w: %s", bliss.ErrEmptyMap, "scrapers"),
		},
		{
			desc:  "return error on empty site string",
			sites: []string{""},
			err:   fmt.Errorf("%w: %s", bliss.ErrEmptyString, "scraper"),
		},
		{
			desc:  "return error from redis Scan scrapers",
			sites: []string{"site1"},
			err:   errors.New("failed Scan"),
		},
		{
			desc:  returnErrorFromDel,
			sites: []string{"site1"},
			err:   errors.New("failed Del"),
		},
		{
			desc:  "prune scrapers on success",
			sites: []string{"site1"},
			err:   nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			mr.Set(ctx, "scrapers:site1:next", "0", 0*time.Second)
			mr.Set(ctx, "scrapers:site2:next", "0", 0*time.Second)
			mr.Set(ctx, "scrapers:site2:history", "0", 0*time.Second)
			mr.Set(ctx, "scrapers:site2:priority", "0", 0*time.Second)
			assert.Equal(t, int64(1), mr.Exists(ctx, "scrapers:site1:next").Val())
			assert.Equal(t, int64(1), mr.Exists(ctx, "scrapers:site2:next").Val())
			assert.Equal(t, int64(1), mr.Exists(ctx, "scrapers:site2:history").Val())
			assert.Equal(t, int64(1), mr.Exists(ctx, "scrapers:site2:priority").Val())

			switch test.desc {
			case "return error from redis Scan scrapers":
				mr.On("Scan", ctx, uint64(0), "scrapers:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case returnErrorFromDel:
				mr.On("Del", ctx, []string{"scrapers:site2:history", "scrapers:site2:next", "scrapers:site2:priority"}).
					Return(redis.NewIntResult(0, test.err))
			}

			err := r.pruneSites(ctx, mr, test.sites)
			assert.Equal(t, test.err, err)

			if test.err == nil {
				assert.Equal(t, int64(1), mr.Exists(ctx, "scrapers:site1:next").Val())
				assert.Equal(t, int64(0), mr.Exists(ctx, "scrapers:site2:next").Val())
				assert.Equal(t, int64(0), mr.Exists(ctx, "scrapers:site2:history").Val())
				assert.Equal(t, int64(0), mr.Exists(ctx, "scrapers:site2:priority").Val())
			}
		})
	}
}

func TestRegisterListing(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc      string
		listing   bliss.Listing
		isDrop    bool
		isDeleted bool
		err       error
	}{
		{
			desc:      "return error from updateLowestListing",
			listing:   bliss.Listing{Book: bliss.Book{ISBN: "0"}, Currency: "EUR", Price: 1, Site: "site1"},
			isDrop:    false,
			isDeleted: false,
			err:       errors.New("failed Scan"),
		},
		{
			desc:      "return error from SetSiteListings",
			listing:   bliss.Listing{Book: bliss.Book{ISBN: "0"}, Currency: "EUR", Price: 1, Site: "site1"},
			isDrop:    false,
			isDeleted: false,
			err:       errors.New("failed SetListing"),
		},
		{
			desc:      "return error from updateHistory",
			listing:   bliss.Listing{Book: bliss.Book{ISBN: "0"}, Currency: "EUR", Price: 1, Site: ""},
			isDrop:    false,
			isDeleted: false,
			err:       fmt.Errorf("%w: %s", bliss.ErrEmptyString, "site"),
		},
		{
			desc:      "return error from updateNext",
			listing:   bliss.Listing{Book: bliss.Book{ISBN: "0"}, Currency: "EUR", Price: 1, Site: "site1"},
			isDrop:    false,
			isDeleted: false,
			err:       errors.New("failed updateNext"),
		},
		{
			desc:      returnNilOnSuccess,
			listing:   bliss.Listing{Book: bliss.Book{ISBN: "0"}, Currency: "EUR", Price: 1, Site: "site1"},
			isDrop:    false,
			isDeleted: false,
			err:       nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			_, err := mr.RPush(ctx, "books:loop", test.listing.Book.ISBN).Result()
			assert.NoError(t, err)

			switch test.desc {
			case "return error from SetSiteListings":
				mr.On("HSet", ctx, "listings:lowest:0",
					[]interface{}{
						"currency", test.listing.Currency,
						"price", test.listing.Price,
						"site", test.listing.Site,
						"format", test.listing.Book.Format.String(),
						"isbn", test.listing.Book.ISBN,
						"title", test.listing.Book.Title}).Return(redis.NewIntResult(0, nil))
				mr.On("HSet", ctx, "listings:site:site1:0",
					[]interface{}{
						"currency", test.listing.Currency,
						"price", test.listing.Price,
						"site", test.listing.Site,
						"format", test.listing.Book.Format.String(),
						"isbn", test.listing.Book.ISBN,
						"title", test.listing.Book.Title}).Return(redis.NewIntResult(0, test.err))

			case "return error from updateNext":
				mr.On("Type", ctx, "books:loop").Return(redis.NewStatusResult("", test.err))

			case "return error from updateLowestListing":
				mr.On("Scan", ctx, uint64(0), "listings:lowest:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))
			}

			isDeleted, isDrop, err := r.RegisterListing(ctx, mr, test.listing, false)
			assert.Equal(t, test.err, err)
			assert.Equal(t, test.isDrop, isDrop)
			assert.Equal(t, test.isDeleted, isDeleted)

			if test.err == nil {
				s, err := mr.HGetAll(ctx, "listings:site:site1:0").Result()
				assert.NoError(t, err)

				price, err := strconv.ParseFloat(s["price"], 32)
				assert.NoError(t, err)

				assert.Equal(t, test.listing.Currency, s["currency"])
				assert.Equal(t, test.listing.Price, float32(price))
			}
		})
	}
}

func TestScan(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc string
		keys []string
		err  error
	}{
		{
			desc: "return error on redis Scan",
			keys: []string{},

			err: errors.New("failed Scan"),
		},
		{
			desc: "return all keys on success",
			keys: []string{"scrapers:site1:next", "scrapers:site2:next"},
			err:  nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			mr.RPush(ctx, "books:loop", "1", "2", "3")
			mr.Set(ctx, "scrapers:site1:next", "1", 0*time.Second)
			mr.Set(ctx, "scrapers:site2:next", "1", 0*time.Second)
			assert.Equal(t, int64(1), mr.Exists(ctx, "books:loop").Val())
			assert.Equal(t, int64(1), mr.Exists(ctx, "scrapers:site1:next").Val())
			assert.Equal(t, int64(1), mr.Exists(ctx, "scrapers:site2:next").Val())

			if test.desc == "return error on redis Scan" {
				mr.On("Scan", ctx, uint64(0), "scrapers:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))
			}

			keys, err := r.scan(ctx, mr, "scrapers:*")
			assert.Equal(t, test.err, err)
			assert.Equal(t, test.keys, keys)

			if test.err == nil {
				assert.Equal(t, int64(1), mr.Exists(ctx, "scrapers:site1:next").Val())
				assert.Equal(t, int64(1), mr.Exists(ctx, "scrapers:site2:next").Val())
			}
		})
	}
}

func TestSetBookLoop(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc  string
		books []bliss.Book
		err   error
	}{
		{
			desc:  "handle empty books slice",
			books: nil,
			err:   fmt.Errorf("%w: %s", bliss.ErrEmptySlice, "books"),
		},
		{
			desc:  returnErrorFromDel,
			books: []bliss.Book{{ISBN: "0"}},
			err:   errors.New("failed Del"),
		},
		{
			desc:  "handle error on RPush",
			books: []bliss.Book{{ISBN: "0"}},
			err:   errors.New("failed RPush"),
		},
		{
			desc:  returnNilOnSuccess,
			books: []bliss.Book{{ISBN: "0"}},
			err:   nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			switch test.desc {
			case returnErrorFromDel:
				mr.On("Del", ctx, []string{"books:loop"}).Return(redis.NewIntResult(0, test.err))

			case "handle error on RPush":
				mr.On("Del", ctx, []string{"books:loop"}).Return(redis.NewIntResult(0, nil))
				mr.On("RPush", ctx, "books:loop", []interface{}{[]string{"0"}}).Return(redis.NewIntResult(0, test.err))

			case returnNilOnSuccess:
				mr.On("Del", ctx, []string{"books:loop"}).Return(redis.NewIntResult(0, nil))
				mr.On("RPush", ctx, "books:loop", []interface{}{[]string{"0"}}).Return(redis.NewIntResult(0, nil))
			}

			err := r.setBookLoop(ctx, mr, test.books)
			assert.Equal(t, test.err, err)
		})
	}
}

func TestSetBookMeta(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc string
		book bliss.Book
		err  error
	}{
		{
			desc: "return error on empty book struct",
			book: bliss.Book{},
			err:  fmt.Errorf("%w: %s", bliss.ErrEmptyStruct, "book"),
		},
		{
			desc: returnErrorFromExists,
			book: bliss.Book{ISBN: "0", Format: bliss.HC, Title: "title1"},
			err:  errors.New("failed Exists"),
		},
		{
			desc: returnErrorFromHSet,
			book: bliss.Book{ISBN: "0", Format: bliss.HC, Title: "title1"},
			err:  errors.New("failed HSet"),
		},
		{
			desc: "set new book meta",
			book: bliss.Book{ISBN: "0", Format: bliss.HC, Title: "title1"},
			err:  nil,
		},
		{
			desc: "update existing book meta",
			book: bliss.Book{ISBN: "0", Format: bliss.HC, Title: "title1"},
			err:  nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			assert.Equal(t, int64(0), mr.Exists(ctx, "books:meta:0").Val())

			key := "books:meta:" + test.book.ISBN
			fields := []interface{}{}
			fields = append(fields, "isbn", test.book.ISBN, "title", test.book.Title, "format", test.book.Format.String())

			switch test.desc {
			case returnErrorFromExists:
				mr.On("Exists", ctx, []string{"books:meta:" + test.book.ISBN}).Return(redis.NewIntResult(0, test.err))

			case returnErrorFromHSet:
				mr.On("HSet", ctx, key, fields).Return(redis.NewIntResult(0, test.err))

			case "update existing book meta":
				mr.HSet(ctx, "books:meta:0", "isbn", "0", "title", "wrong", "format", "wrong")
			}

			err := r.SetBookMeta(ctx, mr, test.book)
			assert.Equal(t, test.err, err)

			if test.err == nil {
				assert.Equal(t, int64(1), mr.Exists(ctx, "books:meta:0").Val())
				hash := mr.HGetAll(ctx, "books:meta:0").Val()
				assert.Equal(t, test.book.ISBN, hash["isbn"])
				assert.Equal(t, test.book.Format.String(), hash["format"])
				assert.Equal(t, test.book.Title, hash["title"])
			}
		})
	}
}

func TestSetExchangeRates(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc     string
		exchange *e.Exchange
		err      error
	}{
		{
			desc:     "handle nil exchange pointer",
			exchange: nil,
			err:      fmt.Errorf("%w: %s", bliss.ErrNilPointer, "exchange"),
		},
		{
			desc:     "handle error on set exchange base",
			exchange: &e.Exchange{Base: "EUR"},
			err:      errors.New("set base failed"),
		},
		{
			desc:     "handle error on set exchange date",
			exchange: &e.Exchange{Base: "EUR", Date: "2020-12-12"},
			err:      errors.New("set date failed"),
		},
		{
			desc:     "handle error on set exchange last_updated",
			exchange: &e.Exchange{},
			err:      errors.New("set last_updated failed"),
		},
		{
			desc:     "handle error on set exchange rates",
			exchange: &e.Exchange{Base: "EUR", Date: "2020-12-12", LastUpdated: 1607731202, Rates: map[string]float64{"EUR": 1.0}},
			err:      errors.New("set rates failed"),
		},
		{
			desc:     returnNilOnSuccess,
			exchange: &e.Exchange{Base: "EUR", Date: "2020-12-12", LastUpdated: 1607731202, Rates: map[string]float64{"EUR": 1.0}},
			err:      nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			switch test.desc {
			case "handle error on set exchange base":
				mr.On("Set", ctx, "exchange:base", test.exchange.Base, 0*time.Second).Return(redis.NewStatusResult("", test.err))

			case "handle error on set exchange date":
				mr.On("Set", ctx, "exchange:base", test.exchange.Base, 0*time.Second).Return(redis.NewStatusResult("", nil))
				mr.On("Set", ctx, "exchange:date", test.exchange.Date, 0*time.Second).Return(redis.NewStatusResult("", test.err))

			case "handle error on set exchange last_updated":
				mr.On("Set", ctx, "exchange:base", test.exchange.Base, 0*time.Second).Return(redis.NewStatusResult("", nil))
				mr.On("Set", ctx, "exchange:date", test.exchange.Date, 0*time.Second).Return(redis.NewStatusResult("", nil))
				mr.On("Set", ctx, "exchange:last_updated", test.exchange.LastUpdated, 0*time.Second).Return(redis.NewStatusResult("", test.err))

			case "handle error on set exchange rates":
				mr.On("Set", ctx, "exchange:base", test.exchange.Base, 0*time.Second).Return(redis.NewStatusResult("", nil))
				mr.On("Set", ctx, "exchange:date", test.exchange.Date, 0*time.Second).Return(redis.NewStatusResult("", nil))
				mr.On("Set", ctx, "exchange:last_updated", test.exchange.LastUpdated, 0*time.Second).Return(redis.NewStatusResult("", nil))
				rates := []interface{}{"EUR", fmt.Sprintf("%f", test.exchange.Rates["EUR"])}
				mr.On("HSet", ctx, "exchange:rates", rates).Return(redis.NewIntResult(0, test.err))

			case returnNilOnSuccess:
			}
			err := r.SetExchangeRates(ctx, mr, test.exchange)
			assert.Equal(t, test.err, err)
		})
	}
}

func TestSetLowestListings(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc     string
		listing  bliss.Listing
		expected map[string]string
		err      error
	}{
		{
			desc:    returnErrorFromHSet,
			listing: bliss.Listing{Book: bliss.Book{ISBN: "0"}, Price: 1, Site: "site1"},
			err:     errors.New("failed HSet"),
		},
		{
			desc:     returnNilOnSuccess,
			listing:  bliss.Listing{Book: bliss.Book{ISBN: "0"}, Price: 1, Site: "site1"},
			expected: map[string]string{"currency": "", "format": "NA", "isbn": "0", "price": "1", "site": "site1", "title": ""},
			err:      nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			key := "listings:lowest:" + test.listing.Book.ISBN

			switch test.desc {
			case returnErrorFromHSet:
				mr.On("HSet", ctx, key,
					[]interface{}{
						"currency", test.listing.Currency,
						"price", test.listing.Price,
						"site", test.listing.Site,
						"format", test.listing.Book.Format.String(),
						"isbn", test.listing.Book.ISBN,
						"title", test.listing.Book.Title}).Return(redis.NewIntResult(0, test.err))

			case returnNilOnSuccess:
			}

			err := r.SetLowestListings(ctx, mr, test.listing)
			assert.Equal(t, test.err, err)

			if test.err == nil {
				hash, err := mr.HGetAll(ctx, key).Result()
				assert.NoError(t, err)
				assert.Equal(t, test.expected, hash)
			}
		})
	}
}

func TestSetSiteListings(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc    string
		listing bliss.Listing
		err     error
	}{
		{
			desc:    "return error on empty listing",
			listing: bliss.Listing{},
			err:     fmt.Errorf("%w: %s", bliss.ErrEmptyStruct, "listing"),
		},
		{
			desc:    returnErrorFromHSet,
			listing: bliss.Listing{Book: bliss.Book{ISBN: "0"}, Currency: "EUR", Price: 1, Site: "site1"},
			err:     errors.New("failed HSet"),
		},
		{
			desc:    returnNilOnSuccess,
			listing: bliss.Listing{Book: bliss.Book{ISBN: "0"}, Currency: "EUR", Price: 1, Site: "site1"},
			err:     nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			switch test.desc {
			case returnErrorFromHSet:
				mr.On("HSet", ctx, "listings:site:site1:0",
					[]interface{}{
						"currency", test.listing.Currency,
						"price", test.listing.Price,
						"site", test.listing.Site,
						"format", test.listing.Book.Format.String(),
						"isbn", test.listing.Book.ISBN,
						"title", test.listing.Book.Title}).Return(redis.NewIntResult(0, test.err))
			default:
			}

			err := r.SetSiteListings(ctx, mr, test.listing)
			assert.Equal(t, test.err, err)

			if test.err == nil {
				s, err := mr.HGetAll(ctx, "listings:site:site1:0").Result()
				assert.NoError(t, err)

				price, err := strconv.ParseFloat(s["price"], 32)
				assert.NoError(t, err)

				assert.Equal(t, test.listing.Currency, s["currency"])
				assert.Equal(t, test.listing.Price, float32(price))
			}
		})
	}
}

func TestSetSites(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc  string
		sites []string
		err   error
	}{
		{
			desc:  "return error on empty scrapers map",
			sites: []string{},
			err:   fmt.Errorf("%w: %s", bliss.ErrEmptyMap, "scrapers"),
		},
		{
			desc:  returnErrorFromExists,
			sites: []string{"site1"},
			err:   errors.New("failed Exists"),
		},
		{
			desc:  "return error from redis LIndex",
			sites: []string{"site1"},
			err:   errors.New("failed LIndex"),
		},
		{
			desc:  returnErrorFromSet,
			sites: []string{"site1"},
			err:   errors.New("failed Set"),
		},
		{
			desc:  "set sites on success",
			sites: []string{"site1", "site2"},
			err:   nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			mr.Set(ctx, "scrapers:site2:next", "1", 0*time.Second)
			mr.RPush(ctx, "books:loop", "1", "2", "3")
			assert.Equal(t, int64(1), mr.Exists(ctx, "books:loop").Val())
			assert.Equal(t, int64(0), mr.Exists(ctx, "scrapers:site1:next").Val())
			assert.Equal(t, int64(1), mr.Exists(ctx, "scrapers:site2:next").Val())

			switch test.desc {
			case returnErrorFromExists:
				mr.On("Exists", ctx, []string{"scrapers:site1:next"}).Return(redis.NewIntResult(0, test.err))

			case "return error from redis LIndex":
				mr.On("LIndex", ctx, "books:loop", int64(0)).Return(redis.NewStringResult("", test.err))

			case returnErrorFromSet:
				mr.On("Set", ctx, "scrapers:site1:next", "1", 0*time.Second).Return(redis.NewStatusResult("", test.err))
			}

			err := r.setSites(ctx, mr, test.sites)
			assert.Equal(t, test.err, err)

			if test.err == nil {
				assert.Equal(t, int64(1), mr.Exists(ctx, "scrapers:site1:next").Val())
				assert.Equal(t, int64(1), mr.Exists(ctx, "scrapers:site2:next").Val())
			}
		})
	}
}

func TestUpdateBooks(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc  string
		books []bliss.Book
		err   error
	}{
		{
			desc:  "return error on empty books slice",
			books: []bliss.Book{},
			err:   fmt.Errorf("%w: %s", bliss.ErrEmptySlice, "books"),
		},
		{
			desc:  "return error from setBookLoop",
			books: []bliss.Book{{ISBN: "0"}},
			err:   errors.New("failed Del"),
		},
		{
			desc:  "return error from SetBookMeta",
			books: []bliss.Book{{ISBN: "0"}},
			err:   errors.New("failed HSet"),
		},
		{
			desc:  "return error from pruneListingsByBook",
			books: []bliss.Book{{ISBN: "0"}},
			err:   errors.New("failed Scan"),
		},
		{
			desc:  "return error from pruneBookMeta",
			books: []bliss.Book{{ISBN: "0"}},
			err:   errors.New("failed Scan"),
		},
		{
			desc:  returnNilOnSuccess,
			books: []bliss.Book{{ISBN: "0"}},
			err:   nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			switch test.desc {
			case "return error on empty books slice":

			case "return error from setBookLoop":
				mr.On("Del", ctx, []string{"books:loop"}).Return(redis.NewIntResult(0, test.err))

			case "return error from SetBookMeta":
				mr.On("HSet", ctx, "books:meta:0", []interface{}{"isbn", "0", "title", "", "format", "NA"}).Return(redis.NewIntResult(0, test.err))

			case "return error from pruneListingsByBook":
				mr.On("Scan", ctx, uint64(0), "listings:site:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case "return error from pruneBookMeta":
				mr.On("Scan", ctx, uint64(0), "listings:site:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), nil))
				mr.On("Scan", ctx, uint64(0), "listings:lowest:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), nil))
				mr.On("Scan", ctx, uint64(0), "books:meta:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case returnNilOnSuccess:
			}

			err := r.UpdateBooks(ctx, mr, test.books...)
			assert.Equal(t, test.err, err)
		})
	}
}

func TestUpdateHistory(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc    string
		isbn    string
		site    string
		loop    []string
		history []string

		err error
	}{
		{
			desc: "return error on empty isbn",
			loop: []string{},
			err:  fmt.Errorf("%w: %s", bliss.ErrEmptyString, "isbn"),
		},
		{
			desc: "return error on empty site",
			loop: []string{},
			isbn: "0",
			err:  fmt.Errorf("%w: %s", bliss.ErrEmptyString, "site"),
		},
		{
			desc: "handle error on lpush",
			loop: []string{},
			isbn: "0",
			site: "site",
			err:  errors.New("failed LPush"),
		},
		{
			desc: "handle error on llen",
			loop: []string{"0"},
			isbn: "0",
			site: "site",
			err:  errors.New("failed LLen"),
		},
		{
			desc:    "handle error on ltrim",
			loop:    []string{"0"},
			history: []string{"0", "1", "2"},
			isbn:    "0",
			site:    "site",
			err:     errors.New("failed LTrim"),
		},
		{
			desc:    returnNilOnSuccess,
			loop:    []string{"0"},
			history: []string{"0", "1", "2"},
			isbn:    "0",
			site:    "site",
			err:     nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			if len(test.loop) != 0 {
				_, err := mr.RPush(ctx, "books:loop", test.loop).Result()
				assert.NoError(t, err)
			}

			if len(test.history) != 0 {
				_, err := mr.RPush(ctx, "scrapers:site:history", test.history).Result()
				assert.NoError(t, err)
			}

			switch test.desc {
			case "handle error on lpush":
				mr.On("LPush", ctx, "scrapers:site:history", []interface{}{test.isbn}).Return(redis.NewIntResult(-1, test.err))

			case "handle error on llen":
				mr.On("LLen", ctx, "books:loop").Return(redis.NewIntResult(int64(-1), test.err))

			case "handle error on ltrim":
				mr.On("LTrim", ctx, "scrapers:site:history", int64(0), int64(len(test.loop)-1)).Return(redis.NewStatusResult("", test.err))

			case returnNilOnSuccess:
				mr.On("LTrim", ctx, "scrapers:site:history", int64(0), int64(len(test.loop)-1)).Return(redis.NewStatusResult("", nil))
			}

			err := r.updateHistory(ctx, mr, test.isbn, test.site)
			assert.Equal(t, test.err, err)
		})
	}
}

func TestUpdateLowestListing(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc      string
		new       bliss.Listing
		low       bliss.Listing
		other     bliss.Listing
		isDeleted bool
		isDrop    bool
		err       error
	}{
		{
			desc: "return error on empty listing struct",
			new:  bliss.Listing{},
			err:  fmt.Errorf("%w: %s", bliss.ErrEmptyStruct, "newListing"),
		},
		{
			desc: "return error from GetLowestListings",
			new:  bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 1, Site: "site1"},
			err:  errors.New("failed GetLowestListings"),
		},
		{
			desc:      "return no drop on new 'not found' listing with no previous lowest",
			new:       bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: bliss.NotFound, Site: "site1"},
			low:       bliss.Listing{},
			isDeleted: false,
			isDrop:    false,
			err:       nil,
		},
		{
			desc:      "return no drop on new listing with no previous lowest",
			new:       bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 1, Site: "site1"},
			low:       bliss.Listing{},
			isDeleted: false,
			isDrop:    false,
			err:       nil,
		},
		{
			desc:      "return no drop on same new and old price",
			new:       bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 1, Site: "site1"},
			low:       bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 1, Site: "site1"},
			other:     bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 1, Site: "site1"},
			isDeleted: false,
			isDrop:    false,
			err:       nil,
		},
		{
			desc:      "return drop on new low",
			new:       bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 1, Site: "site1"},
			low:       bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 2, Site: "site1"},
			isDeleted: false,
			isDrop:    true,
			err:       nil,
		},
		{
			desc:      "return no drop on lowest listing price increase, no next low",
			new:       bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 2, Site: "site1"},
			low:       bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 1, Site: "site1"},
			other:     bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: bliss.NotFound, Site: "site2"},
			isDeleted: false,
			isDrop:    false,
			err:       nil,
		},
		{
			desc:      "return no drop on lowest listing price increase, still lowest",
			new:       bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 2, Site: "site1"},
			low:       bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 1, Site: "site1"},
			other:     bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 3, Site: "site2"},
			isDeleted: false,
			isDrop:    false,
			err:       nil,
		},
		{
			desc:      "return no drop on lowest listing price increase, no longer lowest",
			new:       bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 3, Site: "site1"},
			low:       bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 1, Site: "site1"},
			other:     bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 2, Site: "site2"},
			isDeleted: false,
			isDrop:    false,
			err:       nil,
		},
		{
			desc:      "return no drop on lowest price unlisted and replaced by next low",
			new:       bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: bliss.NotFound, Site: "site1"},
			low:       bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 1, Site: "site1"},
			other:     bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 2, Site: "site2"},
			isDeleted: false,
			isDrop:    false,
			err:       nil,
		},
		{
			desc:      "return no drop on lowest price unlisted and deleted",
			new:       bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: bliss.NotFound, Site: "site1"},
			low:       bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 1, Site: "site1"},
			other:     bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 1, Site: "site1"},
			isDeleted: true,
			isDrop:    false,
			err:       nil,
		},
		{
			desc:      "return no drop on new high listing from other site",
			new:       bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: bliss.NotFound, Site: "site2"},
			low:       bliss.Listing{Book: bliss.Book{ISBN: "1"}, Price: 1, Site: "site1"},
			isDeleted: false,
			isDrop:    false,
			err:       nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			testdata := []bliss.Listing{}

			if test.other != (bliss.Listing{}) {
				testdata = append(testdata, test.other)
			}

			if test.low != (bliss.Listing{}) {
				mr.HSet(ctx, fmt.Sprintf("listings:lowest:%s", test.low.Book.ISBN),
					"isbn", test.low.Book.ISBN,
					"price", test.low.Price,
					"site", test.low.Site,
				)
			}

			for _, td := range testdata {
				mr.Set(ctx, fmt.Sprintf("scrapers:%s:next", td.Site), td.Book.ISBN, 0*time.Second)
				mr.HSet(ctx, fmt.Sprintf("listings:site:%s:%s", td.Site, td.Book.ISBN),
					"isbn", td.Book.ISBN,
					"price", td.Price,
					"site", td.Site,
				)
			}

			switch test.desc {
			case returnErrorOnGetSiteListings:
				mr.On("Scan", ctx, uint64(0), "listings:site:site1:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case "return error from GetLowestListings":
				mr.On("Scan", ctx, uint64(0), "listings:site:site1:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), nil))
				mr.On("Scan", ctx, uint64(0), "listings:lowest:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))
			}

			isDeleted, isDrop, err := r.updateLowestListing(ctx, mr, test.new)
			assert.Equal(t, test.err, err)
			assert.Equal(t, test.isDeleted, isDeleted)
			assert.Equal(t, test.isDrop, isDrop)
		})
	}
}

func TestUpdateNext(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc    string
		last    string
		next    string
		loop    []string
		history []string
		err     error
	}{
		{
			desc: "return error on empty loop",
			loop: []string{},
			last: "0000000000000",
			err:  errors.New("expected redis key type 'list', found ''"),
		},
		{
			desc: "last isbn not in loop, no history fallback, success",
			loop: []string{"1", "2", "3"},
			last: "0",
			next: "1",
			err:  nil,
		},
		{
			desc: "last isbn not in loop, no history fallback, error on set",
			loop: []string{"1", "2", "3"},
			last: "0",
			next: "1",
			err:  errors.New("failed Set"),
		},
		{
			desc:    "last isbn not in loop, return next from history with wrapping",
			loop:    []string{"1", "2", "3"},
			history: []string{"4", "5", "3"},
			last:    "0",
			next:    "1",
			err:     nil,
		},
		{
			desc:    "last isbn not in loop, return next from history, error on set",
			loop:    []string{"1", "2", "3"},
			history: []string{"4", "5", "2"},
			last:    "0",
			next:    "3",
			err:     errors.New("failed Set"),
		},
		{
			desc: "isbn found in loop, no wrapping for next item",
			loop: []string{"0000000000001", "0000000000002", "0000000000003"},
			last: "0000000000001",
			next: "0000000000002",
			err:  nil,
		},
		{
			desc: "isbn found in loop, wrap to top of loop for next item",
			loop: []string{"0000000000001", "0000000000002", "0000000000003"},
			last: "0000000000003",
			next: "0000000000001",
			err:  nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			if len(test.loop) != 0 {
				_, err := mr.RPush(ctx, "books:loop", test.loop).Result()
				assert.NoError(t, err)
			}

			if len(test.history) != 0 {
				_, err := mr.RPush(ctx, "scrapers:site:history", test.history).Result()
				assert.NoError(t, err)
			}

			switch test.desc {
			case "return error on empty loop":
				mr.On("Type", ctx, "books:loop").Return(redis.NewStatusResult("", test.err))

			case "last isbn not in loop, no history fallback, success":

			case "last isbn not in loop, no history fallback, error on set":
				mr.On("Set", ctx, "scrapers:site:next", test.next, 0*time.Second).Return(redis.NewStatusResult("1", test.err))

			case "last isbn not in loop, return next from history with wrapping":

			case "last isbn not in loop, return next from history, error on set":
				mr.On("Set", ctx, "scrapers:site:next", test.next, 0*time.Second).Return(redis.NewStatusResult("1", test.err))

			case "isbn found in loop, wrap to top of loop for next item":
				mr.On("LPos", ctx, "books:loop", test.last, redis.LPosArgs{}).Return(redis.NewIntResult(2, nil))

			case "isbn found in loop, no wrapping for next item":
				mr.On("LPos", ctx, "books:loop", test.last, redis.LPosArgs{}).Return(redis.NewIntResult(0, nil))
			}

			next, err := r.updateNext(ctx, mr, test.last, "site")
			assert.Equal(t, test.err, err)

			switch test.err {
			case nil:
				assert.Equal(t, test.next, next)
			default:
				assert.Empty(t, next)
			}
		})
	}
}

func TestUpdateNotification(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc     string
		message  string
		hash     string
		populate bool
		updated  bool
		err      error
	}{
		{
			desc: "return error if message is empty",
			err:  fmt.Errorf("%w: %s", bliss.ErrEmptyString, "message"),
		},
		{
			desc:     "return false if hash already exists",
			populate: true,
			updated:  false,
			message:  "msg",
			hash:     "19f34ee1e406ea84ca83c835a3301b5d9014a788",
			err:      nil,
		},
		{
			desc:     returnErrorFromExists,
			updated:  false,
			populate: true,
			hash:     "19f34ee1e406ea84ca83c835a3301b5d9014a788",
			message:  "msg",
			err:      errors.New("failed to check hash existence"),
		},
		{
			desc:     returnErrorFromSet,
			updated:  false,
			populate: false,
			hash:     "19f34ee1e406ea84ca83c835a3301b5d9014a788",
			message:  "msg",
			err:      errors.New("failed to set notification hash"),
		},
		{
			desc:     "successfully set notification hash",
			updated:  true,
			populate: false,
			hash:     "19f34ee1e406ea84ca83c835a3301b5d9014a788",
			message:  "msg",
			err:      nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			if test.populate {
				_, err := mr.Set(ctx, "notifications:sent:"+test.hash, "", 7*24*time.Hour).Result()
				assert.NoError(t, err)
			}

			switch test.desc {
			case returnErrorFromExists:
				mr.On("Exists", ctx, []string{"notifications:sent:" + test.hash}).Return(redis.NewIntResult(0, test.err))
			case returnErrorFromSet:
				mr.On("Set", ctx, "notifications:sent:"+test.hash, "", 7*24*time.Hour).Return(redis.NewStatusResult("", test.err))
			}

			updated, err := r.UpdateNotification(ctx, mr, test.message)

			if test.desc == "successfully set notification hash" {
				hash := mr.Get(ctx, "notifications:sent:"+test.hash).Val()
				assert.Empty(t, hash)
			}

			assert.Equal(t, test.err, err)
			assert.Equal(t, test.updated, updated)
		})
	}
}

func TestUpdatePriorityLists(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc   string
		logger *zap.SugaredLogger
		err    error
	}{
		{
			desc: "return error from Scan priority",
			err:  errors.New("failed Scan"),
		},
		{
			desc: returnErrorFromDel,
			err:  errors.New("failed Del"),
		},
		{
			desc: "return error from LRange",
			err:  errors.New("failed LRange"),
		},
		{
			desc: "return error from Exists",
			err:  errors.New("failed Exists"),
		},
		{
			desc: "return error from RPush",
			err:  errors.New("failed RPush"),
		},
		{
			desc: returnNilOnSuccess,
			err:  nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			// populate test data
			priority := []string{"1", "2", "3"}
			_, err := mr.RPush(ctx, "scrapers:site1:priority", []string{"1"}).Result()
			assert.NoError(t, err)
			_, err = mr.RPush(ctx, "scrapers:site2:priority", []string{"2"}).Result()
			assert.NoError(t, err)
			_, err = mr.Set(ctx, "scrapers:site1:next", "2", 0*time.Second).Result()
			assert.NoError(t, err)
			_, err = mr.Set(ctx, "scrapers:site2:next", "1", 0*time.Second).Result()
			assert.NoError(t, err)
			_, err = mr.Set(ctx, "listings:site:site1:1", "key", 0*time.Second).Result()
			assert.NoError(t, err)
			_, err = mr.Set(ctx, "listings:site:site2:2", "key", 0*time.Second).Result()
			assert.NoError(t, err)
			_, err = mr.RPush(ctx, "books:loop", priority).Result()
			assert.NoError(t, err)

			switch test.desc {
			case "return error from Scan priority":
				mr.On("Scan", ctx, uint64(0), "scrapers:*:priority", int64(0)).
					Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case returnErrorFromDel:
				mr.On("Del", ctx, []string{"scrapers:site1:priority", "scrapers:site2:priority"}).
					Return(redis.NewIntResult(int64(0), test.err))

			case "return error from Scan next":
				mr.On("Scan", ctx, uint64(0), "scrapers:*:priority", int64(0)).
					Return(redis.NewScanCmdResult([]string{"scrapers:site1:priority", "scrapers:site2:priority"}, 0, nil))
				mr.On("Scan", ctx, uint64(0), "scrapers:*:next", int64(0)).Maybe().
					Return(redis.NewScanCmdResult([]string{}, 0, test.err))

			case "return error from LRange":
				mr.On("LRange", ctx, "books:loop", int64(0), int64(-1)).Return(redis.NewStringSliceResult([]string{}, test.err))

			case "return error from Exists":
				listings := []string{"listings:site:site1:1", "listings:site:site2:1"}
				mr.On("Exists", ctx, listings).Return(redis.NewIntResult(0, test.err))

			case "return error from RPush":
				mr.On("RPush", ctx, "scrapers:site1:priority", []interface{}{[]string{"1", "2", "3"}}).
					Return(redis.NewIntResult(0, test.err))

			case returnNilOnSuccess:
			}

			sites := []string{"site1", "site2"}
			err = r.updatePriorityLists(ctx, mr, sites)
			assert.Equal(t, test.err, err)

			if test.err == nil {
				p1 := mr.LRange(ctx, "scrapers:site1:priority", 0, -1).Val()
				p2 := mr.LRange(ctx, "scrapers:site2:priority", 0, -1).Val()
				assert.Equal(t, priority, p1)
				assert.Equal(t, priority, p2)
			}
		})
	}
}

func TestUpdateInfluxDBRetention(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc      string
		logger    *zap.SugaredLogger
		err       error
		newPolicy string
		oldPolicy string
		stored    string
		updated   bool
	}{
		{
			desc:      "return error on empty policy",
			newPolicy: "",
			oldPolicy: "",
			updated:   false,
			err:       fmt.Errorf("%w: %s", bliss.ErrEmptyString, "newPolicy"),
		},
		{
			desc:      "return error from redis Get",
			newPolicy: "one_week",
			oldPolicy: "",
			updated:   false,
			err:       errors.New("failed Get"),
		},
		{
			desc:      returnErrorFromSet,
			stored:    "one_day",
			newPolicy: "one_week",
			oldPolicy: "",
			updated:   false,
			err:       errors.New("failed Set"),
		},
		{
			desc:      "return false on same policy as stored",
			stored:    "one_week",
			newPolicy: "one_week",
			oldPolicy: "",
			updated:   false,
			err:       nil,
		},
		{
			desc:      "return true on new policy",
			stored:    "one_day",
			newPolicy: "one_week",
			oldPolicy: "one_day",
			updated:   true,
			err:       nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			_, err := mr.Set(ctx, "influxdb:retention", test.stored, 0*time.Second).Result()
			assert.NoError(t, err)

			switch test.desc {
			case "return error from redis Get":
				mr.On("Get", ctx, "influxdb:retention").Return(redis.NewStringResult("", test.err))

			case returnErrorFromSet:
				mr.On("Set", ctx, "influxdb:retention", test.newPolicy, 0*time.Second).Return(redis.NewStatusResult("", test.err))
			}

			oldPolicy, updated, err := r.UpdateInfluxDBRetention(ctx, mr, test.newPolicy, lg)
			assert.Equal(t, test.oldPolicy, oldPolicy)
			assert.Equal(t, test.updated, updated)
			assert.Equal(t, test.err, err)
		})
	}
}

func TestUpdateSites(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc     string
		scrapers []string
		err      error
	}{
		{
			desc:     "return error on empty scrapers map",
			scrapers: []string{},
			err:      fmt.Errorf("%w: %s", bliss.ErrEmptyMap, "scrapers"),
		},
		{
			desc:     "return error from setScrapers",
			scrapers: []string{"site1"},
			err:      errors.New("failed setScrapers"),
		},
		{
			desc:     "return error from pruneSites",
			scrapers: []string{"site1"},
			err:      errors.New("failed pruneSites"),
		},
		{
			desc:     "return error from pruneListingsBySite",
			scrapers: []string{"site1"},
			err:      errors.New("failed pruneListingsBySite"),
		},
		{
			desc:     "return error from updatePriorityLists",
			scrapers: []string{"site1"},
			err:      errors.New("failed pruneListingsBySite"),
		},
		{
			desc:     returnNilOnSuccess,
			scrapers: []string{"site1"},
			err:      nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			r := Redis{lg: lg}
			mr := newMockRedis()
			ctx := context.Background()

			scrapers := []string{"site1", "site2", "site3"}
			for _, s := range scrapers {
				key := fmt.Sprintf("scrapers:%s:next", s)
				_, err := mr.Set(ctx, key, "0", 0*time.Second).Result()
				assert.NoError(t, err)
			}

			_, err := mr.RPush(ctx, "books:loop", "0").Result()
			assert.NoError(t, err)

			switch test.desc {
			case "return error on empty scrapers map":
			case "return error from setScrapers":
				mr.On("Exists", ctx, []string{"scrapers:site1:next"}).Return(redis.NewIntResult(0, test.err))

			case "return error from pruneSites":
				mr.On("Scan", ctx, uint64(0), "scrapers:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case "return error from pruneListingsBySite":
				mr.On("Scan", ctx, uint64(0), "scrapers:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), nil))
				mr.On("Scan", ctx, uint64(0), "listings:site:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case "return error from updatePriorityLists":
				mr.On("Scan", ctx, uint64(0), "scrapers:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), nil))
				mr.On("Scan", ctx, uint64(0), "listings:site:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), nil))
				mr.On("Scan", ctx, uint64(0), "listings:lowest:*", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), nil))
				mr.On("Scan", ctx, uint64(0), "scrapers:*:priority", int64(0)).Return(redis.NewScanCmdResult([]string{}, uint64(0), test.err))

			case returnNilOnSuccess:
			}

			err = r.UpdateSites(ctx, mr, test.scrapers)
			assert.Equal(t, test.err, err)
		})
	}
}

func TestStringSliceToMap(t *testing.T) {
	tt := []struct {
		desc     string
		slice    []string
		expected map[string]int
		err      error
	}{
		{
			desc:     "return empty map from empty slice",
			slice:    []string{},
			expected: map[string]int{},
			err:      nil,
		},
		{
			desc:     "return map with string and index from slice",
			slice:    []string{"site0", "site1"},
			expected: map[string]int{"site0": 0, "site1": 1},
			err:      nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			assert.Equal(t, test.expected, stringSliceToMap(test.slice))
		})
	}
}

func newMockRedis() *redismock.ClientMock {
	mr, err := miniredis.Run()
	if err != nil {
		panic(err)
	}
	client := redis.NewClient(&redis.Options{
		Addr: mr.Addr(),
	})
	return redismock.NewNiceMock(client)
}
