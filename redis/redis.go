package redis

import (
	"bliss"
	e "bliss/exchange"
	"context"
	"crypto/sha1"
	"errors"
	"fmt"
	"math"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/gregdel/pushover"
	"go.uber.org/zap"
)

// Config holds configuration parameters for redis
type Config struct {
	Address  string
	Database int
	Password string
}

// Redis runs queries against redis
type Redis struct {
	Client *redis.Client
	lg     *zap.SugaredLogger
}

// NewRedis constructs a new Redis struct
func NewRedis(config Config, lg *zap.SugaredLogger) (Redis, error) {
	if lg == nil {
		return Redis{}, fmt.Errorf("%w: %s", bliss.ErrNilPointer, "lg")
	}

	client := redis.NewClient(&redis.Options{
		Addr:     config.Address,
		Password: config.Password,
		DB:       config.Database,
	})

	r := Redis{
		lg:     lg,
		Client: client,
	}
	return r, nil
}

func (r *Redis) assertKeyType(ctx context.Context, client redis.Cmdable, key, keyType string) error {
	t, err := client.Type(ctx, key).Result()
	if err != nil {
		return err
	}

	if t != keyType {
		return fmt.Errorf("expected redis key type '%s', found '%s'", keyType, t)
	}
	return nil
}

// CheckBaseline checks if all scrapers have scraped all isbns at least once and sends baseline/startup status notifications
// returns baseline true if baseline is established
// returns startup false after first successful run
func (r *Redis) CheckBaseline(
	ctx context.Context,
	client redis.Cmdable,
	baseline, startup bool,
	interval int,
	notifyCh chan<- *pushover.Message,
	lg *zap.SugaredLogger,
) (
	retBaseline, retStartup bool, err error,
) {
	if notifyCh == nil {
		return baseline, startup, fmt.Errorf("%w: %s", bliss.ErrNilPointer, "notifyCh")
	}

	if baseline {
		return baseline, startup, nil
	}

	var (
		baselineTotal, baselineScraped int
		baselineETA                    string
		missing                        = map[string]struct{}{}
	)

	meta, err := r.GetBookMeta(ctx, client)
	if err != nil {
		lg.Error(err)
		return baseline, startup, err
	}

	sites, err := r.GetSites(ctx, client)
	if err != nil {
		lg.Error(err)
		return baseline, startup, err
	}

	baselineTotal = len(meta) * len(sites)

	for _, s := range sites {
		siteListings, err := r.GetSiteListings(ctx, client, s)
		if err != nil {
			return baseline, startup, err
		}

		baselineScraped += len(siteListings)

		for isbn := range meta {
			if _, exists := siteListings[isbn]; !exists {
				missing[isbn] = struct{}{}
			}
		}
	}

	if progress := float64(baselineScraped) / float64(baselineTotal); progress < 1.0 {
		baselineETAMinutes := interval * len(missing)
		baselineETASplitHours := baselineETAMinutes / 60
		baselineETASplitMinutes := baselineETAMinutes % 60
		baselineETA = fmt.Sprintf("%vh%vm", baselineETASplitHours, baselineETASplitMinutes)

		lg.Debugf("baseline progress: %.0f%%", math.Floor(progress*float64(100)))
		if startup {
			notifyCh <- &pushover.Message{
				Title: "Book Monitoring Started",
				Message: fmt.Sprintf("Event notifications are disabled until baseline listings "+
					"have been established for all books.\nBaseline will be ready within %s", baselineETA),
				Priority: pushover.PriorityNormal,
			}
		}
		return false, false, nil
	}

	if startup {
		notifyCh <- &pushover.Message{
			Title:    "Book Monitoring Started",
			Message:  "Baseline listings are established\nEvent notifications are enabled",
			Priority: pushover.PriorityNormal,
		}
		lg.Info("baseline listings established, notifications are now enabled")
		return true, false, nil
	}

	notifyCh <- &pushover.Message{
		Title:    "Event Notifications Enabled",
		Message:  "Baseline listings have been established",
		Priority: pushover.PriorityNormal,
	}
	lg.Info("baseline listings established, notifications are now enabled")
	return true, false, nil
}

// DeleteLowestListing deleted a lowest price listing from Redis
func (r *Redis) DeleteLowestListing(ctx context.Context, client redis.Cmdable, isbn string) error {
	_, err := client.Del(ctx, "listings:lowest:"+isbn).Result()
	return err
}

// GetBookMeta gets book metadata from Redis
// returns all book meta if specific isbns are not passed
func (r *Redis) GetBookMeta(ctx context.Context, client redis.Cmdable, isbns ...string) (map[string]bliss.Book, error) {
	var keys []string

	if len(isbns) == 0 {
		var err error
		keys, err = r.scan(ctx, client, "books:meta:*")
		if err != nil {
			return nil, err
		}
	}

	if len(isbns) != 0 {
		for _, isbn := range isbns {
			keys = append(keys, "books:meta:"+isbn)
		}
	}

	meta := map[string]bliss.Book{}
	for _, key := range keys {
		book, err := client.HGetAll(ctx, key).Result()
		if err != nil {
			return nil, err
		}

		meta[book["isbn"]] = bliss.Book{
			ISBN:   book["isbn"],
			Format: bliss.Formats[book["format"]],
			Title:  book["title"],
		}
	}

	return meta, nil
}

// GetExchange gets exchange rates from Redis
func (r *Redis) GetExchange(ctx context.Context, client redis.Cmdable) (*e.Exchange, error) {
	base, err := client.Get(ctx, "exchange:base").Result()
	if err != nil {
		return nil, err
	}

	date, err := client.Get(ctx, "exchange:date").Result()
	if err != nil {
		return nil, err
	}

	lastUpdatedStr, err := client.Get(ctx, "exchange:last_updated").Result()
	if err != nil {
		return nil, err
	}

	ratesMap, err := client.HGetAll(ctx, "exchange:rates").Result()
	if err != nil {
		return nil, err
	}

	lastUpdated, err := strconv.Atoi(lastUpdatedStr)
	if err != nil {
		return nil, err
	}

	rates := map[string]float64{}
	for currency, rate := range ratesMap {
		bitSize := 32
		rate, err := strconv.ParseFloat(rate, bitSize)
		if err != nil {
			return nil, err
		}
		rates[currency] = rate
	}

	exchange := &e.Exchange{
		Base:        base,
		Rates:       rates,
		Date:        date,
		LastUpdated: int32(lastUpdated),
	}

	return exchange, nil
}

// GetListings gets price listings for all sites from Redis
// Returns map[site]map[isbn]Listing
func (r *Redis) GetListings(ctx context.Context, client redis.Cmdable) (map[string]map[string]bliss.Listing, error) {
	listings := map[string]map[string]bliss.Listing{}

	sites, err := r.GetSites(ctx, client)
	if err != nil {
		r.lg.Error(err)
		return nil, err
	}

	for _, site := range sites {
		siteListings, err := r.GetSiteListings(ctx, client, site)
		if err != nil {
			r.lg.Error(err)
			return nil, err
		}
		listings[site] = siteListings
	}

	return listings, nil
}

// GetLowestListings gets lowest price listings from Redis
func (r *Redis) GetLowestListings(ctx context.Context, client redis.Cmdable) (map[string]bliss.Listing, error) {
	keys, err := r.scan(ctx, client, "listings:lowest:*")
	if err != nil {
		return nil, err
	}

	lowest := map[string]bliss.Listing{}
	for _, key := range keys {
		listing, err := client.HGetAll(ctx, key).Result()
		if err != nil {
			return nil, err
		}

		bitSize := 32
		price, err := strconv.ParseFloat(listing["price"], bitSize)
		if err != nil {
			return nil, err
		}

		lowest[listing["isbn"]] = bliss.Listing{
			Book: bliss.Book{
				ISBN:   listing["isbn"],
				Format: bliss.Formats[listing["format"]],
				Title:  listing["title"],
			},
			Currency: listing["currency"],
			Price:    float32(price),
			Site:     listing["site"],
		}
	}
	return lowest, nil
}

// GetNextISBN gets the next ISBN to be scraped by site from Redis
// returns true if the returned ISBN is a prioritized item
func (r *Redis) GetNextISBN(ctx context.Context, client redis.Cmdable, site string) (next string, priority bool, err error) {
	priority = true
	next = ""
	previous, err := client.Get(ctx, "scrapers:"+site+":next").Result()
	if err != nil && err != redis.Nil {
		return "", false, err
	}

	next, err = client.LPop(ctx, "scrapers:"+site+":priority").Result()
	if err != nil && err != redis.Nil {
		return "", false, err
	}

	// item was popped from priority list
	if err == nil {
		return next, priority, nil
	}

	// priority list was empty
	if err == redis.Nil {
		priority = false
	}

	// check that isbn is in books:meta, if not then do updateNext to find new next
	exists, err := client.Exists(ctx, "books:meta:"+previous).Result()
	if err != nil {
		return "", priority, err
	}

	if exists == 0 {
		// site has invalid "next" isbn, update it
		next, err = r.updateNext(ctx, client, previous, site)
		if err != nil {
			return "", priority, err
		}
	}

	if exists == 1 {
		next = previous
	}

	return next, priority, nil
}

// GetSites gets active sites from Redis
func (r *Redis) GetSites(ctx context.Context, client redis.Cmdable) ([]string, error) {
	keys, err := r.scan(ctx, client, "scrapers:*")
	if err != nil {
		return nil, err
	}

	deduplicated := map[string]struct{}{}
	for _, k := range keys {
		site := strings.Split(k, ":")[1]
		deduplicated[site] = struct{}{}
	}

	scrapers := []string{}
	for k := range deduplicated {
		scrapers = append(scrapers, k)
	}
	sort.Strings(scrapers)

	return scrapers, nil
}

// GetSiteListings gets price listings for a specific site from Redis
func (r *Redis) GetSiteListings(ctx context.Context, client redis.Cmdable, site string) (map[string]bliss.Listing, error) {
	match := fmt.Sprintf("listings:site:%s:*", site)
	listingKeys, err := r.scan(ctx, client, match)
	if err != nil {
		return nil, err
	}

	listings := map[string]bliss.Listing{}
	for _, key := range listingKeys {
		listing, err := client.HGetAll(ctx, key).Result()
		if err != nil {
			return nil, err
		}

		bitSize := 32
		price, err := strconv.ParseFloat(listing["price"], bitSize)
		if err != nil {
			return nil, err
		}

		listings[listing["isbn"]] = bliss.Listing{
			Book: bliss.Book{
				ISBN:   listing["isbn"],
				Format: bliss.Formats[listing["format"]],
				Title:  listing["title"],
			},
			Currency: listing["currency"],
			Price:    float32(price),
			Site:     listing["site"],
		}
	}

	return listings, nil
}

// pruneBookMeta removes metadata for books that are not passed to the function
func (r *Redis) pruneBookMeta(ctx context.Context, client redis.Cmdable, books ...bliss.Book) error {
	keys, err := r.scan(ctx, client, "books:meta:*")
	if err != nil {
		return err
	}

	marked := []string{}
	for _, key := range keys {
		remove := true
		for _, b := range books {
			if strings.Split(key, ":")[2] == b.ISBN {
				remove = false
				break
			}
		}

		if remove {
			marked = append(marked, key)
		}
	}

	if len(marked) != 0 {
		if _, err = client.Del(ctx, marked...).Result(); err != nil {
			return err
		}
	}

	r.lg.Debug("deleted book meta:", marked)
	return nil
}

// pruneListingsByBook removes all listings in Redis from books that are not in book loop
func (r *Redis) pruneListingsByBook(ctx context.Context, client redis.Cmdable, books ...bliss.Book) error {
	if len(books) == 0 {
		return fmt.Errorf("%w: %s", bliss.ErrEmptySlice, "books")
	}

	siteListingKeys, err := r.scan(ctx, client, "listings:site:*")
	if err != nil {
		return err
	}

	lowestListingKeys, err := r.scan(ctx, client, "listings:lowest:*")
	if err != nil {
		return err
	}

	bm := map[string]bliss.Book{}
	for _, b := range books {
		bm[b.ISBN] = b
	}

	marked := []string{}
	for _, key := range siteListingKeys {
		// parse isbn from "listings:site:<site>:<isbn>"
		isbn := strings.Split(key, ":")[3]
		if _, exists := bm[isbn]; !exists {
			marked = append(marked, key)
		}
	}

	for _, key := range lowestListingKeys {
		// parse isbn from "listings:lowest:<isbn>"
		isbn := strings.Split(key, ":")[2]
		if _, exists := bm[isbn]; !exists {
			marked = append(marked, key)
		}
	}

	if len(marked) != 0 {
		r.lg.Debug("deleting book keys:", marked)
		if _, err := client.Del(ctx, marked...).Result(); err != nil {
			return err
		}
	}
	return nil
}

// pruneListingsBySite removes all listings in Redis from sites that are disabled
func (r *Redis) pruneListingsBySite(ctx context.Context, client redis.Cmdable, sites []string) error {
	if len(sites) == 0 {
		return fmt.Errorf("%w: %s", bliss.ErrEmptySlice, "sites")
	}

	siteListingKeys, err := r.scan(ctx, client, "listings:site:*")
	if err != nil {
		return err
	}

	lowestListingKeys, err := r.scan(ctx, client, "listings:lowest:*")
	if err != nil {
		return err
	}

	sm := map[string]struct{}{}
	for _, s := range sites {
		sm[s] = struct{}{}
	}

	marked := []string{}
	for _, key := range siteListingKeys {
		// parse site from "listings:site:<site>:<...>"
		site := strings.Split(key, ":")[2]
		if _, exists := sm[site]; !exists {
			marked = append(marked, key)
		}
	}

	for _, key := range lowestListingKeys {
		site, err := client.HGet(ctx, key, "site").Result()
		if err != nil {
			return err
		}

		if _, exists := sm[site]; !exists {
			marked = append(marked, key)
		}
	}

	if len(marked) != 0 {
		r.lg.Debug("deleting scraper keys:", marked)
		if _, err := client.Del(ctx, marked...).Result(); err != nil {
			return err
		}
	}

	return nil
}

// pruneSites removes all disabled scrapers from Redis
func (r *Redis) pruneSites(ctx context.Context, client redis.Cmdable, sites []string) error {
	if len(sites) == 0 {
		return fmt.Errorf("%w: %s", bliss.ErrEmptyMap, "scrapers")
	}

	for _, s := range sites {
		if s == "" {
			return fmt.Errorf("%w: %s", bliss.ErrEmptyString, "scraper")
		}
	}

	scraperKeys, err := r.scan(ctx, client, "scrapers:*")
	if err != nil {
		return err
	}

	sm := map[string]struct{}{}
	for _, s := range sites {
		sm[s] = struct{}{}
	}

	marked := []string{}
	for _, key := range scraperKeys {
		// parse site from "scrapers:site:<...>"
		site := strings.Split(key, ":")[1]
		if _, exists := sm[site]; !exists {
			marked = append(marked, key)
		}
	}

	if len(marked) != 0 {
		r.lg.Debug("deleting keys:", marked)
		if _, err := client.Del(ctx, marked...).Result(); err != nil {
			return err
		}
	}
	return nil
}

// RegisterListing stores a listing in Redis, updates history and the next item to be scraped
// Returns true if the registered listing is a new lowest listing for the ISBN
func (r *Redis) RegisterListing(
	ctx context.Context,
	client redis.Cmdable,
	listing bliss.Listing,
	prioritized bool,
) (
	isDrop, isDeleted bool, err error,
) {
	r.lg.Debugf("RegisterListing handling listing: %+v", listing)
	isDeleted, isDrop, err = r.updateLowestListing(ctx, client, listing)
	if err != nil {
		return false, false, err
	}

	if tmpErr := r.SetSiteListings(ctx, client, listing); tmpErr != nil {
		return false, false, tmpErr
	}

	if tmpErr := r.updateHistory(ctx, client, listing.Book.ISBN, listing.Site); tmpErr != nil {
		return false, false, tmpErr
	}

	if !prioritized {
		if _, err = r.updateNext(ctx, client, listing.Book.ISBN, listing.Site); err != nil {
			return false, false, err
		}
	}
	return
}

func (r *Redis) scan(ctx context.Context, client redis.Cmdable, match string) ([]string, error) {
	var batch, result []string
	cursor := uint64(0)

	for {
		var err error
		batch, cursor, err = client.Scan(ctx, cursor, match, 0).Result()
		if err != nil {
			return []string{}, err
		}

		result = append(result, batch...)

		if cursor == 0 {
			break
		}
	}

	return result, nil
}

// setBookLoop writes the list of books to be scraped to Redis
func (r *Redis) setBookLoop(ctx context.Context, client redis.Cmdable, books []bliss.Book) error {
	if len(books) == 0 {
		return fmt.Errorf("%w: %s", bliss.ErrEmptySlice, "books")
	}

	if _, err := client.Del(ctx, "books:loop").Result(); err != nil {
		return err
	}

	isbns := []string{}
	for _, b := range books {
		isbns = append(isbns, b.ISBN)
	}

	if _, err := client.RPush(ctx, "books:loop", isbns).Result(); err != nil {
		return err
	}
	return nil
}

// SetBookMeta writes book metadata to Redis
func (r *Redis) SetBookMeta(ctx context.Context, client redis.Cmdable, books ...bliss.Book) error {
	for _, book := range books {
		if book == (bliss.Book{}) {
			return fmt.Errorf("%w: %s", bliss.ErrEmptyStruct, "book")
		}

		exists, err := client.Exists(ctx, "books:meta:"+book.ISBN).Result()
		if err != nil {
			return err
		}

		fields := []interface{}{}

		if exists == 1 {
			if book.Format != bliss.NA {
				fields = append(fields, "format", book.Format.String())
			}

			if book.Title != "" {
				fields = append(fields, "title", book.Title)
			}
		}

		if exists != 1 {
			fields = append(fields, "isbn", book.ISBN, "title", book.Title, "format", book.Format.String())
		}

		if len(fields) != 0 {
			if _, err := client.HSet(ctx, "books:meta:"+book.ISBN, fields...).Result(); err != nil {
				return err
			}
		}
	}
	return nil
}

// SetExchangeRates writes new currency exchange rates to Redis
func (r *Redis) SetExchangeRates(ctx context.Context, client redis.Cmdable, exchange *e.Exchange) error {
	if exchange == nil {
		return fmt.Errorf("%w: %s", bliss.ErrNilPointer, "exchange")
	}

	if _, err := client.Set(ctx, "exchange:base", exchange.Base, 0).Result(); err != nil {
		return err
	}

	if _, err := client.Set(ctx, "exchange:date", exchange.Date, 0).Result(); err != nil {
		return err
	}

	if _, err := client.Set(ctx, "exchange:last_updated", exchange.LastUpdated, 0).Result(); err != nil {
		return err
	}

	rates := []interface{}{}
	for currency, rate := range exchange.Rates {
		rates = append(rates, currency, fmt.Sprintf("%f", rate))
	}

	if _, err := client.HSet(ctx, "exchange:rates", rates...).Result(); err != nil {
		return err
	}

	return nil
}

// SetLowestListings writes lowest listings to Redis
func (r *Redis) SetLowestListings(ctx context.Context, client redis.Cmdable, listings ...bliss.Listing) error {
	for _, listing := range listings {
		_, err := client.HSet(ctx, "listings:lowest:"+listing.Book.ISBN,
			"currency", listing.Currency,
			"price", listing.Price,
			"site", listing.Site,
			"format", listing.Book.Format.String(),
			"isbn", listing.Book.ISBN,
			"title", listing.Book.Title,
		).Result()

		if err != nil {
			return err
		}
		r.lg.Debugf("SetLowestListings wrote listing: '%+v'", listing)
	}
	return nil
}

// SetSiteListings writes per site listings to Redis
func (r *Redis) SetSiteListings(ctx context.Context, client redis.Cmdable, listings ...bliss.Listing) error {
	for _, listing := range listings {
		if listing == (bliss.Listing{}) {
			return fmt.Errorf("%w: %s", bliss.ErrEmptyStruct, "listing")
		}

		key := fmt.Sprintf("listings:site:%s:%s", listing.Site, listing.Book.ISBN)
		_, err := client.HSet(ctx, key,
			"currency", listing.Currency,
			"price", listing.Price,
			"site", listing.Site,
			"format", listing.Book.Format.String(),
			"isbn", listing.Book.ISBN,
			"title", listing.Book.Title,
		).Result()
		if err != nil {
			return err
		}
	}
	return nil
}

// setSites writes enabled sites to Redis
func (r *Redis) setSites(ctx context.Context, client redis.Cmdable, sites []string) error {
	if len(sites) == 0 {
		return fmt.Errorf("%w: %s", bliss.ErrEmptyMap, "scrapers")
	}

	for _, s := range sites {
		next := fmt.Sprintf("scrapers:%s:next", s)
		exists, err := client.Exists(ctx, next).Result()
		if err != nil {
			return err
		}

		if exists == 1 {
			continue
		}

		isbn, err := client.LIndex(ctx, "books:loop", 0).Result()
		if err != nil {
			return err
		}

		if _, err := client.Set(ctx, next, isbn, 0*time.Second).Result(); err != nil {
			return err
		}
	}

	return nil
}

// UpdateBooks updates book states in Redis
func (r *Redis) UpdateBooks(ctx context.Context, client redis.Cmdable, books ...bliss.Book) error {
	if len(books) == 0 {
		return fmt.Errorf("%w: %s", bliss.ErrEmptySlice, "books")
	}

	if err := r.setBookLoop(ctx, client, books); err != nil {
		return err
	}

	if err := r.SetBookMeta(ctx, client, books...); err != nil {
		return err
	}

	if err := r.pruneListingsByBook(ctx, client, books...); err != nil {
		return err
	}

	if err := r.pruneBookMeta(ctx, client, books...); err != nil {
		return err
	}

	r.lg.Debug("updated books in redis")
	return nil
}

func (r *Redis) updateHistory(ctx context.Context, client redis.Cmdable, isbn, site string) error {
	if isbn == "" {
		return fmt.Errorf("%w: %s", bliss.ErrEmptyString, "isbn")
	}

	if site == "" {
		return fmt.Errorf("%w: %s", bliss.ErrEmptyString, "site")
	}

	historyKey := fmt.Sprintf("scrapers:%s:history", site)
	historyLen, err := client.LPush(ctx, historyKey, isbn).Result()
	if err != nil {
		return err
	}

	loopLen, err := client.LLen(ctx, "books:loop").Result()
	if err != nil {
		return err
	}

	if loopLen < historyLen {
		if _, err := client.LTrim(ctx, historyKey, 0, loopLen-1).Result(); err != nil {
			return err
		}
	}

	return nil
}

// updateLowestListing determines if a listing is a new low and writes accordingly
// returns true if passed listing is a new lowest listing
func (r *Redis) updateLowestListing(
	ctx context.Context, client redis.Cmdable, newListing bliss.Listing) (isDeleted, isDrop bool, err error) {
	if newListing == (bliss.Listing{}) {
		return false, false, fmt.Errorf("%w: %s", bliss.ErrEmptyStruct, "newListing")
	}
	isbn := newListing.Book.ISBN

	findNextLow := func() (bliss.Listing, bool) {
		var found bool
		nextLow := bliss.Listing{Price: bliss.NotFound}
		sites, _ := r.GetSites(ctx, client)
		for _, site := range sites {
			if site == newListing.Site {
				continue
			}

			// get other site listings for comparison
			listings, _ := r.GetSiteListings(ctx, client, site)
			l, exists := listings[isbn]
			if !exists || l.Price == bliss.NotFound {
				continue
			}

			if nextLow.Price == bliss.NotFound || l.Price < nextLow.Price {
				nextLow = l
				found = true
			}
		}
		return nextLow, found
	}
	setLow := func(l bliss.Listing) error {
		return r.SetLowestListings(ctx, client, l)
	}

	lowestListings, err := r.GetLowestListings(ctx, client)
	if err != nil {
		return false, false, err
	}
	low, lowExists := lowestListings[isbn]

	if newListing.Price != bliss.NotFound {
		if !lowExists {
			r.lg.Debug("updateLowestListing: new lowest price found, no previous")
			return false, false, setLow(newListing)
		}

		// set lowest if new is lower than previous lowest
		if newListing.Price < low.Price {
			r.lg.Debug("updateLowestListing: new lowest price found")
			return false, true, setLow(newListing)
		}

		// do nothing if new listing has same price as lowest
		if newListing.Price == low.Price {
			return false, false, nil
		}

		// handle lowest listing price increase
		if newListing.Site == low.Site {
			nextLow, exists := findNextLow()
			if !exists {
				r.lg.Debug("updateLowestListing: lowest price increased, no next low")
				return false, false, setLow(newListing)
			}

			if nextLow.Price < newListing.Price {
				r.lg.Debug("updateLowestListing: lowest price increased, set next low")
				return false, false, setLow(nextLow)
			}
			r.lg.Debug("updateLowestListing: lowest price increased, still lowest")
			return false, false, nil
		}
	}

	if !lowExists {
		return false, false, nil
	}

	nextLow, nextLowExists := findNextLow()
	if low.Site == newListing.Site && nextLowExists {
		r.lg.Debug("updateLowestListing: lowest unlisted, found next low")
		return false, false, setLow(nextLow)
	}

	if low.Site == newListing.Site && !nextLowExists {
		r.lg.Debug("updateLowestListing: book ran out of stock, deleting lowest listing")
		client.Del(ctx, "listings:lowest:"+isbn)
		return true, false, nil
	}
	return false, false, nil
}

// UpdateNotification updates a notification hash in redis and returns true if a new hash was written,
// i.e. if the notification was not a duplicate.
func (r *Redis) UpdateNotification(ctx context.Context, client redis.Cmdable, message string) (bool, error) {
	if message == "" {
		return false, fmt.Errorf("%w: %s", bliss.ErrEmptyString, "message")
	}

	hash := fmt.Sprintf("%x", sha1.Sum([]byte(message)))
	exists, err := client.Exists(ctx, "notifications:sent:"+hash).Result()
	if err != nil {
		return false, errors.New("failed to check hash existence")
	}

	if exists == 1 {
		return false, nil
	}

	_, err = client.Set(ctx, "notifications:sent:"+hash, "", 7*24*time.Hour).Result()
	if err != nil {
		return false, errors.New("failed to set notification hash")
	}

	return true, nil
}

// UpdateInfluxDBRetention looks up the influxdb retention policy setting stored in redis
// If the current retention policy does not match the stored one it will be overwritten and
// UpdateInfluxDBRetention will return the old policy as a string and true
func (r *Redis) UpdateInfluxDBRetention(
	ctx context.Context,
	client redis.Cmdable,
	newPolicy string,
	lg *zap.SugaredLogger,
) (
	stored string, changed bool, err error,
) {
	if newPolicy == "" {
		return "", false, fmt.Errorf("%w: %s", bliss.ErrEmptyString, "newPolicy")
	}

	oldPolicy, err := client.Get(ctx, "influxdb:retention").Result()
	if err != nil && err != redis.Nil {
		return "", false, err
	}

	if err == redis.Nil || newPolicy != oldPolicy {
		lg.Debug("retention policy stored in redis does not match current")
		_, err := client.Set(ctx, "influxdb:retention", newPolicy, 0*time.Second).Result()
		if err != nil {
			return "", false, err
		}
		return oldPolicy, true, nil
	}

	return "", false, nil
}

func (r *Redis) updateNext(ctx context.Context, client redis.Cmdable, scrapedISBN, site string) (string, error) {
	if err := r.assertKeyType(ctx, client, "books:loop", "list"); err != nil {
		return "", err
	}

	loopLen := client.LLen(ctx, "books:loop").Val()
	pos, err := client.LPos(ctx, "books:loop", scrapedISBN, redis.LPosArgs{}).Result()

	if err != nil {
		// failed to get last scraped isbn from book loop, fall back to history
		nextKey := fmt.Sprintf("scrapers:%s:next", site)
		historyKey := fmt.Sprintf("scrapers:%s:history", site)
		historySlice := client.LRange(ctx, historyKey, 0, -1).Val()
		loopSlice := client.LRange(ctx, "books:loop", 0, -1).Val()

		// backtrack through history to find a valid isbn matching book loop
		loopMap := stringSliceToMap(loopSlice)
		for _, isbn := range historySlice {
			if i, exists := loopMap[isbn]; !exists {
				continue
			} else {
				nextPos := int64(i + 1)
				if nextPos == loopLen {
					nextPos = 0
				}

				_, err = client.Set(ctx, nextKey, loopSlice[nextPos], 0).Result()
				if err != nil {
					return "", err
				}
				return loopSlice[nextPos], nil
			}
		}

		// start from top of book loop if there is no history
		nextISBN := client.LIndex(ctx, "books:loop", 0).Val()
		_, err = client.Set(ctx, nextKey, nextISBN, 0*time.Second).Result()
		if err != nil {
			return "", err
		}

		return nextISBN, nil
	}

	var nextPos int64
	if pos != loopLen-1 {
		nextPos = pos + 1
	}

	nextISBN := client.LIndex(ctx, "books:loop", nextPos).Val()
	nextKey := fmt.Sprintf("scrapers:%s:next", site)
	client.Set(ctx, nextKey, nextISBN, 0)
	return nextISBN, nil
}

// updatePriorityLists updates priority scraping lists in Redis
func (r *Redis) updatePriorityLists(ctx context.Context, client redis.Cmdable, sites []string) error {
	// remove all previous priority lists
	priority, err := r.scan(ctx, client, "scrapers:*:priority")
	if err != nil {
		return err
	}

	if len(priority) != 0 {
		_, err = client.Del(ctx, priority...).Result()
		if err != nil {
			return err
		}
	}

	// get book loop
	isbns, err := client.LRange(ctx, "books:loop", 0, -1).Result()
	if err != nil {
		return err
	}

	// compare book loop to site listings and prioritize missing listings
	sort.Strings(sites)
	var prioritized []string
	for _, isbn := range isbns {
		keys := []string{}

		for _, site := range sites {
			keys = append(keys, fmt.Sprintf("listings:site:%s:%s", site, isbn))
		}

		count, err := client.Exists(ctx, keys...).Result()
		if err != nil {
			return err
		}

		if count != int64(len(keys)) {
			prioritized = append(prioritized, isbn)
		}
	}

	if len(prioritized) != 0 {
		for _, site := range sites {
			key := fmt.Sprintf("scrapers:%s:priority", site)
			_, err := client.RPush(ctx, key, prioritized).Result()
			if err != nil {
				return err
			}
		}
	}

	return nil
}

// UpdateSites updates scraper data in Redis
func (r *Redis) UpdateSites(ctx context.Context, client redis.Cmdable, sites []string) error {
	if len(sites) == 0 {
		return fmt.Errorf("%w: %s", bliss.ErrEmptyMap, "scrapers")
	}

	if err := r.setSites(ctx, client, sites); err != nil {
		return err
	}

	if err := r.pruneSites(ctx, client, sites); err != nil {
		return err
	}

	if err := r.pruneListingsBySite(ctx, client, sites); err != nil {
		return err
	}

	if err := r.updatePriorityLists(ctx, client, sites); err != nil {
		return err
	}

	r.lg.Debug("updated scrapers in redis")
	return nil
}

func stringSliceToMap(strSlice []string) map[string]int {
	m := make(map[string]int)
	for i, s := range strSlice {
		m[s] = i
	}
	return m
}
