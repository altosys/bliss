package bliss

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFormat(t *testing.T) {
	assert.Equal(t, Format(0), NA)
	assert.Equal(t, Format(1), SC)
	assert.Equal(t, Format(2), HC)

	assert.Equal(t, "NA", Format(-1).String())
	assert.Equal(t, "NA", NA.String())
	assert.Equal(t, "SC", SC.String())
	assert.Equal(t, "HC", HC.String())
}
