package notify

import (
	"context"
	"sync"

	"github.com/gregdel/pushover"
	"go.uber.org/zap"
)

const msgBuffer = 100

// Config holds configuration parameters for notifications
type Config struct {
	DropThreshold                     int
	AvailabilityEnabled               bool
	LowStockEnabled                   bool
	PushoverEnabled                   bool
	PushoverUserKey, PushoverAPIToken string
}

// Notify starts up a notification sender
// Messages sent into the returned channel will be sent out as notifications
func Notify(
	ctx context.Context,
	errCh chan error,
	wg *sync.WaitGroup,
	lg *zap.SugaredLogger,
	pushoverEnabled bool,
	app *pushover.Pushover,
	recipient *pushover.Recipient,
) chan *pushover.Message {
	if !pushoverEnabled {
		return nil
	}
	notifyCh := make(chan *pushover.Message, msgBuffer)

	wg.Add(1)
	go func() {
		defer wg.Done()
		defer close(notifyCh)
		for {
			select {
			case msg := <-notifyCh:
				lg.Debug("notifyCh: ", msg)

				if _, err := app.SendMessage(msg, recipient); err != nil {
					errCh <- err
					continue
				}
			case <-ctx.Done():
				return
			}
		}
	}()
	return notifyCh
}
