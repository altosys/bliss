package notify

import (
	"bliss/logger"
	"context"
	"sync"
	"testing"

	"github.com/gregdel/pushover"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

func TestNotify(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	testMsg := &pushover.Message{
		Title:    "test message title",
		Message:  "test message content",
		Priority: pushover.PriorityNormal,
	}

	tt := []struct {
		desc            string
		pushoverEnabled bool
		expectCh        bool
		expectErr       bool
		apiToken        string
		userKey         string
		msg             *pushover.Message
	}{
		{
			desc:            "return nil channel when pushover disabled",
			pushoverEnabled: false,
			apiToken:        "",
			userKey:         "",
			expectCh:        false,
		},
		{
			desc:            "return notification channel when pushover enabled",
			pushoverEnabled: true,
			apiToken:        "apitoken",
			userKey:         "userkey",
			expectCh:        true,
		},
		{
			desc:            "return error on sending message with invalid credentials",
			pushoverEnabled: true,
			apiToken:        "apitoken",
			userKey:         "userkey",
			expectCh:        true,
			expectErr:       true,
			msg:             testMsg,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			wg := sync.WaitGroup{}
			errCh := make(chan error, 100)
			app := pushover.New(test.apiToken)
			recipient := pushover.NewRecipient(test.userKey)

			notifyCh := Notify(ctx, errCh, &wg, lg, test.pushoverEnabled, app, recipient)

			if test.expectCh {
				assert.NotNil(t, notifyCh)
			} else {
				assert.Nil(t, notifyCh)
			}

			if test.msg != nil {
				notifyCh <- test.msg

				if test.expectErr {
					assert.Error(t, <-errCh)
				} else {
					assert.NoError(t, <-errCh)
				}
			}

			cancel()
			wg.Wait()
		})
	}
}
