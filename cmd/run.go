package cmd

import (
	"bliss"
	"bliss/config"
	"bliss/exchange"
	"bliss/influxdb"
	"bliss/logger"
	"bliss/manager"
	"bliss/metrics"
	"bliss/notify"
	"bliss/redis"
	"bliss/utils"
	"context"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/gregdel/pushover"
	"github.com/spf13/cobra"
	"go.uber.org/zap"
)

var splash = `
  _   _ _         
 | |_| |_|___ ___ 
 | . | | |_ -|_ -|
 |___|_|_|___|___|
══════════════════════════════════════════════════════════════════════════════════════════
`

var (
	ctx        context.Context
	cancel     context.CancelFunc
	mutex      sync.Mutex
	tickerUnit = time.Hour
	wg         sync.WaitGroup
)

var runCmd = &cobra.Command{
	Use:     "run",
	Short:   "Run bliss",
	Long:    "Run bliss service",
	Version: bliss.Version,
	Run:     execRun,
}

func execRun(cmd *cobra.Command, args []string) {
	mutex.Lock()
	ctx, cancel = context.WithCancel(context.Background())
	run(ctx, cancel)
}

func run(ctx context.Context, cancel context.CancelFunc) {
	os.Stderr.WriteString(splash)

	ticker := time.NewTicker(24 * tickerUnit)
	errCh := make(chan error, 100)
	wg = sync.WaitGroup{}

	lm := logger.NewManager(false, zap.InfoLevel)
	clg := lm.NewSugaredLogger().Named("config")

	cfg, err := config.BuildConfig(clg)
	if err != nil {
		clg.Fatal(err)
	}

	lm = logger.NewManager(cfg.Log.Developer, cfg.Log.Level)
	clg = lm.NewSugaredLogger().Named("config")
	blg := lm.NewSugaredLogger().Named("bliss")
	elg := lm.NewSugaredLogger().Named("exchange")
	ilg := lm.NewSugaredLogger().Named("influx")
	mlg := lm.NewSugaredLogger().Named("metrics")
	nlg := lm.NewSugaredLogger().Named("notify")
	rlg := lm.NewSugaredLogger().Named("redis")
	slg := lm.NewSugaredLogger().Named("scraper")
	ulg := lm.NewSugaredLogger().Named("utils")

	defer func() {
		wg.Wait()
		close(errCh)
		blg.Info("bliss finished shutdown successfully")
	}()

	var ex *exchange.Exchange
	var m metrics.Metrics
	config.PrintConfig(&cfg, clg)

	idb, err := influxdb.NewInfluxDB(&cfg.InfluxDB, ilg)
	if err != nil {
		blg.Fatal(err)
	}

	rdb, err := redis.NewRedis(cfg.Redis, rlg)
	if err != nil {
		blg.Fatal(err)
	}

	ex, err = exchange.NewExchange(cfg.Currency, elg)
	if err != nil {
		blg.Fatal(err)
	}

	updated, err := ex.UpdateRates(ctx, rdb.Client)
	if err != nil {
		blg.Fatal(err)
	}

	if updated {
		if err = rdb.SetExchangeRates(ctx, rdb.Client, ex); err != nil {
			blg.Fatal(err)
		}
	}

	wg.Add(1)
	go utils.StartInterruptListener(ctx, cancel, &wg, ulg)

	m, err = metrics.NewMetrics(cfg.Metrics.Port, mlg)
	if err != nil {
		blg.Fatal(err)
	}

	// set up metrics server
	m.Serve(ctx, errCh, &wg, mlg)
	metricsCh := m.Record(ctx, &wg, mlg)

	portStr := strconv.Itoa(cfg.Metrics.Port)
	for !utils.IsPortListening("metrics server", "127.0.0.1", portStr, mlg) {
		time.Sleep(1 * time.Second)
	}
	blg.Info("connected to metrics server")

	// set up notifications handler
	pushoverApp := pushover.New(cfg.Notify.PushoverAPIToken)
	pushoverRecipient := pushover.NewRecipient(cfg.Notify.PushoverUserKey)
	notifyCh := notify.Notify(ctx, errCh, &wg, nlg, cfg.Notify.PushoverEnabled, pushoverApp, pushoverRecipient)

	// connect to and update influxdb
	for {
		if _, _, err = idb.Ping(time.Second); err != nil {
			blg.Info("waiting for influxdb...")
			time.Sleep(1 * time.Second)
			continue
		}
		break
	}
	blg.Info("connected to influxdb")

	err = idb.Prune(cfg.Books, cfg.Manager.Scrapers, ilg)
	if err != nil {
		blg.Errorf("failed to prune listings in influxdb: ", err)
	}
	dbCh := idb.Write(ctx, errCh, &wg)

	// connect to and update redis
	for rdb.Client.Ping(ctx).Val() != "PONG" {
		blg.Info("waiting for redis...")
		time.Sleep(1 * time.Second)
	}
	blg.Info("connected to redis")

	blg.Info("updating redis")
	if err = rdb.UpdateBooks(ctx, rdb.Client, cfg.Books...); err != nil {
		rlg.Fatal("failed to update books in redis: ", err)
	}

	sites := []string{}
	for _, s := range cfg.Manager.Scrapers {
		sites = append(sites, s.Site())
	}

	if err = rdb.UpdateSites(ctx, rdb.Client, sites); err != nil {
		blg.Fatal("failed to update scrapers in redis: ", err)
	}
	blg.Info("redis updated, proceeding")

	// move influxdb data to configured retention policy
	oldRetention, retentionChanged, err := rdb.UpdateInfluxDBRetention(ctx, rdb.Client, cfg.InfluxDB.Retention, rlg)
	if err != nil {
		blg.Fatal("failed to update influxdb retention policy state: ", err)
	}

	if retentionChanged {
		if err = idb.MigrateRetentionPolicy(oldRetention, cfg.InfluxDB.Retention); err != nil {
			blg.Fatal("failed to migrate data to new influxdb retention policy: ", err)
		}
	}

	// populate metrics server
	lowest, err := rdb.GetLowestListings(ctx, rdb.Client)
	if err != nil {
		blg.Error("failed to load lowest listings from redis: ", err)
	}

	for _, listing := range lowest {
		metricsCh <- listing
	}

	// set up scrapers
	s := manager.NewManager(rdb, cfg.Notify, cfg.Manager, slg)
	blg.Info("scraping books:")
	utils.LogBooksSorted(cfg.Books, blg)

	// start scraping
	s.Start(ctx, &wg, dbCh, errCh, metricsCh, notifyCh)
	mutex.Unlock()

	for {
		select {
		case <-ctx.Done():
			return
		case err := <-errCh:
			if err != nil {
				blg.Error(err)
			}
		case <-ticker.C:
			updated, err := ex.UpdateRates(ctx, rdb.Client)
			if err != nil {
				elg.Warn(err)
				continue
			}

			if updated {
				if rdb.SetExchangeRates(ctx, rdb.Client, ex) != nil {
					rlg.Warn(err)
				}
			}
		}
	}
}
