package main

import (
	"bliss/cmd"
)

func main() {
	cmd.Execute()
}
