package main

import (
	"testing"

	_ "github.com/influxdata/influxdb1-client" // this is important because of the bug in go mod
)

func TestMain(t *testing.T) {
	t.Log("test start of main()")
	main()
}
