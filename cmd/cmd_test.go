package cmd

import (
	iti "bliss/internal/testing/influxdb"
	itr "bliss/internal/testing/redis"
	"bliss/logger"
	"bliss/utils"
	"context"
	"errors"
	"os"
	"sync"
	"testing"
	"time"

	"github.com/gregdel/pushover"
	_ "github.com/influxdata/influxdb1-client" // this is important because of a bug in go mod
	cobra "github.com/spf13/cobra"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

var (
	seq sync.Mutex
)

func TestCmd(t *testing.T) {
	assert.NoError(t, rootCmd.Execute())
	assert.NoError(t, versionCmd.Execute())
}

func TestExecute(t *testing.T) {
	Execute()
}

func TestPrintVersion(t *testing.T) {
	printVersion(nil, []string{})
}

func TestGetVersionStr(t *testing.T) {
	vs := getVersionStr()
	assert.Contains(t, vs, "builder")
	assert.Contains(t, vs, "commit")
	assert.Contains(t, vs, "date")
	assert.Contains(t, vs, "version")
}

func TestExecRunIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	seq.Lock()
	defer seq.Unlock()
	idbC, influxDBAddr := iti.GetInfluxDBContainer(lg, t)
	redisC, redisAddr := itr.GetRedisContainer(lg, t)
	defer func() {
		assert.NoError(t, idbC.Terminate(context.Background()))
		assert.NoError(t, redisC.Terminate(context.Background()))
	}()

	setupTestEnv(influxDBAddr, redisAddr)
	defer clearTestEnv()

	go execRun(&cobra.Command{}, []string{})
	time.Sleep(1 * time.Second) // allow time for startup
	mutex.Lock()
	defer mutex.Unlock()
	cancel()
	wg.Wait()

	for utils.IsPortListening("metrics server", "127.0.0.1", "8085", zap.NewNop().Sugar()) {
		time.Sleep(50 * time.Millisecond)
	}
}

func TestRunIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	seq.Lock()
	defer seq.Unlock()
	tickerUnit = 50 * time.Millisecond
	idbC, influxDBAddr := iti.GetInfluxDBContainer(lg, t)
	redisC, redisAddr := itr.GetRedisContainer(lg, t)
	defer func() {
		assert.NoError(t, idbC.Terminate(context.Background()))
		assert.NoError(t, redisC.Terminate(context.Background()))
	}()

	setupTestEnv(influxDBAddr, redisAddr)
	defer clearTestEnv()

	mutex.Lock()
	ctx, cancel := context.WithCancel(context.Background())
	go run(ctx, cancel) // unlocks mutex when ready
	mutex.Lock()

	errCh := make(chan error, 100)
	notifyCh := make(chan *pushover.Message, 100)
	errCh <- errors.New("error channel test")
	notifyCh <- &pushover.Message{}
	mutex.Unlock()
	cancel()
	wg.Wait()

	for utils.IsPortListening("metrics server", "127.0.0.1", "8085", zap.NewNop().Sugar()) {
		time.Sleep(50 * time.Millisecond)
	}
}

func setupTestEnv(influxDBAddr, redisAddr string) {
	clearTestEnv()
	os.Setenv("BLISS_BOOKS", "9780141393391")
	os.Setenv("BLISS_CURRENCY", "EUR")
	os.Setenv("BLISS_DEVELOPER_LOGS", "true")
	os.Setenv("BLISS_DB_ADDRESS", influxDBAddr)
	os.Setenv("BLISS_DB_USER", "admin")
	os.Setenv("BLISS_DB_PASSWORD", "bliss")
	os.Setenv("BLISS_REDIS_ADDRESS", redisAddr)
	os.Setenv("BLISS_REDIS_DATABASE", "0")
	os.Setenv("BLISS_REDIS_PASSWORD", "")
	os.Setenv("BLISS_ENABLE_ADLIBRIS", "true")
	os.Setenv("BLISS_INTERVAL", "1")
	os.Setenv("BLISS_PUSHOVER_API_TOKEN", "apitoken")
	os.Setenv("BLISS_PUSHOVER_USER_KEY", "userkey")
}

func clearTestEnv() {
	os.Unsetenv("BLISS_BOOKS")
	os.Unsetenv("BLISS_CURRENCY")
	os.Unsetenv("BLISS_DEVELOPER_LOGS")
	os.Unsetenv("BLISS_DB_ADDRESS")
	os.Unsetenv("BLISS_DB_USER")
	os.Unsetenv("BLISS_DB_PASSWORD")
	os.Unsetenv("BLISS_REDIS_ADDRESS")
	os.Unsetenv("BLISS_REDIS_DATABASE")
	os.Unsetenv("BLISS_REDIS_PASSWORD")
	os.Unsetenv("BLISS_ENABLE_ADLIBRIS")
	os.Unsetenv("BLISS_INTERVAL")
	os.Unsetenv("BLISS_PUSHOVER_API_TOKEN")
	os.Unsetenv("BLISS_PUSHOVER_USER_KEY")
}
