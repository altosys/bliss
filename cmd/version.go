package cmd

import (
	"bliss"
	"bytes"
	"fmt"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
)

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Display version information",
	Long:  "Display version information and other build metadata",
	Run:   printVersion,
}

func printVersion(cmd *cobra.Command, args []string) {
	fmt.Println(getVersionStr())
}

func getVersionStr() string {
	data := [][]string{
		{"builder", bliss.Builder},
		{"commit", bliss.Commit},
		{"date", bliss.Date},
		{"version", bliss.Version},
	}

	buf := new(bytes.Buffer)

	table := tablewriter.NewWriter(buf)
	table.SetBorder(false)
	table.SetTablePadding("\t\t")
	table.SetNoWhiteSpace(true)
	table.AppendBulk(data)
	table.Render()

	return buf.String()
}
