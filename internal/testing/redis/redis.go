package influxdb

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	tc "github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"go.uber.org/zap"
)

func GetRedisContainer(
	lg *zap.SugaredLogger,
	t *testing.T,
) (container tc.Container, address string) {
	ctx := context.Background()
	req := tc.GenericContainerRequest{
		ProviderType: tc.ProviderPodman,
		Started:      true,
		ContainerRequest: tc.ContainerRequest{
			Image:        "redis:6.0.9-alpine3.12",
			AutoRemove:   true,
			SkipReaper:   true,
			ExposedPorts: []string{"6379/tcp"},
			WaitingFor:   wait.ForLog("Ready to accept connections"),
			Cmd:          []string{"redis-server", "--appendonly", "yes"},
		},
	}

	redisC, err := tc.GenericContainer(ctx, req)
	assert.NoError(t, err)
	mappedPort, err := redisC.MappedPort(ctx, "6379/tcp")
	assert.NoError(t, err)
	address = "127.0.0.1:" + mappedPort.Port()
	return redisC, address
}
