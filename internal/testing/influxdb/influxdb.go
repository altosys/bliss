package influxdb

import (
	"context"
	"os"
	"strings"
	"testing"
	"time"

	_ "github.com/influxdata/influxdb1-client" // this is important because of the bug in go mod
	influxdb "github.com/influxdata/influxdb1-client/v2"
	"github.com/stretchr/testify/assert"
	tc "github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"go.uber.org/zap"
)

func GetInfluxDBContainer(
	lg *zap.SugaredLogger,
	t *testing.T,
) (container tc.Container, address string) {
	ctx := context.Background()
	pwd, err := os.Getwd()
	assert.NoError(t, err)
	mountPath := strings.SplitAfter(pwd, "bliss/")[0] + "influxdb/"

	req := tc.GenericContainerRequest{
		ProviderType: tc.ProviderPodman,
		Started:      true,
		ContainerRequest: tc.ContainerRequest{
			Image:        "influxdb:1.8.3",
			AutoRemove:   true,
			SkipReaper:   true,
			ExposedPorts: []string{"8086/tcp"},
			WaitingFor:   wait.ForLog("Listening for signals"),
			Env: map[string]string{
				"INFLUXDB_DB":                  "bliss",
				"INFLUXDB_HTTP_AUTH_ENABLED":   "true",
				"INFLUXDB_ADMIN_ENABLED":       "true",
				"INFLUXDB_ADMIN_USER":          "admin",
				"INFLUXDB_ADMIN_PASSWORD":      "bliss",
				"INFLUXDB_WRITE_USER":          "writer",
				"INFLUXDB_WRITE_USER_PASSWORD": "bliss",
				"INFLUXDB_READ_USER":           "reader",
				"INFLUXDB_READ_USER_PASSWORD":  "bliss",
				"INFLUXDB_REPORTING_DISABLED":  "true",
			},
			Mounts: tc.Mounts(tc.ContainerMount{
				Source: tc.GenericBindMountSource{
					HostPath: mountPath + "/init-influxdb.sh",
				},
				Target: tc.ContainerMountTarget(
					"/docker-entrypoint-initdb.d/init-influxdb.sh",
				),
			}),
		},
	}

	idbC, err := tc.GenericContainer(ctx, req)
	assert.NoError(t, err)
	mappedPort, err := idbC.MappedPort(ctx, "8086/tcp")
	assert.NoError(t, err)
	address = "127.0.0.1:" + mappedPort.Port()

	client, err := influxdb.NewHTTPClient(influxdb.HTTPConfig{
		Addr:     "http://" + address,
		Username: "admin",
		Password: "bliss",
		Timeout:  time.Second,
	})
	assert.NoError(t, err)
	defer client.Close()

	for {
		t.Log("waiting for influxdb initialization...")
		q := influxdb.Query{Command: "show retention policies on bliss"}
		if resp, _ := client.Query(q); resp != nil {
			// break when all retention policies + autogen found
			numRetentionPolicies := 7
			if len(resp.Results[0].Series[0].Values) == numRetentionPolicies {
				break
			}
		}
		time.Sleep(1 * time.Second)
	}
	_, _, err = client.Ping(time.Second)
	assert.NoError(t, err)
	return idbC, address
}
