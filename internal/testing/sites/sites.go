package sites

import (
	"bliss"
	"bliss/mock"
	"bytes"
	"os"
	"strings"
	"testing"

	"github.com/PuerkitoBio/goquery"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

type SiteTestData struct {
	Desc        string
	Book        bliss.Book
	Scraper     bliss.Scraper
	DomDoc      string
	HTTPMocks   map[string]HTTPMock
	ExpCurrency string
	ExpFormat   bliss.Format
	ExpPrice    float32
	ExpTitle    string
}

type HTTPMock struct {
	Doc  string
	Link string
}

func TestSite(t *testing.T, tt []SiteTestData, lg *zap.SugaredLogger) {
	for i := range tt {
		test := &tt[i]
		t.Run(test.Desc, func(t *testing.T) {
			site := strings.Split(test.Desc, " ")[0]
			searchURL := test.Scraper.SearchURL("0000000000000")
			assert.Contains(t, searchURL, site)
			assert.Contains(t, searchURL, "0000000000000")
			assert.Equal(t, test.Scraper.Site(), site)

			var doc *goquery.Document
			var dom *goquery.Selection
			rootFile, err := os.ReadFile(test.HTTPMocks["/"].Doc)
			assert.NoError(t, err)
			buf := bytes.NewBuffer(rootFile)

			// set up link to mock http server in '/' document if traversing
			if test.HTTPMocks["/"].Link != "" {
				var linkFile []byte
				bufStr := buf.String()
				path := test.HTTPMocks["/"].Link
				linkFile, err = os.ReadFile(test.HTTPMocks[path].Doc)
				assert.NoError(t, err)

				ts := mock.HTTPServer(t, path, linkFile)
				defer ts.Close()

				// test handling of getDOM error by not replacing link placeholder
				doc, err = goquery.NewDocumentFromReader(buf)
				assert.NoError(t, err)
				dom = doc.Selection
				currency, price := test.Scraper.ParsePrice(dom, bliss.Book{}, lg)
				assert.Equal(t, test.ExpCurrency, currency)
				assert.Equal(t, float32(bliss.NotFound), price)

				// rewrite buf with valid link to ts
				buf.Reset()
				_, err = buf.WriteString(strings.ReplaceAll(bufStr, "HTTP_MOCK_LINK", ts.URL+path))
				assert.NoError(t, err)
			}

			doc, err = goquery.NewDocumentFromReader(buf)
			assert.NoError(t, err)
			dom = doc.Selection

			currency, price := test.Scraper.ParsePrice(dom, bliss.Book{Format: test.Book.Format, Title: test.Book.Title}, lg)
			assert.Equal(t, test.ExpCurrency, currency, "book price currency")
			assert.Equal(t, test.ExpPrice, price, "book price")
			assert.Equal(t, test.ExpTitle, test.Scraper.ParseTitle(dom), "book title")
			assert.Equal(t, test.ExpFormat, test.Scraper.ParseFormat(dom), "book format")
		})
	}
}
