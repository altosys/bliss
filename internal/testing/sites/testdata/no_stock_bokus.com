<!DOCTYPE html><html lang="sv" class="js" xmlns:book="http://ogp.me/ns/book#" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"><head><script async="true" type="text/javascript" src="https://sslwidget.criteo.com/event?a=6096&amp;v=5.6.1&amp;p0=e%3Dexd%26site_type%3Dd&amp;p1=e%3Dvp%26p%3D9780141393391&amp;p2=e%3Ddis&amp;adce=1&amp;tld=bokus.com&amp;dtycbr=25826" data-owner="criteo-tag"></script><script src="https://static.criteo.net/js/ld/ld.js" id="criteo-loader"></script><script src="https://connect.facebook.net/sv_SE/sdk.js?hash=8c21375d8ceaf0f46c2cb44ca6468c61" async="" crossorigin="anonymous"></script><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script><script type="text/javascript" async="" src="https://track.adform.net/serving/scripts/trackpoint/async/"></script><script src="https://connect.facebook.net/signals/config/522404921474218?v=2.9.22&amp;r=stable" async=""></script><script async="" src="https://connect.facebook.net/en_US/fbevents.js"></script><script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script type="text/javascript" src="https://m.addthis.com/live/red_lojson/300lo.json?si=5f0b30c10d069c81&amp;bkl=0&amp;bl=1&amp;sid=5f0b30c10d069c81&amp;pub=ra-4fd6f84f77dcc16a&amp;rev=v8.28.7-wp&amp;ln=sv&amp;pc=men&amp;cb=0&amp;ab=-&amp;dp=www.bokus.com&amp;fp=bok%2F9780141393391%2Fteenage-mutant-ninja-turtles-the-idw-collection-volume-1%2F&amp;fr=&amp;of=0&amp;pd=0&amp;irt=0&amp;vcl=0&amp;md=0&amp;ct=1&amp;tct=0&amp;abt=0&amp;cdn=0&amp;pi=1&amp;rb=0&amp;gen=100&amp;chr=ISO-8859-1&amp;colc=1594568897830&amp;jsl=1&amp;uvs=5f0b30c1bbb1f499000&amp;skipb=1&amp;callback=addthis.cbs.jsonp__54708566605016220"></script><script type="text/javascript" src="https://v1.addthisedge.com/live/boost/ra-4fd6f84f77dcc16a/_ate.track.config_resp"></script><script type="text/javascript" src="https://z.moatads.com/addthismoatframe568911941483/moatframe.js"></script>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta http-equiv="Content-Language" content="sv">
		<meta name="viewport" content="width=990">		<title>Teenage Mutant Ninja Turtles The Idw Collection Volume 1 - Erik Burnham, Kevin Eastman, Tom Waltz - Bok (9780141393391) | Bokus</title>
		<meta name="title" content="Teenage Mutant Ninja Turtles The Idw Collection Volume 1 - Erik Burnham, Kevin Eastman, Tom Waltz - Bok (9780141393391) | Bokus">
		<meta name="description" content="Pris: 339 kr. Inbunden, 2015. Tillfälligt slut. Bevaka Teenage Mutant Ninja Turtles The Idw Collection Volume 1 så får du ett mejl när boken går att köpa igen.">
		<meta property="fb:app_id" content="126656811299">
		<meta property="og:site_name" content="Bokus.com">
		<meta property="og:title" content="Teenage Mutant Ninja Turtles The Idw Collection Volume 1 av Erik Burnham, Kevin Eastman, Tom Waltz (Bok)">
		<meta property="og:description" content="IDW's relaunch of the Teenage Mutant Ninja Turtles has been a hit with fans and critics alike. Now, collect the series in all-new oversized hardcovers that present the stories in recommended reading order.  Collects the first 12 issues of the new ongoing series, plus the Raphael, Michelangelo, Donatello, Leonardo, and Splinter Micro-Series one-shots spliced in-between.">		<meta property="og:type" content="Book">		<meta property="book:isbn" content="9780141393391">		<meta property="og:image" content="https://image.bokus.com/images/9780141393391_200x_teenage-mutant-ninja-turtles-the-idw-collection-volume-1">		<meta property="og:url" content="https://www.bokus.com/bok/9780141393391/teenage-mutant-ninja-turtles-the-idw-collection-volume-1/">
		<link rel="canonical" href="https://www.bokus.com/bok/9780141393391/teenage-mutant-ninja-turtles-the-idw-collection-volume-1/"><link rel="stylesheet" href="/dist/css/main-v-1591780745042.css">
<!--[if lte IE 9]>
<link rel="stylesheet" href="/dist/css/main_lte_ie9-v-1591780745042.css">
<![endif]-->

<link rel="shortcut icon" type="image/x-icon" href="/includes/css/screen/bin/favicon_5.0.ico"><link rel="stylesheet" href="/dist/css/bokus-v-1591780745042.css">



  <link rel="stylesheet" href="/dist/css/header-v-1591780745042.css">  
  <link rel="stylesheet" href="/dist/css/footer-v-1591780745042.css">




<link rel="stylesheet" href="/dist/css/desktop-v-1591780745042.css">


    <script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-TPG9DKL"></script><script>
        var tw_conf_id="bokus";
        var tw_language="swedish";
        var tw_skipDefaultCss=true;
        var tw_localCss="/includes/css/screen/product_twingly-widget-5.0.0.css";
    </script>
<script type="text/javascript">document.documentElement.className=document.documentElement.className.replace("no-js","js");</script><!--[if lt IE  9]>
    <script type="text/javascript" src="/includes/js/html5shiv.min.js"></script>
<![endif]-->


  

  <script>

    var test_variant;

    

    if (!!test_variant) {
      var ab_test = {
        name: 'no_active_test',
        variant: test_variant
      }
    }
  </script>
  


<script>

    var dataLayer = [];

    var customPageViewUrl = customPageViewUrl || '';
    if(customPageViewUrl) dataLayer.push({'customPageViewUrl': customPageViewUrl});


    

      dataLayer.push({'productPageProductFormat': 'Inbunden'});
      dataLayer.push({'productPageProductStatus': '21'});
      dataLayer.push({'productPageHasPlayTeaser': '0'});

    


</script>


<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+
    dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-TPG9DKL');</script>

	<style></style><style type="text/css">.at-icon{fill:#fff;border:0}.at-icon-wrapper{display:inline-block;overflow:hidden}a .at-icon-wrapper{cursor:pointer}.at-rounded,.at-rounded-element .at-icon-wrapper{border-radius:12%}.at-circular,.at-circular-element .at-icon-wrapper{border-radius:50%}.addthis_32x32_style .at-icon{width:2pc;height:2pc}.addthis_24x24_style .at-icon{width:24px;height:24px}.addthis_20x20_style .at-icon{width:20px;height:20px}.addthis_16x16_style .at-icon{width:1pc;height:1pc}#at16lb{display:none;position:absolute;top:0;left:0;width:100%;height:100%;z-index:1001;background-color:#000;opacity:.001}#at_complete,#at_error,#at_share,#at_success{position:static!important}.at15dn{display:none}#at15s,#at16p,#at16p form input,#at16p label,#at16p textarea,#at_share .at_item{font-family:arial,helvetica,tahoma,verdana,sans-serif!important;font-size:9pt!important;outline-style:none;outline-width:0;line-height:1em}* html #at15s.mmborder{position:absolute!important}#at15s.mmborder{position:fixed!important;width:250px!important}#at15s{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABtJREFUeNpiZGBgaGAgAjAxEAlGFVJHIUCAAQDcngCUgqGMqwAAAABJRU5ErkJggg==);float:none;line-height:1em;margin:0;overflow:visible;padding:5px;text-align:left;position:absolute}#at15s a,#at15s span{outline:0;direction:ltr;text-transform:none}#at15s .at-label{margin-left:5px}#at15s .at-icon-wrapper{width:1pc;height:1pc;vertical-align:middle}#at15s .at-icon{width:1pc;height:1pc}.at4-icon{display:inline-block;background-repeat:no-repeat;background-position:top left;margin:0;overflow:hidden;cursor:pointer}.addthis_16x16_style .at4-icon,.addthis_default_style .at4-icon,.at4-icon,.at-16x16{width:1pc;height:1pc;line-height:1pc;background-size:1pc!important}.addthis_32x32_style .at4-icon,.at-32x32{width:2pc;height:2pc;line-height:2pc;background-size:2pc!important}.addthis_24x24_style .at4-icon,.at-24x24{width:24px;height:24px;line-height:24px;background-size:24px!important}.addthis_20x20_style .at4-icon,.at-20x20{width:20px;height:20px;line-height:20px;background-size:20px!important}.at4-icon.circular,.circular .at4-icon,.circular.aticon{border-radius:50%}.at4-icon.rounded,.rounded .at4-icon{border-radius:4px}.at4-icon-left{float:left}#at15s .at4-icon{text-indent:20px;padding:0;overflow:visible;white-space:nowrap;background-size:1pc;width:1pc;height:1pc;background-position:top left;display:inline-block;line-height:1pc}.addthis_vertical_style .at4-icon,.at4-follow-container .at4-icon{margin-right:5px}html>body #at15s{width:250px!important}#at15s.atm{background:none!important;padding:0!important;width:10pc!important}#at15s_inner{background:#fff;border:1px solid #fff;margin:0}#at15s_head{position:relative;background:#f2f2f2;padding:4px;cursor:default;border-bottom:1px solid #e5e5e5}.at15s_head_success{background:#cafd99!important;border-bottom:1px solid #a9d582!important}.at15s_head_success a,.at15s_head_success span{color:#000!important;text-decoration:none}#at15s_brand,#at15sptx,#at16_brand{position:absolute}#at15s_brand{top:4px;right:4px}.at15s_brandx{right:20px!important}a#at15sptx{top:4px;right:4px;text-decoration:none;color:#4c4c4c;font-weight:700}#at15sptx:hover{text-decoration:underline}#at16_brand{top:5px;right:30px;cursor:default}#at_hover{padding:4px}#at_hover .at_item,#at_share .at_item{background:#fff!important;float:left!important;color:#4c4c4c!important}#at_share .at_item .at-icon-wrapper{margin-right:5px}#at_hover .at_bold{font-weight:700;color:#000!important}#at_hover .at_item{width:7pc!important;padding:2px 3px!important;margin:1px;text-decoration:none!important}#at_hover .at_item.athov,#at_hover .at_item:focus,#at_hover .at_item:hover{margin:0!important}#at_hover .at_item.athov,#at_hover .at_item:focus,#at_hover .at_item:hover,#at_share .at_item.athov,#at_share .at_item:hover{background:#f2f2f2!important;border:1px solid #e5e5e5;color:#000!important;text-decoration:none}.ipad #at_hover .at_item:focus{background:#fff!important;border:1px solid #fff}.at15t{display:block!important;height:1pc!important;line-height:1pc!important;padding-left:20px!important;background-position:0 0;text-align:left}.addthis_button,.at15t{cursor:pointer}.addthis_toolbox a.at300b,.addthis_toolbox a.at300m{width:auto}.addthis_toolbox a{margin-bottom:5px;line-height:initial}.addthis_toolbox.addthis_vertical_style{width:200px}.addthis_button_facebook_like .fb_iframe_widget{line-height:100%}.addthis_button_facebook_like iframe.fb_iframe_widget_lift{max-width:none}.addthis_toolbox a.addthis_button_counter,.addthis_toolbox a.addthis_button_facebook_like,.addthis_toolbox a.addthis_button_facebook_send,.addthis_toolbox a.addthis_button_facebook_share,.addthis_toolbox a.addthis_button_foursquare,.addthis_toolbox a.addthis_button_linkedin_counter,.addthis_toolbox a.addthis_button_pinterest_pinit,.addthis_toolbox a.addthis_button_tweet{display:inline-block}.addthis_toolbox span.addthis_follow_label{display:none}.addthis_toolbox.addthis_vertical_style span.addthis_follow_label{display:block;white-space:nowrap}.addthis_toolbox.addthis_vertical_style a{display:block}.addthis_toolbox.addthis_vertical_style.addthis_32x32_style a{line-height:2pc;height:2pc}.addthis_toolbox.addthis_vertical_style .at300bs{margin-right:4px;float:left}.addthis_toolbox.addthis_20x20_style span{line-height:20px}.addthis_toolbox.addthis_32x32_style span{line-height:2pc}.addthis_toolbox.addthis_pill_combo_style .addthis_button_compact .at15t_compact,.addthis_toolbox.addthis_pill_combo_style a{float:left}.addthis_toolbox.addthis_pill_combo_style a.addthis_button_tweet{margin-top:-2px}.addthis_toolbox.addthis_pill_combo_style .addthis_button_compact .at15t_compact{margin-right:4px}.addthis_default_style .addthis_separator{margin:0 5px;display:inline}div.atclear{clear:both}.addthis_default_style .addthis_separator,.addthis_default_style .at4-icon,.addthis_default_style .at300b,.addthis_default_style .at300bo,.addthis_default_style .at300bs,.addthis_default_style .at300m{float:left}.at300b img,.at300bo img{border:0}a.at300b .at4-icon,a.at300m .at4-icon{display:block}.addthis_default_style .at300b,.addthis_default_style .at300bo,.addthis_default_style .at300m{padding:0 2px}.at300b,.at300bo,.at300bs,.at300m{cursor:pointer}.addthis_button_facebook_like.at300b:hover,.addthis_button_facebook_like.at300bs:hover,.addthis_button_facebook_send.at300b:hover,.addthis_button_facebook_send.at300bs:hover{opacity:1}.addthis_20x20_style .at15t,.addthis_20x20_style .at300bs{overflow:hidden;display:block;height:20px!important;width:20px!important;line-height:20px!important}.addthis_32x32_style .at15t,.addthis_32x32_style .at300bs{overflow:hidden;display:block;height:2pc!important;width:2pc!important;line-height:2pc!important}.at300bs{overflow:hidden;display:block;background-position:0 0;height:1pc;width:1pc;line-height:1pc!important}.addthis_default_style .at15t_compact,.addthis_default_style .at15t_expanded{margin-right:4px}#at_share .at_item{width:123px!important;padding:4px;margin-right:2px;border:1px solid #fff}#at16p{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABtJREFUeNpiZGBgaGAgAjAxEAlGFVJHIUCAAQDcngCUgqGMqwAAAABJRU5ErkJggg==);z-index:10000001;position:absolute;top:50%;left:50%;width:300px;padding:10px;margin:0 auto;margin-top:-185px;margin-left:-155px;font-family:arial,helvetica,tahoma,verdana,sans-serif;font-size:9pt;color:#5e5e5e}#at_share{margin:0;padding:0}#at16pt{position:relative;background:#f2f2f2;height:13px;padding:5px 10px}#at16pt a,#at16pt h4{font-weight:700}#at16pt h4{display:inline;margin:0;padding:0;font-size:9pt;color:#4c4c4c;cursor:default}#at16pt a{position:absolute;top:5px;right:10px;color:#4c4c4c;text-decoration:none;padding:2px}#at15sptx:focus,#at16pt a:focus{outline:thin dotted}#at15s #at16pf a{top:1px}#_atssh{width:1px!important;height:1px!important;border:0!important}.atm{width:10pc!important;padding:0;margin:0;line-height:9pt;letter-spacing:normal;font-family:arial,helvetica,tahoma,verdana,sans-serif;font-size:9pt;color:#444;background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABtJREFUeNpiZGBgaGAgAjAxEAlGFVJHIUCAAQDcngCUgqGMqwAAAABJRU5ErkJggg==);padding:4px}.atm-f{text-align:right;border-top:1px solid #ddd;padding:5px 8px}.atm-i{background:#fff;border:1px solid #d5d6d6;padding:0;margin:0;box-shadow:1px 1px 5px rgba(0,0,0,.15)}.atm-s{margin:0!important;padding:0!important}.atm-s a:focus{border:transparent;outline:0;transition:none}#at_hover.atm-s a,.atm-s a{display:block;text-decoration:none;padding:4px 10px;color:#235dab!important;font-weight:400;font-style:normal;transition:none}#at_hover.atm-s .at_bold{color:#235dab!important}#at_hover.atm-s a:hover,.atm-s a:hover{background:#2095f0;text-decoration:none;color:#fff!important}#at_hover.atm-s .at_bold{font-weight:700}#at_hover.atm-s a:hover .at_bold{color:#fff!important}.atm-s a .at-label{vertical-align:middle;margin-left:5px;direction:ltr}.at_PinItButton{display:block;width:40px;height:20px;padding:0;margin:0;background-image:url(//s7.addthis.com/static/t00/pinit00.png);background-repeat:no-repeat}.at_PinItButton:hover{background-position:0 -20px}.addthis_toolbox .addthis_button_pinterest_pinit{position:relative}.at-share-tbx-element .fb_iframe_widget span{vertical-align:baseline!important}#at16pf{height:auto;text-align:right;padding:4px 8px}.at-privacy-info{position:absolute;left:7px;bottom:7px;cursor:pointer;text-decoration:none;font-family:helvetica,arial,sans-serif;font-size:10px;line-height:9pt;letter-spacing:.2px;color:#666}.at-privacy-info:hover{color:#000}.body .wsb-social-share .wsb-social-share-button-vert{padding-top:0;padding-bottom:0}.body .wsb-social-share.addthis_counter_style .addthis_button_tweet.wsb-social-share-button{padding-top:40px}.body .wsb-social-share.addthis_counter_style .addthis_button_facebook_like.wsb-social-share-button{padding-top:21px}@media print{#at4-follow,#at4-share,#at4-thankyou,#at4-whatsnext,#at4m-mobile,#at15s,.at4,.at4-recommended{display:none!important}}@media screen and (max-width:400px){.at4win{width:100%}}@media screen and (max-height:700px) and (max-width:400px){.at4-thankyou-inner .at4-recommended-container{height:122px;overflow:hidden}.at4-thankyou-inner .at4-recommended .at4-recommended-item:first-child{border-bottom:1px solid #c5c5c5}}</style><style type="text/css">.at-branding-logo{font-family:helvetica,arial,sans-serif;text-decoration:none;font-size:10px;display:inline-block;margin:2px 0;letter-spacing:.2px}.at-branding-logo .at-branding-icon{background-image:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAMAAAC67D+PAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAZQTFRF////+GlNUkcc1QAAAB1JREFUeNpiYIQDBjQmAwMmkwEM0JnY1WIxFyDAABGeAFEudiZsAAAAAElFTkSuQmCC")}.at-branding-logo .at-branding-icon,.at-branding-logo .at-privacy-icon{display:inline-block;height:10px;width:10px;margin-left:4px;margin-right:3px;margin-bottom:-1px;background-repeat:no-repeat}.at-branding-logo .at-privacy-icon{background-image:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAKCAMAAABR24SMAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABhQTFRF8fr9ot/xXcfn2/P5AKva////////AKTWodjhjAAAAAd0Uk5T////////ABpLA0YAAAA6SURBVHjaJMzBDQAwCAJAQaj7b9xifV0kUKJ9ciWxlzWEWI5gMF65KUTv0VKkjVeTerqE/x7+9BVgAEXbAWI8QDcfAAAAAElFTkSuQmCC")}.at-branding-logo span{text-decoration:none}.at-branding-logo .at-branding-addthis,.at-branding-logo .at-branding-powered-by{color:#666}.at-branding-logo .at-branding-addthis:hover{color:#333}.at-cv-with-image .at-branding-addthis,.at-cv-with-image .at-branding-addthis:hover{color:#fff}a.at-branding-logo:visited{color:initial}.at-branding-info{display:inline-block;padding:0 5px;color:#666;border:1px solid #666;border-radius:50%;font-size:10px;line-height:9pt;opacity:.7;transition:all .3s ease;text-decoration:none}.at-branding-info span{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px}.at-branding-info:before{content:'i';font-family:Times New Roman}.at-branding-info:hover{color:#0780df;border-color:#0780df}</style><link rel="stylesheet" type="text/css" href="/includes/css/screen/product_twingly-widget-5.0.0.css"><script async="" src="https://static.hotjar.com/c/hotjar-466035.js?sv=6"></script><style type="text/css"></style><script type="text/javascript" language="JavaScript" src="https://s3.amazonaws.com/www2.twingly.com/3E9A387255305AC7069D96699A62B5"></script><script charset="utf-8" src="https://platform.twitter.com/js/button.1378e6a69a23712ca26755ee3c4084b4.js"></script><style type="text/css">.fb_hidden{position:absolute;top:-10000px;z-index:10001}.fb_reposition{overflow:hidden;position:relative}.fb_invisible{display:none}.fb_reset{background:none;border:0;border-spacing:0;color:#000;cursor:auto;direction:ltr;font-family:"lucida grande", tahoma, verdana, arial, sans-serif;font-size:11px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:1;margin:0;overflow:visible;padding:0;text-align:left;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;visibility:visible;white-space:normal;word-spacing:normal}.fb_reset>div{overflow:hidden}@keyframes fb_transform{from{opacity:0;transform:scale(.95)}to{opacity:1;transform:scale(1)}}.fb_animate{animation:fb_transform .3s forwards}
.fb_dialog{background:rgba(82, 82, 82, .7);position:absolute;top:-10000px;z-index:10001}.fb_dialog_advanced{border-radius:8px;padding:10px}.fb_dialog_content{background:#fff;color:#373737}.fb_dialog_close_icon{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 0 transparent;cursor:pointer;display:block;height:15px;position:absolute;right:18px;top:17px;width:15px}.fb_dialog_mobile .fb_dialog_close_icon{left:5px;right:auto;top:5px}.fb_dialog_padding{background-color:transparent;position:absolute;width:1px;z-index:-1}.fb_dialog_close_icon:hover{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -15px transparent}.fb_dialog_close_icon:active{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -30px transparent}.fb_dialog_iframe{line-height:0}.fb_dialog_content .dialog_title{background:#6d84b4;border:1px solid #365899;color:#fff;font-size:14px;font-weight:bold;margin:0}.fb_dialog_content .dialog_title>span{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yd/r/Cou7n-nqK52.gif) no-repeat 5px 50%;float:left;padding:5px 0 7px 26px}body.fb_hidden{height:100%;left:0;margin:0;overflow:visible;position:absolute;top:-10000px;transform:none;width:100%}.fb_dialog.fb_dialog_mobile.loading{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ya/r/3rhSv5V8j3o.gif) white no-repeat 50% 50%;min-height:100%;min-width:100%;overflow:hidden;position:absolute;top:0;z-index:10001}.fb_dialog.fb_dialog_mobile.loading.centered{background:none;height:auto;min-height:initial;min-width:initial;width:auto}.fb_dialog.fb_dialog_mobile.loading.centered #fb_dialog_loader_spinner{width:100%}.fb_dialog.fb_dialog_mobile.loading.centered .fb_dialog_content{background:none}.loading.centered #fb_dialog_loader_close{clear:both;color:#fff;display:block;font-size:18px;padding-top:20px}#fb-root #fb_dialog_ipad_overlay{background:rgba(0, 0, 0, .4);bottom:0;left:0;min-height:100%;position:absolute;right:0;top:0;width:100%;z-index:10000}#fb-root #fb_dialog_ipad_overlay.hidden{display:none}.fb_dialog.fb_dialog_mobile.loading iframe{visibility:hidden}.fb_dialog_mobile .fb_dialog_iframe{position:sticky;top:0}.fb_dialog_content .dialog_header{background:linear-gradient(from(#738aba), to(#2c4987));border-bottom:1px solid;border-color:#043b87;box-shadow:white 0 1px 1px -1px inset;color:#fff;font:bold 14px Helvetica, sans-serif;text-overflow:ellipsis;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0;vertical-align:middle;white-space:nowrap}.fb_dialog_content .dialog_header table{height:43px;width:100%}.fb_dialog_content .dialog_header td.header_left{font-size:12px;padding-left:5px;vertical-align:middle;width:60px}.fb_dialog_content .dialog_header td.header_right{font-size:12px;padding-right:5px;vertical-align:middle;width:60px}.fb_dialog_content .touchable_button{background:linear-gradient(from(#4267B2), to(#2a4887));background-clip:padding-box;border:1px solid #29487d;border-radius:3px;display:inline-block;line-height:18px;margin-top:3px;max-width:85px;padding:4px 12px;position:relative}.fb_dialog_content .dialog_header .touchable_button input{background:none;border:none;color:#fff;font:bold 12px Helvetica, sans-serif;margin:2px -12px;padding:2px 6px 3px 6px;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog_content .dialog_header .header_center{color:#fff;font-size:16px;font-weight:bold;line-height:18px;text-align:center;vertical-align:middle}.fb_dialog_content .dialog_content{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/y9/r/jKEcVPZFk-2.gif) no-repeat 50% 50%;border:1px solid #4a4a4a;border-bottom:0;border-top:0;height:150px}.fb_dialog_content .dialog_footer{background:#f5f6f7;border:1px solid #4a4a4a;border-top-color:#ccc;height:40px}#fb_dialog_loader_close{float:left}.fb_dialog.fb_dialog_mobile .fb_dialog_close_button{text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog.fb_dialog_mobile .fb_dialog_close_icon{visibility:hidden}#fb_dialog_loader_spinner{animation:rotateSpinner 1.2s linear infinite;background-color:transparent;background-image:url(https://static.xx.fbcdn.net/rsrc.php/v3/yD/r/t-wz8gw1xG1.png);background-position:50% 50%;background-repeat:no-repeat;height:24px;width:24px}@keyframes rotateSpinner{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}
.fb_iframe_widget{display:inline-block;position:relative}.fb_iframe_widget span{display:inline-block;position:relative;text-align:justify}.fb_iframe_widget iframe{position:absolute}.fb_iframe_widget_fluid_desktop,.fb_iframe_widget_fluid_desktop span,.fb_iframe_widget_fluid_desktop iframe{max-width:100%}.fb_iframe_widget_fluid_desktop iframe{min-width:220px;position:relative}.fb_iframe_widget_lift{z-index:1}.fb_iframe_widget_fluid{display:inline}.fb_iframe_widget_fluid span{width:100%}</style></head>

	<body class="product-page">
		
			
		
		
		<noscript>&lt;iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TPG9DKL" height="0" width="0" style="display:none;visibility:hidden"&gt;&lt;/iframe&gt;</noscript>

    <script>
  dataLayer.push({"event":"ViewContent","contentType":"product","contentIds":["9780141393391"],"contentName":"Product Page"});
</script>



		<div class="page">

			<div id="mobile-link" class="hidden">
				<a href="?use_mobile=1" rel="nofollow">Gå till mobilversionen av bokus.com</a>
			</div>


      
			  





    

      <header class="header">
  <div class="sub-menu">
  <div class="sub-menu__content">
    <ul id="puff-list" class="sub-menu__list sub-menu__list--puffs not-logged-in">
      <li class="sub-menu__list-item">
        <div class="sub-menu__puff">
          <svg class="icon icon--checkmark icon--orange icon--small" viewBox=" 0 0 48 48">
  <use data-color="orange" xlink:href="/icons/svg/checkmark.svg#checkmark"></use>
</svg>
          Fri frakt
        </div>
      </li>
      <li class="sub-menu__list-item">
        <div class="sub-menu__puff">
          <svg class="icon icon--checkmark icon--orange icon--small" viewBox=" 0 0 48 48">
  <use data-color="orange" xlink:href="/icons/svg/checkmark.svg#checkmark"></use>
</svg>
          Billiga böcker
        </div>
      </li>
      <li class="sub-menu__list-item">
        <div class="sub-menu__puff">
          <svg class="icon icon--checkmark icon--orange icon--small" viewBox=" 0 0 48 48">
  <use data-color="orange" xlink:href="/icons/svg/checkmark.svg#checkmark"></use>
</svg>
          Snabba leveranser
        </div>
      </li>
    </ul>
    <ul class="sub-menu__list sub-menu__list--links">
      <li id="user-links" class="sub-menu__list-item sub-menu__list-item--user">
        <span id="js-login-status">Inloggad som </span>
        <a class="sub-menu__link" href="/cgi-bin/logout_user_info.cgi">(logga ut)</a>
      </li>
      <li class="sub-menu__list-item">
        <a class="sub-menu__link" data-track="click" data-track-category="Header" data-track-action="Click" data-track-label="Mina sidor" href="/cgi-bin/log_in_real.cgi">
          <svg class="icon icon--person icon--gray icon--small" viewBox=" 0 0 48 48">
  <use data-color="gray" xlink:href="/icons/svg/person.svg#person"></use>
</svg>
          Mina sidor
        </a>
      </li>
      <li class="sub-menu__list-item">
        <a class="sub-menu__link" data-track="click" data-track-category="Header" data-track-action="Click" data-track-label="Kundservice" href="/kundservice">
          <svg class="icon icon--chat_bubble icon--gray icon--small" viewBox=" 0 0 48 48">
  <use data-color="gray" xlink:href="/icons/svg/chat_bubble.svg#chat_bubble"></use>
</svg>
          Kundservice
        </a>
      </li>
      <li class="sub-menu__list-item">
        <a class="sub-menu__link" data-track="click" data-track-category="Header" data-track-action="Click" data-track-label="Företag" href="/foretag">Företag</a>
      </li>
      <li class="sub-menu__list-item">
        <a class="sub-menu__link" data-track="click" data-track-category="Header" data-track-action="Click" data-track-label="Bibliotek &amp; Off. förvaltning" href="/bibliotek-offentlig-verksamhet">Bibliotek &amp; Off. förvaltning</a>
      </li>
    </ul>
  </div>
</div>

  <div class="header__container">
    <div class="header__main-section">
      <div class="header__logo-wrapper">
        <a href="/" data-track="click" data-track-action="click" data-track-category="header" data-track-label="logo">
          <img class="header__logo" src="/img/bokus_logo.svg" onerror="this.src='/img/bokus_logo.png'; this.onerror=null;" alt="Bokus bokhandel - välj bland över sex miljoner böcker" title="Till vår bokhandels startsida">
        </a>
      </div>
      <div class="header__search-wrapper">
        <div class="header__search">
          <div class="search">

  <div class="search-box" role="search">
  <form data-search-wrapper="" action="/cgi-bin/product_search.cgi" data-track="submit" data-track-category="header" data-track-label="search" data-track-action="submit">
      <input type="hidden" name="ac_used" value="no" id="ac_used">
      <input class="search-box__input ac_input" type="search" data-test-element="searchInput" placeholder="Sök bland 10 miljoner böcker" auto:complete="off" autocorrect="off" autocapitalize="off" tabindex="1" data-search="" name="search_word" value="" autocomplete="off">
      <button id="qs-b" tabindex="2" class="search-box__search-button" type="submit">
        <svg class="icon icon--search icon--white icon--medium" viewBox=" 0 0 48 48">
  <use data-color="white" xlink:href="/icons/svg/search.svg#search"></use>
</svg>
      </button>
      <span id="qs-kw-clear" style="display: none;"></span>
    </form>
  </div>
</div>
<div id="ac-wrapper" class="search-suggestions"></div>

        </div>
      </div>
      <div class="header__cart-wrapper">
        <div class="header__cart basket-widget basket-empty">
          <button class="cart js-basket-toggler" data-track="click" data-track-module="basket" data-track-category="header" data-track-action="click" data-track-label="Basket empty">
  <div class="cart__label">Varukorg
    <svg class="icon icon--keyboard_arrow_down icon--dark-gray icon--medium" viewBox=" 0 0 48 48">
  <use data-color="dark-gray" xlink:href="/icons/svg/keyboard_arrow_down.svg#keyboard_arrow_down"></use>
</svg>
  </div>
  <svg class="icon icon--shopping_basket icon--dark-gray icon--large" viewBox=" 0 0 48 48">
  <use data-color="dark-gray" xlink:href="/icons/svg/shopping_basket.svg#shopping_basket"></use>
</svg>
  <div class="cart__items-count">
    <span class="basket-toggler__items"></span>
  </div>
</button>
   <div id="js-basket-popup" class="basket basket--popup dialog dialog--popup cart-empty"><div id="js-basket-content" class="basket-content " "="">

                    <h2>Din varukorg</h2>

                    



                        <p>Din varukorg är tom.</p>

                        


                    </div></div>

    <div id="js-add-to-basket-popup" class="basket-add basket-add--popup dialog dialog--popup fixedsticky"></div>



          <a href="/cgi-bin/cart_send_in.cgi" data-track-pageview="" data-track-url="/virtual/basket/goto-checkout" data-track-title="Go to checkout" class="js-goto-checkout button button--primary header__cart-link basket-empty">
            <span class="basket-widget__CTA--regular">Till kassan</span>
            <span class="basket-widget__CTA--punchout">Till punchout</span>
          </a>
        </div>
      </div>
    </div>
    <div class="header__navigation">
    
      

<div class="navigation-menu">
  <ul class="navigation-menu__list">
    <li class="navigation-menu__list-item">
      <a data-mega-menu-toggle="" class="navigation-menu__link" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Item Våra böcker" href="javascript:;" aria-haspopup="true">
        Våra böcker
        <svg class="icon icon--keyboard_arrow_down icon--dark-gray icon--medium" viewBox=" 0 0 48 48">
  <use data-color="dark-gray" xlink:href="/icons/svg/keyboard_arrow_down.svg#keyboard_arrow_down"></use>
</svg>
      </a>
      




<div data-mega-menu="" class="categories-menu" role="navigation" aria-label="Kategorier">
  <div class="categories-menu__wrapper">
    <div class="categories-menu__content">
      <div class="categories-menu__block categories-menu__block--left">
        <p class="categories-menu__list-title">Populärt</p>
        <ul class="categories-menu__list">
          
            <li class="categories-menu__list-item">
              <a data-category="popular" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Barn- &amp; tonårsböcker" href="/kategori/barn-tonar" class="categories-menu__link">Barn- &amp; tonårsböcker</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a data-category="popular" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Pocketböcker" href="/pocketbocker" class="categories-menu__link">Pocketböcker</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a data-category="popular" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Deckare" href="/kategori/deckare" class="categories-menu__link">Deckare</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a data-category="popular" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Skönlitteratur" href="/kategori/skonlitteratur" class="categories-menu__link">Skönlitteratur</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a data-category="popular" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Student" href="/student" class="categories-menu__link">Student</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a data-category="popular" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub E-böcker" href="/e-bocker" class="categories-menu__link">E-böcker</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a data-category="popular" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Ljudböcker" href="/ljudbocker" class="categories-menu__link">Ljudböcker</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a data-category="popular" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Topplistor" href="/topplistor/bokustoppen" class="categories-menu__link">Topplistor</a>
            </li>
          
        </ul>
      </div>
      <div class="categories-menu__block categories-menu__block--center">
        <p class="categories-menu__list-title">Ämnen</p>
        <ul class="categories-menu__list">
          
            <li class="categories-menu__list-item">
              <a href="/kategori/barn-tonar" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Barn &amp; tonår" class="categories-menu__link">Barn &amp; tonår</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/biografier" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Biografier" class="categories-menu__link">Biografier</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/data-it" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Data &amp; IT" class="categories-menu__link">Data &amp; IT</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/deckare" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Deckare" class="categories-menu__link">Deckare</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/djur-natur" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Djur &amp; natur" class="categories-menu__link">Djur &amp; natur</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/ekonomi-ledarskap" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Ekonomi &amp; ledarskap" class="categories-menu__link">Ekonomi &amp; ledarskap</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/familj-halsa" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Familj &amp; hälsa" class="categories-menu__link">Familj &amp; hälsa</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/film-radio-tv" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Film, radio &amp; TV" class="categories-menu__link">Film, radio &amp; TV</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/filosofi-religion" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Filosofi &amp; religion" class="categories-menu__link">Filosofi &amp; religion</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/hem-tradgard" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Hem &amp; trädgård" class="categories-menu__link">Hem &amp; trädgård</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/historia-arkeologi" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Historia &amp; arkeologi" class="categories-menu__link">Historia &amp; arkeologi</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/juridik" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Juridik" class="categories-menu__link">Juridik</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/kultur" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Kultur" class="categories-menu__link">Kultur</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/barn-tonar/laromedel" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Läromedel" class="categories-menu__link">Läromedel</a>
            </li>
          
        </ul>
        <ul class="categories-menu__list">
          
            <li class="categories-menu__list-item">
              <a href="/kategori/mat-dryck" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Mat &amp; dryck" class="categories-menu__link">Mat &amp; dryck</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/medicin" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Medicin" class="categories-menu__link">Medicin</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/naturvetenskap-teknik" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Naturvetenskap &amp; teknik" class="categories-menu__link">Naturvetenskap &amp; teknik</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/psykologi-pedagogik" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Psykologi &amp; pedagogik" class="categories-menu__link">Psykologi &amp; pedagogik</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/resor" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Resor" class="categories-menu__link">Resor</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/samhalle-politik" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Samhälle &amp; politik" class="categories-menu__link">Samhälle &amp; politik</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/skrivbocker-kalendrar" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Skrivböcker &amp; kalendrar" class="categories-menu__link">Skrivböcker &amp; kalendrar</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/skonlitteratur" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Skönlitteratur" class="categories-menu__link">Skönlitteratur</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/sport-fritid-hobby" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Sport, fritid &amp; hobby" class="categories-menu__link">Sport, fritid &amp; hobby</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/sprak-ordbocker" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Språk &amp; ordböcker" class="categories-menu__link">Språk &amp; ordböcker</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/tecknade-serier" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Tecknade serier" class="categories-menu__link">Tecknade serier</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/trend-livsstil" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Trend &amp; livsstil" class="categories-menu__link">Trend &amp; livsstil</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/kategori/uppslagsverk-referenser" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Uppslagsverk &amp; referenser" class="categories-menu__link">Uppslagsverk &amp; referenser</a>
            </li>
          
        </ul>

      </div>
      <div class="categories-menu__block categories-menu__block--right">
        <p class="categories-menu__list-title">Barnböcker</p>
        <ul class="categories-menu__list">
          
            <li class="categories-menu__list-item">
              <a href="/barn-tonar/0-3" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Läsålder 0-3 år" class="categories-menu__link">Läsålder 0-3 år</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/barn-tonar/3-6" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Läsålder 3-6 år" class="categories-menu__link">Läsålder 3-6 år</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/barn-tonar/6-9" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Läsålder 6-9 år" class="categories-menu__link">Läsålder 6-9 år</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/barn-tonar/9-12" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Läsålder 9-12 år" class="categories-menu__link">Läsålder 9-12 år</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/barn-tonar/12-15" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Läsålder 12-15 år" class="categories-menu__link">Läsålder 12-15 år</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/barn-tonar/unga-vuxna" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Unga vuxna" class="categories-menu__link">Unga vuxna</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/barn-tonar/lattlast" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Lättläst" class="categories-menu__link">Lättläst</a>
            </li>
          
        </ul>
        <p class="categories-menu__list-title">Boktips</p>
        <ul class="categories-menu__list">
          
            <li class="categories-menu__list-item">
              <a href="/nyheter/aktuellt-just-nu" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Aktuella böcker" class="categories-menu__link">Aktuella böcker</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/vi-kan-bocker" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Redaktionens tips" class="categories-menu__link">Redaktionens tips</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/prisbelonade-bocker" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Prisvinnare" class="categories-menu__link">Prisvinnare</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/nyhetsbrev" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Nyhetsbrev" class="categories-menu__link">Nyhetsbrev</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a href="/populara-forfattare" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Populära författare" class="categories-menu__link">Populära författare</a>
            </li>
          
        </ul>
      </div>
      <div class="categories-menu__block">
        <p class="categories-menu__list-title">Nyheter</p>
        <ul class="categories-menu__list">
          
            <li class="categories-menu__list-item">
              <a data-category="news" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Populära nyheter" href="/nyheter" class="categories-menu__link">Populära nyheter</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a data-category="news" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Pocketnyheter" href="/pocketbocker" class="categories-menu__link">Pocketnyheter</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a data-category="news" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Nyheter på engelska" href="/nyheter/pa-engelska" class="categories-menu__link">Nyheter på engelska</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a data-category="news" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Förboka" href="/nyheter/forboka" class="categories-menu__link">Förboka</a>
            </li>
          
        </ul>
        <p class="categories-menu__list-title">Erbjudanden</p>
        <ul class="categories-menu__list">
          
            <li class="categories-menu__list-item">
              <a data-category="tips" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Aktuella erbjudanden" href="/erbjudanden" class="categories-menu__link">Aktuella erbjudanden</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a data-category="tips" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Fyndhörnan" href="/fyndhornan" class="categories-menu__link">Fyndhörnan</a>
            </li>
          
            <li class="categories-menu__list-item">
              <a data-category="tips" data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Pluspriser" href="/pluspris" class="categories-menu__link">Pluspriser</a>
            </li>
          
        </ul>
      </div>
    </div>
    <div class="categories-menu__footer">
      
      <div class="categories-menu__column">
        <a data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Play" href="/play">
          <img data-lazy-src="/img/megamenu/megamenu-play.png" data-lazy-trigger="mouseover" data-lazy-trigger-element="[data-mega-menu-toggle]">
        </a>
      </div>
      <div class="categories-menu__column">
        <a data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Pluspriser" href="/pluspris">
          <img data-lazy-src="/img/megamenu/megamenu-plus.png" data-lazy-trigger="mouseover" data-lazy-trigger-element="[data-mega-menu-toggle]">
        </a>
      </div>
      <div class="categories-menu__column">
        <a data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Nyhetsbrev" href="/nyhetsbrev">
          <img data-lazy-src="/img/megamenu/megamenu-newsletter.png" data-lazy-trigger="mouseover" data-lazy-trigger-element="[data-mega-menu-toggle]">
        </a>
      </div>
      <div class="categories-menu__column">
        <a data-track="click" data-track-action="click" data-track-category="Header" data-track-label="Main Nav Sub Presentkort" href="/presentkort">
          <img data-lazy-src="/img/megamenu/megamenu-giftcard.png" data-lazy-trigger="mouseover" data-lazy-trigger-element="[data-mega-menu-toggle]">
        </a>
      </div>
    </div>
  </div>
</div>


    </li>
          
        <li class="navigation-menu__list-item">
          <a data-track="click" data-track-category="Header" data-track-action="Click" data-track-label="Main Nav Item Nyheter" href="/nyheter" class="navigation-menu__link">
            Nyheter
          </a>
        </li>
            
        <li class="navigation-menu__list-item">
          <a data-track="click" data-track-category="Header" data-track-action="Click" data-track-label="Main Nav Item Topplistor" href="/topplistor/bokustoppen" class="navigation-menu__link">
            Topplistor
          </a>
        </li>
            
        <li class="navigation-menu__list-item">
          <a data-track="click" data-track-category="Header" data-track-action="Click" data-track-label="Main Nav Item Bokus Play" href="/play" class="navigation-menu__link">
            Bokus Play
          </a>
        </li>
                  
        <li class="navigation-menu__list-item">
          <a data-track="click" data-track-category="Header" data-track-action="Click" data-track-label="Main Nav Item Erbjudanden" href="/erbjudanden" class="navigation-menu__link">
            Erbjudanden
          </a>
        </li>
                  
        <li class="navigation-menu__list-item">
          <a data-track="click" data-track-category="Header" data-track-action="Click" data-track-label="Main Nav Item Barn &amp; tonår" href="/kategori/barn-tonar" class="navigation-menu__link">
            Barn &amp; tonår
          </a>
        </li>
            
        <li class="navigation-menu__list-item">
          <a data-track="click" data-track-category="Header" data-track-action="Click" data-track-label="Main Nav Item Student" href="/student" class="navigation-menu__link">
            Student
          </a>
        </li>
            
        <li class="navigation-menu__list-item">
          <a data-track="click" data-track-category="Header" data-track-action="Click" data-track-label="Main Nav Item Pocket" href="/pocketbocker" class="navigation-menu__link">
            Pocket
          </a>
        </li>
            
        <li class="navigation-menu__list-item">
          <a data-track="click" data-track-category="Header" data-track-action="Click" data-track-label="Main Nav Item E-böcker" href="/e-bocker" class="navigation-menu__link">
            E-böcker
          </a>
        </li>
            
        <li class="navigation-menu__list-item">
          <a data-track="click" data-track-category="Header" data-track-action="Click" data-track-label="Main Nav Item Ljudböcker" href="/ljudbocker" class="navigation-menu__link">
            Ljudböcker
          </a>
        </li>
      
  </ul>
</div>

    
    </div>
  </div>
</header>


    

  


			
			
			
			
      
        
      
			
			

			<div class="main-jacket">

				<div class="main-wrapper">

					<div role="main" class="main u-cf" itemscope="" itemtype="https://schema.org/Product">

   


         
           
         

         <div class="main__inner">

             <div class="main__leftCol">

    <div class="product-image product-page__product-image">
        <img class="product-image__img" width="200" src="//image.bokus.com/images/9780141393391_200x_teenage-mutant-ninja-turtles-the-idw-collection-volume-1" alt="Teenage Mutant Ninja Turtles The Idw Collection Volume 1 (inbunden)">
    </div>

    




        <div class="Section Section--narrow Section--left">
            <strong>Fler böcker inom</strong>
            <ul class="ui-list">
                    <li class="ui-list__item"><a href="/cgi-bin/product_search.cgi?subject=1.7.18">
                        Tecknade serier
                    </a></li>
                    <li class="ui-list__item"><a href="/kategori/tecknade-serier/tecknade-serier">
                        Tecknade serier
                    </a></li>
            </ul>
        </div>

    <dl class="product-page__facts u-bc-wild-sand">            <dt>Format</dt>
            <dd>Inbunden
                        (Hardback)            </dd>
            <dt>Språk</dt>
            <dd>Engelska</dd>
             <dt>Antal sidor</dt>
             <dd>384</dd>        
            <dt>Utgivningsdatum</dt>
            <dd>2015-06-09</dd>
            <dt>Förlag</dt>
            <dd><a href="/cgi-bin/product_search.cgi?publisher=Idea%20Design%20Works">
                Idea &amp; Design Works
            </a></dd>
            <dt>Medarbetare</dt>
            <dd>Eastman, Kevin B./Lynch, Brian</dd>
            <dt>Illustrationer</dt>
            <dd>chiefly illustrations (colour)</dd>
            <dt>Volymtitel</dt>
            <dd>Volume 1 The IDW Collection</dd>
            <dt>Dimensioner</dt>
            <dd>277 x 183 x 28 mm</dd>
            <dt>Vikt</dt>
            <dl>1498 g</dl>
            <dt>Antal komponenter</dt>
            <dd>1</dd>
            <dt>ISBN</dt>
            <dd>9780141393391</dd>
    </dl>

    <div class="lazyload js-your-recent-history Section Section--padded Section--bordered Section--narrow"></div>


</div>

             <div class="main__rightCol">

        <div class="Section Section--narrow Section--padded u-p-l--3 u-bc-wild-sand">
            <h3 class="u-weight--normal">Du kanske gillar</h3>
            <ul class="ui-list ui-list--huge">
                    <li class="ui-list__item">

    <div class=" Item  Item--small">

        <div class="Item__media">
            <a href="/bok/9781684051304/teenage-mutant-ninja-turtles-the-idw-collection-volume-6/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MTMwNDsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                <img class="Item__image js-lazy " src="/img/px.gif" data-src="//image.bokus.com/images2/9781684051304_medium_teenage-mutant-ninja-turtles-the-idw-collection-volume-6" alt="Teenage Mutant Ninja Turtles The IDW Collection Volume 6" width="116" height="182">
                <noscript>&lt;img class="Item__image" src="//image.bokus.com/images2/9781684051304_medium_teenage-mutant-ninja-turtles-the-idw-collection-volume-6" alt="Teenage Mutant Ninja Turtles The IDW Collection Volume 6" width="116"&gt;</noscript>
            </a>
        </div>

        <div class="Item__info">

            <div class="Item__body">

                <a href="/bok/9781684051304/teenage-mutant-ninja-turtles-the-idw-collection-volume-6/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MTMwNDsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                    <h3 class="Item__title Item__title--small Item__title--truncated">Teenage Mutant Ninja Turtles The IDW Collection Volume 6</h3>                    <h4 class="Item__authors Item__authors--small u-textTruncate">
                            Paul Allor, 
                            Kevin Eastman, 
                            Tom Waltz
                    </h4>
                </a>


                    <div class="Item__edition milli">
                        <span class="Item__format">Inbunden</span>
                    </div>



            </div>


                <div class="Item__footer">

                    <div class="Item__pricing pricing pricing--small">

                        


                                <span class="pricing__price">439</span>


                    </div>


                </div>


        </div>
</div>

                    </li>
                    <li class="ui-list__item">

    <div class=" Item  Item--small">

        <div class="Item__media">
            <a href="/bok/9781684053704/teenage-mutant-ninja-turtles/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MzcwNDsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                <img class="Item__image js-lazy " src="/img/px.gif" data-src="//image.bokus.com/images2/9781684053704_medium_teenage-mutant-ninja-turtles" alt="Teenage Mutant Ninja Turtles" width="116" height="182">
                <noscript>&lt;img class="Item__image" src="//image.bokus.com/images2/9781684053704_medium_teenage-mutant-ninja-turtles" alt="Teenage Mutant Ninja Turtles" width="116"&gt;</noscript>
            </a>
        </div>

        <div class="Item__info">

            <div class="Item__body">

                <a href="/bok/9781684053704/teenage-mutant-ninja-turtles/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MzcwNDsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                    <h3 class="Item__title Item__title--small Item__title--truncated">Teenage Mutant Ninja Turtles</h3>                    <h4 class="Item__authors Item__authors--small u-textTruncate">
                            Ben Bates, 
                            Kevin Eastman, 
                            Dustin Weaver
                    </h4>
                </a>


                    <div class="Item__edition milli">
                        <span class="Item__format">Inbunden</span>
                    </div>



            </div>


                <div class="Item__footer">

                    <div class="Item__pricing pricing pricing--small">

                        


                                <span class="pricing__price">439</span>


                    </div>


                </div>


        </div>
</div>

                    </li>
                    <li class="ui-list__item">

    <div class=" Item  Item--small">

        <div class="Item__media">
            <a href="/bok/9781684054831/teenage-mutant-ninja-turtles/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1NDgzMTsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                <img class="Item__image js-lazy " src="/img/px.gif" data-src="//image.bokus.com/images2/9781684054831_medium_teenage-mutant-ninja-turtles" alt="Teenage Mutant Ninja Turtles" width="116" height="182">
                <noscript>&lt;img class="Item__image" src="//image.bokus.com/images2/9781684054831_medium_teenage-mutant-ninja-turtles" alt="Teenage Mutant Ninja Turtles" width="116"&gt;</noscript>
            </a>
        </div>

        <div class="Item__info">

            <div class="Item__body">

                <a href="/bok/9781684054831/teenage-mutant-ninja-turtles/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1NDgzMTsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                    <h3 class="Item__title Item__title--small Item__title--truncated">Teenage Mutant Ninja Turtles</h3>                    <h4 class="Item__authors Item__authors--small u-textTruncate">
                            Paul Allor, 
                            Kevin Eastman, 
                            Ian Flynn
                    </h4>
                </a>


                    <div class="Item__edition milli">
                        <span class="Item__format">Häftad</span>
                    </div>



            </div>


                <div class="Item__footer">

                    <div class="Item__pricing pricing pricing--small">

                        


                                <span class="pricing__price">259</span>


                    </div>


                </div>


        </div>
</div>

                    </li>
                    <li class="ui-list__item">

    <div class=" Item  Item--small">

        <div class="Item__media">
            <a href="/bok/9781684051700/tales-of-the-teenage-mutant-ninja-turtles-omnibus-vol-1/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MTcwMDsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                <img class="Item__image js-lazy " src="/img/px.gif" data-src="//image.bokus.com/images2/9781684051700_medium_tales-of-the-teenage-mutant-ninja-turtles-omnibus-vol-1" alt="Tales Of The Teenage Mutant Ninja Turtles Omnibus, Vol. 1" width="116" height="182">
                <noscript>&lt;img class="Item__image" src="//image.bokus.com/images2/9781684051700_medium_tales-of-the-teenage-mutant-ninja-turtles-omnibus-vol-1" alt="Tales Of The Teenage Mutant Ninja Turtles Omnibus, Vol. 1" width="116"&gt;</noscript>
            </a>
        </div>

        <div class="Item__info">

            <div class="Item__body">

                <a href="/bok/9781684051700/tales-of-the-teenage-mutant-ninja-turtles-omnibus-vol-1/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MTcwMDsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                    <h3 class="Item__title Item__title--small Item__title--truncated">Tales Of The Teenage Mutant Ninja Turtles Omnibus, Vol. 1</h3>                    <h4 class="Item__authors Item__authors--small u-textTruncate">
                            Ryan Brown, 
                            Kevin Eastman, 
                            Peter Laird, 
                            Jim Lawson
                    </h4>
                </a>


                    <div class="Item__edition milli">
                        <span class="Item__format">Häftad</span>
                    </div>



            </div>


                <div class="Item__footer">

                    <div class="Item__pricing pricing pricing--small">

                        


                                <span class="pricing__price">329</span>


                    </div>


                </div>


        </div>
</div>

                    </li>
                    <li class="ui-list__item">

    <div class=" Item  Item--small">

        <div class="Item__media">
            <a href="/bok/9781684050901/teenage-mutant-ninja-turtles-the-idw-collection-volume-5/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MDkwMTsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                <img class="Item__image js-lazy " src="/img/px.gif" data-src="//image.bokus.com/images2/9781684050901_medium_teenage-mutant-ninja-turtles-the-idw-collection-volume-5" alt="Teenage Mutant Ninja Turtles The Idw Collection Volume 5" width="116" height="182">
                <noscript>&lt;img class="Item__image" src="//image.bokus.com/images2/9781684050901_medium_teenage-mutant-ninja-turtles-the-idw-collection-volume-5" alt="Teenage Mutant Ninja Turtles The Idw Collection Volume 5" width="116"&gt;</noscript>
            </a>
        </div>

        <div class="Item__info">

            <div class="Item__body">

                <a href="/bok/9781684050901/teenage-mutant-ninja-turtles-the-idw-collection-volume-5/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MDkwMTsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                    <h3 class="Item__title Item__title--small Item__title--truncated">Teenage Mutant Ninja Turtles The Idw Collection Volume 5</h3>                    <h4 class="Item__authors Item__authors--small u-textTruncate">
                            Kevin Eastman, 
                            Tom Waltz
                    </h4>
                </a>


                    <div class="Item__edition milli">
                        <span class="Item__format">Inbunden</span>
                    </div>



            </div>


                <div class="Item__footer">

                    <div class="Item__pricing pricing pricing--small">

                        


                                <span class="pricing__price">439</span>


                    </div>


                </div>


        </div>
</div>

                    </li>
                    <li class="ui-list__item">

    <div class=" Item  Item--small">

        <div class="Item__media">
            <a href="/bok/9781608871858/teenage-mutant-ninja-turtles/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTYwODg3MTg1ODsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                <img class="Item__image js-lazy " src="/img/px.gif" data-src="//image.bokus.com/images2/9781608871858_medium_teenage-mutant-ninja-turtles" alt="Teenage Mutant Ninja Turtles" width="116" height="182">
                <noscript>&lt;img class="Item__image" src="//image.bokus.com/images2/9781608871858_medium_teenage-mutant-ninja-turtles" alt="Teenage Mutant Ninja Turtles" width="116"&gt;</noscript>
            </a>
        </div>

        <div class="Item__info">

            <div class="Item__body">

                <a href="/bok/9781608871858/teenage-mutant-ninja-turtles/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTYwODg3MTg1ODsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                    <h3 class="Item__title Item__title--small Item__title--truncated">Teenage Mutant Ninja Turtles</h3>                    <h4 class="Item__authors Item__authors--small u-textTruncate">
                            Andrew Farago
                    </h4>
                </a>


                    <div class="Item__edition milli">
                        <span class="Item__format">Inbunden</span>
                    </div>



            </div>


                <div class="Item__footer">

                    <div class="Item__pricing pricing pricing--small">

                        


                                <span class="pricing__price">319</span>


                    </div>


                </div>


        </div>
</div>

                    </li>
                    <li class="ui-list__item">

    <div class=" Item  Item--small">

        <div class="Item__media">
            <a href="/bok/9781613770887/teenage-mutant-ninja-turtles-the-ultimate-collection-volume-2/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTYxMzc3MDg4NzsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                <img class="Item__image js-lazy " src="/img/px.gif" data-src="//image.bokus.com/images2/9781613770887_medium_teenage-mutant-ninja-turtles-the-ultimate-collection-volume-2" alt="Teenage Mutant Ninja Turtles The Ultimate Collection Volume 2" width="116" height="182">
                <noscript>&lt;img class="Item__image" src="//image.bokus.com/images2/9781613770887_medium_teenage-mutant-ninja-turtles-the-ultimate-collection-volume-2" alt="Teenage Mutant Ninja Turtles The Ultimate Collection Volume 2" width="116"&gt;</noscript>
            </a>
        </div>

        <div class="Item__info">

            <div class="Item__body">

                <a href="/bok/9781613770887/teenage-mutant-ninja-turtles-the-ultimate-collection-volume-2/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTYxMzc3MDg4NzsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                    <h3 class="Item__title Item__title--small Item__title--truncated">Teenage Mutant Ninja Turtles The Ultimate Collection Volume 2</h3>                    <h4 class="Item__authors Item__authors--small u-textTruncate">
                            Kevin Eastman
                    </h4>
                </a>


                    <div class="Item__edition milli">
                        <span class="Item__format">Inbunden</span>
                    </div>



            </div>


                <div class="Item__footer">

                    <div class="Item__pricing pricing pricing--small">

                        


                                <span class="pricing__price">439</span>


                    </div>


                </div>


        </div>
</div>

                    </li>
                    <li class="ui-list__item">

    <div class=" Item  Item--small">

        <div class="Item__media">
            <a href="/bok/9781603094320/teenage-mutant-ninja-turtles-bodycount/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTYwMzA5NDMyMDsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                <img class="Item__image js-lazy " src="/img/px.gif" data-src="//image.bokus.com/images2/9781603094320_medium_teenage-mutant-ninja-turtles-bodycount" alt="Teenage Mutant Ninja Turtles Bodycount" width="116" height="182">
                <noscript>&lt;img class="Item__image" src="//image.bokus.com/images2/9781603094320_medium_teenage-mutant-ninja-turtles-bodycount" alt="Teenage Mutant Ninja Turtles Bodycount" width="116"&gt;</noscript>
            </a>
        </div>

        <div class="Item__info">

            <div class="Item__body">

                <a href="/bok/9781603094320/teenage-mutant-ninja-turtles-bodycount/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTYwMzA5NDMyMDsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                    <h3 class="Item__title Item__title--small Item__title--truncated">Teenage Mutant Ninja Turtles Bodycount</h3>                    <h4 class="Item__authors Item__authors--small u-textTruncate">
                            Kevin Eastman
                    </h4>
                </a>


                    <div class="Item__edition milli">
                        <span class="Item__format">Inbunden</span>
                    </div>



            </div>


                <div class="Item__footer">

                    <div class="Item__pricing pricing pricing--small">

                        


                                <span class="pricing__price">239</span>


                    </div>


                </div>


        </div>
</div>

                    </li>
                    <li class="ui-list__item">

    <div class=" Item  Item--small">

        <div class="Item__media">
            <a href="/bok/9781684052820/teenage-mutant-ninja-turtles-the-idw-collection-volume-7/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MjgyMDsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                <img class="Item__image js-lazy " src="/img/px.gif" data-src="//image.bokus.com/images2/9781684052820_medium_teenage-mutant-ninja-turtles-the-idw-collection-volume-7" alt="Teenage Mutant Ninja Turtles The Idw Collection Volume 7" width="116" height="182">
                <noscript>&lt;img class="Item__image" src="//image.bokus.com/images2/9781684052820_medium_teenage-mutant-ninja-turtles-the-idw-collection-volume-7" alt="Teenage Mutant Ninja Turtles The Idw Collection Volume 7" width="116"&gt;</noscript>
            </a>
        </div>

        <div class="Item__info">

            <div class="Item__body">

                <a href="/bok/9781684052820/teenage-mutant-ninja-turtles-the-idw-collection-volume-7/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MjgyMDsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                    <h3 class="Item__title Item__title--small Item__title--truncated">Teenage Mutant Ninja Turtles The Idw Collection Volume 7</h3>                    <h4 class="Item__authors Item__authors--small u-textTruncate">
                            Kevin Eastman, 
                            Tom Waltz
                    </h4>
                </a>


                    <div class="Item__edition milli">
                        <span class="Item__format">Inbunden</span>
                    </div>



            </div>


                <div class="Item__footer">

                    <div class="Item__pricing pricing pricing--small">

                        


                                <span class="pricing__price">519</span>


                    </div>


                </div>


        </div>
</div>

                    </li>
                    <li class="ui-list__item">

    <div class=" Item  Item--small">

        <div class="Item__media">
            <a href="/bok/9781401262785/batmantmnt-vol-1/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTQwMTI2Mjc4NTsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                <img class="Item__image js-lazy " src="/img/px.gif" data-src="//image.bokus.com/images2/9781401262785_medium_batmantmnt-vol-1" alt="Batman/Tmnt Vol. 1" width="116" height="182">
                <noscript>&lt;img class="Item__image" src="//image.bokus.com/images2/9781401262785_medium_batmantmnt-vol-1" alt="Batman/Tmnt Vol. 1" width="116"&gt;</noscript>
            </a>
        </div>

        <div class="Item__info">

            <div class="Item__body">

                <a href="/bok/9781401262785/batmantmnt-vol-1/" data-ticket="Oy9wcm9kdWN0LXBhZ2UveW91LW1heS1saWtlL3JlY29tbWVuZC1iYXNlZC1vbi1wcm9kdWN0OyM7cHJvZHVjdF9rZXk7OTc4MTQwMTI2Mjc4NTsjO1NZTiQwO05PTkU6Tk9ORTswOw">
                    <h3 class="Item__title Item__title--small Item__title--truncated">Batman/Tmnt Vol. 1</h3>                    <h4 class="Item__authors Item__authors--small u-textTruncate">
                            James Tynion
                    </h4>
                </a>


                    <div class="Item__edition milli">
                        <span class="Item__format">Häftad</span>
                    </div>



            </div>


                <div class="Item__footer">

                    <div class="Item__pricing pricing pricing--small">

                        


                                <span class="pricing__price">219</span>


                    </div>


                </div>


        </div>
</div>

                    </li>
            </ul>
        </div>

</div>

           <div class="main__centerCol">
             

<meta itemprop="name" content="Teenage Mutant Ninja Turtles The Idw Collection Volume 1">
<meta itemprop="productID" content="isbn:9780141393391">
<div itemprop="offers" itemscope="" itemtype="https://schema.org/Offer">
    <meta itemprop="priceCurrency" content="SEK">
    <meta itemprop="price" content="339.00">
    <meta itemprop="availability" content="https://schema.org/OutOfStock">
</div>
<div itemprop="aggregateRating" itemscope="" itemtype="https://schema.org/AggregateRating">
    <meta itemprop="ratingValue" content="5">
    
        <meta itemprop="ratingCount" content="3">
</div>

    <div class="product-page__header">
        
        <img class="product-image__img product-image__img--superimposed" width="200" src="//image.bokus.com/images/9780141393391_200x_teenage-mutant-ninja-turtles-the-idw-collection-volume-1" alt="Teenage Mutant Ninja Turtles The Idw Collection Volume 1 (inbunden)">

        <h1 class="product-page__title">
            Teenage Mutant Ninja Turtles The Idw Collection Volume 1
        </h1>


        <div class="u-m-t--1 deci">
           av 
                <a href="/cgi-bin/product_search.cgi?authors=Erik%20Burnham" title="Fler böcker av författare Erik Burnham">Erik Burnham</a>,
                <a href="/cgi-bin/product_search.cgi?authors=Kevin%20Eastman" title="Fler böcker av författare Kevin Eastman">Kevin Eastman</a>,
                <a href="/cgi-bin/product_search.cgi?authors=Tom%20Waltz" title="Fler böcker av författare Tom Waltz">Tom Waltz</a>
        </div>

            <div class="product-page__rating rating u-m-t--1 centi">
                <span class="Icon Icon--rating-large" data-rating="5" aria-label="Betyg 5 av 5"></span>
                (3 röster)
                
            </div>

        <div class="product-page-edition-tags-wrapper u-cf">

            <div class="product-page__edition">
                    <span class="product__format" title="Bok med hårda pärmar och sydd bindning">Inbunden                    </span>



                <span>Engelska, 2015-06-09</span>

            </div><div class="product-page__tags">



            </div>
        </div>

            

            <div class="pricing pricing--huge product-page__pricing">
                <div class="inline-block u-vertical--middle">



                        <span class="pricing__price">339</span>


                </div>
                <!--

            -->

            </div>



            <div class="u-m-t--3">
                <div class="grid grid--middle">
                    <div class="grid__item grid__item--size1of2">
                         <a class="btn btn--large btn--full" href="https://www.bokus.com/cgi-bin/book_watch_customer.cgi?in=9780141393391" rel="nofollow" data-ticket="Oy9maW5kX3Byb2R1Y3RzOyM7cHJvZHVjdF9rZXk7OTc4MTYzMTQwMTExNDsjO09CSkVDVElWRSQ7Tk9ORTpOT05FOzA7" data-track="click" data-track-category="Product" data-track-action="Add-To-Watchlist" data-track-label="Detailed-Product">
                            Bevaka
                        </a>
                    </div><!--
                    --><div class="grid__item grid__item--size1of2">
                            <a class="favourite-btn" href="https://www.bokus.com/cgi-bin/wishlist_add.cgi?ISBN=9780141393391" rel="nofollow" data-ticket="Oy9maW5kX3Byb2R1Y3RzOyM7cHJvZHVjdF9rZXk7OTc4MTYzMTQwMTExNDsjO09CSkVDVElWRSQ7Tk9ORTpOT05FOzA7" data-track="click" data-track-category="Product" data-track-action="Add-To-Favorites" data-track-label="Detailed-Product">
                                Spara som favorit
                            </a>
                    </div>
                </div>
            </div>

            <div class="centi u-m-t--2">


                        
                            <strong>Bokens leverantör håller tillfälligt stängt på grund av Coronapandemin.</strong>
                            
                                Klicka "Bevaka" för att få ett mejl när boken går att beställa igen.
                            
                        


            </div>



        


    </div>

    <div id="product-description" class="Section">

            <div class="expand" data-lines="10" data-remove-button="1">
                <div class="expand__inner">
                  <div class="expand__inner-content">
                    IDW's relaunch of the Teenage Mutant Ninja Turtles has been a hit with fans and critics alike. Now, collect the series in all-new oversized hardcovers that present the stories in recommended reading order.  Collects the first 12 issues of the new ongoing series, plus the Raphael, Michelangelo, Donatello, Leonardo, and Splinter Micro-Series one-shots spliced in-between.                      
                  </div>
                </div>
                
            </div>


        <div class="product-page__share-widget">
            <span>
                <a class="addthis_button_facebook_like at300b" addthis:title="Gilla oss!" fb:like:layout="button_count"><div class="fb-like fb_iframe_widget" data-layout="button_count" data-show_faces="false" data-share="false" data-action="like" data-width="90" data-height="25" data-font="arial" data-href="https://www.bokus.com/bok/9780141393391/teenage-mutant-ninja-turtles-the-idw-collection-volume-1/" data-send="false" style="height: 25px;" fb-xfbml-state="rendered" fb-iframe-plugin-query="action=like&amp;app_id=172525162793917&amp;container_width=0&amp;font=arial&amp;height=25&amp;href=https%3A%2F%2Fwww.bokus.com%2Fbok%2F9780141393391%2Fteenage-mutant-ninja-turtles-the-idw-collection-volume-1%2F&amp;layout=button_count&amp;locale=sv_SE&amp;sdk=joey&amp;send=false&amp;share=false&amp;show_faces=false&amp;width=90"><span style="vertical-align: bottom; width: 70px; height: 20px;"><iframe name="f3dbb0d6a33cb48" width="90px" height="25px" data-testid="fb:like Facebook Social Plugin" title="fb:like Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v2.6/plugins/like.php?action=like&amp;app_id=172525162793917&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df1b3d8dd39fe65c%26domain%3Dwww.bokus.com%26origin%3Dhttps%253A%252F%252Fwww.bokus.com%252Ff2a45c8c56fb6d2%26relation%3Dparent.parent&amp;container_width=0&amp;font=arial&amp;height=25&amp;href=https%3A%2F%2Fwww.bokus.com%2Fbok%2F9780141393391%2Fteenage-mutant-ninja-turtles-the-idw-collection-volume-1%2F&amp;layout=button_count&amp;locale=sv_SE&amp;sdk=joey&amp;send=false&amp;share=false&amp;show_faces=false&amp;width=90" class="" style="border: none; visibility: visible; width: 70px; height: 20px;"></iframe></span></div></a>
            </span>
            <span>
                <a class="addthis_button_pinterest at300b" pi:pinit:media=""></a>
            </span>
            <span>
                <a class="addthis_button_tweet at300b" tw:text="Teenage Mutant Ninja Turtles The Idw Collection Volume 1 av Erik Burnham, Kevin Eastman, Tom Waltz" tw:count="none"><div class="tweet_iframe_widget" style="width: 61px; height: 25px;"><span><iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true" class="twitter-share-button twitter-share-button-rendered twitter-tweet-button" title="Twitter Tweet Button" src="https://platform.twitter.com/widgets/tweet_button.c4b33f07650267db9f8a72eaac551cac.sv.html#dnt=false&amp;id=twitter-widget-0&amp;lang=sv&amp;original_referer=https%3A%2F%2Fwww.bokus.com%2Fbok%2F9780141393391%2Fteenage-mutant-ninja-turtles-the-idw-collection-volume-1%2F&amp;size=m&amp;text=Teenage%20Mutant%20Ninja%20Turtles%20The%20Idw%20Collection%20Volume%201%20av%20Erik%20Burnham%2C%20Kevin%20Eastman%2C%20Tom%20Waltz&amp;time=1594568898264&amp;type=share&amp;url=https%3A%2F%2Fwww.bokus.com%2Fbok%2F9780141393391%2Fteenage-mutant-ninja-turtles-the-idw-collection-volume-1%2F%23.XwswwS95V0U.twitter" data-url="https://www.bokus.com/bok/9780141393391/teenage-mutant-ninja-turtles-the-idw-collection-volume-1/#.XwswwS95V0U.twitter" style="position: static; visibility: visible; width: 67px; height: 20px;"></iframe></span></div></a>
            </span>
        </div>

    </div>






    <div class="Section" id="customer-reviews">

<div class="Section__header">
    <h2 class="Section__heading" role="heading">Kundrecensioner
    </h2>

        <div class="Section__subHeading">Har du läst boken? <a href="https://www.bokus.com/cgi-bin/rate_product.cgi?ISBN=9780141393391" rel="nofollow">
                Sätt ditt betyg »</a></div>
    

</div>



    </div>

        <div class="Section">
            <div class="Section__header">
                <h2 class="Section__heading" role="heading">
                        Fler böcker av författarna
                </h2>
                <div class="Section__subHeading">
                            <a href="/cgi-bin/product_search.cgi?authors=Erik%20Burnham" title="Erik Burnham">Erik Burnham</a>,
                            <a href="/cgi-bin/product_search.cgi?authors=Kevin%20Eastman" title="Kevin Eastman">Kevin Eastman</a>,
                            <a href="/cgi-bin/product_search.cgi?authors=Tom%20Waltz" title="Tom Waltz">Tom Waltz</a>
                </div>
            </div>
            <div class="Section__body">
<div class="ProductGrid-container">
    <div class="ProductGrid">
            <ul class="ProductGrid__row">
                <li class="ProductGrid__col">

    <div class="ProductGrid__item ProductGrid__item--tall Item  Item--stacked">

        <div class="Item__media">
            <a href="/bok/9781684055906/teenage-mutant-ninja-turtles/" data-ticket="Oy9maW5kX3Byb2R1Y3RzOyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1NTkwNjsjOztOT05FOk5PTkU7MDs">
                <img class="Item__image js-lazy js-size-image" src="/img/px.gif" data-src="//image.bokus.com/images2/9781684055906_200x_teenage-mutant-ninja-turtles" alt="Teenage Mutant Ninja Turtles" width="116" height="182">
                <noscript>&lt;img class="Item__image" src="//image.bokus.com/images2/9781684055906_200x_teenage-mutant-ninja-turtles" alt="Teenage Mutant Ninja Turtles" width="116"&gt;</noscript>
            </a>
        </div>

        <div class="Item__info">

            <div class="Item__body">

                <a href="/bok/9781684055906/teenage-mutant-ninja-turtles/" data-ticket="Oy9maW5kX3Byb2R1Y3RzOyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1NTkwNjsjOztOT05FOk5PTkU7MDs">
                    <h3 class="Item__title  Item__title--truncated">Teenage Mutant Ninja Turtles</h3>                    <h4 class="Item__authors  u-textTruncate">
                            Kevin Eastman, 
                            Tom Waltz, 
                            Erik Burnham
                    </h4>
                </a>




            </div>


                <div class="Item__footer">

                    <div class="Item__pricing pricing">

                        


                                <span class="pricing__price">439</span>


                    </div>


                <div class="Item__cta">
                    <a href="/basket/add?product=9781684055906" rel="nofollow" class="btn btn--primary btn--small btn--basket-add  btn--full js-add-to-basket" data-ticket="Oy9maW5kX3Byb2R1Y3RzOyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1NTkwNjsjOztOT05FOk5PTkU7MDs" data-track="click" data-track-category="Product" data-track-action="Add-To-Basket" data-track-label="By-Same-Author-Block" data-track-pageview="" data-track-url="/virtual/basket/product-added" data-track-title="Product added" data-track-contentids="9781684055906">
                        <span class="btn__label"><span class="btn__text">Köp</span></span>
                    </a>
                </div>

                <div class="Item__delivery">



                            Skickas inom  vardagar.

                </div>


                </div>


        </div>
</div>

                
                </li><li class="ProductGrid__col">

    <div class="ProductGrid__item ProductGrid__item--tall Item  Item--stacked">

        <div class="Item__media">
            <a href="/bok/9781684050901/teenage-mutant-ninja-turtles-the-idw-collection-volume-5/" data-ticket="Oy9maW5kX3Byb2R1Y3RzOyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MDkwMTsjOztOT05FOk5PTkU7MDs">
                <img class="Item__image js-lazy js-size-image" src="/img/px.gif" data-src="//image.bokus.com/images2/9781684050901_200x_teenage-mutant-ninja-turtles-the-idw-collection-volume-5" alt="Teenage Mutant Ninja Turtles The Idw Collection Volume 5" width="116" height="182">
                <noscript>&lt;img class="Item__image" src="//image.bokus.com/images2/9781684050901_200x_teenage-mutant-ninja-turtles-the-idw-collection-volume-5" alt="Teenage Mutant Ninja Turtles The Idw Collection Volume 5" width="116"&gt;</noscript>
            </a>
        </div>

        <div class="Item__info">

            <div class="Item__body">

                <a href="/bok/9781684050901/teenage-mutant-ninja-turtles-the-idw-collection-volume-5/" data-ticket="Oy9maW5kX3Byb2R1Y3RzOyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MDkwMTsjOztOT05FOk5PTkU7MDs">
                    <h3 class="Item__title  Item__title--truncated">Teenage Mutant Ninja Turtles The Idw Collection Volume 5</h3>                    <h4 class="Item__authors  u-textTruncate">
                            Kevin Eastman, 
                            Tom Waltz
                    </h4>
                </a>




            </div>


                <div class="Item__footer">

                    <div class="Item__pricing pricing">

                        


                                <span class="pricing__price">439</span>


                    </div>


                <div class="Item__cta">
                    <a href="/basket/add?product=9781684050901" rel="nofollow" class="btn btn--primary btn--small btn--basket-add  btn--full js-add-to-basket" data-ticket="Oy9maW5kX3Byb2R1Y3RzOyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MDkwMTsjOztOT05FOk5PTkU7MDs" data-track="click" data-track-category="Product" data-track-action="Add-To-Basket" data-track-label="By-Same-Author-Block" data-track-pageview="" data-track-url="/virtual/basket/product-added" data-track-title="Product added" data-track-contentids="9781684050901">
                        <span class="btn__label"><span class="btn__text">Köp</span></span>
                    </a>
                </div>

                <div class="Item__delivery">



                            Skickas inom  vardagar.

                </div>


                </div>


        </div>
</div>

                
                </li><li class="ProductGrid__col">

    <div class="ProductGrid__item ProductGrid__item--tall Item  Item--stacked">

        <div class="Item__media">
            <a href="/bok/9781684052820/teenage-mutant-ninja-turtles-the-idw-collection-volume-7/" data-ticket="Oy9maW5kX3Byb2R1Y3RzOyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MjgyMDsjOztOT05FOk5PTkU7MDs">
                <img class="Item__image js-lazy js-size-image" src="/img/px.gif" data-src="//image.bokus.com/images2/9781684052820_200x_teenage-mutant-ninja-turtles-the-idw-collection-volume-7" alt="Teenage Mutant Ninja Turtles The Idw Collection Volume 7" width="116" height="182">
                <noscript>&lt;img class="Item__image" src="//image.bokus.com/images2/9781684052820_200x_teenage-mutant-ninja-turtles-the-idw-collection-volume-7" alt="Teenage Mutant Ninja Turtles The Idw Collection Volume 7" width="116"&gt;</noscript>
            </a>
        </div>

        <div class="Item__info">

            <div class="Item__body">

                <a href="/bok/9781684052820/teenage-mutant-ninja-turtles-the-idw-collection-volume-7/" data-ticket="Oy9maW5kX3Byb2R1Y3RzOyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MjgyMDsjOztOT05FOk5PTkU7MDs">
                    <h3 class="Item__title  Item__title--truncated">Teenage Mutant Ninja Turtles The Idw Collection Volume 7</h3>                    <h4 class="Item__authors  u-textTruncate">
                            Kevin Eastman, 
                            Tom Waltz
                    </h4>
                </a>




            </div>


                <div class="Item__footer">

                    <div class="Item__pricing pricing">

                        


                                <span class="pricing__price">519</span>


                    </div>


                <div class="Item__cta">
                    <a href="/basket/add?product=9781684052820" rel="nofollow" class="btn btn--primary btn--small btn--basket-add  btn--full js-add-to-basket" data-ticket="Oy9maW5kX3Byb2R1Y3RzOyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MjgyMDsjOztOT05FOk5PTkU7MDs" data-track="click" data-track-category="Product" data-track-action="Add-To-Basket" data-track-label="By-Same-Author-Block" data-track-pageview="" data-track-url="/virtual/basket/product-added" data-track-title="Product added" data-track-contentids="9781684052820">
                        <span class="btn__label"><span class="btn__text">Köp</span></span>
                    </a>
                </div>

                <div class="Item__delivery">



                            Skickas inom  vardagar.

                </div>


                </div>


        </div>
</div>

                
                </li><li class="ProductGrid__col">

    <div class="ProductGrid__item ProductGrid__item--tall Item  Item--stacked">

        <div class="Item__media">
            <a href="/bok/9781684051304/teenage-mutant-ninja-turtles-the-idw-collection-volume-6/" data-ticket="Oy9maW5kX3Byb2R1Y3RzOyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MTMwNDsjOztOT05FOk5PTkU7MDs">
                <img class="Item__image js-lazy js-size-image" src="/img/px.gif" data-src="//image.bokus.com/images2/9781684051304_200x_teenage-mutant-ninja-turtles-the-idw-collection-volume-6" alt="Teenage Mutant Ninja Turtles The IDW Collection Volume 6" width="116" height="182">
                <noscript>&lt;img class="Item__image" src="//image.bokus.com/images2/9781684051304_200x_teenage-mutant-ninja-turtles-the-idw-collection-volume-6" alt="Teenage Mutant Ninja Turtles The IDW Collection Volume 6" width="116"&gt;</noscript>
            </a>
        </div>

        <div class="Item__info">

            <div class="Item__body">

                <a href="/bok/9781684051304/teenage-mutant-ninja-turtles-the-idw-collection-volume-6/" data-ticket="Oy9maW5kX3Byb2R1Y3RzOyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MTMwNDsjOztOT05FOk5PTkU7MDs">
                    <h3 class="Item__title  Item__title--truncated">Teenage Mutant Ninja Turtles The IDW Collection Volume 6</h3>                    <h4 class="Item__authors  u-textTruncate">
                            Paul Allor, 
                            Kevin Eastman, 
                            Tom Waltz
                    </h4>
                </a>




            </div>


                <div class="Item__footer">

                    <div class="Item__pricing pricing">

                        


                                <span class="pricing__price">439</span>


                    </div>


                <div class="Item__cta">
                    <a href="/basket/add?product=9781684051304" rel="nofollow" class="btn btn--primary btn--small btn--basket-add  btn--full js-add-to-basket" data-ticket="Oy9maW5kX3Byb2R1Y3RzOyM7cHJvZHVjdF9rZXk7OTc4MTY4NDA1MTMwNDsjOztOT05FOk5PTkU7MDs" data-track="click" data-track-category="Product" data-track-action="Add-To-Basket" data-track-label="By-Same-Author-Block" data-track-pageview="" data-track-url="/virtual/basket/product-added" data-track-title="Product added" data-track-contentids="9781684051304">
                        <span class="btn__label"><span class="btn__text">Köp</span></span>
                    </a>
                </div>

                <div class="Item__delivery">



                            Skickas inom  vardagar.

                </div>


                </div>


        </div>
</div>

                
            </li></ul>
    </div>
</div>

            </div>
        </div>


    <div class="Section">

<div class="Section__header">
    <h2 class="Section__heading" role="heading">Bloggat om Teenage Mutant Ninja Turtles The Idw Coll...
    </h2>


</div>

        <div class="Section__body tw_widget" id="tw_link_widget" style="display:none;"></div>
    </div>
        <div class="Section">

<div class="Section__header">
    <h2 class="Section__heading" role="heading">Övrig information
    </h2>


</div>

            <div class="Section__body">
                <p>Tom Waltz is a former active duty U.S. Marine, Desert Storm vet, and former California National Guard Military Policeman. He is an editor for premiere comic book publisher IDW Publishing (www.IDWpublishing.com), as well as the writer of critically-acclaimed graphic novels, including TEENAGE MUTANT NINJA TURTLES, THE LAST FALL, CHILDREN OF THE GRAVE, FINDING PEACE (with Nathan St John), AFTER THE FIRE, SILENT HILL: SINNER'S REWARD, SILENT HILL: PAST LIFE, SILENT HILL DOWNPOUR: ANNE'S STORY and others. He has also written for video games, including as co-writer for SILENT HILL: DOWNPOUR (Konami), and writer for GHOSTBUSTERS: SANCTUM OF SLIME (Atari), TEENAGE MUTANT NINJA TURTLES: BROTHERS UNITE (Paramount), and TEENAGE MUTANT NINJA TURTLES: MUTANTS IN MANHATTAN (Activision). He grew up in Clinton, Michigan and currently makes his home in San Diego, California with his wife and two children. Kevin Eastman - Born in 1962 in Portland, Maine, Kevin began drawing as soon as he was able to hold a crayon. His discovery of comic books gave meaning to his crazed doodling. When Kevin discovered and studied the work of Jack Kirby, Russ Heath, Richard Corben, Vaughn Bode and John Severin, he began to hone his craft. His first published work appeared in 1980, a year or so before he met Peter Laird. In May 1984, he and Laird published Teenage Mutant Ninja Turtles #1, creating the hit heroes in a half-shell. Eastman co-writes Teenage Mutant Ninja Turtles for IDW and can sometimes be found at the San Diego Comic Art Gallery.</p>
                <p></p><p></p><p></p><p></p>
            </div>
        </div>

           </div>

        </div>

  		</div>

  	</div>

  </div>

	<a href="https://faq.bokus.com" class="kundo-forum-right" target="bokus-faq">
	    Frågor &amp; svar
	</a>
<div id="footer" class="footer">
	<div id="brand-icons" class="footer__brand-icons">
		
		<img class="js-lazy" data-src="/icons/svg/klarna.svg#klarna" width="65">
		<img class="js-lazy" data-src="/icons/svg/postnord.svg#postnord" width="115">
		<img class="js-lazy" data-src="/icons/svg/dbschenker.svg#dbschenker" width="115">
		<img class="js-lazy" data-src="/icons/svg/citymail.svg#citymail" width="45">
		<img class="js-lazy" data-src="/icons/svg/mastercard.svg#mastercard" width="65">
		<img class="js-lazy" data-src="/icons/svg/visa.svg#visa" width="60">
		<img class="js-lazy" data-src="/icons/svg/americanexpress.svg#americanexpress" width="40">		
	</div>
	<div class="footer__link-belt">
		<div class="footer__link-item">
			<ul class="footer__link-list">
				<li class="footer__link-list-item"><a href="/kundservice">Kundservice</a></li>
				<li class="footer__link-list-item"><a href="https://faq.bokus.com/org/bokhandelsgruppen-i-sverige-yifi/" target="bokus-faq">Vanliga frågor &amp; svar</a></li>
				<li class="footer__link-list-item"><a href="https://faq.bokus.com/org/bokhandelsgruppen-i-sverige-yifi/posts/praise" target="bokus-faq">Frakt &amp; leverans</a></li>
				<li class="footer__link-list-item"><a href="https://faq.bokus.com/org/bokhandelsgruppen-i-sverige-yifi/posts/angeratt" target="bokus-faq">Ångra</a></li>
				<li class="footer__link-list-item"><a href="/cgi-bin/product_search.cgi?show_advanced=1">Avancerad sökning</a>
			</li></ul>
		</div>
		<div class="footer__link-item">
			<ul class="footer__link-list">
				<li class="footer__link-list-item"><a href="/kundservice/english">In English</a></li>
				<li class="footer__link-list-item"><a href="/foretag">Företag</a></li>
				<li class="footer__link-list-item"><a href="/bibliotek-offentlig-verksamhet">Bibliotek &amp;<br>Offentlig verksamhet</a></li>
				<li class="footer__link-list-item"><a href="/nyhetsbrev">Nyhetsbrev</a></li>
			</ul>
		</div>
		<div class="footer__link-item">
			<ul class="footer__link-list">
				<li class="footer__link-list-item"><a href="/leverantor">Leverantör</a></li>
				<li class="footer__link-list-item"><a href="/lankpartner">Affiliate</a></li>
				<li class="footer__link-list-item"><a target="_blank" href="https://bokhandelsgruppen.teamtailor.com/">Jobba hos oss</a></li>
				<li class="footer__link-list-item"><a href="/om-bokus">Om Bokus</a></li>
				<li class="footer__link-list-item"><a href="https://news.cision.com/se/bokhandelsgruppen" target="_blank">Pressrum</a></li>
			</ul>
		</div>		
		<div class="footer__link-item mobile-hidden">
			<strong>Följ oss:</strong>
			<div>
				<a class="footer__social-media-link" href="https://www.instagram.com/bokus_com/" target="_blank" title="Bokus på Instagram">
					<img class="footer__social-media-img js-lazy" data-src="/icons/svg/instagram.svg#instagram">
				</a>
				<a class="footer__social-media-link" href="https://www.facebook.com/bokuscom" target="_blank" title="Bokus på Facebook">
					<img class="footer__social-media-img js-lazy" data-src="/icons/svg/facebook.svg#facebook">
				</a>
			</div>
		</div>
	</div>
	<div id="footer-copy-wrapper" class="footer__copy-wrapper">
		<div id="footer-copy" class="footer__copy">
			<ul class="footer__copy-list">
				<li class="footer__copy-list-item">© 2020 Bokus AB</li>
				<li class="footer__copy-list-item">Org.nr 556493-0492</li>
				<li class="footer__copy-list-item"><a href="/kundservice/personuppgiftsbehandling">Sidan använder cookies</a></li>
				<li class="footer__copy-list-item"><a href="/kundservice/personuppgiftsbehandling">Behandling av personuppgifter</a></li>
				<li class="footer__copy-list-item"><a href="/kundservice/avtalsvillkor">Avtalsvillkor</a></li>
			</ul>
		</div>
	</div>
</div>
<div id="cookies-msg" class="">
	Vi använder <a href="/kundservice/personuppgiftsbehandling">cookies</a> för att förbättra din upplevelse av bokus.com. Genom att fortsätta godkänner du vår användning av cookies. <a href="#" id="cookies-accept-link">Jag förstår</a>
</div>
<script src="/dist/js/desktop/main-v-1591780745042.js" type="text/javascript"></script>

<script src="/dist/js/shared/bokus-v-1591780745042.js" type="text/javascript"></script>


      

			

    <script>
        show_your_recent_history = 1;
    </script>
    <script src="//s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4fd6f84f77dcc16a&amp;async=1"></script>
    <script src="//eu.widgetdata.twingly.com/scripts/widget/twingly.widget.3.1.0.min.js"></script>
    


<script id="criteo-tracker">
    var do_load_criteo_script = 1;
    window.criteo_q=window.criteo_q||[];
    window.criteo_q.push( 
        {event:"setAccount",account:6096},
        {event:"setSiteType",type:"d"},
            {event:"viewItem",item:"9780141393391"}
    );
</script>

		</div><div id="_atssh" style="visibility: hidden; height: 1px; width: 1px; position: absolute; top: -9999px; z-index: 100000;"><iframe id="_atssh862" title="AddThis utility frame" src="https://s7.addthis.com/static/sh.f48a1a04fe8dbf021b4cda1d.html#rand=0.6267223024672322&amp;iit=1594568897826&amp;tmr=load%3D1594568897679%26core%3D1594568897705%26main%3D1594568897823%26ifr%3D1594568897828&amp;cb=0&amp;cdn=0&amp;md=0&amp;kw=&amp;ab=-&amp;dh=www.bokus.com&amp;dr=&amp;du=https%3A%2F%2Fwww.bokus.com%2Fbok%2F9780141393391%2Fteenage-mutant-ninja-turtles-the-idw-collection-volume-1%2F&amp;href=https%3A%2F%2Fwww.bokus.com%2Fbok%2F9780141393391%2Fteenage-mutant-ninja-turtles-the-idw-collection-volume-1%2F&amp;dt=Teenage%20Mutant%20Ninja%20Turtles%20The%20Idw%20Collection%20Volume%201%20av%20Erik%20Burnham%2C%20Kevin%20Eastman%2C%20Tom%20Waltz%20(Bok)&amp;dbg=0&amp;cap=tc%3D0%26ab%3D0&amp;inst=1&amp;jsl=1&amp;prod=undefined&amp;lng=sv&amp;ogt=url%2Cimage%2Ctype%3DBook%2Cdescription%2Ctitle%2Csite_name&amp;pc=men&amp;pub=ra-4fd6f84f77dcc16a&amp;ssl=1&amp;sid=5f0b30c10d069c81&amp;srf=0.01&amp;ver=300&amp;xck=0&amp;xtr=0&amp;og=site_name%3DBokus.com%26title%3DTeenage%2520Mutant%2520Ninja%2520Turtles%2520The%2520Idw%2520Collection%2520Volume%25201%2520av%2520Erik%2520Burnham%252C%2520Kevin%2520Eastman%252C%2520Tom%2520Waltz%2520(Bok)%26description%3DIDW's%2520relaunch%2520of%2520the%2520Teenage%2520Mutant%2520Ninja%2520Turtles%2520has%2520been%2520a%2520hit%2520with%2520fans%2520and%2520critics%2520alike.%2520Now%252C%2520collect%2520the%2520series%2520in%2520all-new%2520oversized%2520hardcovers%2520that%2520present%2520the%2520stories%2520in%2520recommended%2520reading%2520order.%2520%2520Collects%2520the%2520first%252012%2520issues%2520of%2520the%2520new%2520ongoing%2520series%252C%2520plus%2520the%2520Raphael%252C%2520Michelangelo%252C%2520Donatello%252C%2520Leonardo%252C%2520and%2520Splinter%2520Micro-Series%2520one-shots%2520spliced%2520in-between.%26type%3DBook%26image%3Dhttps%253A%252F%252Fimage.bokus.com%252Fimages%252F9780141393391_200x_teenage-mutant-ninja-turtles-the-idw-collection-volume-1%26url%3Dhttps%253A%252F%252Fwww.bokus.com%252Fbok%252F9780141393391%252Fteenage-mutant-ninja-turtles-the-idw-collection-volume-1%252F&amp;csi=undefined&amp;rev=v8.28.7-wp&amp;ct=1&amp;xld=1&amp;xd=1" style="height: 1px; width: 1px; position: absolute; top: 0px; z-index: 100000; border: 0px; left: 0px;"></iframe></div><style id="service-icons-0"></style>
	


<script type="text/javascript" id="">(function(a,c,e,f,d,b){a.hj=a.hj||function(){(a.hj.q=a.hj.q||[]).push(arguments)};a._hjSettings={hjid:466035,hjsv:6};d=c.getElementsByTagName("head")[0];b=c.createElement("script");b.async=1;b.src=e+a._hjSettings.hjid+f+a._hjSettings.hjsv;d.appendChild(b)})(window,document,"https://static.hotjar.com/c/hotjar-",".js?sv\x3d");</script>
<script type="text/javascript" id="">!function(b,e,f,g,a,c,d){b.fbq||(a=b.fbq=function(){a.callMethod?a.callMethod.apply(a,arguments):a.queue.push(arguments)},b._fbq||(b._fbq=a),a.push=a,a.loaded=!0,a.version="2.0",a.queue=[],c=e.createElement(f),c.async=!0,c.src=g,d=e.getElementsByTagName(f)[0],d.parentNode.insertBefore(c,d))}(window,document,"script","https://connect.facebook.net/en_US/fbevents.js");fbq("set","autoConfig","false","522404921474218");fbq("init","522404921474218");</script>

<script type="text/javascript" id="">fbq("track","PageView");</script><script type="text/javascript" id="">window._adftrack=Array.isArray(window._adftrack)?window._adftrack:window._adftrack?[window._adftrack]:[];window.myAdformObj=window.myAdformObj||{};window.myAdformObj.pm="783927";window.myAdformObj.pagename=encodeURIComponent("Product Page");window._adftrack=window.myAdformObj;
(function(){var a=document.createElement("script");a.type="text/javascript";a.async=!0;a.src="https://track.adform.net/serving/scripts/trackpoint/async/";var b=document.getElementsByTagName("script")[0];b.parentNode.insertBefore(a,b)})();</script><script type="text/javascript" id="" src="https://adtr.io/jsTag?ap=1064669487"></script><script type="text/javascript" id="">fbq("track","ViewContent",{content_name:"Product Page",content_type:"product",content_ids:["9780141393391"]});</script><div id="fb-root" class=" fb_reset"><script src="//connect.facebook.net/sv_SE/sdk.js#version=v2.6" async=""></script><div style="position: absolute; top: -10000px; width: 0px; height: 0px;"><div></div></div></div><iframe scrolling="no" frameborder="0" allowtransparency="true" src="https://platform.twitter.com/widgets/widget_iframe.c4b33f07650267db9f8a72eaac551cac.html?origin=https%3A%2F%2Fwww.bokus.com" title="Twitter settings iframe" style="display: none;"></iframe></body></html>
TypeError: Attempting to change the setter of an unconfigurable property.
TypeError: Attempting to change the setter of an unconfigurable property.
