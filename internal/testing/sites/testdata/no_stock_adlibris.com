<!DOCTYPE html><!--[if IE 9 ]><html class="no-js ie ie9" lang="sv"><![endif]--><!--[if !IE]><!--><html class="wf-not-yet-loaded history supports touchevents csscolumns csscolumns-width csscolumns-span csscolumns-fill csscolumns-gap csscolumns-rule csscolumns-rulecolor csscolumns-rulestyle csscolumns-rulewidth csscolumns-breakbefore csscolumns-breakafter csscolumns-breakinside csstransforms csstransforms3d csstransitions" lang="sv"><!--<![endif]--><head>
<style>
    .async-hide {
        opacity: 0 !important
    }
</style>
<script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script type="text/javascript" async="" src="//www.googleadservices.com/pagead/conversion_async.js"></script><script type="text/javascript" async="" src="https://www.google-analytics.com/gtm/js?id=GTM-5WSC9DZ&amp;cid=540535420.1594568271"></script><script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-MFT3M8"></script><script async="" src="https://www.google-analytics.com/analytics.js"></script><script>
(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
    h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
    (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
})(window,document.documentElement,'async-hide','dataLayer',4000,
    {'GTM-5WSC9DZ':true});</script>



<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-27333980-1', 'auto');
    ga('require', 'GTM-5WSC9DZ');

    ga('send', 'pageview');
</script><script>
    var dataLayer = dataLayer || [];
    dataLayer.push({ "google_tag_params":{"ecomm_totalvalue":"","ecomm_pagetype":"searchresults","ecomm_prodid":"","ecomm_pname":"","ecomm_pvalue":"","ecomm_quantity":"","ecomm_category":""} })
;
</script>
<script type="text/javascript">
        var dataLayer = dataLayer || [];
        dataLayer.push({
            'UAID': 'UA-27333980-1',
            'pageView': ''
        });
</script>




<!-- Google Tag Manager -->
<script>
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window, document, 'script', 'dataLayer', 'GTM-MFT3M8');</script>
<!-- End Google Tag Manager -->


<script>window.featureCutoffFromZip = true;</script>

<title>9780141393391  | Adlibris</title>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=2, user-scalable=yes">
<meta name="format-detection" content="telephone=no">
<meta name="google-site-verification" content="O_rr-t6r0HlV1ZD2noOVyGfAMJ4TeTXG638F53Kv65A">
<meta name="theme-color" content="#ED1C24">
    <link rel="manifest" href="/app-manifest-se.json">










<meta name="description" content="Hos Adlibris hittar du miljontals böcker och produkter inom 9780141393391 Vi har ett brett sortiment av böcker, garn, leksaker, pyssel, sällskapsspel, dekoration och mycket mer för en inspirerande vardag. Alltid bra priser, fri frakt från 199 kr och snabb leverans. | Adlibris">


<link rel="icon" type="image/png" href="/Staticimages/favicon/favicon.png">
<link rel="icon" type="image/svg+xml" href="/Staticimages/favicon/favicon.svg">

<link rel="apple-touch-icon" sizes="76x76" href="/Staticimages/TouchIcons/apple-touch-icon-76x76-precomposed.png">
<link rel="apple-touch-icon" sizes="114x114" href="/Staticimages/TouchIcons/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon" sizes="120x120" href="/Staticimages/TouchIcons/apple-touch-icon-120x120-precomposed.png">
<link rel="apple-touch-icon" sizes="144x144" href="/Staticimages/TouchIcons/apple-touch-icon-144x144-precomposed.png">
<link rel="apple-touch-icon" sizes="152x152" href="/Staticimages/TouchIcons/apple-touch-icon-152x152-precomposed.png">
<link rel="apple-touch-icon" sizes="180x180" href="/Staticimages/TouchIcons/apple-touch-icon-180x180-precomposed.png">


    <script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "WebSite",
        "url": "https://www.adlibris.com/se",
        "potentialAction": {
            "@type": "SearchAction",
            "target": "https://www.adlibris.com/se/sok?&q={search_term_string}",
            "query-input": "required name=search_term_string"
        }
    }
    </script>

<link rel="preload" crossorigin="anonymous" href="https://s3.adlibris.com/dist/long_term_cache201912041650/Roboto-Regular.woff2" as="font">
<link rel="preload" crossorigin="anonymous" href="https://s3.adlibris.com/dist/long_term_cache201912041650/Roboto-Bold.woff2" as="font">
<link rel="preload" crossorigin="anonymous" href="https://s3.adlibris.com/dist/long_term_cache201912041650/Montserrat-Regular.woff2" as="font">
<link rel="preload" crossorigin="anonymous" href="https://s3.adlibris.com/dist/long_term_cache201912041650/Montserrat-SemiBold.woff2" as="font">
<link rel="preload" crossorigin="anonymous" href="https://s3.adlibris.com/dist/long_term_cache201912041650/Montserrat-Bold.woff2" as="font">
<link rel="preload" crossorigin="anonymous" href="https://s3.adlibris.com/dist/long_term_cache201912041650/material-icons.woff" as="font">
<link href="https://s3.adlibris.com/dist/c73216d3add0c084b6c48647bfd8b470a16f2fd8/critical_styling.css" media="screen, print" rel="stylesheet" type="text/css">
<link data-href="https://s3.adlibris.com/dist/c73216d3add0c084b6c48647bfd8b470a16f2fd8/global_styling.css" href="https://s3.adlibris.com/dist/c73216d3add0c084b6c48647bfd8b470a16f2fd8/global_styling.css" media="screen, print" rel="stylesheet" as="style" type="text/css" data-js-hook-globalstylingstylesheet="">

    <link rel="canonical" href="https://www.adlibris.com/se/sok?q=9780141393391">




    

    

        <script>
        window.dataLayer.push(
            {
                'event': 'searchPageWithQueryAndResult',
                'searchQuery': '9780141393391',
                'fullQueryString': 'q=9780141393391'
            });
    </script>

<script src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1069613287/?random=1594568271067&amp;cv=9&amp;fst=1594568271067&amp;num=1&amp;guid=ON&amp;resp=GooglemKTybQhCsO&amp;u_h=1440&amp;u_w=2560&amp;u_ah=1440&amp;u_aw=2560&amp;u_cd=24&amp;u_his=1&amp;u_tz=120&amp;u_java=false&amp;u_nplug=1&amp;u_nmime=2&amp;gtm=2wg6o0&amp;ig=1&amp;data=ecomm_totalvalue%3D%3Becomm_pagetype%3Dsearchresults%3Becomm_prodid%3D%3Becomm_pname%3D%3Becomm_pvalue%3D%3Becomm_quantity%3D%3Becomm_category%3D&amp;frm=0&amp;url=https%3A%2F%2Fwww.adlibris.com%2Fse%2Fsok%3Fq%3D9780141393391&amp;tiba=9780141393391%20%7C%20Adlibris&amp;hn=www.googleadservices.com&amp;async=1&amp;rfmt=3&amp;fmt=4"></script><style type="text/css"></style><link rel="stylesheet" type="text/css" href="https://s3.adlibris.com/dist/c73216d3add0c084b6c48647bfd8b470a16f2fd8/global-styling.css"><script charset="utf-8" src="https://s3.adlibris.com/dist/c73216d3add0c084b6c48647bfd8b470a16f2fd8/global-styling.js"></script><meta name="robots" content="noindex"></head>
<body class="search  sesv">



<!-- Google Tag Manager (noscript) -->
<noscript>
    &lt;iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MFT3M8"
            height="0" width="0" style="display:none;visibility:hidden"&gt;&lt;/iframe&gt;
</noscript>
<!-- End Google Tag Manager (noscript) -->

    <div id="detect-breakpoint"></div>

    <div id="wrapper" data-notify-initialized="true">
<div data-js-hook-page-layout="" data-initialized="true"><div class="info-banner" style="color: rgb(255, 255, 255); background-color: rgb(232, 17, 33);"><span class="info-banner__text"><a href="https://www.adlibris.com/se/avdelning/lagerrensning-a9590">Stor lagerrensning - fynda nu!</a></span><style>
					.info-banner__text a,
					.info-banner__text a:hover,
					.info-banner__text a:focus {
						color: #FFFFFF;
						-webkit-text-decoration-color: #FFFFFF;
						text-decoration-color: #FFFFFF;
					}
				</style></div><div class="page-header"><div class="header-menu__wrapper "><button class="header-menu__toggler "><div class="header-menu__toggler__icon"><span></span><span></span><span></span><span></span></div><div class="header-menu__toggler__text">Meny</div></button><menu class="header-menu "><div class="header-menu__section--filler"></div><button class="header-menu__close btn btn--std"><span class="header-menu__close__text">Stäng</span><i class="mega-menu-container__close material-icons__close"></i></button></menu></div><div class="page-header__logo"><a href="/se" class="page-header__logo__link"><svg class="adlibris-logo-svg" height="24px" enable-background="new 0 0 98 24" version="1.1" viewBox="0 0 98 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><path class="adlibris-logo-svg__path" d="m5.1 23.4h-5.1l7.3-20.5h5.1l7.1 20.5h-5.8l-0.8-2.9h-6.9l-0.9 2.9zm5-11.2l-0.7-3.6-0.8 3.6-1.7 5h4.6l-1.4-5zm18.8-11.4h5.3v22.7h-3.7l-1.4-1.8-1.7 1.9-0.8 0.3c-2.4 0.5-4.8-0.4-6.3-2.2-1.3-1.9-1.9-4.1-1.8-6.3 0-1.4 0.2-2.9 0.6-4.2 0.3-1 0.9-2 1.7-2.7 0.6-0.7 1.4-1.2 2.2-1.6 0.8-0.3 1.7-0.4 2.5-0.4 0.7 0 1.4 0.1 2 0.3 0.5 0 1 0.2 1.5 0.4v-6.4h-0.1zm0 9.9c-0.3-0.1-0.6-0.2-0.9-0.4-0.4-0.1-0.9-0.2-1.3-0.1-0.4 0-0.8 0.1-1.1 0.1-0.3 0.1-0.6 0.3-0.8 0.6-0.3 0.3-0.4 0.7-0.5 1.2-0.1 0.6-0.2 1.3-0.2 1.9v2.6c-0.1 1.1 0.2 2.2 0.7 3.2 0.4 0.6 1.2 1 2 0.9 0.5 0 0.9-0.1 1.3-0.4 0.3-0.2 0.7-0.5 0.9-0.8l-0.1-8.8zm7.2-9.9h5.3v22.7h-5.3v-22.7zm10.1 4.3c-0.7 0-1.4-0.2-2-0.6-0.6-0.5-0.9-1.2-0.8-1.9 0-0.7 0.3-1.4 0.8-1.9 1.2-0.9 2.8-0.9 4 0 0.6 0.5 0.9 1.2 0.9 1.9s-0.3 1.4-0.8 1.9c-0.7 0.4-1.4 0.6-2.1 0.6zm-2.6 1.5h5.3v16.8h-5.3v-16.8zm9.9 16.9h-2.6v-22.7h5.3v6.8c0.5-0.4 1-0.8 1.6-1 0.7-0.2 1.5-0.4 2.2-0.4 1.9-0.1 3.8 0.8 4.9 2.3 1.2 2.1 1.7 4.4 1.5 6.7 0.1 2.2-0.5 4.4-1.7 6.3-1.2 1.5-3.1 2.4-5.1 2.2-0.6 0-1.3-0.1-1.9-0.2-0.5-0.1-0.9-0.3-1.4-0.5l-1.4-1.9-1.4 2.4zm2.5-3.3l1.1 0.4c0.4 0.1 0.9 0.1 1.3 0.1 0.7 0 1.4-0.3 1.7-0.9 0.5-1 0.7-2.1 0.6-3.2v-2.4c0.1-1.1-0.1-2.2-0.5-3.2-0.4-0.6-1.2-0.9-2-0.9-0.5 0-0.9 0.1-1.3 0.3-0.3 0.2-0.7 0.4-1 0.6l0.1 9.2zm17.9-13.5c0.3-0.1 0.7-0.2 1-0.3 0.4-0.1 0.8-0.1 1.2-0.1s0.9 0.1 1.3 0.2c0.3 0.1 0.5 0.3 0.7 0.4l-0.8 4.3c-0.3-0.1-0.6-0.2-0.9-0.3-0.5-0.1-1-0.1-1.4-0.1-0.5 0-1 0.1-1.4 0.1s-0.9 0.1-1.3 0.3v12.2h-5.1v-16.7h3.6l1.4 2.7 1.7-2.7zm7.7-1.6c-0.7 0-1.4-0.2-2-0.6-0.6-0.5-0.9-1.2-0.8-1.9 0-0.7 0.3-1.4 0.8-1.9 1.2-0.9 2.8-0.9 4 0 0.6 0.5 0.9 1.2 0.8 1.9 0 0.7-0.3 1.4-0.8 1.9-0.6 0.4-1.3 0.6-2 0.6zm-2.6 1.5h5.3v16.8h-5.3v-16.8zm12.1 17.3c-1.1 0-2.3-0.1-3.4-0.4-0.8-0.2-1.5-0.4-2.2-0.7l0.5-3.4c0.5 0.2 1.2 0.4 1.9 0.6 0.8 0.2 1.6 0.3 2.4 0.3 0.8 0.1 1.5-0.1 2.2-0.3 0.4-0.3 0.7-0.8 0.6-1.2 0-0.3-0.1-0.5-0.2-0.7-0.2-0.2-0.4-0.3-0.7-0.4l-1.1-0.2c-0.5 0-1.1-0.1-1.8-0.3-0.6-0.1-1.1-0.3-1.7-0.5-0.5-0.2-1-0.5-1.3-0.9-0.4-0.5-0.7-1-0.8-1.6-0.2-0.7-0.3-1.5-0.3-2.2-0.2-1.8 0.5-3.4 1.8-4.5 1.5-0.9 3.3-1.4 5.1-1.3 1.1 0 2.2 0.1 3.2 0.3 0.7 0.1 1.5 0.4 2.1 0.7l-0.8 3.6c-0.7-0.2-1.4-0.4-2-0.5-0.7-0.1-1.4-0.2-2.1-0.1-0.8-0.1-1.5 0-2.2 0.3-0.4 0.2-0.7 0.6-0.6 1.1 0 0.2 0.1 0.4 0.2 0.6 0.2 0.2 0.4 0.3 0.6 0.4 0.4 0.1 0.8 0.2 1.1 0.3l1.8 0.4c0.7 0.1 1.3 0.3 1.9 0.6 0.5 0.2 1 0.5 1.4 0.9s0.7 1 0.8 1.6c0.3 0.7 0.4 1.4 0.4 2.2 0.1 1.5-0.5 3-1.7 4.1-1.4 0.9-3.2 1.3-5.1 1.2z" style="fill: rgb(237, 28, 36);"></path></svg></a></div><button class="page-header__toggler page-header__toggler--account  "><i class="material-icons__person"></i><span class="page-header__toggler__text">Logga in</span></button><div class="account-and-login page-header__dropdown-content" data-collapsed="true" style="height: 0px;"><div class="page-header__dropdown-content__inner"><div class="page-header__dropdown-content__inner page-header__dropdown-content__inner--filler"></div></div></div><button class="page-header__toggler page-header__toggler--cart  "><i class="material-icons__shopping_cart"></i><span class="page-header__toggler__text">Kundvagn</span></button><div class="mini-cart page-header__dropdown-content page-header__dropdown-content--collapsed" data-collapsed="true" style="height: 0px;"><div class="page-header__dropdown-content__inner page-header__dropdown-content__inner--filler"></div></div><div class="page-header__search "><div class="page-header__search__form__wrapper   page-header__search__form__wrapper--search-page" data-collapsed="true"><form class="page-header__search__form  " method="get" action="/se/sok"><select id="page-header__search__select-category" class="page-header__search__select-category " style="width: 100%;"><option value=" ">Alla kategorier</option><option value="categoryfacet:böcker" data-icon="book-category-icon">Böcker</option><option value="categoryfacet:baby" data-icon="baby-category-icon">Baby</option><option value="categoryfacet:barnkläder &amp; barnväskor" data-icon="children-clothes">Barnkläder &amp; barnväskor</option><option value="categoryfacet:fest &amp; roliga prylar" data-icon="party-category-icon">Fest &amp; roliga prylar</option><option value="categoryfacet:garn &amp; stickning" data-icon="yarn-category-icon">Garn &amp; stickning</option><option value="categoryfacet:inredning" data-icon="interior-decoration-category-icon">Inredning</option><option value="categoryfacet:konstnärsmaterial &amp; diy" data-icon="diy-category-icon">Konstnärsmaterial &amp; DIY</option><option value="categoryfacet:leksaker" data-icon="toys-category-icon">Leksaker</option><option value="categoryfacet:mobilt" data-icon="electronics-category-icon">Mobilt</option><option value="categoryfacet:papper &amp; kontor" data-icon="office-supplies-category-icon">Papper &amp; kontor</option><option value="categoryfacet:skönhet" data-icon="cosmetics-category-icon">Skönhet</option><option value="categoryfacet:smycken" data-icon="jewelry-category-icon">Smycken</option><option value="categoryfacet:sällskapsspel &amp; pussel" data-icon="board-games-category-icon">Sällskapsspel &amp; pussel</option><option value="categoryfacet:väskor &amp; accessoarer" data-icon="accessories-category-icon">Väskor &amp; accessoarer</option><option value="categoryfacet:yoga, sport &amp; friluftsliv" data-icon="yoga-category-icon">Yoga, sport &amp; friluftsliv</option></select><span style="position: absolute; visibility: hidden; z-index: -1;">Alla kategorier</span><div class="page-header__search__input__wrapper"><input class="page-header__search__input" type="search" id="q" name="q" maxlength="200" autocapitalize="off" autocorrect="off" autocomplete="off" placeholder="Sök bland miljontals produkter!" value="9780141393391"><button id="clear-search" class="page-header__search__clear" type="button"><i class="material-icons__cancel"></i></button><div class="page-header__search__seperator"></div><button type="submit" class="page-header__search__submit" id="search-button"><i class="material-icons__search"></i></button></div></form></div></div></div><div class="notifications-bar  "><div class="notifications-bar__info-bar" data-collapsed="false" style="height: 0px;"><div class="notifications-bar__info-message notifications-bar__info-message--info notifications-bar__info-message--cookie"><p class="notifications-bar__info-message__message"><span class="notifications-bar__info-message__message__text">Genom att fortsätta godkänner du att vi använder <a href="/se/kundtjanst/security">cookies</a> på sajten.</span><button class="notifications-bar__info-message__message__close btn btn--std">Jag förstår</button></p></div></div></div></div>


        <div class="nav-viewport" data-js-hook-nav-viewport="" data-initialized="true">
            <div class="container nav-container" id="nav-container">

                <div id="main-container" class="main-container">

                    <div role="main" id="main">
                        
<div class="search-result">
    <script>
    searchPageCategoryFacetsUrl = "/se/categoryfacets?categoryFacetConfiguration=DefaultCategoryFacetConfiguration&targetAction=Show&targetController=Search&q=9780141393391";
</script>

    <div class="breadcrumbs">
    <a href="/se"><i class="material-icons__home "></i></a>&nbsp;/
Sök</div>



        <div class="search-result__desktop-grid">

            <div class="search-result__filter search-result__filter--desktop">
                <div class="search-result__filter__toggler search-result__filter__toggler--active">
                    <i class="material-icons__tune"></i>
                    Filter
                </div>
                <div class="search-result__filter__options">



                    <div data-js-hook-category-facets-target-desktop=""><div class="category-facets">
    <div class="column-box">
        <button class="column-box__header h4 btn btn--transparent" data-js-hook-category-category-facet-toggler="" data-toggler-active-class="column-box__header--expanded">
            <span class="column-box__header__text">Kategorier</span>
            <i class="material-icons__keyboard_arrow_down"></i>
        </button>
        <ul class="column-box__items" data-js-hook-column-box__items__categories="" data-collapsed="true" style="height: 0;">
            



<li class="column-box__item ">
        <span class="         category-facet--active-category
         category-facet--current
     category-facet
 column-box__item__text">Alla kategorier</span>            <ul>



<li class="column-box__item ">
        <span class="         category-facet--active-category
         category-facet--current
     category-facet
 column-box__item__text">Böcker</span>            <ul>



<li class="column-box__item column-box__item--indent">
        <a class="         category-facet--no-sub-facets
     category-facet
 column-box__item__link" href="/se/sok?q=9780141393391&amp;filter=categoryfacet:b%C3%B6cker%2fhumor+%26+presentb%C3%B6cker">Humor &amp; presentböcker</a> <span class="column-box__item__amount">(1)</span>    </li>




<li class="column-box__item column-box__item--indent">
        <a class="         category-facet--no-sub-facets
     category-facet
 column-box__item__link" href="/se/sok?q=9780141393391&amp;filter=categoryfacet:b%C3%B6cker%2fkonst+%26+musik">Konst &amp; musik</a> <span class="column-box__item__amount">(1)</span>    </li>




<li class="column-box__item column-box__item--indent">
        <a class="         category-facet--no-sub-facets
     category-facet
 column-box__item__link" href="/se/sok?q=9780141393391&amp;filter=categoryfacet:b%C3%B6cker%2fsk%C3%B6nlitteratur">Skönlitteratur</a> <span class="column-box__item__amount">(1)</span>    </li>
</ul>
</li>
</ul>
</li>

        </ul>
    </div>
</div></div>
                    


                </div>
            </div>

            <div id="search" class="search-result__result section">
                
                <h1 class="search-result__header">
    <span class="search-result__header__pretext">Sökt på: </span>9780141393391
</h1>


                






                <div class="search-result__handle-search-result">
                    <div class="search-result__filter__toggler" data-js-hook-toggler-trigger="" data-toggler-target="[data-js-hook-search-result-filter]" data-toggler-active-class="search-result__filter__toggler--active">
                        <i class="material-icons__tune"></i>
                        Filter
                        <i class="material-icons__keyboard_arrow_down"></i>
                    </div>
                    

<div class="search-result__sort">
</div>
                    <div data-js-hook-search-result-views="" data-initialized="true"><div class="search-result__views"><span class="search-result__views__common-text">Välj visningsvy</span><div class="search-result__views__view search-result__views__view--list search-result__views__view--active" title="Visa som lista"><i class="material-icons__view_list"></i></div><div class="search-result__views__view " title="Visa som rutnät"><i class="material-icons__view_grid"></i></div></div></div>
                </div>

                <div class="search-result__filter__options">



                    <div class="search-result__filter__options__facets" data-js-hook-search-result-filter="" data-collapsed="true" style="height: 0;">
                        <div data-js-hook-category-facets-target=""><div class="category-facets">
    <div class="column-box">
        <button class="column-box__header h4 btn btn--transparent" data-js-hook-category-category-facet-toggler="" data-toggler-active-class="column-box__header--expanded">
            <span class="column-box__header__text">Kategorier</span>
            <i class="material-icons__keyboard_arrow_down"></i>
        </button>
        <ul class="column-box__items" data-js-hook-column-box__items__categories="" data-collapsed="true" style="height: 0;">
            



<li class="column-box__item ">
        <span class="         category-facet--active-category
         category-facet--current
     category-facet
 column-box__item__text">Alla kategorier</span>            <ul>



<li class="column-box__item ">
        <span class="         category-facet--active-category
         category-facet--current
     category-facet
 column-box__item__text">Böcker</span>            <ul>



<li class="column-box__item column-box__item--indent">
        <a class="         category-facet--no-sub-facets
     category-facet
 column-box__item__link" href="/se/sok?q=9780141393391&amp;filter=categoryfacet:b%C3%B6cker%2fhumor+%26+presentb%C3%B6cker">Humor &amp; presentböcker</a> <span class="column-box__item__amount">(1)</span>    </li>




<li class="column-box__item column-box__item--indent">
        <a class="         category-facet--no-sub-facets
     category-facet
 column-box__item__link" href="/se/sok?q=9780141393391&amp;filter=categoryfacet:b%C3%B6cker%2fkonst+%26+musik">Konst &amp; musik</a> <span class="column-box__item__amount">(1)</span>    </li>




<li class="column-box__item column-box__item--indent">
        <a class="         category-facet--no-sub-facets
     category-facet
 column-box__item__link" href="/se/sok?q=9780141393391&amp;filter=categoryfacet:b%C3%B6cker%2fsk%C3%B6nlitteratur">Skönlitteratur</a> <span class="column-box__item__amount">(1)</span>    </li>
</ul>
</li>
</ul>
</li>

        </ul>
    </div>
</div></div>
                        


                    </div>
                </div>

                    <h3 class="search-result__result__amount">
                        1 träff
                    </h3>
                <div class="search-result__products__wrapper">
    
    <div class="search-result__products search-result__products--initialized search-result__products--list-view-active" data-js-hook-search-result-products="">
<div class="search-result-content search-result__products__list-view">
    <div class="search-result__products__list-view__list-wrapper">
        <div class="search-result__products__list-view__list">
            <div class="product-search_list pagination-target" id="search-hits" data-js-hook-search-result-list-view-grid="">

<div class="search-result__list-view__product__wrapper" data-js-hook-product-id-list-view="" data-prodject-id="51b4e039-2f8f-4872-87f2-ed60d933bfce" data-page="https://www.adlibris.com/se/sok?q=9780141393391">
    <div class="search-result__list-view__product search-result__product" data-pageindex="1" itemprop="mainEntity" data-js-hook-search-result-list-view-grid-product="">
        <div class="search-result__list-view__product__image-and-information-container ">
            <a href="/se/bok/teenage-mutant-ninja-turtles-the-idw-collection-volume-2-9780141393391" data-notify="Oy9tb2JpbGVzaXRlLXBhZ2VzLXNlYXJjaGxpc3Qvc2VhcmNoLWhpdHMtd2l0aC1jb3VudC9zZWFyY2gtaGl0czsjO3Byb2R1Y3Rfa2V5OzUxQjRFMDM5LTJGOEYtNDg3Mi04N0YyLUVENjBEOTMzQkZDRTtEQTgxMjFERi0zMjk1LTQ1RUYtODY1Ri1EQTlDODRENUY5Q0Y7T0JKRUNUSVZFJDtOT05FOk5PTkU7NTs">
                

<img itemprop="image" src="https://s1.adlibris.com/images/21417450/teenage-mutant-ninja-turtles-the-idw-collection-volume-2.jpg" alt="Teenage Mutant Ninja Turtles The Idw Collection Volume 2">
            </a>
            
            <div class="search-result__list-view__product__information">

                <h4 itemprop="name" class="heading--searchlist-title">
                    <a class="search-result__product__name" href="/se/bok/teenage-mutant-ninja-turtles-the-idw-collection-volume-2-9780141393391" data-notify="Oy9tb2JpbGVzaXRlLXBhZ2VzLXNlYXJjaGxpc3Qvc2VhcmNoLWhpdHMtd2l0aC1jb3VudC9zZWFyY2gtaGl0czsjO3Byb2R1Y3Rfa2V5OzUxQjRFMDM5LTJGOEYtNDg3Mi04N0YyLUVENjBEOTMzQkZDRTtEQTgxMjFERi0zMjk1LTQ1RUYtODY1Ri1EQTlDODRENUY5Q0Y7T0JKRUNUSVZFJDtOT05FOk5PTkU7NTs">
                        Teenage Mutant Ninja Turtles The Idw Collection Volume 2
                    </a>
                </h4>

                <div class="heading--searchlist-more">


                            <div class="product-item__authors">
                                <span>av </span>



        <a itemprop="author" itemscope="" itemtype="https://schema.org/Person" rel="author" href="/se/sok?filter=author%3AMike%20Costa">
            Mike Costa
            <meta itemprop="name" content="Mike Costa">
        </a>                            </div>

                            <div class="product-item__price-from price-from">

                                    <span class="book-format">inbunden, </span>
                                            <span>2016, </span>
                                        <span>Engelska, </span>
                                        <span>ISBN 9780141393391</span>
                            </div>

                        <p class="search-result__list-view__product__information__description">
The IDW Collection presents the TMNT stories in recommended reading order. Volume 2 collects issues #13-20 of the ongoing series, the Casey Jones, April, Fugitoid, Krang, and                                <span>…</span>
                        </p>

                    <div class="product-item__current-price">
                    </div>
                </div>
            </div>
        </div>



<div class="variants__wrapper">
    <div class="variants  " data-toggler-target-variants="51b4e039-2f8f-4872-87f2-ed60d933bfce">
                    <div class="variant">
                        <div class="variant__info">
                            <a href="/se/bok/teenage-mutant-ninja-turtles-the-idw-collection-volume-2-9780141393391" data-notify="Oy9tb2JpbGVzaXRlLXBhZ2VzLXNlYXJjaGxpc3Qvc2VhcmNoLWhpdHMtd2l0aC1jb3VudC9zZWFyY2gtaGl0czsjO3Byb2R1Y3Rfa2V5OzUxQjRFMDM5LTJGOEYtNDg3Mi04N0YyLUVENjBEOTMzQkZDRTtEQTgxMjFERi0zMjk1LTQ1RUYtODY1Ri1EQTlDODRENUY5Q0Y7T0JKRUNUSVZFJDtOT05FOk5PTkU7NTs">
                                <span class="format ">
inbunden
                                </span>
                            </a>
                            <div class="processing-time">
                                            <span class="processing-time">
                Tillfälligt slut
            </span>

                            </div>
                        </div>
                        <div class="purchase__wrapper">
    <div class="purchase ">
            <a class="btn btn--divided " aria-label="Bevaka inbunden" rel="nofollow" onclick="LIB.analytics.trackEvent('addToWatchlist', this)" data-gaqlabel="Book" href="/se/konto/login?returnUrl=%2Fse%2Fbevakningar%2Fadd%2Fda8121df-3295-45ef-865f-da9c84d5f9cf%3FreturnUrl%3D%252Fse%252Fbevakningar%252Fwatch">
                <span class="btn--first-divider">
                    

<div class="price sek">
    <span>
        422 <span class="currency">kr</span>
    </span>
</div>
                </span>
                <span class="btn--second-divider add">
                    <i class="material-icons__visibility"></i>
                </span>
            </a>
    </div>
    <div class="product__purchase__extra-info">
                    <div class="watch-text-wrapper">
                <a class="watch-text 
               " href="/se/konto/login?returnUrl=%2Fse%2Fbevakningar%2Fadd%2Fda8121df-3295-45ef-865f-da9c84d5f9cf%3FreturnUrl%3D%252Fse%252Fbevakningar%252Fwatch">Bevaka</a>
                <span class="watch-text you-watch" style="display: none">Du bevakar denna</span>
                <a class="watch-link" href="/se/bevakningar/watch" style="display: none">Till bevakningar</a>
            </div>

    </div>

                        </div>

                    </div>
                    <button class="variants__show-all-variants btn btn--transparent" onclick="window.showMoreContent(this)" data-toggler-target="[data-toggler-target-variants='51b4e039-2f8f-4872-87f2-ed60d933bfce']" data-toggler-active-class="variants--expanded">
                Visa alla format
                <i class="material-icons__keyboard_arrow_down"></i>
            </button>
    </div>
</div>
        
    </div>
</div>
            </div>
        </div>
    </div>
    
    
</div>
<div class="search-result__products__grid-view">
    <div class="search-result__products__grid-view__grid-wrapper">
        <div class="search-result__products__grid-view__grid" data-js-hook-search-result-grid-view-grid="">



<div class="search-result__grid-view__product search-result__product" data-js-hook-product-id-grid-view="" data-prodject-id="51b4e039-2f8f-4872-87f2-ed60d933bfce">
    <div class="search-result__grid-view__product__part1">
        <a class="search-result__grid-view__product__image__product-link" href="/se/bok/teenage-mutant-ninja-turtles-the-idw-collection-volume-2-9780141393391" data-notify="Oy9tb2JpbGVzaXRlLXBhZ2VzLXNlYXJjaGxpc3Qvc2VhcmNoLWhpdHMtd2l0aC1jb3VudC9zZWFyY2gtaGl0czsjO3Byb2R1Y3Rfa2V5OzUxQjRFMDM5LTJGOEYtNDg3Mi04N0YyLUVENjBEOTMzQkZDRTtEQTgxMjFERi0zMjk1LTQ1RUYtODY1Ri1EQTlDODRENUY5Q0Y7T0JKRUNUSVZFJDtOT05FOk5PTkU7NTs">
            

<img itemprop="image" src="/pixel.gif" data-src="https://s1.adlibris.com/images/21417450/teenage-mutant-ninja-turtles-the-idw-collection-volume-2.jpg" alt="Teenage Mutant Ninja Turtles The Idw Collection Volume 2">
            
        </a>
        <div class="search-result__product__additional-info">
                <div class="search-result__product__additional-info__content search-result__product__additional-info__content--format">
                    <span class="search-result__product__additional-info__content__text">inbunden</span>
                </div>
                    </div>
    </div>

    <div class="search-result__grid-view__product__part2 ">
        <a href="/se/bok/teenage-mutant-ninja-turtles-the-idw-collection-volume-2-9780141393391" data-notify="Oy9tb2JpbGVzaXRlLXBhZ2VzLXNlYXJjaGxpc3Qvc2VhcmNoLWhpdHMtd2l0aC1jb3VudC9zZWFyY2gtaGl0czsjO3Byb2R1Y3Rfa2V5OzUxQjRFMDM5LTJGOEYtNDg3Mi04N0YyLUVENjBEOTMzQkZDRTtEQTgxMjFERi0zMjk1LTQ1RUYtODY1Ri1EQTlDODRENUY5Q0Y7T0JKRUNUSVZFJDtOT05FOk5PTkU7NTs" class="search-result__grid-view__product__name h4 search-result__product__name  line-clamp line-clamp__3">
            Teenage Mutant Ninja Turtles The Idw Collection Volume 2
        </a>
    </div>

    <div class="search-result__grid-view__product__part3">

    <div class="purchase ">
            <a class="btn btn--divided " aria-label="Bevaka inbunden" rel="nofollow" onclick="LIB.analytics.trackEvent('addToWatchlist', this)" data-gaqlabel="Book" href="/se/konto/login?returnUrl=%2Fse%2Fbevakningar%2Fadd%2Fda8121df-3295-45ef-865f-da9c84d5f9cf%3FreturnUrl%3D%252Fse%252Fbevakningar%252Fwatch">
                <span class="btn--first-divider">
                    

<div class="price sek">
    <span>
        422 <span class="currency">kr</span>
    </span>
</div>
                </span>
                <span class="btn--second-divider add">
                    <i class="material-icons__visibility"></i>
                </span>
            </a>
    </div>
    <div class="product__purchase__extra-info">
                    <div class="watch-text-wrapper">
                <a class="watch-text 
               " href="/se/konto/login?returnUrl=%2Fse%2Fbevakningar%2Fadd%2Fda8121df-3295-45ef-865f-da9c84d5f9cf%3FreturnUrl%3D%252Fse%252Fbevakningar%252Fwatch">Bevaka</a>
                <span class="watch-text you-watch" style="display: none">Du bevakar denna</span>
                <a class="watch-link" href="/se/bevakningar/watch" style="display: none">Till bevakningar</a>
            </div>

    </div>

    </div>
</div>
        </div>
    </div>
    

</div>

    </div>
</div>

            </div>

        </div>

</div>


                    </div>

                </div>

            </div>
        </div>

        <footer role="contentinfo" id="footer" class="footer">
<nav class="footer__navigation">
    <div class="section">
        <div class="bd container">
            <div class="list">
                <h3 class="logo large">
                    <a class="bd" href="/">
                        
<svg class="adlibris-logo-svg" height="24px" enable-background="new 0 0 98 24" version="1.1" viewBox="0 0 98 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><path class="adlibris-logo-svg__path" d="m5.1 23.4h-5.1l7.3-20.5h5.1l7.1 20.5h-5.8l-0.8-2.9h-6.9l-0.9 2.9zm5-11.2l-0.7-3.6-0.8 3.6-1.7 5h4.6l-1.4-5zm18.8-11.4h5.3v22.7h-3.7l-1.4-1.8-1.7 1.9-0.8 0.3c-2.4 0.5-4.8-0.4-6.3-2.2-1.3-1.9-1.9-4.1-1.8-6.3 0-1.4 0.2-2.9 0.6-4.2 0.3-1 0.9-2 1.7-2.7 0.6-0.7 1.4-1.2 2.2-1.6 0.8-0.3 1.7-0.4 2.5-0.4 0.7 0 1.4 0.1 2 0.3 0.5 0 1 0.2 1.5 0.4v-6.4h-0.1zm0 9.9c-0.3-0.1-0.6-0.2-0.9-0.4-0.4-0.1-0.9-0.2-1.3-0.1-0.4 0-0.8 0.1-1.1 0.1-0.3 0.1-0.6 0.3-0.8 0.6-0.3 0.3-0.4 0.7-0.5 1.2-0.1 0.6-0.2 1.3-0.2 1.9v2.6c-0.1 1.1 0.2 2.2 0.7 3.2 0.4 0.6 1.2 1 2 0.9 0.5 0 0.9-0.1 1.3-0.4 0.3-0.2 0.7-0.5 0.9-0.8l-0.1-8.8zm7.2-9.9h5.3v22.7h-5.3v-22.7zm10.1 4.3c-0.7 0-1.4-0.2-2-0.6-0.6-0.5-0.9-1.2-0.8-1.9 0-0.7 0.3-1.4 0.8-1.9 1.2-0.9 2.8-0.9 4 0 0.6 0.5 0.9 1.2 0.9 1.9s-0.3 1.4-0.8 1.9c-0.7 0.4-1.4 0.6-2.1 0.6zm-2.6 1.5h5.3v16.8h-5.3v-16.8zm9.9 16.9h-2.6v-22.7h5.3v6.8c0.5-0.4 1-0.8 1.6-1 0.7-0.2 1.5-0.4 2.2-0.4 1.9-0.1 3.8 0.8 4.9 2.3 1.2 2.1 1.7 4.4 1.5 6.7 0.1 2.2-0.5 4.4-1.7 6.3-1.2 1.5-3.1 2.4-5.1 2.2-0.6 0-1.3-0.1-1.9-0.2-0.5-0.1-0.9-0.3-1.4-0.5l-1.4-1.9-1.4 2.4zm2.5-3.3l1.1 0.4c0.4 0.1 0.9 0.1 1.3 0.1 0.7 0 1.4-0.3 1.7-0.9 0.5-1 0.7-2.1 0.6-3.2v-2.4c0.1-1.1-0.1-2.2-0.5-3.2-0.4-0.6-1.2-0.9-2-0.9-0.5 0-0.9 0.1-1.3 0.3-0.3 0.2-0.7 0.4-1 0.6l0.1 9.2zm17.9-13.5c0.3-0.1 0.7-0.2 1-0.3 0.4-0.1 0.8-0.1 1.2-0.1s0.9 0.1 1.3 0.2c0.3 0.1 0.5 0.3 0.7 0.4l-0.8 4.3c-0.3-0.1-0.6-0.2-0.9-0.3-0.5-0.1-1-0.1-1.4-0.1-0.5 0-1 0.1-1.4 0.1s-0.9 0.1-1.3 0.3v12.2h-5.1v-16.7h3.6l1.4 2.7 1.7-2.7zm7.7-1.6c-0.7 0-1.4-0.2-2-0.6-0.6-0.5-0.9-1.2-0.8-1.9 0-0.7 0.3-1.4 0.8-1.9 1.2-0.9 2.8-0.9 4 0 0.6 0.5 0.9 1.2 0.8 1.9 0 0.7-0.3 1.4-0.8 1.9-0.6 0.4-1.3 0.6-2 0.6zm-2.6 1.5h5.3v16.8h-5.3v-16.8zm12.1 17.3c-1.1 0-2.3-0.1-3.4-0.4-0.8-0.2-1.5-0.4-2.2-0.7l0.5-3.4c0.5 0.2 1.2 0.4 1.9 0.6 0.8 0.2 1.6 0.3 2.4 0.3 0.8 0.1 1.5-0.1 2.2-0.3 0.4-0.3 0.7-0.8 0.6-1.2 0-0.3-0.1-0.5-0.2-0.7-0.2-0.2-0.4-0.3-0.7-0.4l-1.1-0.2c-0.5 0-1.1-0.1-1.8-0.3-0.6-0.1-1.1-0.3-1.7-0.5-0.5-0.2-1-0.5-1.3-0.9-0.4-0.5-0.7-1-0.8-1.6-0.2-0.7-0.3-1.5-0.3-2.2-0.2-1.8 0.5-3.4 1.8-4.5 1.5-0.9 3.3-1.4 5.1-1.3 1.1 0 2.2 0.1 3.2 0.3 0.7 0.1 1.5 0.4 2.1 0.7l-0.8 3.6c-0.7-0.2-1.4-0.4-2-0.5-0.7-0.1-1.4-0.2-2.1-0.1-0.8-0.1-1.5 0-2.2 0.3-0.4 0.2-0.7 0.6-0.6 1.1 0 0.2 0.1 0.4 0.2 0.6 0.2 0.2 0.4 0.3 0.6 0.4 0.4 0.1 0.8 0.2 1.1 0.3l1.8 0.4c0.7 0.1 1.3 0.3 1.9 0.6 0.5 0.2 1 0.5 1.4 0.9s0.7 1 0.8 1.6c0.3 0.7 0.4 1.4 0.4 2.2 0.1 1.5-0.5 3-1.7 4.1-1.4 0.9-3.2 1.3-5.1 1.2z" style="fill: #ED1C24;"></path></svg>

                    </a>
                </h3>
                <ul>
                    <li><h4><a href="/se/kundtjanst/about">Om Adlibris</a></h4></li>
                    <li><h4><a href="/" onclick="document.cookie=&quot;culture=; expires=Thu, 01 Jan 2000 00:00:00 GMT; path=/;&quot;;">Välj land/språk</a></h4></li>
                    <li><h4><a href="https://www.adlibris.com/storkund/">Bibliotek &amp; Offentlig verksamhet</a></h4></li>
                    <li><h4><a href="/se/kundtjanst/store">Butiker</a></h4></li>
                    <li><h4><a href="/se/kundtjanst/work-at-adlibris">Jobba hos oss</a></h4></li>
                    <li><h4><a href="https://newsroom.notified.com/adlibris-press">Press</a></h4></li>
                </ul>
            </div>

            <div class="list">
                <h3 class="heading--footer">Mitt konto</h3>
                <ul>
                        <li><h4><a href="/se/konto">Logga in</a></h4></li>

                    <li><h4><a href="/se/konto/orders">Beställningar</a></h4></li>
                    <li><h4><a href="/se/onskelista/watch" class="no-ajax">Önskelistor</a></h4></li>
                    <li><h4><a href="/se/bevakningar/watch" class="no-ajax">Bevakningar</a></h4></li>
                    <li><h4><a href="/se/konto/settings">Kunduppgifter</a></h4></li>
                    <li><h4><a href="/se/konto/library">Digitalt bibliotek</a></h4></li>
                    <li><h4><a href="/se/kundtjanst/personuppgifter">Personuppgifter</a></h4></li>
                </ul>
            </div>

            <div class="list">
                <h3 class="heading--footer">Kundservice</h3>
                <ul>
                    <li><h4><a href="/se/kundtjanst">Kontakta oss
</a></h4></li>
<li><h4><a href="/se/kundtjanst/terms-and-conditions">Köpvillkor
</a></h4></li>

                    <li><h4><a href="https://adlibris-fragor.kundo.se" target="_blank">Vanliga frågor &amp; svar</a></h4></li>
                </ul>
            </div>

<div class="interact-with-adlibris">
    <div class="interact-with-adlibris__newsletter-signup">

    <div data-js-hook-newsletter-opt-in-footer="" class="newsletter-opt-in">
        <form class="newsletter-opt-in__form" action="/newsletter/newsletteroptin" method="post" novalidate="true">
    <label for="email" class="newsletter-opt-in__label">Förtur till kampanjer, erbjudanden &amp; nyheter</label>
    <input name="__RequestVerificationToken" type="hidden" value="1yl-xQYf8E2nDbAXTt1UUBzibX_4PCGpFA_99T03CNXogFnK5xsJ_1u6J-3LuQ9gfGqW6bbTlkzs0BsBNlhrEGyH7ek1">
    <input type="email" maxlength="100" pattern="^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*(\.\w{2,})+$" required="" id="email" name="email" placeholder="exempel@email.com" class="newsletter-opt-in__input" data-validation-worng-format-error="Angiven e-postadress är ogiltig" data-validation-empty-field-error="Fältet kan inte vara tomt" data-validation-pattern-mismatch="Angiven e-postadress är ogiltig" data-service-unavailable-error="Det gick inte att skriva användaren för nyhetsbrevet, försök igen senare">
    <button class="btn btn--black btn--medium newsletter-opt-in__button" type="submit">Skicka</button>

</form>

        <span class="btn btn--std btn--medium newsletter-opt-in__sent-message">
            <i class="newsletter-opt-in__sent-message__check-icon material-icons__check"></i>
            <span class="newsletter-opt-in__sent-message__text">Tack! Du prenumerar nu på våra nyhetsbrev</span>
        </span>
    </div>
</div>
    <div class="interact-with-adlibris__side-block aside-1">

    <div class="follow">
        <h3 class="heading--footer">Följ oss</h3>
        <div class="social">
            <ul>
                <li class="link-icon icon-fb"><h4><a href="https://www.facebook.com/adlibris.com">Facebook</a></h4></li>
                <li class="link-icon icon-ig"><h4><a href="https://www.instagram.com/adlibris.com/">Instagram</a></h4></li>
                <li class="link-icon icon-ig"><h4><a href="https://www.instagram.com/adlibrisdiy/">Pyssel &amp; DIY</a></h4></li>
            </ul>
        </div>
    </div>





</div>
</div>
 

        </div>
    </div>
</nav>
<div class="footer__white-part">
    <div class="section footer-section-secondary">
        <div class="bd footer-section-secondary__infotext">
            <p class="about">Hos Adlibris hittar du billiga böcker online och ett stort utbud av garn, leksaker, pussel och pyssel &amp; DIY. Vårt breda utbud gör det enkelt att finna det du behöver för en inspirerande vardag. Adlibris är en del av Adlibrisgruppen där även e-handelssajterna <a href=" https://www.discshop.se/" target="_blank">Discshop</a> och <a href=" https://www.odla.nu/shop//" target="_blank">Odla</a> ingår.  </p>
            <ul>
                <li>
                    <a href="/se/kundtjanst/security#cookies">Hantering av cookies</a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="footer__copyright">
    <p>
        Adlibris AB Copyright © 1999-2020
    </p>
</div>

        </footer>



    </div>

    <!-- WEB-20 -->
    
    

<script>
    var globalData = {
        menuStructure: {"ToggleButton":"Meny","ToggleButtonClose":"Stäng","Notice":"Kolla! Ny meny","CatgegoriesHeader":"Upptäck vårt breda sortiment","Categories":[{"Promoted":false,"SearchFilter":"categoryfacet:böcker","Header":"Böcker","Link":"/se/sok?\u0026filter=categoryfacet:böcker","LinkNoFollow":true,"SvgIcon":"book-category-icon","CategoryPanelIndex":0,"Index":1,"Sections":[{"sectionsHeader":"","sectionLinks":[{"header":"Populära boknyheter\r","link":"/se/kampanj/boktips-nyhet"},{"header":"Personalens bästa boktips\r","link":"/se/kampanj/personalens-boktips"},{"header":"Pocket\r","link":"/se/kampanj/pocket"},{"header":"E-böcker\r","link":"/se/kampanj/e-bocker"},{"header":"Ljudböcker\r","link":"/se/kampanj/ljudbocker"},{"header":"Studentlitteratur\r","link":"/se/kampanj/studentlitteratur"},{"header":"Barnböcker\r","link":"/se/barnbocker"},{"header":"Tonår \u0026 unga vuxna\r","link":"/se/avdelning/barn-ungdom-9508?filter=age_group%3A50-60"},{"header":"Böcker på engelska","link":"/se/kampanj/nya-bocker-engelska"}]},{"sectionsHeader":"Kampanjer \u0026 tips","sectionLinks":[{"header":"Förhandsbeställ nyheter\t\r","link":"/se/kampanj/forhandsbestall"},{"header":"Romaner SOMMARREA\t\r","link":"/se/kampanj/sommarrea-romaner"},{"header":"E-böcker SOMMARREA\t\r","link":"/se/kampanj/sommarrea-e-bocker"},{"header":"Deckare SOMMARREA\t\r","link":"/se/kampanj/sommarrea-deckare"},{"header":"Kokböcker SOMMARREA\t\r","link":"/se/kampanj/sommarrea-kokbocker"},{"header":"4 böcker för 100 kr\t\r","link":"/se/kampanj/lagerrensning"},{"header":"Sommarpocket 4 för 169 kr\t\r","link":"/se/kampanj/sommarpocket"},{"header":"Nya hälsoböcker\t\r","link":"/se/kampanj/nya-halsobocker"},{"header":"Nya barnböcker\t\r","link":"/se/kampanj/barnbocker-nyheter"},{"header":"Barnböcker SOMMARREA\t\r","link":"/se/kampanj/sommarrea-barnbocker-3-6-ar"},{"header":"Sommarlovsläsning\t\r","link":"/se/kampanj/semester-lassemaja"},{"header":"#Oursea – Mumin 75 år\t\r","link":"/se/kampanj/oursea-mumin-75-ar"},{"header":"Pippi Långstrump fyller 75 år!\t\r","link":"/se/kampanj/pippi-langstrump"},{"header":"Presentböcker \t\r","link":"/se/kampanj/presentbocker"},{"header":"Novellix presentaskar\t\r","link":"/se/kampanj/present-noveller"},{"header":"Aktuellt i media\t\r","link":"/se/kampanj/aktuellt-i-media"},{"header":"Böcker på engelska 3 för 100 kr\r","link":"/se/kampanj/lagerrensning-engelska-bocker-1"},{"header":"Pocket på engelska 2 för 129 kr\t\r","link":"/se/kampanj/sommarlasning-summer-reading"},{"header":"Nyheter på engelska\t\r","link":"/se/kampanj/nya-bocker-engelska"},{"header":"Women\u0027s prize for fiction 2020\t\r","link":"/se/kampanj/womens-prize-for-fiction-1"},{"header":"Månadens bokpärlor\t\r","link":"/se/kampanj/manadensparlor"},{"header":"Trädgårdstider\t\r","link":"/se/kampanj/bocker-tradgard"},{"header":"Djur \u0026 Natur för de minsta\t\r","link":"/se/kampanj/djur-natur-barn"},{"header":"Bullet Journal\t\r","link":"/se/kampanj/bulletjournal"},{"header":"Hållbarhet \u0026 eko\t\r","link":"/se/kampanj/hallbarhet"},{"header":"Res i Sverige\t\r","link":"/se/kampanj/res-i-sverige"},{"header":"Vegetariska kokböcker\t\r","link":"/se/kampanj/vegetariska-kokbocker"},{"header":"Föräldratips","link":"/se/kampanj/foraldratips"}]}]},{"Promoted":false,"SearchFilter":"","Header":"Studentlitteratur","Link":"/se/kampanj/studentlitteratur","LinkNoFollow":false,"SvgIcon":"student-category-icon","CategoryPanelIndex":0,"Index":2,"Sections":[]},{"Promoted":false,"SearchFilter":"categoryfacet:baby","Header":"Baby","Link":"/se/avdelning/baby-a1876","LinkNoFollow":false,"SvgIcon":"baby-category-icon","CategoryPanelIndex":0,"Index":999,"Sections":[{"sectionsHeader":"Kampanjer \u0026 tips","sectionLinks":[{"header":"Stor lagerrensning - Baby\r","link":"/se/avdelning/lagerrensning-baby-a9592"},{"header":"Resevagnar SOMMARREA\r","link":"/se/kampanj/resevagnar"},{"header":"Föräldratips!\r","link":"/se/kampanj/foraldratips"},{"header":"Allt till babyshowern\r","link":"/se/kampanj/babyshower"},{"header":"Bebis på G?\r","link":"/se/kampanj/gravid-boktips"},{"header":"Populära varumärken","link":"/se/kampanj/britax"}]}]},{"Promoted":false,"SearchFilter":"","Header":"Barnböcker","Link":"/se/sok?filter=categoryfacet:böcker%2fbarn+%26+ungdom","LinkNoFollow":false,"SvgIcon":"children-book-category-icon","CategoryPanelIndex":0,"Index":999,"Sections":[]},{"Promoted":false,"SearchFilter":"categoryfacet:barnkläder \u0026 barnväskor","Header":"Barnkläder \u0026 barnväskor","Link":"/se/avdelning/barnklader-barnvaskor-a9522","LinkNoFollow":false,"SvgIcon":"children-clothes","CategoryPanelIndex":0,"Index":999,"Sections":[]},{"Promoted":false,"SearchFilter":"","Header":"Barnpyssel","Link":"/se/avdelning/barnpyssel-a1467","LinkNoFollow":false,"SvgIcon":"children-diy-category-icon","CategoryPanelIndex":0,"Index":999,"Sections":[]},{"Promoted":false,"SearchFilter":"categoryfacet:fest \u0026 roliga prylar","Header":"Fest \u0026 roliga prylar","Link":"/se/avdelning/fest-roliga-prylar-a9105","LinkNoFollow":false,"SvgIcon":"party-category-icon","CategoryPanelIndex":0,"Index":999,"Sections":[]},{"Promoted":false,"SearchFilter":"categoryfacet:garn \u0026 stickning","Header":"Garn \u0026 stickning","Link":"/se/avdelning/garn-stickning-a1418","LinkNoFollow":false,"SvgIcon":"yarn-category-icon","CategoryPanelIndex":0,"Index":999,"Sections":[{"sectionsHeader":"Kampanjer \u0026 tips","sectionLinks":[{"header":"Gratis mönster\r","link":"/se/kampanj/create-with-adlibris-monsterkatalog"},{"header":"Stor lagerrensning - Garn upp till 60%\r","link":"/se/avdelning/lagerrensning-garn-stickning-a9593?id=a9593\u0026pn=2"},{"header":"Novita bomullsgarn 20%","link":"/se/kampanj/novita"}]}]},{"Promoted":false,"SearchFilter":"categoryfacet:inredning","Header":"Inredning","Link":"/se/avdelning/inredning-a9197","LinkNoFollow":false,"SvgIcon":"interior-decoration-category-icon","CategoryPanelIndex":0,"Index":999,"Sections":[{"sectionsHeader":"Kampanjer \u0026 tips","sectionLinks":[{"header":"Stor lagerrensning - Inredning\r","link":"/se/avdelning/lagerrensning-inredning-a9591"},{"header":"Muminposters - endast hos oss!","link":"/se/kampanj/muminposters"}]}]},{"Promoted":false,"SearchFilter":"categoryfacet:konstnärsmaterial \u0026 diy","Header":"Konstnärsmaterial \u0026 DIY","Link":"/se/avdelning/pyssel-diy-a1197","LinkNoFollow":false,"SvgIcon":"diy-category-icon","CategoryPanelIndex":0,"Index":999,"Sections":[{"sectionsHeader":"Kampanjer \u0026 tips","sectionLinks":[{"header":"Stor lagerrensning - Pyssel \u0026 DIY\r","link":"/se/avdelning/lagerrensning-pyssel-diy-a9600"},{"header":"Bullet journal\r","link":"/se/kampanj/bulletjournal"},{"header":"Posters av Ink \u0026 Lise","link":"/se/kampanj/ink-and-lise-posters"}]}]},{"Promoted":false,"SearchFilter":"categoryfacet:leksaker","Header":"Leksaker","Link":"/se/avdelning/leksaker-spel-a1129","LinkNoFollow":false,"SvgIcon":"toys-category-icon","CategoryPanelIndex":0,"Index":999,"Sections":[{"sectionsHeader":"Kampanjer \u0026 tips","sectionLinks":[{"header":"Stor lagerrensning - Leksaker\r","link":"/se/avdelning/lagerrensning-leksaker-a9596"},{"header":"LEGO\r","link":"/se/kampanj/legonyheter"},{"header":"Sommarlek upp till 20%","link":"/se/kampanj/sommarlek"}]}]},{"Promoted":false,"SearchFilter":"categoryfacet:mobilt","Header":"Mobilt","Link":"/se/avdelning/mobilt-a1020","LinkNoFollow":false,"SvgIcon":"electronics-category-icon","CategoryPanelIndex":0,"Index":999,"Sections":[]},{"Promoted":false,"SearchFilter":"categoryfacet:papper \u0026 kontor","Header":"Papper \u0026 kontor","Link":"/se/avdelning/papper-kontor-a1000","LinkNoFollow":false,"SvgIcon":"office-supplies-category-icon","CategoryPanelIndex":0,"Index":999,"Sections":[{"sectionsHeader":"Kampanjer \u0026 tips","sectionLinks":[{"header":"Stor lagerrensning - Papper \u0026 kontor\r","link":"/se/avdelning/lagerrensning-papper-kontor-a9594"},{"header":"Bullet journal\r","link":"/se/kampanj/bulletjournal"},{"header":"Kalkylatorer \u0026 miniräknare","link":"/se/kampanj/miniraknare-kalkylator"}]}]},{"Promoted":false,"SearchFilter":"categoryfacet:skönhet","Header":"Skönhet","Link":"/se/avdelning/skonhet-a9086","LinkNoFollow":false,"SvgIcon":"cosmetics-category-icon","CategoryPanelIndex":0,"Index":999,"Sections":[]},{"Promoted":false,"SearchFilter":"categoryfacet:smycken","Header":"Smycken","Link":"/se/avdelning/smycken-a9368","LinkNoFollow":false,"SvgIcon":"jewelry-category-icon","CategoryPanelIndex":0,"Index":999,"Sections":[]},{"Promoted":false,"SearchFilter":"categoryfacet:sällskapsspel \u0026 pussel","Header":"Sällskapsspel \u0026 pussel","Link":"/se/avdelning/sallskapsspel-pussel-a1148","LinkNoFollow":false,"SvgIcon":"board-games-category-icon","CategoryPanelIndex":0,"Index":999,"Sections":[{"sectionsHeader":"Kampanjer \u0026 tips","sectionLinks":[{"header":"Stor lagerrensning - Sällskapsspel \u0026 pussel\r","link":"/se/avdelning/sallskapsspel-pussel-a1148"},{"header":"Pussel 20% rabatt","link":"/se/kampanj/clementoni-pussel"}]}]},{"Promoted":false,"SearchFilter":"categoryfacet:väskor \u0026 accessoarer","Header":"Väskor \u0026 accessoarer","Link":"/se/avdelning/vaskor-accessoarer-a9378","LinkNoFollow":false,"SvgIcon":"accessories-category-icon","CategoryPanelIndex":0,"Index":999,"Sections":[]},{"Promoted":false,"SearchFilter":"categoryfacet:yoga, sport \u0026 friluftsliv","Header":"Yoga, sport \u0026 friluftsliv","Link":"/se/avdelning/yoga-sport-friluftsliv-a9079","LinkNoFollow":false,"SvgIcon":"yoga-category-icon","CategoryPanelIndex":0,"Index":999,"Sections":[]}],"MiscellaneousHeader":"","Miscellaneous":[{"Header":"Presentkort","Link":"/se/kampanj/presentkort","LinkNoFollow":false,"SvgIcon":"","CategoryPanelIndex":999,"Index":2,"Sections":[]},{"Header":"Bibliotek \u0026 offentlig verksamhet","Link":"https://www.adlibris.com/storkund/","LinkNoFollow":false,"SvgIcon":"","CategoryPanelIndex":999,"Index":3,"Sections":[]},{"Header":"Samarbetsförlag","Link":"http://www.adlibris.com/se/organisationer/shopperhomepage.aspx","LinkNoFollow":false,"SvgIcon":"","CategoryPanelIndex":999,"Index":4,"Sections":[]},{"Header":"Butiker","Link":"/se/kundtjanst/store","LinkNoFollow":false,"SvgIcon":"","CategoryPanelIndex":999,"Index":5,"Sections":[]},{"Header":"Kundservice","Link":"/se/kundtjanst","LinkNoFollow":false,"SvgIcon":"","CategoryPanelIndex":999,"Index":6,"Sections":[]}],"OffersHeader":"","Offers":[{"Header":"Outlet","Link":"/se/kampanj/fyndhornan","LinkNoFollow":false,"SvgIcon":"","CategoryPanelIndex":1,"Index":3,"Sections":[]},{"Header":"Alla kampanjer just nu","Link":"","LinkNoFollow":false,"SvgIcon":"","CategoryPanelIndex":999,"Index":4,"Sections":[{"sectionsHeader":"","sectionLinks":[{"header":"Romaner SOMMARREA\t\r","link":"/se/kampanj/sommarrea-romaner"},{"header":"E-böcker SOMMARREA\t\r","link":"/se/kampanj/sommarrea-e-bocker"},{"header":"Deckare SOMMARREA\t\r","link":"/se/kampanj/sommarrea-deckare"},{"header":"Kokböcker SOMMARREA\r","link":"/se/kampanj/sommarrea-kokbocker"},{"header":"Barnböcker SOMMARREA\t\t\r","link":"/se/kampanj/sommarrea-barnbocker-3-6-ar"},{"header":"4 böcker för 100 kr\t\r","link":"/se/kampanj/lagerrensning"},{"header":"Stor lagerrensning - Upp till 70%\r","link":"/se/avdelning/lagerrensning-a9590"},{"header":"Pocket 4 för 3\r","link":"/se/kampanj/billiga-pocket-4for3"},{"header":"Fynda pocket för 39kr/st\r","link":"/se/kampanj/fynda-pocket"},{"header":"Sommarlovsläsning\t\r","link":"/se/kampanj/semester-lassemaja"},{"header":"Sommarens bästa fantasyböcker\r","link":"/se/kampanj/fantasy-sommar"},{"header":"Nya barnböcker\t\r","link":"/se/kampanj/barnbocker-nyheter"},{"header":"Pippi Långstrump fyller 75 år!\t\r","link":"/se/kampanj/pippi-langstrump"},{"header":"Novita garn SOMMARREA\r","link":"/se/kampanj/sommarrea-garn-novita"},{"header":"Sommarlek upp till 20%\t\r","link":"/se/kampanj/sommarlek"},{"header":"Uteleksaker SOMMARREA upp till 50%\r","link":"/se/kampanj/sommarrea-uteleksaker"},{"header":"Resevagnar SOMMARREA\r","link":"/se/kampanj/resevagnar"},{"header":"Pussel 20%\r","link":"/se/kampanj/clementoni-pussel"},{"header":"Badlakan upp till 20%\t\r","link":"/se/kampanj/badlakan-vuxen"},{"header":"Presentböcker \t\r","link":"/se/kampanj/presentbocker"},{"header":"Novellix presentaskar\t\r","link":"/se/kampanj/present-noveller"},{"header":"Böcker på engelska 3 för 100 kr\r","link":"/se/kampanj/lagerrensning-engelska-bocker-1"},{"header":"Pocket på engelska 2 för 129 kr\t\r","link":"/se/kampanj/sommarlasning-summer-reading"},{"header":"Nyheter på engelska\t\r","link":"/se/kampanj/nya-bocker-engelska"},{"header":"Månadens bokpärlor 20%\t\r","link":"/se/kampanj/manadensparlor"},{"header":"Trädgårdstider\t\r","link":"/se/kampanj/bocker-tradgard"},{"header":"Djur \u0026 Natur för de minsta\t\r","link":"/se/kampanj/djur-natur-barn"},{"header":"Bullet Journal\t\r","link":"/se/kampanj/bulletjournal"},{"header":"#Oursea – Mumin 75 år\t\r","link":"/se/kampanj/oursea-mumin-75-ar"},{"header":"Hållbarhet \u0026 eko\t\r","link":"/se/kampanj/hallbarhet"},{"header":"Res i Sverige!\t\r","link":"/se/kampanj/res-i-sverige"},{"header":"Vegetariska kokböcker\t\r","link":"/se/kampanj/vegetariska-kokbocker"},{"header":"Föräldratips\t\r","link":"/se/kampanj/foraldratips"},{"header":"LEGO\t\r","link":"/se/kampanj/legonyheter"},{"header":"Presentkort","link":"/se/kampanj/presentkort"}]}]}]},
        homeLink: "/se",
        userIsLoggedIn: "False" === "True",
        accountText: "Konto",
        logInText: "Logga in",
        search: {
            searchActionUrl: "/se/sok",
            autocompleteActionUrl: "/se/autocomplete/show",
            allowEmptySearch: "False".toLowerCase() === "true",
            hideCategorySelector:
                "False".toLowerCase() === "true",
            categorySelectorEnableABTest: "true".toLowerCase() ===
                'true',
            currentRouteController: "search",
            translations: {
                placeholder: "Sök bland miljontals produkter!",
                placeholderDesktop:
                    "Sök bland miljontals böcker, leksaker, garn, köksartiklar och mycket mer! ",
                categorySelectorPlaceHolder: "Sök inom",
                categorySelectorAllCategories: "Alla kategorier"
            }
        },
        account: {
            accountTogglerText: "Konto",
            accountHeading: "Mitt konto",
            settings: {
                url: "/se/konto/settings",
                text: "Kunduppgifter"
            },
            orders: {
                url: "/se/konto/orders",
                text: "Beställningar"
            },
            wishlist: {
                url: "/se/onskelista/watch",
                text: "Önskelistor"
            },
            watchlist: {
                url: "/se/bevakningar/watch",
                text: "Bevakningar"
            },
            library: {
                url: "/se/konto/library",
                text: "Digitalt bibliotek"
            },
            resetPassword: {
                url: "/se/konto/requestpassword",
                text: "Glömt ditt användarnamn eller lösenord?"
            },
            login: {
                heading: "Logga in",
                togglerText: "Logga in",
                actionUrl: "/se/konto/login",
                returnUrl: "/se/sok?q=9780141393391",
                userName: {
                    labelText: "Användarnamn",
                    placeholderText: "Användarnamn",
                    valueMissingMessage: "Användarnamn krävs"
                },
                password: {
                    labelText: "Lösenord",
                    placeholderText: "Lösenord",
                    valueMissingMessage: "Lösenord krävs",
                    tooShortMessage: "Lösenordet är för kort" //TODO add migration
                },
                submitText: "Logga in"
            },
            createAccount: {
                heading: "Skapa konto",
                private: {
                    url: "/se/konto/createprivate",
                    text: "För privatperson"
                },
                organization: {
                    url: "/se/konto/createorganisation",
                    text: "För företag"
                }
            },
            logout: {
                heading: "<b>Inloggad som:</b> ",
                buttonText: "Logga ut",
                formData: {
                    url: "/se/konto/logout",
                    returnUrl: "/se/sok?q=9780141393391"
                }
            }
        },
        cart: {
            CartGetUrl: "/se/minicart/get",
            CartAddItemUrl: "/se/minicart/add",
            CartAddItemsUrl: "/se/minicart/addmultiple",
            CartRemoveItemUrl: "/se/minicart/remove",
            CartUpdateItemUrl: "/se/minicart/update",
            CheckoutUrl: "/se/checkouttransfer/transfer",
            Cookie: {
                Key: "miniCartID_sv-SE",
                Value: ""
            },
            CultureCode: "sv-SE",
            LegacyImageSizesEnabled: "True".toLowerCase() === "true",
            Translations: {
                cartEmptyMessage: "Kundvagnen är tom",
                cartText: "Kundvagn",
                closeText: "Stäng",
                defaultErrorMessage: "Något gick fel! Försök igen!",
                discountHeaderText: "Rabatt",
                loadingCartMessage: "Laddar varukorgen...",
                quantityOfText: "st av",
                toCheckoutText: "Till kassan",
                totalHeaderText: "Summa inkl moms",
                wasAddedText: "är tillagd i kundvagnen.",
                and: "och"
            }
        },
        notification: {
            cookiesInfo: {
                text: 'Genom att fortsätta godkänner du att vi använder <a href="/se/kundtjanst/security">cookies</a> på sajten.',
                desktopText: 'Vi använder <a href="/se/kundtjanst/security">cookies</a> för att ge dig bästa möjliga upplevelse på sajten. Genom att fortsätta godkänner du att vi använder cookies.',
                consentText: "Jag förstår",
                cookieKey: "adlibrisCookieAccept"
            },
            addToCart: {
                abTestVersion: 0,
                showCartText: "Visa kundvagn",
                closeButtonText: "Fortsätt handla"
            },
            infoBanner: {
                textColor: "#FFFFFF",
                backgroundColor: "#e81121",
                linkColor: "#FFFFFF",
                mobileText: "\u003ca href=\&quot;https://www.adlibris.com/se/avdelning/lagerrensning-a9590\&quot;\u003eStor lagerrensning - fynda nu!\u003c/a\u003e",
                desktopText: "\u003ca href=\&quot;https://www.adlibris.com/se/avdelning/lagerrensning-a9590\&quot;\u003eStor lagerrensning - fynda nu!\u003c/a\u003e"
            }
        },
        translations: {
            loadingPage: "Laddar sidans innehåll..."
        },
        webpackBundlePathLongTermCache: "https://s3.adlibris.com/dist/long_term_cache201912041650",
        controllerName: "search",
        actionName: "show"
    };
</script>

<code data-hook-js-translations-shared-layout="" data-translation-error-errortext="Ett fel uppstod" data-translation-error-close="Avbryt" data-translation-error-retry="Försök igen." data-translation-cart-error="Ett fel uppstod när produkten skulle läggas i kundvagnen" data-translation-cart-confirmremove="Ta bort produkten ur kundvagnen?" data-translation-shared-readfull="Läs&nbsp;mer" data-translation-checkout-cartisempty="Din varukorg är tom. Du omdirigeras till startsidan.">
</code>

<script>
    window.webpackBundlePath = "https://s3.adlibris.com/dist/c73216d3add0c084b6c48647bfd8b470a16f2fd8";
    window.webpackBundlePathLongTermCache = "https://s3.adlibris.com/dist/long_term_cache201912041650";
</script>
<script src="https://s3.adlibris.com/dist/c73216d3add0c084b6c48647bfd8b470a16f2fd8/runtime.js"></script>
<script src="https://s3.adlibris.com/dist/c73216d3add0c084b6c48647bfd8b470a16f2fd8/vendor.js"></script>
<script src="https://s3.adlibris.com/dist/c73216d3add0c084b6c48647bfd8b470a16f2fd8/globals.js"></script><script type="text/javascript" id="">function getSelectionText(){var a="";window.getSelection?a=window.getSelection().toString():document.selection&&"Control"!=document.selection.type&&(a=document.selection.createRange().text);return a}document.addEventListener("copy",function(a){dataLayer.push({event:"textCopied",clipboardText:getSelectionText(),clipboardLength:getSelectionText().length})});</script><script type="text/javascript" id="">try{(function(){var f="TRADEDOUBLER",m="TRADEDOUBLER_MECENAT",n="studentkortet",k=function(b){var c=document.location.search,a="";do{var e=c.indexOf(b+"\x3d");if(-1!==e){c=c.substr(e+b.length+1,c.length-e);var g=c.indexOf("\x26");g=-1!==g?c.substr(0,g):c;a=""===a||""===g?a+g:a+(", "+g)}}while(-1!==e);for(b=a.indexOf("+");-1!==b;)a=a.substr(0,b)+" "+a.substr(b+1,a.length),b=a.indexOf("+");return a.replace(/^\s+|\s+$/g,"")},h=function(b){var c=document.cookie;b+="\x3d";var a=c.indexOf("; "+b);if(-1===
a){if(a=c.indexOf(b),0!==a)return null}else a+=2;var e=document.cookie.indexOf(";",a);-1===e&&(e=c.length);return decodeURI(c.substring(a+b.length,e))},l=function(b,c,a){var e=3<arguments.length&&void 0!==arguments[3]?arguments[3]:"/",g=4<arguments.length&&void 0!==arguments[4]?arguments[4]:window.location.hostname.replace("www.",""),f=arguments[5],d=new Date;d.setTime(d.getTime());a&&(a*=864E5);d=new Date(d.getTime()+a);document.cookie=b+"\x3d"+encodeURI(c)+(a?"; expires\x3d"+d.toUTCString():"")+
(e?"; path\x3d"+e:"")+(g?"; domain\x3d"+g:"")+(f?"; secure":"")},d=k("tduid");""!==d?h(f)||l(f,d,365):d=h(f);"mecenat"!==k("referer")||h(m)||""===d||l(m,d,365);"studentkortet"!==k("utm_source")||h(n)||l(n,"studentkortet",365);null!==sessionStorage.getItem(f)&&""!==sessionStorage.getItem(f)||sessionStorage.setItem(f,d)})()}catch(f){console.log(f.message)};</script>
<script type="text/javascript" id="" src="https://adtr.io/jsTag?ap=736792777"></script>
<script src="https://s3.adlibris.com/dist/c73216d3add0c084b6c48647bfd8b470a16f2fd8/pageLayout.js"></script>






    <script>
        var searchTranslations = {
            showAs: "Välj visningsvy",
            showAsList: "Visa som lista",
            showAsGrid: "Visa som rutnät"
        };
    </script>
    <script defer="" src="https://s3.adlibris.com/dist/c73216d3add0c084b6c48647bfd8b470a16f2fd8/modules_search.js"></script>
    <script defer="" src="https://s3.adlibris.com/dist/c73216d3add0c084b6c48647bfd8b470a16f2fd8/component_search.js"></script>


<script type="text/javascript" id="" src="//static.criteo.net/js/ld/ld.js"></script><script type="text/javascript" id="">var metaTagAdlibris=document.createElement("meta");metaTagAdlibris.name="robots";metaTagAdlibris.content="noindex";document.head.appendChild(metaTagAdlibris);</script></body></html>
TypeError: Attempting to change the setter of an unconfigurable property.
TypeError: Attempting to change the setter of an unconfigurable property.
