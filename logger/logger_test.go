package logger

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func TestNewManager(t *testing.T) {
	tt := []struct {
		desc          string
		developerLogs bool
		level         zapcore.Level
	}{
		{
			desc:  "create new log manager with error level",
			level: zap.ErrorLevel,
		},
		{
			desc:  "create new log manager with warn level",
			level: zap.WarnLevel,
		},
		{
			desc:  "create new log manager with info level",
			level: zap.InfoLevel,
		},
		{
			desc:          "create new log manager with debug level and developer logs",
			developerLogs: true,
			level:         zap.DebugLevel,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			lm := NewManager(test.developerLogs, test.level)
			lg := lm.NewSugaredLogger().Named(t.Name())

			assert.Equal(t, test.developerLogs, lm.developerLogs)
			assert.Equal(t, zap.NewAtomicLevelAt(test.level), lm.level)

			var logType string
			switch test.developerLogs {
			case true:
				logType = "developer"
			case false:
				logType = "production"
			}

			lg.Debugf("%s debug log", logType)
			lg.Infof("%s info log", logType)
			lg.Warnf("%s warn log", logType)
			lg.Errorf("%s error log", logType)
		})
	}
}

func TestSetLogLevel(t *testing.T) {
	lm := NewManager(true, zap.DebugLevel)

	tt := []struct {
		desc  string
		level zapcore.Level
	}{
		{
			desc:  "set log level to error",
			level: zap.ErrorLevel,
		},
		{
			desc:  "set log level to warn",
			level: zap.WarnLevel,
		},
		{
			desc:  "set log level to info",
			level: zap.InfoLevel,
		},
		{
			desc:  "set log level to debug",
			level: zap.DebugLevel,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			lm.SetLogLevel(test.level)
			assert.Equal(t, zap.NewAtomicLevelAt(test.level), lm.level)
		})
	}
}

func TestGetLogLevel(t *testing.T) {
	lm := NewManager(true, zap.DebugLevel)

	tt := []struct {
		desc  string
		level zapcore.Level
	}{
		{
			desc:  "get log level error",
			level: zap.ErrorLevel,
		},
		{
			desc:  "get log level warn",
			level: zap.WarnLevel,
		},
		{
			desc:  "get log level info",
			level: zap.InfoLevel,
		},
		{
			desc:  "get log level debug",
			level: zap.DebugLevel,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			atomicLevel := zap.NewAtomicLevelAt(test.level)
			lm.level = atomicLevel
			assert.Equal(t, atomicLevel.Level().CapitalString(), lm.GetLogLevel())
		})
	}
}

func TestNewSugaredLogger(t *testing.T) {
	tt := []struct {
		desc          string
		developerLogs bool
		level         zapcore.Level
	}{
		{
			desc:          "create new sugared logger with debug level",
			developerLogs: false,
			level:         zap.DebugLevel,
		},
		{
			desc:          "create new sugared logger with info level",
			developerLogs: false,
			level:         zap.InfoLevel,
		},
		{
			desc:          "create new sugared logger with warn level",
			developerLogs: false,
			level:         zap.WarnLevel,
		},
		{
			desc:          "create new sugared logger with error level",
			developerLogs: true,
			level:         zap.ErrorLevel,
		},
	}

	for i, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			lm := NewManager(true, test.level)
			lg := lm.NewSugaredLogger().Named(t.Name() + strconv.Itoa(i))
			assert.NotNil(t, lg)

			switch test.level {
			case zap.DebugLevel:
				lg.Debug("debug level set")
			case zap.InfoLevel:
				lg.Info("info level set")
			case zap.WarnLevel:
				lg.Warn("warn level set")
			case zap.ErrorLevel:
				lg.Error("error level set")
			case zap.PanicLevel:
				// not used
			case zap.DPanicLevel:
				// not used
			case zap.FatalLevel:
				// not used
			default:
			}

			lg.Debug("debug log")
			lg.Info("info log")
			lg.Warn("warn log")
			lg.Error("error log")
		})
	}
}
