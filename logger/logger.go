package logger

import (
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// Config holds configuration parameters for loggers
type Config struct {
	Developer bool
	Level     zapcore.Level
}

// Manager manages creation and configuration of loggers
type Manager struct {
	level         zap.AtomicLevel
	developerLogs bool
}

func timeEncoder(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString(t.Format("2006-01-02 15:04:05"))
}

// NewManager creates a new LogManger
func NewManager(developerLogs bool, level zapcore.Level) *Manager {
	return &Manager{
		level:         zap.NewAtomicLevelAt(level),
		developerLogs: developerLogs,
	}
}

// NewSugaredLogger creates a new SugaredLogger
func (l *Manager) NewSugaredLogger() *zap.SugaredLogger {
	var zcfg zap.Config

	if l.developerLogs {
		zcfg = zap.NewDevelopmentConfig()
	} else {
		zcfg = zap.NewProductionConfig()
		zcfg.Encoding = "console"
		zcfg.EncoderConfig = zapcore.EncoderConfig{
			TimeKey:        "ts",
			LevelKey:       "level",
			MessageKey:     "msg",
			StacktraceKey:  "stacktrace",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    zapcore.LowercaseLevelEncoder,
			EncodeTime:     timeEncoder,
			EncodeDuration: zapcore.SecondsDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
		}
	}

	zcfg.Level = l.level

	logger, _ := zcfg.Build()
	return logger.Sugar()
}

// GetLogLevel returns the configured log level
func (l *Manager) GetLogLevel() string {
	return l.level.Level().CapitalString()
}

// SetLogLevel sets the configured log level
func (l *Manager) SetLogLevel(newLevel zapcore.Level) {
	l.level.SetLevel(newLevel)
}
