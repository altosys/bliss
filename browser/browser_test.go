package browser

import (
	"bliss/mock"
	"bytes"
	"os"
	"strings"
	"testing"

	"github.com/PuerkitoBio/goquery"
	"github.com/stretchr/testify/assert"
)

func TestGetDOM(t *testing.T) {
	tt := []struct {
		desc      string
		proxyAddr string
		doc       string
		err       bool
	}{
		{
			desc: "return dom and nil on success",
			err:  false,
			doc:  "../internal/testing/sites/testdata/adlibris_in_stock_hc",
		},
		{
			desc: "return nil and error on fail",
			err:  true,
			doc:  "../internal/testing/sites/testdata/empty",
		},
		{
			desc:      "return nil and error on fail with proxy",
			proxyAddr: "socks5://invalid:addr",
			err:       true,
			doc:       "../internal/testing/sites/testdata/empty",
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			f, err := os.ReadFile(test.doc)
			assert.NoError(t, err)

			ts := mock.HTTPServer(t, "/", f)
			defer ts.Close()

			doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(f))
			assert.NoError(t, err)
			docDOM := doc.Selection

			if test.proxyAddr != "" {
				os.Setenv("BLISS_PROXY_ADDRESS", test.proxyAddr)
				defer os.Clearenv()
			}

			if test.err {
				dom, err := GetDOM("url:invalid")
				assert.Error(t, err)
				assert.Nil(t, dom)
			}

			if !test.err {
				tsDOM, err := GetDOM(ts.URL)
				assert.NoError(t, err)

				if strings.Contains(test.doc, "empty") {
					assert.Empty(t, docDOM.Text())
					assert.Empty(t, tsDOM.Text())
				}

				if !strings.Contains(test.doc, "empty") {
					assert.NotEmpty(t, docDOM.Text())
					assert.NotEmpty(t, tsDOM.Text())
				}
				assert.Equal(t, docDOM.Text(), tsDOM.Text())
			}
		})
	}
}
