package browser

import (
	"math/rand"
	"os"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/tommyalatalo/surf"
)

// GetDOM requests and return a DOM selection for the requested URL.
// if BLISS_PROXY_ADDRESS is set it will be used for routing the request
func GetDOM(url string) (*goquery.Selection, error) {
	rand.Seed(time.Now().UnixNano())
	bow := surf.NewBrowser()
	bow.SetUserAgent(userAgents[rand.Intn(len(userAgents))])

	if os.Getenv("BLISS_PROXY_ADDRESS") != "" {
		if err := bow.SetProxy(os.Getenv("BLISS_PROXY_ADDRESS")); err != nil {
			return nil, err
		}
	}

	if err := bow.Open(url); err != nil {
		return nil, err
	}
	return bow.Dom(), nil
}
