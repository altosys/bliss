package manager

import (
	"context"
	"math/rand"
	"time"
)

type randomTicker struct {
	ctx context.Context
	C   chan time.Time
	min int64
	max int64
}

func newRandomTicker(ctx context.Context, min, max time.Duration) *randomTicker {
	rt := &randomTicker{
		ctx: ctx,
		C:   make(chan time.Time),
		min: min.Nanoseconds(),
		max: max.Nanoseconds(),
	}
	go rt.loop()
	return rt
}

func (rt *randomTicker) loop() {
	t := time.NewTimer(rt.nextInterval())
	for {
		select {
		case <-rt.ctx.Done():
			t.Stop()
			return
		case <-t.C:
			select {
			case rt.C <- time.Now():
				t.Stop()
				t = time.NewTimer(rt.nextInterval())
			default:
			}
		}
	}
}

func (rt *randomTicker) nextInterval() time.Duration {
	interval := rand.Int63n(rt.max-rt.min) + rt.min
	return time.Duration(interval) * time.Nanosecond
}
