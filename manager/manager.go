package manager

import (
	"bliss"
	"bliss/notify"
	r "bliss/redis"
	"context"
	"fmt"
	"math"
	"math/rand"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/gregdel/pushover"
	"go.uber.org/zap"
)

// Config holds configuration parameters for scrapers
type Config struct {
	Interval     int
	IntervalUnit time.Duration
	Enabled      map[string]struct{}
	Scrapers     []bliss.Scraper
	MetaScrapers []bliss.Scraper
}

// Manager manages scrapers and notifications
type Manager struct {
	interval     int
	intervalUnit time.Duration
	notifyConfig notify.Config
	redis        r.Redis
	state        state
	lg           *zap.SugaredLogger
	scrapers     map[string]bliss.Scraper
	metaScrapers []bliss.Scraper
}

type state struct {
	mutex    sync.RWMutex
	baseline bool
	startup  bool
}

// NewManager create a new scraper manager
func NewManager(
	redis r.Redis,
	notifyConfig notify.Config,
	scraperConfig Config,
	lg *zap.SugaredLogger,
) *Manager {
	scrapers := map[string]bliss.Scraper{}
	for _, s := range scraperConfig.Scrapers {
		scrapers[s.Site()] = s
	}

	return &Manager{
		interval:     scraperConfig.Interval,
		intervalUnit: scraperConfig.IntervalUnit,
		lg:           lg,
		redis:        redis,
		notifyConfig: notifyConfig,
		metaScrapers: scraperConfig.MetaScrapers,
		scrapers:     scrapers,
		state: state{
			mutex:    sync.RWMutex{},
			baseline: false,
			startup:  true,
		},
	}
}

// Start starts up configured scrapers
func (m *Manager) Start(
	ctx context.Context,
	wg *sync.WaitGroup,
	dbCh chan<- bliss.Listing,
	errCh chan<- error,
	metricsCh chan<- bliss.Listing,
	notifyCh chan<- *pushover.Message,
) {
	lg := m.lg
	rand.Seed(time.Now().UTC().UnixNano())
	var swg sync.WaitGroup
	var err error

	handleError := func(err error) {
		lg.Error(err)
		if errCh != nil {
			errCh <- err
		}
	}

	checkBaseLine := func() {
		if notifyCh != nil {
			m.state.mutex.Lock()
			m.state.baseline, m.state.startup, err = m.redis.CheckBaseline(ctx,
				m.redis.Client, m.state.baseline, m.state.startup, m.interval, notifyCh, lg)
			if err != nil {
				handleError(err)
			}
			m.state.mutex.Unlock()
		}
	}
	checkBaseLine()

	lg.Info("starting scrapers...")
	for _, s := range m.scrapers {
		wg.Add(1)
		swg.Add(1)
		go func(ctx context.Context, s bliss.Scraper) {
			defer wg.Done()
			defer swg.Done()
			lg.Infof("scraper started for %s", s.Site())

			jitter := 0.75
			max := time.Duration(m.interval) * m.intervalUnit
			min := time.Duration(int(float32(jitter) * float32(max)))
			rt := newRandomTicker(ctx, min, max)

			for {
				select {
				case <-ctx.Done():
					lg.Debugf("stopping scraper for %s", s.Site())
					return
				case <-rt.C:
					isbn, prioritized, err := m.redis.GetNextISBN(ctx, m.redis.Client, s.Site())
					if err != nil {
						handleError(err)
						continue
					}

					book := m.getBookMeta(ctx, isbn, lg)
					exchange, err := m.redis.GetExchange(ctx, m.redis.Client)
					if err != nil {
						handleError(err)
						continue
					}

					listing, err := s.Scrape(book, s, false, lg)
					if err != nil {
						handleError(err)
						continue
					}

					// convert listing to expected currency
					listing.Price, listing.Currency = exchange.ConvertPrice(
						listing.Price,
						listing.Currency,
						exchange.Base,
						lg,
					)

					// fetch lowest listing from Redis
					lowest, err := m.redis.GetLowestListings(ctx, m.redis.Client)
					if err != nil {
						handleError(err)
						continue
					}
					low := lowest[isbn]

					// send stock notifications before registering new listing
					m.notifyLowStock(ctx, listing, notifyCh, lg)
					m.notifyAvailable(ctx, listing, notifyCh, lg)

					isDrop, isDeleted, err := m.redis.RegisterListing(
						ctx,
						m.redis.Client,
						listing,
						prioritized,
					)
					if err != nil {
						handleError(err)
						continue
					}

					if isDrop || isDeleted {
						metricsCh <- listing
					}

					if isDrop {
						m.notifyPriceDrop(ctx, low, listing, notifyCh, lg)
					}

					// do not send to dbCh if context was canceled (i.e. shutdown initiated)
					if ctx.Err() == nil {
						dbCh <- listing
					}

					if notifyCh != nil {
						checkBaseLine()
					}

					lg.Info(fmt.Sprintf(
						"%-20s %-55s %-3s %-4v %v",
						s.Site(),
						book.Title,
						book.Format,
						exchange.Base,
						fmt.Sprintf("%.2f", listing.Price),
					))
				}
			}
		}(ctx, s)
	}
	lg.Info("finished starting scrapers")
}

func (m *Manager) notifyLowStock(
	ctx context.Context,
	l bliss.Listing,
	notifyCh chan<- *pushover.Message,
	lg *zap.SugaredLogger,
) {
	m.state.mutex.RLock()
	defer m.state.mutex.RUnlock()

	if !m.notifyConfig.LowStockEnabled {
		lg.Debug("low stock notifications disabled, no notification")
		return
	}

	if notifyCh == nil {
		lg.Debug("notifyCh is nil, no low stock notification")
		return
	}

	if !m.state.baseline {
		lg.Debug("baseline not established, no low stock notification")
		return
	}

	if l.Price != bliss.NotFound {
		lg.Debugf("price found for %s, no low stock notification", l.Book.Title)
		return
	}

	listings, err := m.redis.GetListings(ctx, m.redis.Client)
	if err != nil {
		lg.Error(err)
		return
	}

	previous := listings[l.Site]
	if previous, exists := previous[l.Book.ISBN]; exists && previous.Price == bliss.NotFound {
		return
	}

	var inStockSites []string
	for site := range m.scrapers {
		siteListings := listings[site]
		if listing, exists := siteListings[l.Book.ISBN]; exists && listing.Price != bliss.NotFound {
			inStockSites = append(inStockSites, site)
		}
	}

	lowStockNotifiable := len(inStockSites) == 2
	if !lowStockNotifiable {
		return
	}

	var lastInStockSite string
	for _, site := range inStockSites {
		if site != l.Site {
			lastInStockSite = site
			break
		}
	}

	link := fmt.Sprintf(
		"<a href=%q>%s</a>",
		m.scrapers[lastInStockSite].SearchURL(l.Book.ISBN),
		lastInStockSite,
	)
	lg.Debugf("sending low stock notification for '%s'", l.Book.Title)

	lastInStockSiteListings := listings[lastInStockSite]
	lastInStockPrice := lastInStockSiteListings[l.Book.ISBN].Price
	lastInStockCurrency := lastInStockSiteListings[l.Book.ISBN].Currency
	message := fmt.Sprintf(
		"<b>%s</b> may be running out of stock and is currently only available at %s for %.0f %s",
		l.Book.Title,
		link,
		lastInStockPrice,
		lastInStockCurrency,
	)

	unique, err := m.redis.UpdateNotification(ctx, m.redis.Client, message)
	if err != nil {
		lg.Error(err)
		return
	}

	if !unique {
		lg.Debugf("duplicate low stock notification for '%s', skipping send", l.Book.Title)
		return
	}

	notifyCh <- &pushover.Message{
		Title:     "Book Stock Running Low",
		Message:   message,
		Priority:  pushover.PriorityNormal,
		Timestamp: time.Now().Unix(),
		URL:       m.scrapers[lastInStockSite].SearchURL(l.Book.ISBN),
		URLTitle:  fmt.Sprintf("View listing at %s", lastInStockSite),
		HTML:      true,
		Retry:     60 * time.Second,
		Expire:    24 * time.Hour,
	}
}

func (m *Manager) notifyPriceDrop(
	ctx context.Context,
	oldListing, newListing bliss.Listing,
	notifyCh chan<- *pushover.Message,
	lg *zap.SugaredLogger,
) {
	m.state.mutex.RLock()
	defer m.state.mutex.RUnlock()

	if m.notifyConfig.DropThreshold == 0 {
		lg.Debug("notification drop threshold is 0, skipping price drop notification")
		return
	}

	if notifyCh == nil {
		lg.Debug("notifyCh is nil, skipping price drop notification")
		return
	}

	if !m.state.baseline {
		lg.Debug("baseline not established, skipping price drop notification")
		return
	}

	dropAbsolute := newListing.Price - oldListing.Price
	dropPercent := math.Round(float64((100 - (newListing.Price/oldListing.Price)*100)))
	if dropPercent < float64(m.notifyConfig.DropThreshold) {
		lg.Debugf(
			"notification threshold %v%% filtered price drop message (dropped %v%%)",
			m.notifyConfig.DropThreshold,
			dropPercent,
		)
		lg.Debug("price drop below notification threshold, skipping price drop notification")
		return
	}

	dropLink := fmt.Sprintf(
		"<a href=%q>%s</a>",
		m.scrapers[newListing.Site].SearchURL(newListing.Book.ISBN),
		newListing.Site,
	)

	title := fmt.Sprintf("Book Price Dropped By %.f%%", dropPercent)
	msgFormat := "<b>%s</b> dropped by %.0f%% (%.0f %s) to %.0f %s at %s\n\n%s"
	message := fmt.Sprintf(msgFormat, newListing.Book.Title, dropPercent,
		dropAbsolute, newListing.Currency, newListing.Price, newListing.Currency, dropLink,
		"(No listings found from other sellers)")

	// build list of other prices if applicable
	listings, err := m.redis.GetListings(ctx, m.redis.Client)
	if err != nil {
		lg.Error(err)
		return
	}

	stocked := []bliss.Listing{}
	for site := range m.scrapers {
		if site == newListing.Site {
			continue
		}

		siteListings := listings[site]
		if _, exists := siteListings[newListing.Book.ISBN]; exists {
			if siteListings[newListing.Book.ISBN].Price != bliss.NotFound {
				stocked = append(stocked, siteListings[newListing.Book.ISBN])
			}
		}
	}

	if len(stocked) != 0 {
		sort.SliceStable(stocked, func(i, j int) bool {
			return stocked[i].Site < stocked[j].Site
		})

		sb := strings.Builder{}
		fmt.Fprintf(&sb, "<b>Listings from other sellers:</b>")

		maxMsgLength := 1024
		for i, s := range stocked {
			trunc := fmt.Sprintf("\n(+%v more)", len(stocked)-i)
			line := fmt.Sprintf("\n<a href=%q>%s</<a> %.0f %s",
				m.scrapers[s.Site].SearchURL(newListing.Book.ISBN),
				s.Site, s.Price, newListing.Currency)
			// write trunc line if new line won't fit
			if sb.Len()+len(line) > maxMsgLength {
				fmt.Fprint(&sb, trunc)
				break
			}
			// write trunc line if current line fits, but next iteration trunc doesn't
			if (maxMsgLength-(sb.Len()+len(line))) < len(trunc) && i != len(stocked)-1 {
				fmt.Fprint(&sb, trunc)
				break
			}
			fmt.Fprint(&sb, line)
		}
		message = fmt.Sprintf(msgFormat, newListing.Book.Title, dropPercent, dropAbsolute,
			newListing.Currency, newListing.Price, newListing.Currency, dropLink, sb.String())
	}

	unique, err := m.redis.UpdateNotification(ctx, m.redis.Client, message)
	if err != nil {
		lg.Error(err)
		return
	}

	if !unique {
		lg.Debugf(
			"duplicate price drop notification for '%s', skipping send",
			newListing.Book.Title,
		)
		return
	}

	lg.Debugf("sending price drop notification for '%s'", newListing.Book.Title)
	notifyCh <- &pushover.Message{
		Title:     title,
		Message:   message,
		Priority:  pushover.PriorityNormal,
		Timestamp: time.Now().Unix(),
		URL:       m.scrapers[newListing.Site].SearchURL(newListing.Book.ISBN),
		URLTitle:  fmt.Sprintf("View listing at %s", newListing.Site),
		HTML:      true,
		Retry:     60 * time.Second,
		Expire:    24 * time.Hour,
	}
}

func (m *Manager) notifyAvailable(
	ctx context.Context,
	l bliss.Listing,
	notifyCh chan<- *pushover.Message,
	lg *zap.SugaredLogger,
) {
	m.state.mutex.RLock()
	defer m.state.mutex.RUnlock()

	if !m.notifyConfig.AvailabilityEnabled {
		lg.Debug("availablility notifications disabled, skipping send")
		return
	}

	if notifyCh == nil {
		lg.Debug("notifyCh is nil, skipping availability notification")
		return
	}

	if !m.state.baseline {
		lg.Debug("baseline not established, skipping price drop notification")
		return
	}

	if l.Price == bliss.NotFound {
		lg.Debug("notifyAvailable: price not found - not available")
		return
	}

	listings, err := m.redis.GetListings(ctx, m.redis.Client)
	if err != nil {
		lg.Error(err)
		return
	}

	// check that no site has stock
	for site := range m.scrapers {
		siteListings := listings[site]

		if len(siteListings) == 0 {
			continue
		}

		if siteListings[l.Book.ISBN].Price != bliss.NotFound {
			return
		}
	}

	link := fmt.Sprintf("<a href=%q>%s</a>", m.scrapers[l.Site].SearchURL(l.Book.ISBN), l.Site)
	message := fmt.Sprintf(
		"<b>%s</b> is now available at %s for %.0f %s",
		l.Book.Title,
		link,
		l.Price,
		l.Currency,
	)

	unique, err := m.redis.UpdateNotification(ctx, m.redis.Client, message)
	if err != nil {
		lg.Error(err)
		return
	}

	if !unique {
		lg.Debugf("duplicate availability notification for '%s', skipping send", l.Book.Title)
		return
	}

	lg.Debugf("sending availability notification for '%s'", l.Book.Title)
	notifyCh <- &pushover.Message{
		Title:     "Book Available",
		Message:   message,
		Priority:  pushover.PriorityNormal,
		Timestamp: time.Now().Unix(),
		URL:       m.scrapers[l.Site].SearchURL(l.Book.ISBN),
		URLTitle:  fmt.Sprintf("View listing at %s", l.Site),
		HTML:      true,
		Retry:     60 * time.Second,
		Expire:    24 * time.Hour,
	}
}

func (m *Manager) getBookMeta(ctx context.Context, isbn string, lg *zap.SugaredLogger) bliss.Book {
	meta, err := m.redis.GetBookMeta(ctx, m.redis.Client, isbn)
	if err != nil {
		lg.Error(err)
		return bliss.Book{}
	}

	isMetaComplete := func(b bliss.Book) bool {
		return b.Format != bliss.NA && b.Title != ""
	}

	book, exists := meta[isbn]
	if exists && isMetaComplete(book) {
		return book
	}
	if !exists {
		return bliss.Book{}
	}

	lg.Debugf("fetching metadata for %s", isbn)
	book = bliss.Book{ISBN: isbn}

	for _, s := range m.metaScrapers {
		listing, err := s.Scrape(book, s, true, lg)
		if err != nil {
			lg.Error(err)
			continue
		}

		if listing.Book.Title != "" {
			lg.Debugf("found title '%s' for %s from %s", listing.Book.Title, isbn, s.Site())
			book.Title = listing.Book.Title
		} else {
			lg.Debugf("no title found for %s from %s", isbn, s.Site())
		}

		if listing.Book.Format != bliss.NA {
			lg.Debugf("found format '%s' for %s from %s", listing.Book.Format, isbn, s.Site())
			book.Format = listing.Book.Format
		} else {
			lg.Debugf("no format found for %s from %s", isbn, s.Site())
		}

		if isMetaComplete(book) {
			break
		}
	}

	if book.Title == "" {
		lg.Infof("no title found for %s from any source", isbn)
	}

	if book.Format == bliss.NA {
		lg.Infof("no format found for %s from any source", isbn)
	}

	if isMetaComplete(book) {
		if err := m.redis.SetBookMeta(ctx, m.redis.Client, book); err != nil {
			m.lg.Error(err)
		}
	}
	return book
}
