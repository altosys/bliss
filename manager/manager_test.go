package manager

import (
	"bliss"
	e "bliss/exchange"
	"bliss/logger"
	"bliss/mock"
	n "bliss/notify"
	r "bliss/redis"
	"bliss/sites"
	"context"
	"errors"
	"fmt"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/alicebob/miniredis/v2"
	"github.com/go-redis/redis/v8"
	"github.com/gregdel/pushover"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

const (
	test1com = "test1.com"
	test2com = "test2.com"
)

func TestNewManager(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())
	ncfg := n.Config{
		LowStockEnabled:     true,
		AvailabilityEnabled: true,
	}

	mockScraper := mock.GetScraper()
	mockScraper.SiteFn = func() string { return "mock" }
	mockMetaScraper := mock.GetScraper()
	mockMetaScraper.SiteFn = func() string { return "meta" }

	scfg := Config{
		Interval:     1,
		IntervalUnit: time.Millisecond,
		MetaScrapers: []bliss.Scraper{mockMetaScraper},
		Scrapers:     []bliss.Scraper{mockScraper},
	}

	rr, err := r.NewRedis(r.Config{}, lg)
	assert.NoError(t, err)

	t.Run("create new scraper managers with single site", func(t *testing.T) {
		m := NewManager(rr, ncfg, scfg, lg)

		_, exists := m.scrapers["mock"]
		assert.True(t, exists)
		assert.Len(t, m.scrapers, 1)
		assert.Equal(t, m.metaScrapers, scfg.MetaScrapers)
		assert.Equal(t, lg, m.lg)
		assert.Equal(t, ncfg, m.notifyConfig)
		assert.Equal(t, rr, m.redis)
		assert.Equal(t, scfg.Interval, m.interval)
		assert.Equal(t, scfg.IntervalUnit, m.intervalUnit)
		assert.Equal(t, false, m.state.baseline)
		assert.Equal(t, true, m.state.startup)
	})
}

func TestGetBookMeta(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	sample := bliss.Book{
		Format: bliss.HC,
		ISBN:   "9780141393391",
		Title:  "Frankenstein",
	}

	tt := []struct {
		desc     string
		doc      string
		redis    bliss.Book
		expected bliss.Book
	}{
		{
			desc:     "update incomplete metadata",
			redis:    bliss.Book{ISBN: sample.ISBN},
			expected: sample,
			doc:      "../internal/testing/sites/testdata/bookdepository_hc",
		},
		{
			desc:     "return empty if no metadata found",
			redis:    bliss.Book{ISBN: sample.ISBN},
			expected: bliss.Book{},
			doc:      "../internal/testing/sites/testdata/bookdepository_not_found",
		},
		{
			desc:     "return empty on unknown isbn",
			redis:    bliss.Book{ISBN: "0000000000000"},
			expected: bliss.Book{},
			doc:      "../internal/testing/sites/testdata/empty",
		},
		{
			desc:     "return empty on redis error",
			redis:    bliss.Book{ISBN: "0000000000000"},
			expected: bliss.Book{},
			doc:      "../internal/testing/sites/testdata/empty",
		},
		{
			desc:     "return complete metadata from redis",
			redis:    sample,
			expected: sample,
			doc:      "../internal/testing/sites/testdata/empty",
		},
		{
			desc:     "return empty on error fetching dom",
			redis:    bliss.Book{ISBN: sample.ISBN},
			expected: bliss.Book{},
			doc:      "../internal/testing/sites/testdata/empty",
		},
		{
			desc:     "handle all web metadata sources failing",
			redis:    bliss.Book{ISBN: sample.ISBN},
			expected: bliss.Book{},
			doc:      "../internal/testing/sites/testdata/empty",
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			tr := newTestRedis(t, lg)
			defer tr.Client.Close()

			if test.redis != (bliss.Book{}) {
				isbn := test.redis.ISBN
				title := test.redis.Title
				format := test.redis.Format.String()
				_, err := tr.Client.HSet(ctx, "books:meta:"+isbn, "isbn", isbn, "format", format, "title", title).
					Result()
				assert.NoError(t, err)
			}

			f, err := os.ReadFile(test.doc)
			assert.NoError(t, err)
			ts := mock.HTTPServer(t, "/", f)
			defer ts.Close()

			mockScraper := mock.GetScraper()
			mockScraper.ScrapeFn = sites.Scrape
			mockScraper.SearchURLFn = func(isbn string) string { return ts.URL }

			switch test.desc {
			case "return empty on redis error":
				tr.Client.Close()
			case "return empty on error fetching dom":
				mockScraper.SearchURLFn = func(isbn string) string { return "http://:invalid" }
			case "handle all web metadata sources failing":
				mockScraper.SearchURLFn = func(string) string { return "http://null" }
			}

			m := &Manager{
				lg:           lg,
				metaScrapers: []bliss.Scraper{mockScraper},
				redis:        tr,
			}

			book := m.getBookMeta(ctx, sample.ISBN, lg)
			if test.expected != (bliss.Book{}) {
				assert.Equal(t, test.expected, book)
			}
		})
	}
}

func TestStart(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc          string
		doc           string
		err           error
		title         string
		notifications int
		listing       bool
		baseline      bool
		startup       bool
	}{
		{
			desc:          "successful run with resulting price drop listing",
			baseline:      true,
			startup:       false,
			listing:       true,
			notifications: 2, // baseline not established on start
			title:         "Frankenstein",
			doc:           "../internal/testing/sites/testdata/bookdepository_hc",
		},
		{
			desc:     "handle error from redis GetNextISBN",
			baseline: false,
			listing:  false,
			doc:      "../internal/testing/sites/testdata/empty",
			err:      errors.New("expected redis key type 'list', found 'none'"),
		},
		{
			desc:     "handle error from redis GetExchange",
			baseline: true,
			listing:  false,
			doc:      "../internal/testing/sites/testdata/empty",
			err:      redis.Nil,
		},
		{
			desc:     "handle error from scrape",
			baseline: true,
			listing:  false,
			err:      errors.New("failed to fetch DOM for 'Frankenstein' at bookdepository.com"),
			doc:      "../internal/testing/sites/testdata/empty",
		},
		{
			desc:     "handle error from redis GetLowestListings",
			baseline: true,
			listing:  false,
			err:      errors.New("strconv.NumError"),
			doc:      "../internal/testing/sites/testdata/bookdepository_hc",
		},
		{
			desc:          "handle error from redis CheckBaseline",
			baseline:      false,
			startup:       true,
			listing:       true,
			notifications: 1,
			err:           errors.New("context canceled"),
			doc:           "../internal/testing/sites/testdata/bookdepository_hc",
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			wg := sync.WaitGroup{}
			ctx, cancel := context.WithCancel(context.Background())
			dbCh := make(chan bliss.Listing, 1)
			errCh := make(chan error, 1)
			metricsCh := make(chan bliss.Listing, 1)
			notifyCh := make(chan *pushover.Message, 3)

			f, err := os.ReadFile(test.doc)
			assert.NoError(t, err)
			ts := mock.HTTPServer(t, "/", f)
			defer ts.Close()

			ncfg := n.Config{
				DropThreshold:       1,
				LowStockEnabled:     true,
				AvailabilityEnabled: true,
			}

			mockScraper := mock.GetScraper()
			mockScraper.ScrapeFn = sites.Scrape
			mockScraper.SearchURLFn = func(isbn string) string { return ts.URL + "/" }
			scfg := Config{
				Interval:     1,
				IntervalUnit: time.Millisecond,
				Scrapers:     []bliss.Scraper{mockScraper},
			}

			tr := newTestRedis(t, lg)
			defer tr.Client.Close()

			// write redis exchange
			exchange := e.Exchange{
				Base: "EUR",
				Rates: map[string]float64{
					"EUR": 1,
					"SEK": 2,
				},
			}
			err = tr.SetExchangeRates(ctx, tr.Client, &exchange)
			assert.NoError(t, err)

			// write redis book loop and meta
			book := bliss.Book{
				Format: bliss.HC,
				ISBN:   "9780141393391",
				Title:  "Frankenstein",
			}
			assert.NoError(t, tr.UpdateBooks(ctx, tr.Client, book))
			assert.NoError(t, tr.UpdateSites(ctx, tr.Client, []string{mockScraper.Site()}))

			// write redis lowest listing to trigger price drop notification
			listing := bliss.Listing{
				Site:     "bookdepository.com",
				Book:     bliss.Book{ISBN: "9780141393391"},
				Currency: "EUR",
				Price:    1000,
			}
			err = tr.SetSiteListings(ctx, tr.Client, listing)
			assert.NoError(t, err)
			err = tr.SetLowestListings(ctx, tr.Client, listing)
			assert.NoError(t, err)
			m := NewManager(tr, ncfg, scfg, lg)

			// preparation
			switch test.desc {
			case "handle error from redis GetNextISBN":
				_, err = tr.Client.Del(ctx, "books:loop").Result()
				assert.NoError(t, err)
				_, err = tr.Client.Del(ctx, "scrapers:"+mockScraper.Site()+":next").Result()
				assert.NoError(t, err)
				_, err = tr.Client.Del(ctx, "scrapers:"+mockScraper.Site()+":priority").Result()
				assert.NoError(t, err)
				// set dummy site so that at least one active site is found
				_, err = tr.Client.Set(ctx, "scrapers:dummy:next", "0", 0*time.Second).Result()
				assert.NoError(t, err)

			case "handle error from redis GetExchange":
				_, err = tr.Client.Del(ctx, "exchange:base").Result()
				assert.NoError(t, err)

			case "handle error from redis GetLowestListings":
				_, err = tr.Client.HSet(ctx, "listings:lowest:"+book.ISBN, "price", "unparseable").
					Result()
				assert.NoError(t, err)

			case "handle error from getDOM":
				mockScraper.SearchURLFn = func(isbn string) string { return ts.URL }

			case "handle error from redis CheckBaseline":
				cancel() // cause CheckBaseline to crash with a canceled context
			}

			assert.Contains(
				t,
				m.scrapers[mockScraper.Site()].SearchURL(book.ISBN),
				"http://127.0.0.1",
			)
			m.Start(ctx, &wg, dbCh, errCh, metricsCh, notifyCh)

			// assertions
			switch test.desc {
			case "handle error from getDOM":
				assert.IsType(t, &url.Error{}, <-errCh)

			case "handle error from redis GetLowestListings":
				assert.IsType(t, &strconv.NumError{}, <-errCh)

			case "handle error from redis CheckBaseline":
				assert.Equal(t, test.err, <-errCh)

			default:
				if test.err != nil {
					assert.Equal(t, test.err, <-errCh)
				}

				if test.listing {
					assert.NotEmpty(t, <-dbCh)
					assert.NotEmpty(t, <-metricsCh)
					assert.Eventually(
						t,
						func() bool { return len(notifyCh) == test.notifications },
						time.Second,
						10*time.Millisecond,
					)
				}
			}

			cancel()
			wg.Wait()
			ts.Close()
			close(dbCh)
			close(errCh)
			close(metricsCh)
			close(notifyCh)

			m.state.mutex.RLock()
			assert.Equal(t, test.baseline, m.state.baseline)
			assert.Equal(t, test.startup, m.state.startup)
			m.state.mutex.RUnlock()
		})
	}
}

func TestNotifyLowStock(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	scraper1 := mock.GetScraper()
	scraper1.SiteFn = func() string { return test1com }
	scraper1.SearchURLFn = func(isbn string) string { return "test1.com/search/" + isbn }
	scraper2 := mock.GetScraper()
	scraper2.SiteFn = func() string { return test2com }
	scraper2.SearchURLFn = func(isbn string) string { return "test2.com/search/" + isbn }

	tt := []struct {
		desc               string
		lowStockEnabled    bool
		baseline           bool
		notifyCh           chan *pushover.Message
		notificationHashes []string
		listing            bliss.Listing
		siteListings       []bliss.Listing
		message            *pushover.Message
		scrapers           []bliss.Scraper
	}{
		{
			desc:            "no message when low stock notifications disabled",
			lowStockEnabled: false,
			baseline:        true,
			notifyCh:        make(chan *pushover.Message, 1),
		},
		{
			desc:            "no message when notifyCh is nil",
			lowStockEnabled: true,
			baseline:        true,
			notifyCh:        nil,
		},
		{
			desc:            "no message when baseline not established",
			lowStockEnabled: true,
			baseline:        false,
			notifyCh:        make(chan *pushover.Message, 1),
		},
		{
			desc:            "no message when new listing has price",
			lowStockEnabled: true,
			baseline:        true,
			notifyCh:        make(chan *pushover.Message, 1),
			listing:         bliss.Listing{Site: test1com, Price: 1},
		},
		{
			desc:            "no message on error from GetSiteListings",
			lowStockEnabled: true,
			baseline:        true,
			notifyCh:        make(chan *pushover.Message, 1),
			listing:         bliss.Listing{Site: test1com, Price: bliss.NotFound},
		},
		{
			desc:            "no message when only one site has stock and other site returns no price",
			lowStockEnabled: true,
			baseline:        true,
			notifyCh:        make(chan *pushover.Message, 1),
			listing: bliss.Listing{
				Book: bliss.Book{
					Format: bliss.HC,
					ISBN:   "0000000000000",
					Title:  "Frankenstein",
				},
				Site:  test2com,
				Price: -1,
			},
			message:  nil,
			scrapers: []bliss.Scraper{scraper1, scraper2},
			siteListings: []bliss.Listing{
				{
					Book: bliss.Book{
						Format: bliss.HC,
						ISBN:   "0000000000000",
						Title:  "Frankenstein",
					},
					Site:  test1com,
					Price: 1,
				},
				{
					Book: bliss.Book{
						Format: bliss.HC,
						ISBN:   "0000000000000",
						Title:  "Frankenstein",
					},
					Site:  test2com,
					Price: -1,
				},
			},
		},
		{
			desc:            "no message when site is already out of stock",
			lowStockEnabled: true,
			baseline:        true,
			notifyCh:        make(chan *pushover.Message, 1),
			listing: bliss.Listing{
				Book: bliss.Book{
					Format: bliss.HC,
					ISBN:   "0000000000000",
					Title:  "Frankenstein",
				},
				Site:  test1com,
				Price: -1,
			},
			message:  nil,
			scrapers: []bliss.Scraper{scraper1},
			siteListings: []bliss.Listing{
				{
					Book: bliss.Book{
						Format: bliss.HC,
						ISBN:   "0000000000000",
						Title:  "Frankenstein",
					},
					Site:  test1com,
					Price: -1,
				},
			},
		},
		{
			desc:            "no message when last site goes out of stock",
			lowStockEnabled: true,
			baseline:        true,
			notifyCh:        make(chan *pushover.Message, 1),
			listing: bliss.Listing{
				Book: bliss.Book{
					Format: bliss.HC,
					ISBN:   "0000000000000",
					Title:  "Frankenstein",
				},
				Site:  test1com,
				Price: -1,
			},
			message:  nil,
			scrapers: []bliss.Scraper{scraper1},
			siteListings: []bliss.Listing{
				{
					Book: bliss.Book{
						Format: bliss.HC,
						ISBN:   "0000000000000",
						Title:  "Frankenstein",
					},
					Site:  test1com,
					Price: 1,
				},
			},
		},
		{
			desc:            "message when one of two sites goes out of stock",
			lowStockEnabled: true,
			baseline:        true,
			notifyCh:        make(chan *pushover.Message, 1),
			listing: bliss.Listing{
				Book: bliss.Book{
					Format: bliss.HC,
					ISBN:   "0000000000000",
					Title:  "Frankenstein",
				},
				Currency: "EUR",
				Price:    -1,
				Site:     test2com,
			},
			message: &pushover.Message{
				Title: "Book Stock Running Low",
				Message: "<b>Frankenstein</b> may be running out of stock and is currently only available at " +
					"<a href=\"test1.com/search/0000000000000\">test1.com</a> for 1 EUR",
				Priority: pushover.PriorityNormal,
				URL:      "test1.com/search/0000000000000",
				URLTitle: "View listing at test1.com",
			},
			scrapers: []bliss.Scraper{scraper1, scraper2},
			siteListings: []bliss.Listing{
				{
					Book: bliss.Book{
						Format: bliss.HC,
						ISBN:   "0000000000000",
						Title:  "Frankenstein",
					},
					Currency: "EUR",
					Price:    1,
					Site:     test1com,
				},
				{
					Book: bliss.Book{
						Format: bliss.HC,
						ISBN:   "0000000000000",
						Title:  "Frankenstein",
					},
					Currency: "EUR",
					Price:    2,
					Site:     test2com,
				},
			},
		},
		{
			desc:            "no message when notification hash is duplicate",
			lowStockEnabled: true,
			baseline:        true,
			notifyCh:        make(chan *pushover.Message, 1),
			listing: bliss.Listing{
				Book: bliss.Book{
					Format: bliss.HC,
					ISBN:   "0000000000000",
					Title:  "Frankenstein",
				},
				Currency: "EUR",
				Price:    -1,
				Site:     test2com,
			},
			notificationHashes: []string{"fc257b517172d286357373d34f2b283802535f95"},
			scrapers:           []bliss.Scraper{scraper1, scraper2},
			siteListings: []bliss.Listing{
				{
					Book: bliss.Book{
						Format: bliss.HC,
						ISBN:   "0000000000000",
						Title:  "Frankenstein",
					},
					Currency: "EUR",
					Price:    1,
					Site:     test1com,
				},
				{
					Book: bliss.Book{
						Format: bliss.HC,
						ISBN:   "0000000000000",
						Title:  "Frankenstein",
					},
					Currency: "EUR",
					Price:    2,
					Site:     test2com,
				},
			},
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			tr := newTestRedis(t, lg)
			defer tr.Client.Close()

			rr, err := r.NewRedis(r.Config{}, lg)
			assert.NoError(t, err)
			rr.Client = tr.Client

			scrapersMap := map[string]bliss.Scraper{}
			for _, s := range test.scrapers {
				scrapersMap[s.Site()] = s
			}

			m := Manager{
				redis:    rr,
				scrapers: scrapersMap,
				notifyConfig: n.Config{
					LowStockEnabled: test.lowStockEnabled,
				},
				state: state{
					mutex:    sync.RWMutex{},
					baseline: test.baseline,
				},
			}

			test.listing.Currency = "EUR"
			setNotificationHashes(ctx, t, test.notificationHashes, tr)

			if test.siteListings != nil {
				books := []bliss.Book{}
				for _, sl := range test.siteListings {
					books = append(books, sl.Book)
				}
				err := tr.UpdateBooks(ctx, tr.Client, books...)
				assert.NoError(t, err)
				err = tr.SetSiteListings(ctx, tr.Client, test.siteListings...)
				assert.NoError(t, err)
			}

			if test.scrapers != nil {
				sitesSlice := []string{}
				for _, s := range test.scrapers {
					sitesSlice = append(sitesSlice, s.Site())
				}
				err := tr.UpdateSites(ctx, tr.Client, sitesSlice)
				assert.NoError(t, err)
			}

			if test.desc == "no message on error from GetSiteListings" {
				cancel()
			}

			m.notifyLowStock(ctx, test.listing, test.notifyCh, lg)

			if test.message != nil {
				msg := <-test.notifyCh
				assert.Equal(t, test.message.Title, msg.Title)
				assert.Equal(t, test.message.Message, msg.Message)
				assert.Equal(t, test.message.Priority, msg.Priority)
				assert.Equal(t, test.message.URL, msg.URL)
				assert.Equal(t, test.message.URLTitle, msg.URLTitle)
			}

			if test.notifyCh != nil {
				assert.Empty(t, test.notifyCh)
				close(test.notifyCh)
			}
		})
	}
}

func TestNotifyPriceDrop(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	scraper1 := mock.GetScraper()
	scraper1.SiteFn = func() string { return test1com }
	scraper1.SearchURLFn = func(isbn string) string { return "test1.com/search/" + isbn }
	stocked1 := mock.GetScraper()
	stocked1.SiteFn = func() string { return "stocked1.com" }
	stocked1.SearchURLFn = func(isbn string) string { return "stocked1.com/search/" + isbn }
	stocked2 := mock.GetScraper()
	stocked2.SiteFn = func() string { return "stocked2.com" }
	stocked2.SearchURLFn = func(isbn string) string { return "stocked2.com/search/" + isbn }
	stocked3 := mock.GetScraper()
	stocked3.SiteFn = func() string { return "stocked3.com" }
	stocked3.SearchURLFn = func(isbn string) string { return "stocked3.com/search/" + isbn }

	tt := []struct {
		desc               string
		baseline           bool
		notificationHashes []string
		notifyCh           chan *pushover.Message
		old, new           float32
		wantMsg            *pushover.Message
		scrapers           map[string]bliss.Scraper
		siteListings       []bliss.Listing
		threshold          int
	}{
		{
			desc:     "return if drop threshold is 0",
			baseline: true,
			notifyCh: make(chan *pushover.Message, 1),
			wantMsg:  nil,
		},
		{
			desc:     "return if notifyCh is nil",
			baseline: true,
			notifyCh: nil,
			wantMsg:  nil,
		},
		{
			desc:     "return if baseline not established",
			baseline: false,
			notifyCh: make(chan *pushover.Message, 1),
			wantMsg:  nil,
		},
		{
			desc:     "send no message on no price drop",
			baseline: true,
			notifyCh: make(chan *pushover.Message, 1),
			old:      100,
			new:      100,
			wantMsg:  nil,
		},
		{
			desc:      "send price drop notification",
			baseline:  true,
			notifyCh:  make(chan *pushover.Message, 1),
			old:       100,
			new:       95,
			threshold: 1,
			wantMsg: &pushover.Message{
				Title:    "Book Price Dropped By 5%",
				Priority: pushover.PriorityNormal,
				Message: "<b>Title</b> dropped by 5% (-5 EUR) to 95 EUR at " +
					"<a href=\"test1.com/search/0000000000000\">test1.com</a>\n\n(No listings found from other sellers)",
				URLTitle: "View listing at test1.com",
			},
		},
		{
			desc:      "price drop with other listings available",
			baseline:  true,
			notifyCh:  make(chan *pushover.Message, 1),
			old:       100,
			new:       50,
			threshold: 1,
			scrapers: map[string]bliss.Scraper{
				test1com:       scraper1,
				"stocked1.com": stocked1,
				"stocked2.com": stocked2,
			},
			wantMsg: &pushover.Message{
				Title:    "Book Price Dropped By 50%",
				Priority: pushover.PriorityNormal,
				Message: "<b>Title</b> dropped by 50% (-50 EUR) to 50 EUR at " +
					"<a href=\"test1.com/search/0000000000000\">test1.com</a>\n\n<b>Listings from other sellers:" +
					"</b>\n<a href=\"stocked1.com/search/0000000000000\">stocked1.com</<a> 200 EUR\n" +
					"<a href=\"stocked2.com/search/0000000000000\">stocked2.com</<a> 300 EUR",
				URLTitle: "View listing at test1.com",
			},
			siteListings: []bliss.Listing{
				{
					Book:  bliss.Book{ISBN: "0000000000000"},
					Site:  "stocked1.com",
					Price: 200,
				},
				{
					Book:  bliss.Book{ISBN: "0000000000000"},
					Site:  "stocked2.com",
					Price: 300,
				},
			},
		},
		{
			desc:      "notification level filters tested level",
			baseline:  true,
			notifyCh:  make(chan *pushover.Message, 1),
			old:       100,
			new:       99,
			threshold: 2,
			wantMsg:   nil,
		},
		{
			desc:      "truncate other sellers in message when new line too long",
			baseline:  true,
			notifyCh:  make(chan *pushover.Message, 1),
			old:       100,
			new:       50,
			threshold: 1,
			scrapers: map[string]bliss.Scraper{
				test1com:       scraper1,
				"stocked1.com": stocked1,
				"stocked2.com": stocked2,
			},
			wantMsg: &pushover.Message{
				Title:    "Book Price Dropped By 50%",
				Priority: pushover.PriorityNormal,
				Message: fmt.Sprintf("<b>Title</b> dropped by 50%% (-50 EUR) to 50 EUR at "+
					"<a href=\"test1.com/search/%s\">test1.com</a>\n\n<b>Listings from other sellers:"+
					"</b>\n<a href=\"stocked1.com/search/%s\">stocked1.com</<a> 200 EUR\n(+1 more)",
					strings.Repeat("0", 500), strings.Repeat("0", 500)),
				URLTitle: "View listing at test1.com",
			},
			siteListings: []bliss.Listing{
				{
					Book:  bliss.Book{ISBN: strings.Repeat("0", 500)},
					Site:  "stocked1.com",
					Price: 200,
				},
				{
					Book:  bliss.Book{ISBN: strings.Repeat("0", 500)},
					Site:  "stocked2.com",
					Price: 300,
				},
			},
		},
		{
			desc:      "truncate other sellers in message when next iteration too long",
			baseline:  true,
			notifyCh:  make(chan *pushover.Message, 1),
			old:       100,
			new:       50,
			threshold: 1,
			scrapers: map[string]bliss.Scraper{
				test1com:       scraper1,
				"stocked1.com": stocked1,
				"stocked2.com": stocked2,
				"stocked3.com": stocked3,
			},
			wantMsg: &pushover.Message{
				Title:    "Book Price Dropped By 50%",
				Priority: pushover.PriorityNormal,
				Message: fmt.Sprintf("<b>Title</b> dropped by 50%% (-50 EUR) to 50 EUR at "+
					"<a href=\"test1.com/search/%s\">test1.com</a>\n\n<b>Listings from other sellers:"+
					"</b>\n<a href=\"stocked1.com/search/%s\">stocked1.com</<a> 300 EUR\n(+2 more)",
					strings.Repeat("0", 435), strings.Repeat("0", 435)),
				URLTitle: "View listing at test1.com",
			},
			siteListings: []bliss.Listing{
				{
					Book:  bliss.Book{ISBN: strings.Repeat("0", 435)},
					Site:  "stocked1.com",
					Price: 300,
				},
				{
					Book:  bliss.Book{ISBN: strings.Repeat("0", 435)},
					Site:  "stocked2.com",
					Price: 300,
				},
				{
					Book:  bliss.Book{ISBN: strings.Repeat("0", 435)},
					Site:  "stocked3.com",
					Price: 300,
				},
			},
		},
		{
			desc:               "notification prevented by UpdateNotification",
			baseline:           true,
			notifyCh:           make(chan *pushover.Message, 1),
			old:                100,
			new:                90,
			threshold:          1,
			notificationHashes: []string{"791466306520593c02e04e50b396c1dd0d1cce11"},
		},
		{
			desc:      "handle error on GetSiteListings",
			baseline:  true,
			notifyCh:  make(chan *pushover.Message, 1),
			old:       100,
			new:       80,
			threshold: 1,
			wantMsg:   nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			tr := newTestRedis(t, lg)
			defer tr.Client.Close()

			// set up default scraper
			scrapers := map[string]bliss.Scraper{test1com: scraper1}
			// override scrapers per test
			if test.scrapers != nil {
				scrapers = test.scrapers
			}

			setNotificationHashes(ctx, t, test.notificationHashes, tr)

			if test.siteListings != nil {
				for _, l := range test.siteListings {
					_, err := tr.Client.Set(ctx, "scrapers:"+l.Site+":next", "isbn", 0*time.Second).
						Result()
					assert.NoError(t, err)
				}
				err := tr.SetSiteListings(ctx, tr.Client, test.siteListings...)
				assert.NoError(t, err)
			}

			rr, err := r.NewRedis(r.Config{}, lg)
			assert.NoError(t, err)
			rr.Client = tr.Client

			m := Manager{
				notifyConfig: n.Config{
					DropThreshold: test.threshold,
				},
				redis:    rr,
				scrapers: scrapers,
				state: state{
					mutex:    sync.RWMutex{},
					baseline: test.baseline,
				},
			}

			old := bliss.Listing{
				Book: bliss.Book{
					ISBN:  "0000000000000",
					Title: "Title",
				},
				Site:     test1com,
				Currency: "EUR",
				Price:    test.old,
			}

			newListing := bliss.Listing{
				Book: bliss.Book{
					ISBN:  "0000000000000",
					Title: "Title",
				},
				Site:     test1com,
				Currency: "EUR",
				Price:    test.new,
			}

			switch test.desc {
			case "handle error on GetSiteListings":
				cancel() // canceled context will cause error
			case "truncate other sellers in message when new line too long":
				newListing.Book.ISBN = strings.Repeat("0", 500)
			case "truncate other sellers in message when next iteration too long":
				newListing.Book.ISBN = strings.Repeat("0", 435)
			}

			m.notifyPriceDrop(ctx, old, newListing, test.notifyCh, lg)

			if test.wantMsg != nil {
				msg := <-test.notifyCh
				assert.Empty(t, test.notifyCh)
				assert.Equal(t, test.wantMsg.Title, msg.Title)
				assert.Equal(t, test.wantMsg.Message, msg.Message)
				assert.Equal(t, test.wantMsg.Priority, msg.Priority)
				assert.Equal(t, test.wantMsg.URLTitle, msg.URLTitle)
			} else {
				assert.Empty(t, test.notifyCh)
			}
		})
	}
}

func TestNotifyAvailable(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	scraper1 := mock.GetScraper()
	scraper1.SiteFn = func() string { return test1com }
	scraper1.SearchURLFn = func(isbn string) string { return "test1.com/search/" + isbn }
	scraper2 := mock.GetScraper()
	scraper2.SiteFn = func() string { return test2com }
	scraper2.SearchURLFn = func(isbn string) string { return "test2.com/search/" + isbn }

	tt := []struct {
		desc                string
		availabilityEnabled bool
		baseline            bool
		notificationHashes  []string
		notifyCh            chan *pushover.Message
		listing             bliss.Listing
		wantMsg             *pushover.Message
		scrapers            map[string]bliss.Scraper
		siteListings        []bliss.Listing
	}{
		{
			desc:                "return if availability notifications are disabled",
			availabilityEnabled: false,
		},
		{
			desc:                "return if notifyCh is nil",
			availabilityEnabled: true,
			notifyCh:            nil,
		},
		{
			desc:                "return if baseline is not established",
			availabilityEnabled: true,
			notifyCh:            make(chan *pushover.Message, 1),
			baseline:            false,
		},
		{
			desc:                "return if price is not found",
			availabilityEnabled: true,
			baseline:            true,
			notifyCh:            make(chan *pushover.Message, 1),
			listing:             bliss.Listing{Price: -1},
		},
		{
			desc:                "return if already in stock",
			availabilityEnabled: true,
			baseline:            true,
			notifyCh:            make(chan *pushover.Message, 1),
			listing: bliss.Listing{
				Book: bliss.Book{
					Format: bliss.HC,
					ISBN:   "0000000000000",
					Title:  "Frankenstein",
				},
				Site:     test1com,
				Currency: "EUR",
				Price:    1,
			},
			siteListings: []bliss.Listing{
				{
					Book: bliss.Book{
						Format: bliss.HC,
						ISBN:   "0000000000000",
						Title:  "Frankenstein",
					},
					Site:  test1com,
					Price: 1,
				},
			},
			scrapers: map[string]bliss.Scraper{
				test1com: scraper1,
			},
		},
		{
			desc:                "send message when book is made available",
			availabilityEnabled: true,
			baseline:            true,
			notifyCh:            make(chan *pushover.Message, 1),
			wantMsg: &pushover.Message{
				Title:    "Book Available",
				Priority: pushover.PriorityNormal,
				Message:  "<b>Frankenstein</b> is now available at <a href=\"test1.com/search/0000000000000\">test1.com</a> for 1 EUR",
				URLTitle: "View listing at test1.com",
			},
			listing: bliss.Listing{
				Book: bliss.Book{
					Format: bliss.HC,
					ISBN:   "0000000000000",
					Title:  "Frankenstein",
				},
				Site:     test1com,
				Currency: "EUR",
				Price:    1,
			},
			scrapers: map[string]bliss.Scraper{
				test1com: scraper1,
			},
		},
		{
			desc:                "return if other site has stock",
			availabilityEnabled: true,
			baseline:            true,
			notifyCh:            make(chan *pushover.Message, 1),
			listing: bliss.Listing{
				Book: bliss.Book{
					Format: bliss.HC,
					ISBN:   "0000000000000",
					Title:  "Frankenstein",
				},
				Site:     test1com,
				Currency: "EUR",
				Price:    1,
			},
			siteListings: []bliss.Listing{
				{
					Book: bliss.Book{
						Format: bliss.HC,
						ISBN:   "0000000000000",
						Title:  "Frankenstein",
					},
					Site:  test2com,
					Price: 1,
				},
			},
			scrapers: map[string]bliss.Scraper{
				test1com: scraper1,
				test2com: scraper2,
			},
		},
		{
			desc:                "handle error on GetListings",
			availabilityEnabled: true,
			baseline:            true,
			notifyCh:            make(chan *pushover.Message, 1),
			listing:             bliss.Listing{Price: 1},
			scrapers: map[string]bliss.Scraper{
				test1com: scraper1,
			},
		},
		{
			desc:                "notification prevented by UpdateNotification",
			availabilityEnabled: true,
			baseline:            true,
			notifyCh:            make(chan *pushover.Message, 1),
			notificationHashes:  []string{"6a8bef4b5967130b6070d0f1722738c6d00f6875"},
			listing: bliss.Listing{
				Book: bliss.Book{
					Format: bliss.HC,
					ISBN:   "0000000000000",
					Title:  "Frankenstein",
				},
				Site:     test1com,
				Currency: "EUR",
				Price:    1,
			},
			scrapers: map[string]bliss.Scraper{
				test1com: scraper1,
			},
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			tr := newTestRedis(t, lg)
			defer tr.Client.Close()

			setNotificationHashes(ctx, t, test.notificationHashes, tr)

			if test.siteListings != nil {
				err := tr.SetSiteListings(ctx, tr.Client, test.siteListings...)
				assert.NoError(t, err)
			}

			rr, err := r.NewRedis(r.Config{}, lg)
			assert.NoError(t, err)
			rr.Client = tr.Client

			m := Manager{
				redis:    rr,
				scrapers: test.scrapers,
				notifyConfig: n.Config{
					AvailabilityEnabled: test.availabilityEnabled,
				},
				state: state{
					baseline: test.baseline,
				},
			}

			tr.Client.Set(ctx, "scrapers:test1.com:next", "1", 0*time.Second)
			tr.Client.Set(ctx, "scrapers:test2.com:next", "1", 0*time.Second)

			if test.desc == "handle error on GetListings" {
				cancel() // canceled context will cause error
			}

			m.notifyAvailable(ctx, test.listing, test.notifyCh, lg)

			if test.wantMsg != nil {
				msg := <-test.notifyCh
				assert.Empty(t, test.notifyCh)
				assert.Equal(t, test.wantMsg.Title, msg.Title)
				assert.Equal(t, test.wantMsg.Message, msg.Message)
				assert.Equal(t, test.wantMsg.Priority, msg.Priority)
				assert.Equal(t, test.wantMsg.URLTitle, msg.URLTitle)
			}

			if test.notifyCh != nil && test.wantMsg != nil {
				assert.Empty(t, test.notifyCh)
				close(test.notifyCh)
			}
		})
	}
}

func setNotificationHashes(ctx context.Context, t *testing.T, hashes []string, rr r.Redis) {
	for _, hash := range hashes {
		_, err := rr.Client.Set(ctx, "notifications:sent:"+hash, "", 7*24*time.Hour).Result()
		assert.NoError(t, err)
	}
}

func newTestRedis(t *testing.T, lg *zap.SugaredLogger) r.Redis {
	mr, err := miniredis.Run()
	if err != nil {
		panic(err)
	}

	rr, err := r.NewRedis(r.Config{}, lg)
	assert.NoError(t, err)

	client := redis.NewClient(&redis.Options{
		Addr: mr.Addr(),
	})
	rr.Client = client
	return rr
}
