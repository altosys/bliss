package manager

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNewRandomTicker(t *testing.T) {
	tt := []struct {
		min, max time.Duration
	}{
		{
			min: 1 * time.Second,
			max: 2 * time.Second,
		},
	}

	for _, test := range tt {
		ctx, cancel := context.WithCancel(context.Background())
		rt := newRandomTicker(ctx, test.min, test.max)
		assert.Equal(t, test.max, time.Duration(rt.max))
		assert.Equal(t, test.min, time.Duration(rt.min))
		assert.Equal(t, ctx, rt.ctx)
		cancel()
	}
}

func TestLoop(t *testing.T) {
	tt := []struct {
		min, max time.Duration
		ctx      context.Context
	}{
		{
			min: 10 * time.Millisecond,
			max: 20 * time.Millisecond,
			ctx: context.Background(),
		},
	}

	for _, test := range tt {
		rt := newRandomTicker(test.ctx, test.min, test.max)
		<-rt.C
	}
}

func TestNextInterval(t *testing.T) {
	tt := []struct {
		min, max time.Duration
		ctx      context.Context
	}{
		{
			min: 1 * time.Nanosecond,
			max: 2 * time.Nanosecond,
			ctx: context.Background(),
		},
	}

	for _, test := range tt {
		rt := newRandomTicker(test.ctx, test.min, test.max)
		interval := rt.nextInterval()
		assert.True(t, test.min <= interval && interval <= test.max)
	}
}
