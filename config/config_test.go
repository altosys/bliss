package config

import (
	"bliss"
	"bliss/influxdb"
	"bliss/logger"
	"bliss/manager"
	"bliss/metrics"
	"bliss/notify"
	"bliss/redis"
	"bliss/sites/adlibris"
	"bliss/sites/akademibokhandeln"
	"bliss/sites/amazon"
	"bliss/sites/blackwells"
	"bliss/sites/bokus"
	"bliss/sites/bookdepository"
	"bliss/sites/bookswagon"
	"bliss/sites/cdon"
	"bliss/sites/flipkart"
	"bliss/sites/forbiddenplanet"
	"bliss/sites/scifibokhandeln"
	"bliss/sites/waltscomicshop"
	"bliss/sites/waterstones"
	"bliss/sites/wordery"
	"errors"
	"fmt"
	"net/url"
	"os"
	"strconv"
	"testing"
	"time"

	_ "github.com/influxdata/influxdb1-client" // this is important because of the bug in go mod
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func readEnv() {
	viper.SetEnvPrefix("BLISS")
	viper.AutomaticEnv()
}

func TestCheckProxyAddress(t *testing.T) {
	readEnv()
	defer os.Clearenv()

	tt := []struct {
		desc string
		addr string
		err  error
	}{
		{
			desc: "return nil on valid proxy address",
			addr: "socks5h://localhost:2000",
			err:  nil,
		},
		{
			desc: "return error on invalid proxy address",
			addr: "://localhost:invalid",
			err:  &url.Error{},
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			os.Setenv("BLISS_PROXY_ADDRESS", test.addr)
			err := checkProxyAddress()
			assert.IsType(t, test.err, err)
		})
	}
}
func TestCheckRequiredVarsSet(t *testing.T) {
	readEnv()
	defer os.Clearenv()

	vars := []string{"CURRENCY", "INTERVAL", "DB_ADDRESS",
		"DB_NAME", "DB_USER", "DB_PASSWORD", "DB_RETENTION"}

	tt := []struct {
		desc     string
		setUntil string
		err      error
	}{
		{
			desc:     "catch BLISS_CURRENCY not set",
			setUntil: "BLISS_BOOKS",
			err:      fmt.Errorf("%w: %s", bliss.ErrEmptyString, "BLISS_CURRENCY"),
		},
		{
			desc:     "catch BLISS_INTERVAL not set",
			setUntil: "BLISS_CURRENCY",
			err:      fmt.Errorf("%w: %s", bliss.ErrEmptyString, "BLISS_INTERVAL"),
		},
		{
			desc:     "catch BLISS_DB_ADDRESS not set",
			setUntil: "BLISS_INTERVAL",
			err:      fmt.Errorf("%w: %s", bliss.ErrEmptyString, "BLISS_DB_ADDRESS"),
		},
		{
			desc:     "catch BLISS_DB_NAME not set",
			setUntil: "BLISS_DB_ADDRESS",
			err:      fmt.Errorf("%w: %s", bliss.ErrEmptyString, "BLISS_DB_NAME"),
		},
		{
			desc:     "catch BLISS_DB_USER not set",
			setUntil: "BLISS_DB_NAME",
			err:      fmt.Errorf("%w: %s", bliss.ErrEmptyString, "BLISS_DB_USER"),
		},
		{
			desc:     "catch BLISS_DB_PASSWORD not set",
			setUntil: "BLISS_DB_USER",
			err:      fmt.Errorf("%w: %s", bliss.ErrEmptyString, "BLISS_DB_PASSWORD"),
		},
		{
			desc:     "catch BLISS_DB_RETENTION not set",
			setUntil: "BLISS_DB_PASSWORD",
			err:      fmt.Errorf("%w: %s", bliss.ErrEmptyString, "BLISS_DB_RETENTION"),
		},
		{
			desc:     "return config and nil error when all variables are set",
			setUntil: "BLISS_DB_RETENTION",
			err:      nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			for _, v := range vars {
				os.Setenv(test.setUntil, "value")
				if v == test.setUntil {
					break
				}
			}

			if test.err == nil {
				for _, tst := range tt {
					os.Setenv(tst.setUntil, "value")
				}
			}

			err := checkRequiredVarsSet()
			assert.Equal(t, test.err, err)
		})
	}
}

func TestParseBooks(t *testing.T) {
	readEnv()
	defer os.Clearenv()

	tt := []struct {
		desc  string
		env   string
		file  string
		books []bliss.Book
		err   error
	}{
		{
			desc: "parse books from env",
			env:  "0000000000001 HC Book 1",
			books: []bliss.Book{
				{
					ISBN:   "0000000000001",
					Format: bliss.HC,
					Title:  "Book 1",
				},
			},
			err: nil,
		},
		{
			desc: "parse books from file",
			file: "../internal/testing/config/testdata/books.yml",
			err:  nil,
			books: []bliss.Book{
				{
					ISBN:   "1000000000000",
					Format: bliss.HC,
					Title:  "Book 1",
				},
				{
					ISBN:   "2000000000000",
					Format: bliss.SC,
					Title:  "Book 2",
				},
			},
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			if test.env != "" {
				assert.NoError(t, os.Setenv("BLISS_BOOKS", test.env))
			}

			if test.file != "" {
				assert.NoError(t, os.Unsetenv("BLISS_BOOKS"))
				assert.NoError(t, os.Setenv("BLISS_BOOKS_FILE", test.file))
			}

			books, err := parseBooks()
			assert.Equal(t, test.err, err)
			assert.Equal(t, test.books, books)
		})
	}
}

func TestParseCurrency(t *testing.T) {
	readEnv()
	defer os.Clearenv()

	tt := []struct {
		desc     string
		currency string
		err      error
	}{
		{
			desc:     "return error when currency code is too long",
			currency: "toolong",
			err:      fmt.Errorf("%w: %s", bliss.ErrInvalidCurrencyCode, "toolong"),
		},
		{
			desc:     "return error when currency code contains non-alphanumeric characters",
			currency: "1X3",
			err:      fmt.Errorf("%w: %s", bliss.ErrInvalidCurrencyCode, "1X3"),
		},
		{
			desc:     "return error nil on valid currency code",
			currency: "EUR",
			err:      nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			os.Setenv("BLISS_CURRENCY", test.currency)

			currency, err := parseCurrency()
			if test.err == nil {
				assert.Equal(t, test.currency, currency)
			}

			assert.Equal(t, test.err, err)
		})
	}
}

func TestBuildInfluxDbConfig(t *testing.T) {
	readEnv()
	defer os.Clearenv()

	tt := []struct {
		desc        string
		dbAddr      string
		dbName      string
		dbUser      string
		dbPswd      string
		dbRetention string
		cfg         influxdb.Config
		err         error
	}{
		{
			desc:   "return error on empty influxdb address",
			dbAddr: "",
			err:    fmt.Errorf("%w: %s", bliss.ErrEmptyString, ""),
		},
		{
			desc:        "return config and nil error on valid configuration",
			dbAddr:      "db:1",
			dbName:      "name",
			dbUser:      "user",
			dbPswd:      "pswd",
			dbRetention: "one_month",
			cfg: influxdb.Config{
				Address:   "db:1",
				Database:  "name",
				User:      "user",
				Password:  "pswd",
				Retention: "one_month",
				Precision: "s",
			},
			err: nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			os.Setenv("BLISS_DB_ADDRESS", test.dbAddr)
			os.Setenv("BLISS_DB_NAME", test.dbName)
			os.Setenv("BLISS_DB_USER", test.dbUser)
			os.Setenv("BLISS_DB_PASSWORD", test.dbPswd)
			os.Setenv("BLISS_DB_RETENTION", test.dbRetention)
			cfg, err := buildInfluxDBConfig()

			assert.Equal(t, test.err, err)

			if test.err == nil {
				assert.Equal(t, test.cfg, cfg)
			}
			os.Clearenv()
		})
	}
}

func TestBuildMetricsConfig(t *testing.T) {
	readEnv()
	defer os.Clearenv()

	tt := []struct {
		desc string
		port int
		cfg  metrics.Config
		err  error
	}{
		{
			desc: "return error on port below valid range",
			port: 0,
			err:  bliss.ErrPortOutOfRange,
		},
		{
			desc: "return error on port above valid range",
			port: 65536,
			err:  bliss.ErrPortOutOfRange,
		},
		{
			desc: "return config and nil error on port in valid range",
			port: 1,
			cfg:  metrics.Config{Port: 1},
			err:  nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			port := strconv.Itoa(test.port)
			os.Setenv("BLISS_HTTP_PORT", port)
			cfg, err := buildMetricsConfig()

			assert.Equal(t, test.err, errors.Unwrap(err))

			if test.err == nil {
				assert.Equal(t, test.cfg, cfg)
			}
		})
	}
}

func TestBuildLogConfig(t *testing.T) {
	readEnv()
	defer os.Clearenv()

	tt := []struct {
		desc      string
		level     zapcore.Level
		developer bool
		err       error
	}{
		{
			desc:  "return error on invalid log level",
			level: -2,
			err:   errInvalidArgument,
		},
		{
			desc:  "return config and nil error on debug log level",
			level: zapcore.DebugLevel,
			err:   nil,
		},
		{
			desc:  "return config and nil error on info log level",
			level: zap.InfoLevel,
			err:   nil,
		},
		{
			desc:  "return config and nil error on warn log level",
			level: zap.WarnLevel,
			err:   nil,
		},
		{
			desc:      "return config and nil error on error log level",
			level:     zap.ErrorLevel,
			developer: true,
			err:       nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			os.Setenv("BLISS_LOG_LEVEL", test.level.String())
			os.Setenv("BLISS_DEVELOPER_LOGS", strconv.FormatBool(test.developer))

			cfg, err := buildLogConfig()
			assert.Equal(t, test.err, errors.Unwrap(err))
			assert.Equal(t, test.developer, cfg.Developer)

			if test.err == nil {
				assert.Equal(t, test.level, cfg.Level)
			}
		})
	}
}

func TestBuildNotifyConfig(t *testing.T) {
	readEnv()
	defer os.Clearenv()

	tt := []struct {
		desc         string
		apiToken     string
		userKey      string
		lowStock     string
		availability string
		threshold    int
		err          error
		cfg          notify.Config
	}{
		{
			desc:     "return config and nil error notifications disabled",
			apiToken: "",
			userKey:  "",
			cfg:      notify.Config{PushoverEnabled: false},
			err:      nil,
		},
		{
			desc:     "return error on pushover api token set and user key not set",
			apiToken: "token",
			userKey:  "",
			cfg:      notify.Config{},
			err:      errInvalidArgument,
		},
		{
			desc:     "return error on pushover user key set and api token not set",
			apiToken: "",
			userKey:  "key",
			cfg:      notify.Config{},
			err:      errInvalidArgument,
		},
		{
			desc:      "return error on notification drop threshold less than 0",
			apiToken:  "token",
			userKey:   "key",
			lowStock:  "false",
			threshold: -1,
			cfg:       notify.Config{},
			err:       errInvalidArgument,
		},
		{
			desc:      "return error on notification drop threshold more than 100",
			apiToken:  "token",
			userKey:   "key",
			lowStock:  "false",
			threshold: 101,
			cfg:       notify.Config{},
			err:       errInvalidArgument,
		},
		{
			desc:     "return config and nil error on valid configuration with low stock notifications",
			apiToken: "token",
			userKey:  "key",
			lowStock: "true",
			cfg: notify.Config{
				PushoverEnabled:  true,
				LowStockEnabled:  true,
				PushoverAPIToken: "token",
				PushoverUserKey:  "key",
			},
			err: nil,
		},
		{
			desc:         "return config and nil error on valid configuration with availability notifications",
			apiToken:     "token",
			userKey:      "key",
			availability: "true",
			cfg: notify.Config{
				PushoverEnabled:     true,
				AvailabilityEnabled: true,
				PushoverAPIToken:    "token",
				PushoverUserKey:     "key",
			},
			err: nil,
		},
		{
			desc:     "return config and nil error on valid configuration without stock notifications",
			apiToken: "token",
			userKey:  "key",
			cfg: notify.Config{
				PushoverEnabled:  true,
				PushoverAPIToken: "token",
				PushoverUserKey:  "key",
			},
			err: nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			os.Setenv("BLISS_PUSHOVER_API_TOKEN", test.apiToken)
			os.Setenv("BLISS_PUSHOVER_USER_KEY", test.userKey)
			os.Setenv("BLISS_NOTIFY_LOW_STOCK", test.lowStock)
			os.Setenv("BLISS_NOTIFY_AVAILABLE", test.availability)
			os.Setenv("BLISS_NOTIFY_DROP_THRESHOLD", fmt.Sprint(test.threshold))

			cfg, err := buildNotifyConfig()
			assert.Equal(t, test.err, errors.Unwrap(err))
			assert.Equal(t, test.cfg, cfg)

			os.Clearenv()
		})
	}
}

func TestBuildRedisConfig(t *testing.T) {
	readEnv()
	defer os.Clearenv()

	tt := []struct {
		desc     string
		address  string
		password string
		database int
		cfg      redis.Config
		err      error
	}{
		{
			desc:    "return error on empty redis address",
			address: "",
			err:     fmt.Errorf("%w: %s", bliss.ErrEmptyString, ""),
		},
		{
			desc:     "return config and nil error on valid configuration",
			address:  "redis:1",
			password: "password",
			database: 0,
			cfg: redis.Config{
				Address:  "redis:1",
				Password: "password",
				Database: 0,
			},
			err: nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			os.Setenv("BLISS_REDIS_ADDRESS", test.address)
			os.Setenv("BLISS_REDIS_PASSWORD", test.password)
			os.Setenv("BLISS_REDIS_DATABASE", fmt.Sprint(test.database))
			cfg, err := buildRedisConfig()

			assert.Equal(t, test.err, err)

			if test.err == nil {
				assert.Equal(t, test.cfg, cfg)
			}
			os.Clearenv()
		})
	}
}

func TestBuildManagerConfig(t *testing.T) {
	readEnv()
	defer os.Clearenv()

	metaScrapers := []bliss.Scraper{
		bookdepository.Scraper(),
		wordery.Scraper(),
		blackwells.Scraper(),
		bokus.Scraper(),
		adlibris.Scraper(),
	}

	tt := []struct {
		desc              string
		interval          string
		enableAdlibris    bool
		enableAllScrapers bool
		err               error
		cfg               manager.Config
	}{
		{
			desc:     "return error on negative interval",
			interval: "-1",
			err:      errInvalidInterval,
		},
		{
			desc:     "return error on no scrapers enabled",
			interval: "1",
			err:      errNoScrapersEnabled,
		},
		{
			desc:           "return config and nil error on one scraper enabled",
			interval:       "1",
			enableAdlibris: true,
			cfg: manager.Config{
				Interval:     1,
				IntervalUnit: time.Minute,
				MetaScrapers: metaScrapers,
				Scrapers:     []bliss.Scraper{adlibris.Scraper()},
			},
			err: nil,
		},
		{
			desc:              "return config and nil error on all scrapers enabled",
			interval:          "1",
			enableAllScrapers: true,
			cfg: manager.Config{
				Interval:     1,
				IntervalUnit: time.Minute,
				MetaScrapers: metaScrapers,
				Scrapers: []bliss.Scraper{
					adlibris.Scraper(),
					akademibokhandeln.Scraper(),
					amazon.Scraper(".ae"),
					amazon.Scraper(".ca"),
					amazon.Scraper(".co.uk"),
					amazon.Scraper(".com"),
					amazon.Scraper(".com.au"),
					amazon.Scraper(".de"),
					amazon.Scraper(".es"),
					amazon.Scraper(".fr"),
					amazon.Scraper(".in"),
					amazon.Scraper(".it"),
					amazon.Scraper(".nl"),
					amazon.Scraper(".se"),
					blackwells.Scraper(),
					bokus.Scraper(),
					bookdepository.Scraper(),
					bookswagon.Scraper(),
					cdon.Scraper(),
					flipkart.Scraper(),
					forbiddenplanet.Scraper(),
					scifibokhandeln.Scraper(),
					waltscomicshop.Scraper(),
					waterstones.Scraper(),
					wordery.Scraper(),
				},
			},
			err: nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			os.Clearenv()
			os.Setenv("BLISS_INTERVAL", test.interval)
			os.Setenv("BLISS_ENABLE_ADLIBRIS", strconv.FormatBool(test.enableAdlibris))

			if test.enableAllScrapers {
				envVars := []string{
					"BLISS_ENABLE_ADLIBRIS",
					"BLISS_ENABLE_AKADEMIBOKHANDELN",
					"BLISS_ENABLE_AMAZON_AE",
					"BLISS_ENABLE_AMAZON_CA",
					"BLISS_ENABLE_AMAZON_COM",
					"BLISS_ENABLE_AMAZON_COM_AU",
					"BLISS_ENABLE_AMAZON_CO_UK",
					"BLISS_ENABLE_AMAZON_DE",
					"BLISS_ENABLE_AMAZON_ES",
					"BLISS_ENABLE_AMAZON_FR",
					"BLISS_ENABLE_AMAZON_IN",
					"BLISS_ENABLE_AMAZON_IT",
					"BLISS_ENABLE_AMAZON_NL",
					"BLISS_ENABLE_AMAZON_SE",
					"BLISS_ENABLE_BOKUS",
					"BLISS_ENABLE_BLACKWELLS",
					"BLISS_ENABLE_BOOKDEPOSITORY",
					"BLISS_ENABLE_BOOKSWAGON",
					"BLISS_ENABLE_CDON",
					"BLISS_ENABLE_FLIPKART",
					"BLISS_ENABLE_FORBIDDEN_PLANET",
					"BLISS_ENABLE_SCIFIBOKHANDELN",
					"BLISS_ENABLE_WALTS_COMIC_SHOP",
					"BLISS_ENABLE_WATERSTONES",
					"BLISS_ENABLE_WORDERY",
				}

				for _, env := range envVars {
					os.Setenv(env, "true")
				}
			}

			cfg, err := buildScraperConfig()
			if unwrapped := errors.Unwrap(err); unwrapped != nil {
				err = unwrapped
			}

			assert.Equal(t, test.err, err)
			assert.Equal(t, test.cfg, cfg)
		})
	}
}

func TestBuildConfig(t *testing.T) {
	readEnv()
	defer os.Clearenv()
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())
	type fault struct{ k, v string }

	tt := []struct {
		desc     string
		expected Config
		fault    fault
		err      error
	}{
		{
			desc:  "return error from checkProxyAddress",
			fault: fault{k: "BLISS_PROXY_ADDRESS", v: "::invalid::"},
			err:   errors.New("missing protocol scheme"),
		},
		{
			desc:  "return error from checkRequiredVarsSet",
			fault: fault{k: "BLISS_CURRENCY", v: ""},
			err:   bliss.ErrEmptyString,
		},
		{
			desc:  "return error from setGlobalVars",
			fault: fault{k: "BLISS_BOOKS", v: "0"},
			err:   errInvalidISBN,
		},
		{
			desc:  "return error from parseCurrency",
			fault: fault{k: "BLISS_CURRENCY", v: "invalid"},
			err:   bliss.ErrInvalidCurrencyCode,
		},
		{
			desc:  "return error from buildMetricsConfig",
			fault: fault{k: "BLISS_HTTP_PORT", v: "0"},
			err:   bliss.ErrPortOutOfRange,
		},
		{
			desc:  "return error from buildInfluxDbConfig",
			fault: fault{k: "BLISS_DB_ADDRESS", v: "http://"},
			err:   bliss.ErrInvalidAddr,
		},
		{
			desc:  "return error from buildLogConfig",
			fault: fault{k: "BLISS_LOG_LEVEL", v: "invalid"},
			err:   errInvalidArgument,
		},
		{
			desc:  "return error from buildNotifyConfig",
			fault: fault{k: "BLISS_PUSHOVER_USER_KEY", v: ""},
			err:   errInvalidArgument,
		},
		{
			desc:  "return error from buildRedisConfig",
			fault: fault{k: "BLISS_REDIS_ADDRESS", v: "%"},
			err:   bliss.ErrInvalidAddr,
		},
		{
			desc:  "return error from buildScraperConfig",
			fault: fault{k: "BLISS_INTERVAL", v: "0"},
			err:   errInvalidInterval,
		},
		{
			desc: "return config and nil error on success",
			err:  nil,
			expected: Config{
				Books:    []bliss.Book{{ISBN: "0000000000000", Title: "", Format: 0}},
				Currency: "EUR",
				InfluxDB: influxdb.Config{
					Database:  "dbName",
					Address:   "http://db:1",
					User:      "dbUser",
					Password:  "dbPswd",
					Precision: "s",
					Retention: "one_week"},
				Manager: manager.Config{
					Interval:     1,
					IntervalUnit: time.Minute,
					Scrapers:     []bliss.Scraper{adlibris.Scraper()},
					MetaScrapers: []bliss.Scraper{
						bookdepository.Scraper(),
						wordery.Scraper(),
						blackwells.Scraper(),
						bokus.Scraper(),
						adlibris.Scraper(),
					},
				},
				Metrics: metrics.Config{Port: 8085},
				Notify: notify.Config{
					AvailabilityEnabled: false,
					DropThreshold:       10,
					LowStockEnabled:     false,
					PushoverEnabled:     true,
					PushoverAPIToken:    "pushoverApiToken",
					PushoverUserKey:     "pushoverUserKey",
				},
				Redis: redis.Config{
					Address:  "redis:1",
					Database: 1,
					Password: "redisPswd",
				},
			},
		},
	}

	env := map[string]string{
		"BLISS_PROXY_ADDRESS":      "socks5h://localhost:8000",
		"BLISS_BOOKS":              "0000000000000",
		"BLISS_CURRENCY":           "EUR",
		"BLISS_HTTP_PORT":          "8085",
		"BLISS_INTERVAL":           "1",
		"BLISS_LOG_LEVEL":          "INFO",
		"BLISS_DB_ADDRESS":         "http://db:1",
		"BLISS_DB_NAME":            "dbName",
		"BLISS_DB_USER":            "dbUser",
		"BLISS_DB_PASSWORD":        "dbPswd",
		"BLISS_DB_RETENTION":       "one_week",
		"BLISS_REDIS_ADDRESS":      "redis:1",
		"BLISS_REDIS_DATABASE":     "1",
		"BLISS_REDIS_PASSWORD":     "redisPswd",
		"BLISS_PUSHOVER_API_TOKEN": "pushoverApiToken",
		"BLISS_PUSHOVER_USER_KEY":  "pushoverUserKey",
		"BLISS_ENABLE_ADLIBRIS":    "true",
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			os.Clearenv()
			for k, v := range env {
				os.Setenv(k, v)
			}

			if test.fault != (fault{}) {
				os.Setenv(test.fault.k, test.fault.v)
			}

			result, err := BuildConfig(lg)
			if unwrapped := errors.Unwrap(err); unwrapped != nil {
				err = unwrapped
			}
			assert.Equal(t, test.err, err)
			assert.Equal(t, test.expected, result)
		})
	}
}
func TestPrintConfig(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	cfg := Config{
		Currency: "EUR",
		InfluxDB: influxdb.Config{
			Address:   "http://db:1",
			Retention: "one_week"},
		Manager: manager.Config{
			Interval: 1,
		},
		Metrics: metrics.Config{Port: 1},
		Notify: notify.Config{
			AvailabilityEnabled: false,
			LowStockEnabled:     true,
			PushoverEnabled:     true,
			DropThreshold:       10,
		},
	}
	bliss.Version = "TestPrintConfigurationVersion"
	PrintConfig(&cfg, lg)

	// print use of environment variable for book list
	os.Setenv("BLISS_BOOKS", "0000000000001 HC Book 1")
	PrintConfig(&cfg, lg)
}
