package config

import (
	"bliss"
	"bliss/influxdb"
	"bliss/logger"
	"bliss/manager"
	"bliss/metrics"
	"bliss/notify"
	"bliss/redis"
	"bliss/sites/adlibris"
	"bliss/sites/akademibokhandeln"
	"bliss/sites/amazon"
	"bliss/sites/blackwells"
	"bliss/sites/bokus"
	"bliss/sites/bookdepository"
	"bliss/sites/bookswagon"
	"bliss/sites/cdon"
	"bliss/sites/flipkart"
	"bliss/sites/forbiddenplanet"
	"bliss/sites/scifibokhandeln"
	"bliss/sites/waltscomicshop"
	"bliss/sites/waterstones"
	"bliss/sites/wordery"
	"bliss/utils"
	"errors"
	"fmt"
	"net/url"
	"strings"
	"time"

	"github.com/spf13/viper"
	"go.uber.org/zap"
	"golang.org/x/text/currency"
)

var (
	errInvalidArgument   = errors.New("invalid argument")
	errDuplicateISBN     = errors.New("duplicate ISBNs in config")
	errInvalidISBN       = errors.New("invalid ISBN number")
	errInvalidInterval   = errors.New("invalid scrape interval")
	errNoScrapersEnabled = errors.New("no scrapers enabled")
)

// Config holds all configuration parameters loaded from the environment
type Config struct {
	Books    []bliss.Book
	Currency string
	InfluxDB influxdb.Config
	Manager  manager.Config
	Metrics  metrics.Config
	Log      logger.Config
	Notify   notify.Config
	Redis    redis.Config
}

// BuildConfig loads configuration from the environment and builds a Config struct containing the parameters
func BuildConfig(lg *zap.SugaredLogger) (Config, error) {
	scrapeInterval := 5
	httpPort := 8085

	viper.SetEnvPrefix("BLISS")
	viper.AutomaticEnv()
	viper.SetDefault("LOG_LEVEL", "INFO")
	viper.SetDefault("NOTIFY_DROP_THRESHOLD", "10")
	viper.SetDefault("INTERVAL", scrapeInterval)
	viper.SetDefault("DB_ADDRESS", "influxdb:8086")
	viper.SetDefault("DB_NAME", "bliss")
	viper.SetDefault("DB_USER", "admin")
	viper.SetDefault("DB_PASSWORD", "bliss")
	viper.SetDefault("DB_RETENTION", "one_week")
	viper.SetDefault("REDIS_ADDRESS", "redis:6379")
	viper.SetDefault("REDIS_PASSWORD", "")
	viper.SetDefault("REDIS_DATABASE", "0")
	viper.SetDefault("HTTP_PORT", httpPort)

	if err := checkProxyAddress(); err != nil {
		return Config{}, err
	}

	if err := checkRequiredVarsSet(); err != nil {
		return Config{}, err
	}

	cfgBooks, err := parseBooks()
	if err != nil {
		return Config{}, err
	}

	cfgCurrency, err := parseCurrency()
	if err != nil {
		return Config{}, err
	}

	influxDBConfig, err := buildInfluxDBConfig()
	if err != nil {
		return Config{}, err
	}

	logConfig, err := buildLogConfig()
	if err != nil {
		return Config{}, err
	}

	metricsConfig, err := buildMetricsConfig()
	if err != nil {
		return Config{}, err
	}

	notifyConfig, err := buildNotifyConfig()
	if err != nil {
		return Config{}, err
	}

	redisConfig, err := buildRedisConfig()
	if err != nil {
		return Config{}, err
	}

	scraperConfig, err := buildScraperConfig()
	if err != nil {
		return Config{}, err
	}

	cfg := Config{
		Books:    cfgBooks,
		Currency: cfgCurrency,
		InfluxDB: influxDBConfig,
		Metrics:  metricsConfig,
		Log:      logConfig,
		Notify:   notifyConfig,
		Redis:    redisConfig,
		Manager:  scraperConfig,
	}
	return cfg, nil
}

// PrintConfig prints a configuration summary
func PrintConfig(cfg *Config, lg *zap.SugaredLogger) {
	lg.Info("version: ", bliss.Version)
	lg.Info("log level: ", cfg.Log.Level.CapitalString())

	if viper.IsSet("BOOKS") {
		lg.Info("book list source: environment")
	} else {
		lg.Info("book list source: ", viper.GetViper().ConfigFileUsed())
	}

	lg.Info("currency: ", cfg.Currency)
	lg.Info("notifications enabled: ", cfg.Notify.PushoverEnabled)
	lg.Info("low stock notifications: ", cfg.Notify.LowStockEnabled)
	lg.Info("availability notifications: ", cfg.Notify.AvailabilityEnabled)
	lg.Infof("price drop notification threshold: %v%%", cfg.Notify.DropThreshold)
	lg.Info("influxdb address: ", cfg.InfluxDB.Address)
	lg.Info("influxdb retention: ", cfg.InfluxDB.Retention)
	lg.Info("redis address: ", cfg.Redis.Address)
	lg.Infof("scrape interval: %vm", cfg.Manager.Interval)

	scrapeCycleHours := cfg.Manager.Interval * len(cfg.Books) / 60
	scrapeCycleMinutes := cfg.Manager.Interval * len(cfg.Books) % 60
	lg.Infof("scrape cycle: %vh%vm", scrapeCycleHours, scrapeCycleMinutes)
	dividerLength := 58
	lg.Infof(strings.Repeat("-", dividerLength))
}

func checkProxyAddress() error {
	_, err := url.Parse(viper.GetString("PROXY_ADDRESS"))
	return err
}

func checkRequiredVarsSet() error {
	for _, v := range []string{"CURRENCY", "INTERVAL", "DB_ADDRESS",
		"DB_NAME", "DB_USER", "DB_PASSWORD", "DB_RETENTION"} {
		if viper.GetString(v) == "" {
			return fmt.Errorf("%w: %s", bliss.ErrEmptyString, fmt.Sprintf("%s%s", "BLISS_", v))
		}
	}
	return nil
}

func parseCurrency() (string, error) {
	c := viper.GetString("CURRENCY")
	if _, err := currency.ParseISO(c); err != nil {
		return "", fmt.Errorf("%w: %s", bliss.ErrInvalidCurrencyCode, c)
	}
	return c, nil
}

func parseBooks() ([]bliss.Book, error) {
	if viper.IsSet("BOOKS") {
		return readBooksFromEnv()
	}
	return readBooksFromFile()
}

func buildInfluxDBConfig() (influxdb.Config, error) {
	if err := utils.CheckAddress(viper.GetString("DB_ADDRESS")); err != nil {
		return influxdb.Config{}, err
	}

	cfg := influxdb.Config{
		Address:   viper.GetString("DB_ADDRESS"),
		Database:  viper.GetString("DB_NAME"),
		User:      viper.GetString("DB_USER"),
		Password:  viper.GetString("DB_PASSWORD"),
		Retention: viper.GetString("DB_RETENTION"),
		Precision: "s",
	}

	return cfg, nil
}

func buildMetricsConfig() (metrics.Config, error) {
	if viper.GetInt("HTTP_PORT") < 1 || viper.GetInt("HTTP_PORT") > 65535 {
		return metrics.Config{}, fmt.Errorf("%w: %s", bliss.ErrPortOutOfRange, "BLISS_HTTP_PORT")
	}

	cfg := metrics.Config{Port: viper.GetInt("HTTP_PORT")}
	return cfg, nil
}

func buildLogConfig() (logger.Config, error) {
	cfg := logger.Config{Level: -2}

	switch strings.ToLower(viper.GetString("LOG_LEVEL")) {
	case "debug":
		cfg.Level = zap.DebugLevel
	case "info":
		cfg.Level = zap.InfoLevel
	case "warn":
		cfg.Level = zap.WarnLevel
	case "error":
		cfg.Level = zap.ErrorLevel
	default:
		return logger.Config{}, fmt.Errorf(
			"%w: %s=%s",
			errInvalidArgument,
			"BLISS_LOG_LEVEL",
			viper.GetString("LOG_LEVEL"),
		)
	}

	if viper.GetBool("DEVELOPER_LOGS") {
		cfg.Developer = true
	}

	return cfg, nil
}

func buildNotifyConfig() (notify.Config, error) {
	if viper.IsSet("PUSHOVER_USER_KEY") && !viper.IsSet("PUSHOVER_API_TOKEN") {
		return notify.Config{},
			fmt.Errorf(
				"%w: %s is set but %s is not",
				errInvalidArgument,
				"BLISS_PUSHOVER_USER_KEY",
				"BLISS_PUSHOVER_API_TOKEN",
			)
	}

	if !viper.IsSet("PUSHOVER_USER_KEY") && viper.IsSet("PUSHOVER_API_TOKEN") {
		return notify.Config{},
			fmt.Errorf(
				"%w: %s is set but %s is not",
				errInvalidArgument,
				"BLISS_PUSHOVER_API_TOKEN",
				"BLISS_PUSHOVER_USER_KEY",
			)
	}

	if !viper.IsSet("PUSHOVER_USER_KEY") && !viper.IsSet("PUSHOVER_API_TOKEN") {
		return notify.Config{}, nil
	}

	cfg := notify.Config{
		DropThreshold:       viper.GetInt("NOTIFY_DROP_THRESHOLD"),
		LowStockEnabled:     viper.GetBool("NOTIFY_LOW_STOCK"),
		AvailabilityEnabled: viper.GetBool("NOTIFY_AVAILABLE"),
		PushoverUserKey:     viper.GetString("PUSHOVER_USER_KEY"),
		PushoverAPIToken:    viper.GetString("PUSHOVER_API_TOKEN"),
		PushoverEnabled:     true,
	}

	if cfg.DropThreshold < 0 || 100 < cfg.DropThreshold {
		return notify.Config{},
			fmt.Errorf(
				"%w: NOTIFY_DROP_THRESHOLD must be between 0 and 100",
				errInvalidArgument,
			)
	}

	return cfg, nil
}

func buildRedisConfig() (redis.Config, error) {
	if err := utils.CheckAddress(viper.GetString("REDIS_ADDRESS")); err != nil {
		return redis.Config{}, err
	}

	cfg := redis.Config{
		Address:  viper.GetString("REDIS_ADDRESS"),
		Database: viper.GetInt("REDIS_DATABASE"),
		Password: viper.GetString("REDIS_PASSWORD"),
	}

	return cfg, nil
}

func buildScraperConfig() (manager.Config, error) {
	interval := viper.GetInt("INTERVAL")
	if interval < 1 {
		return manager.Config{}, fmt.Errorf(
			"%w: interval must be a positive integer",
			errInvalidInterval,
		)
	}

	env := []string{
		"ENABLE_ADLIBRIS",
		"ENABLE_AKADEMIBOKHANDELN",
		"ENABLE_AMAZON_AE",
		"ENABLE_AMAZON_CA",
		"ENABLE_AMAZON_CO_UK",
		"ENABLE_AMAZON_COM",
		"ENABLE_AMAZON_COM_AU",
		"ENABLE_AMAZON_DE",
		"ENABLE_AMAZON_ES",
		"ENABLE_AMAZON_FR",
		"ENABLE_AMAZON_IN",
		"ENABLE_AMAZON_IT",
		"ENABLE_AMAZON_NL",
		"ENABLE_AMAZON_SE",
		"ENABLE_BLACKWELLS",
		"ENABLE_BOKUS",
		"ENABLE_BOOKDEPOSITORY",
		"ENABLE_BOOKSWAGON",
		"ENABLE_CDON",
		"ENABLE_FLIPKART",
		"ENABLE_FORBIDDEN_PLANET",
		"ENABLE_SCIFIBOKHANDELN",
		"ENABLE_WALTS_COMIC_SHOP",
		"ENABLE_WATERSTONES",
		"ENABLE_WORDERY",
	}

	scrapers := []bliss.Scraper{}
	for _, v := range env {
		if viper.GetBool(v) {
			switch v {
			case "ENABLE_ADLIBRIS":
				scrapers = append(scrapers, adlibris.Scraper())
			case "ENABLE_AKADEMIBOKHANDELN":
				scrapers = append(scrapers, akademibokhandeln.Scraper())
			case "ENABLE_AMAZON_AE":
				scrapers = append(scrapers, amazon.Scraper(".ae"))
			case "ENABLE_AMAZON_CA":
				scrapers = append(scrapers, amazon.Scraper(".ca"))
			case "ENABLE_AMAZON_CO_UK":
				scrapers = append(scrapers, amazon.Scraper(".co.uk"))
			case "ENABLE_AMAZON_COM":
				scrapers = append(scrapers, amazon.Scraper(".com"))
			case "ENABLE_AMAZON_COM_AU":
				scrapers = append(scrapers, amazon.Scraper(".com.au"))
			case "ENABLE_AMAZON_DE":
				scrapers = append(scrapers, amazon.Scraper(".de"))
			case "ENABLE_AMAZON_ES":
				scrapers = append(scrapers, amazon.Scraper(".es"))
			case "ENABLE_AMAZON_FR":
				scrapers = append(scrapers, amazon.Scraper(".fr"))
			case "ENABLE_AMAZON_IN":
				scrapers = append(scrapers, amazon.Scraper(".in"))
			case "ENABLE_AMAZON_IT":
				scrapers = append(scrapers, amazon.Scraper(".it"))
			case "ENABLE_AMAZON_NL":
				scrapers = append(scrapers, amazon.Scraper(".nl"))
			case "ENABLE_AMAZON_SE":
				scrapers = append(scrapers, amazon.Scraper(".se"))
			case "ENABLE_BLACKWELLS":
				scrapers = append(scrapers, blackwells.Scraper())
			case "ENABLE_BOKUS":
				scrapers = append(scrapers, bokus.Scraper())
			case "ENABLE_BOOKDEPOSITORY":
				scrapers = append(scrapers, bookdepository.Scraper())
			case "ENABLE_BOOKSWAGON":
				scrapers = append(scrapers, bookswagon.Scraper())
			case "ENABLE_CDON":
				scrapers = append(scrapers, cdon.Scraper())
			case "ENABLE_FLIPKART":
				scrapers = append(scrapers, flipkart.Scraper())
			case "ENABLE_FORBIDDEN_PLANET":
				scrapers = append(scrapers, forbiddenplanet.Scraper())
			case "ENABLE_SCIFIBOKHANDELN":
				scrapers = append(scrapers, scifibokhandeln.Scraper())
			case "ENABLE_WALTS_COMIC_SHOP":
				scrapers = append(scrapers, waltscomicshop.Scraper())
			case "ENABLE_WATERSTONES":
				scrapers = append(scrapers, waterstones.Scraper())
			case "ENABLE_WORDERY":
				scrapers = append(scrapers, wordery.Scraper())
			}
		}
	}

	if len(scrapers) == 0 {
		return manager.Config{}, errNoScrapersEnabled
	}

	cfg := manager.Config{
		Interval:     interval,
		IntervalUnit: time.Minute,
		MetaScrapers: []bliss.Scraper{
			bookdepository.Scraper(),
			wordery.Scraper(),
			blackwells.Scraper(),
			bokus.Scraper(),
			adlibris.Scraper()},
		Scrapers: scrapers,
	}

	return cfg, nil
}
