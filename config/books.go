package config

import (
	"bliss"
	"fmt"
	"regexp"
	"sort"
	"strings"

	"github.com/spf13/viper"
)

func readBooksFromFile() ([]bliss.Book, error) {
	file := "/etc/bliss/books.yml"
	if viper.IsSet("BOOKS_FILE") {
		file = viper.GetString("BOOKS_FILE")
	}
	viper.SetConfigFile(file)

	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}

	// check that the books.yml file has expected fields
	if !viper.InConfig("hardcover") && !viper.InConfig("softcover") {
		return nil, fmt.Errorf("%s contains neither 'softcover' or 'hardcover' fields", file)
	}

	// check that file has content in the expected fields
	fileHasContent := false
	numHardcovers := len(viper.GetViper().GetStringMapStringSlice("hardcover"))
	numSoftcovers := len(viper.GetViper().GetStringMapStringSlice("softcover"))
	fileHasContent = numHardcovers > 0 || numSoftcovers > 0
	if !fileHasContent {
		return nil, fmt.Errorf("file '%s' has no books listed", file)
	}

	// convert viper config map to books list
	books := []bliss.Book{}
	for _, f := range []string{"hardcover", "softcover"} {
		for isbn, title := range viper.GetViper().GetStringMapString(f) {
			if err := checkValidISBN13(isbn); err != nil {
				return nil, err
			}

			var format bliss.Format
			switch f {
			case "hardcover":
				format = bliss.HC
			case "softcover":
				format = bliss.SC
			}

			book := bliss.Book{
				ISBN:   isbn,
				Title:  title,
				Format: format,
			}
			books = append(books, book)
		}
	}
	sortBooksSliceByISBN(books)
	return books, nil
}

func readBooksFromEnv() ([]bliss.Book, error) {
	bookList := strings.Split(strings.TrimSpace(viper.GetString("BOOKS")), "\n")

	if len(bookList) == 0 || bookList[0] == "" {
		return nil, fmt.Errorf("%w: %s", bliss.ErrEmptyString, "BLISS_BOOKS")
	}

	seen := map[string]bool{}
	books := []bliss.Book{}

	for _, b := range bookList {
		numFields := 3
		fields := strings.SplitN(b, " ", numFields)
		if len(fields) == 1 && fields[0] == "" {
			continue
		}

		isbn := fields[0]
		if err := checkValidISBN13(isbn); err != nil {
			return nil, err
		}

		if seen[isbn] {
			return nil, fmt.Errorf("%w: %s", errDuplicateISBN, isbn)
		}

		seen[isbn] = true
		book := bliss.Book{ISBN: fields[0]}

		onlyISBN := 1
		allMeta := 3
		if len(fields) > onlyISBN {
			formats := map[string]struct{}{"PB": {}, "SC": {}, "HC": {}}
			if _, exists := formats[strings.ToUpper(fields[1])]; !exists {
				book.Title = strings.TrimSpace(strings.Join(fields[1:], " "))
			} else {
				book.Format = bliss.Formats[strings.ToUpper(fields[1])]
				if len(fields) == allMeta {
					book.Title = strings.TrimSpace(strings.Join(fields[2:], " "))
				}
			}
		}
		books = append(books, book)
	}

	sortBooksSliceByISBN(books)
	return books, nil
}

// check that isbn contains only numbers and is of correct length
func checkValidISBN13(isbn string) error {
	lenISBN13 := 13
	rgx := regexp.MustCompile(`\D+`)
	if stripped := rgx.ReplaceAllString(isbn, ""); len(stripped) != lenISBN13 {
		return fmt.Errorf("%w: %s", errInvalidISBN, isbn)
	}
	return nil
}

func sortBooksSliceByISBN(books []bliss.Book) {
	sort.Slice(books, func(i, j int) bool {
		return books[i].ISBN < books[j].ISBN
	})
}
