package config

import (
	"bliss"
	"fmt"
	"io/fs"
	"os"
	"syscall"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReadBooksFromFile(t *testing.T) {
	tt := []struct {
		desc  string
		file  string
		books []bliss.Book
		err   error
	}{
		{
			desc: "return error on default books file not existing",
			err: &fs.PathError{
				Op:   "open",
				Path: "/etc/bliss/books.yml",
				Err:  syscall.Errno(0x2),
			},
		},
		{
			desc: "return error on specified books file not existing",
			file: "does_not_exist.yml",
			err: &fs.PathError{
				Op:   "open",
				Path: "does_not_exist.yml",
				Err:  syscall.Errno(0x2),
			},
		},
		{
			desc: "return error on empty books file",
			file: "../internal/testing/config/testdata/books_empty.yml",
			err: fmt.Errorf(
				"%s contains neither 'softcover' or 'hardcover' fields",
				"../internal/testing/config/testdata/books_empty.yml",
			),
		},
		{
			desc: "return error on empty hardcover/softcover field",
			file: "../internal/testing/config/testdata/books_empty_fields.yml",
			err: fmt.Errorf(
				"file '%s' has no books listed",
				"../internal/testing/config/testdata/books_empty_fields.yml",
			),
		},
		{
			desc: "return error on invalid isbn in file",
			file: "../internal/testing/config/testdata/books_invalid_isbn.yml",
			err:  fmt.Errorf("%w: %s", errInvalidISBN, "1"),
		},
		{
			desc: "return last value on duplicate isbn",
			file: "../internal/testing/config/testdata/books_duplicate_isbns.yml",
			err:  nil,
			books: []bliss.Book{
				{
					ISBN:   "1000000000001",
					Title:  "Book Y",
					Format: bliss.HC,
				},
			},
		},
		{
			desc: "return config and nil error on successful parsing",
			file: "../internal/testing/config/testdata/books.yml",
			err:  nil,
			books: []bliss.Book{
				{
					ISBN:   "1000000000000",
					Title:  "Book 1",
					Format: bliss.HC,
				},
				{
					ISBN:   "2000000000000",
					Title:  "Book 2",
					Format: bliss.SC,
				},
			},
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			os.Clearenv()
			if test.file != "" {
				assert.NoError(t, os.Setenv("BLISS_BOOKS_FILE", test.file))
			}
			readEnv()

			books, err := readBooksFromFile()
			assert.Equal(t, test.err, err)
			assert.Equal(t, test.books, books)
		})
	}
}

func TestReadBooksFromEnv(t *testing.T) {
	readEnv()
	defer os.Clearenv()

	tt := []struct {
		desc  string
		env   string
		books []bliss.Book
		err   error
	}{
		{
			desc: "return error on empty books list",
			env:  "",
			err:  fmt.Errorf("%w: %s", bliss.ErrEmptyString, "BLISS_BOOKS"),
		},
		{
			desc: "return error on invalid isbn",
			env:  "invalid",
			err:  fmt.Errorf("%w: %s", errInvalidISBN, "invalid"),
		},
		{
			desc: "return error on duplicate isbn",
			env:  "0000000000000\n0000000000000",
			err:  fmt.Errorf("%w: %s", errDuplicateISBN, "0000000000000"),
		},
		{
			desc: "ignore empty line in books list",
			env:  "0000000000001\n\n0000000000002",
			err:  nil,
			books: []bliss.Book{
				{
					ISBN:   "0000000000001",
					Title:  "",
					Format: 0,
				},
				{
					ISBN:   "0000000000002",
					Title:  "",
					Format: 0,
				},
			},
		},
		{
			desc: "return config and nil error on successful parsing",
			env: fmt.Sprintf("%s\n%s\n%s\n%s\n%s\n%s\n%s",
				"0000000000001",
				"0000000000002 title",
				"0000000000003 PB",
				"0000000000004 SC",
				"0000000000005 HC",
				"0000000000006 HC title",
				"0000000000007 HC   title whitespaced",
			),
			books: []bliss.Book{
				{
					ISBN:   "0000000000001",
					Title:  "",
					Format: 0,
				},
				{
					ISBN:   "0000000000002",
					Title:  "title",
					Format: 0,
				},
				{
					ISBN:   "0000000000003",
					Title:  "",
					Format: 1,
				},
				{
					ISBN:   "0000000000004",
					Title:  "",
					Format: 1,
				},
				{
					ISBN:   "0000000000005",
					Title:  "",
					Format: 2,
				},
				{
					ISBN:   "0000000000006",
					Title:  "title",
					Format: 2,
				},
				{
					ISBN:   "0000000000007",
					Title:  "title whitespaced",
					Format: 2,
				},
			},
			err: nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			assert.NoError(t, os.Setenv("BLISS_BOOKS", test.env))

			books, err := readBooksFromEnv()
			assert.Equal(t, test.err, err)
			assert.Equal(t, test.books, books)
		})
	}
}

func TestCheckValidISBN13(t *testing.T) {
	tt := []struct {
		desc string
		isbn string
		err  error
	}{
		{
			desc: "return error when isbn has incorrect length",
			isbn: "123",
			err:  fmt.Errorf("%w: %s", errInvalidISBN, "123"),
		},
		{
			desc: "return error when isbn contains other characters than numbers",
			isbn: "abc",
			err:  fmt.Errorf("%w: %s", errInvalidISBN, "abc"),
		},
		{
			desc: "return nil when correctly formatted isbn is passed",
			isbn: "1234567890123",
			err:  nil,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			err := checkValidISBN13(test.isbn)
			assert.Equal(t, test.err, err)
		})
	}
}

func TestSortBooksSliceByISBN(t *testing.T) {
	tt := []struct {
		desc  string
		books []bliss.Book
		want  []bliss.Book
	}{
		{
			desc: "sort books in order of isbn",
			books: []bliss.Book{
				{ISBN: "3"},
				{ISBN: "2"},
				{ISBN: "1"},
			},
			want: []bliss.Book{
				{ISBN: "1"},
				{ISBN: "2"},
				{ISBN: "3"},
			},
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			sortBooksSliceByISBN(test.books)
			assert.Equal(t, test.want, test.books)
		})
	}
}
