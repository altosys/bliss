package utils

import (
	"bliss"
	"context"
	"fmt"
	"net"
	"os"
	"os/signal"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/leekchan/accounting"
	"go.uber.org/zap"
)

// StartInterruptListener waits for SIGINT signal for graceful shutdown
func StartInterruptListener(ctx context.Context, cancel context.CancelFunc, wg *sync.WaitGroup, lg *zap.SugaredLogger) {
	signalCh := make(chan os.Signal, 1)
	defer close(signalCh)
	signal.Notify(signalCh, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)

	lg.Debug("interrupt signal listener started")
	select {
	case <-signalCh:
		lg.Info("interrupt signal received, shutting down...")
		signal.Stop(signalCh)
		cancel()
	case <-ctx.Done():
		lg.Debug("stopping interrupt signal listener")
		signal.Stop(signalCh)
	}
	wg.Done()
}

// CheckAddress checks if an address is valid
func CheckAddress(addr string) error {
	if addr == "" {
		return fmt.Errorf("%w: %s", bliss.ErrEmptyString, addr)
	}

	if strings.Contains(addr, "http://") {
		stripped := strings.ReplaceAll(addr, "http://", "")
		if stripped == "" {
			return fmt.Errorf("%w: %s", bliss.ErrInvalidAddr, addr)
		}
		addr = stripped
	}

	rgx := regexp.MustCompile(`^[a-zA-Z0-9_\-./:]*$`)
	if !rgx.MatchString(addr) {
		return fmt.Errorf("%w: %s", bliss.ErrInvalidAddr, addr)
	}

	if strings.Contains(addr, ":") {
		base, bitSize := 10, 64
		if _, portStr, err := net.SplitHostPort(addr); err != nil {
			return err
		} else if port, err := strconv.ParseInt(portStr, base, bitSize); err != nil {
			return fmt.Errorf("%w: %s", bliss.ErrInvalidPort, addr)
		} else if port <= 0 || port > 65535 {
			return fmt.Errorf("%w: %s", bliss.ErrPortOutOfRange, addr)
		}
	}
	return nil
}

// LogBooksSorted logs the passed books in order by title
func LogBooksSorted(books []bliss.Book, lg *zap.SugaredLogger) {
	sort.SliceStable(books, func(i, j int) bool {
		return books[i].Title < books[j].Title
	})

	for i, book := range books {
		if book.Title == "" {
			book.Title = "<title to be scraped>"
		}
		lg.Infof("%03d. %s %s %s", i+1, book.ISBN, book.Format, book.Title)
	}
}

// IsPortListening checks if a specified port is listening
func IsPortListening(service, host, port string, lg *zap.SugaredLogger) bool {
	timeout := time.Second
	conn, err := net.DialTimeout("tcp", net.JoinHostPort(host, port), timeout)
	if err == nil && conn != nil {
		defer conn.Close()
		lg.Debugf("successfully connected to %s at %s:%s", service, host, port)
		return true
	}
	lg.Debugf("failed connecting to %s at %s:%s", service, host, port)
	return false
}

// PriceStringToFloat removes illegal characters and converts price to float32
func PriceToFloat(currency, priceStr string, lg *zap.SugaredLogger) float32 {
	if priceStr == "" {
		return bliss.NotFound
	}
	precision := 2
	priceStr = accounting.UnformatNumber(priceStr, precision, currency)

	// convert to float32
	bitSize := 32
	price64, err := strconv.ParseFloat(priceStr, bitSize)
	if err != nil {
		lg.Errorf("failed to parse price to float32: %s", err.Error())
		return bliss.NotFound
	}

	if price64 == 0 {
		return float32(bliss.NotFound)
	}
	return float32(price64)
}
