package utils

import (
	"bliss"
	"bliss/logger"
	"context"
	"fmt"
	"net"
	"sync"
	"syscall"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

func TestStartInterruptListener(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())
	ctx, cancel := context.WithCancel(context.Background())

	// interrupt using SIGINT
	wg := sync.WaitGroup{}
	wg.Add(1)
	go StartInterruptListener(ctx, cancel, &wg, lg)
	time.Sleep(200 * time.Millisecond)
	assert.NoError(t, syscall.Kill(syscall.Getpid(), syscall.SIGINT))
	wg.Wait()

	// interrupt using context cancel
	wg.Add(1)
	ctx, cancel = context.WithCancel(context.Background())
	go StartInterruptListener(ctx, cancel, &wg, lg)
	cancel()
	wg.Wait()
}

func TestCheckAddress(t *testing.T) {
	tt := []struct {
		desc string
		addr string
		err  error
	}{
		{
			desc: "return error on empty string",
			addr: "",
			err:  fmt.Errorf("%w: %s", bliss.ErrEmptyString, ""),
		},
		{
			desc: "return error on invalid characters",
			addr: "&%/(",
			err:  fmt.Errorf("%w: %s", bliss.ErrInvalidAddr, "&%/("),
		},
		{
			desc: "return error on letters in port",
			addr: "port:alpha",
			err:  fmt.Errorf("%w: %s", bliss.ErrInvalidPort, "port:alpha"),
		},
		{
			desc: "return error on too many colons",
			addr: "so:many:colons",
			err:  &net.AddrError{Err: "too many colons in address", Addr: "so:many:colons"},
		},
		{
			desc: "return error on only passing scheme",
			addr: "http://",
			err:  fmt.Errorf("%w: %s", bliss.ErrInvalidAddr, "http://"),
		},
		{
			desc: "return nil on address with schema and host",
			addr: "http://localhost",
			err:  nil,
		},
		{
			desc: "return nil on address with host only",
			addr: "localhost",
			err:  nil,
		},
		{
			desc: "return error on port below valid range",
			addr: "http://localhost:0",
			err:  fmt.Errorf("%w: %s", bliss.ErrPortOutOfRange, "localhost:0"),
		},
		{
			desc: "return nil on valid address with scheme and port",
			addr: "http://localhost:80",
			err:  nil,
		},
		{
			desc: "return error on port above valid range",
			addr: "http://localhost:65536",
			err:  fmt.Errorf("%w: %s", bliss.ErrPortOutOfRange, "localhost:65536"),
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			assert.Equal(t, test.err, CheckAddress(test.addr))
		})
	}
}

func TestIsPortListening(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	assert.False(t, IsPortListening("test", "localhost", "9999", lg))

	listener, err := net.Listen("tcp", "localhost:9999")
	assert.NoError(t, err)
	assert.NotNil(t, listener)
	defer listener.Close()

	assert.True(t, IsPortListening("test", "localhost", "9999", lg))
}

func TestLogBooksSorted(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc  string
		books []bliss.Book
	}{
		{
			desc: "log single book",
			books: []bliss.Book{
				{
					Format: bliss.SC,
					ISBN:   "1111111111111",
					Title:  "title1",
				},
			},
		},
		{
			desc: "sort and log books out of title order",
			books: []bliss.Book{
				{
					Format: bliss.SC,
					ISBN:   "3333333333333",
					Title:  "title3",
				},
				{
					Format: bliss.HC,
					ISBN:   "2222222222222",
					Title:  "title2",
				},
				{
					Format: bliss.SC,
					ISBN:   "1111111111111",
					Title:  "title1",
				},
			},
		},
		{
			desc: "sort and log books with missing title",
			books: []bliss.Book{
				{
					Format: bliss.SC,
					ISBN:   "1111111111111",
					Title:  "title1",
				},
				{
					Format: bliss.NA,
					ISBN:   "2222222222222",
					Title:  "",
				},
				{
					Format: bliss.NA,
					ISBN:   "3333333333333",
					Title:  "title3",
				},
			},
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			LogBooksSorted(test.books, lg)
		})
	}
}

func TestPriceToFloat(t *testing.T) {
	lm := logger.NewManager(true, zap.DebugLevel)
	lg := lm.NewSugaredLogger().Named(t.Name())

	tt := []struct {
		desc     string
		priceStr string
		price    float32
	}{
		{
			desc:     "return bliss.NotFound on empty price string",
			priceStr: "",
			price:    bliss.NotFound,
		},
		{
			desc:     "return bliss.NotFound on parse error",
			priceStr: "1,1,1,1",
			price:    bliss.NotFound,
		},
		{
			desc:     "return parsed price on success",
			priceStr: "$123,456EUR",
			price:    123.46,
		},
	}

	for _, test := range tt {
		t.Run(test.desc, func(t *testing.T) {
			assert.Equal(t, test.price, PriceToFloat("EUR", test.priceStr, lg))
		})
	}
}
